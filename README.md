# sequence-controller

## Repo setup

### Python env setup

#### 1. Setup python environment
```
python3 -m venv env
```
#### 2. Activate python source
```
source env/bin/activate
```

#### 3. Upgrade PIP installer
```
pip install --upgrade pip
```
#### 4a. Install PyQT
```
pip install PyQt5
```

#### 4b. Install requirements (if requirements file is present)
```
pip install -r requirements.txt
```
### Qt creator python executables setup

#### 1. RunPython program

[Stackoverflow link](https://stackoverflow.com/questions/24100602/developing-python-applications-in-qt-creator)

* Open Qt Creator and go to Tools->Options->Environment->External Tools. Click Add->Add category and create a new category (for example, Python).
* Then, select the created category and click Add->Add Tool to create a new tool - RunPy for example. Select the created tool and fill the fields on the right:
  * Description - any value
  * Executable - /home/soso/Projects/sequence-controller/env/bin/python3
  * Arguments - %{CurrentDocument:FilePath}
  * Working directory - %{CurrentDocument:Path}
  * Environment - QT_LOGGING_TO_CONSOLE=1

#### 2. Convert .ui to .py

* Click again on the Add->Add Tool button to create a new tool - PyUic now. Select it again and fill the fields on the right:
  * Description - any value
  * Executable - /home/soso/Projects/sequence-controller/env/bin/pyuic5
  * Arguments - %{CurrentDocument:FilePath} -o UI_%{CurrentDocument:FileBaseName}.py
  * Working directory - %{CurrentDocument:Path}
  * Environment - QT_LOGGING_TO_CONSOLE=1

NOTE: when using ui to py converter the UI file must be selected. When using RunPython the python file must be selected

#### 3. Create keyboard shortcut for executing python scripts

Ctrl+Shift+r = load Python
Ctrl+Shift+n = create new python file from ui

### Run Python from terminal

1. Open Terminal with **Ctrl+Alt+T** 

2. Go to the projects folder
```
cd Projects/sequence-controller/
```
3. Activate python environment
```
source env/bin/activate
```
4. Go to application folder
```
cd 05_06/05_06app/
```
**Run Python file**
```
python3 sequence_controller.py 
```

### Install fonts
1. Open Terminal with **Ctrl+Alt+T**

2. Install Font Manager
```
sudo apt-get install font-manager
```
3. Run the program and Click on Manage Fonts button, select Install Fonts

4. Go to = Projects/ui-elements/01_sequence-controller/style_guide/fonts

5. Select all the styles and Click ok

## Design info

#### Status circles
File path = 01_sequence-controller/style_guide/icons/feedback_dots/
* Blue - disarmed - FeedbackCircles-02.png ![Blue](../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png)
* Gray - before latch - FeedbackCircles-01.png ![Blue](../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-01.png)
* Yellow - latched - FeedbackCircles-04.png ![Blue](../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-04.png)
* Green - After latch - FeedbackCircles-03.png ![Blue](../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-03.png)
* Red - Problems? - FeedbackCircles-05.png ![Blue](../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-05.png)

#### Armed/Disarmed icons
File path = 01_sequence-controller/style_guide/icons/feedback_dots/

Armed 

![Blue](../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/armedButton.png)

Disarmed 

![Blue](../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/disarmedButton.png)
