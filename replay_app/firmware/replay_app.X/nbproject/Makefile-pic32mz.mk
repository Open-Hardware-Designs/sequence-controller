#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-pic32mz.mk)" "nbproject/Makefile-local-pic32mz.mk"
include nbproject/Makefile-local-pic32mz.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=pic32mz
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/replay_app.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/replay_app.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../../../../../../microchip/harmony/v2_06/framework/driver/tmr/src/dynamic/drv_tmr.c ../../../../../../microchip/harmony/v2_06/framework/driver/usart/src/dynamic/drv_usart.c ../../../../../../microchip/harmony/v2_06/framework/driver/usart/src/dynamic/drv_usart_buffer_queue.c ../../../../../../microchip/harmony/v2_06/framework/driver/usart/src/dynamic/drv_usart_read_write.c ../../../../../../microchip/harmony/v2_06/framework/system/console/src/sys_console.c ../../../../../../microchip/harmony/v2_06/framework/system/console/src/sys_console_uart.c ../../../../../../microchip/harmony/v2_06/framework/system/debug/src/sys_debug.c ../../../../../../microchip/harmony/v2_06/framework/system/dma/src/sys_dma.c ../../../../../../microchip/harmony/v2_06/framework/system/int/src/sys_int_pic32.c ../../../../../../microchip/harmony/v2_06/framework/system/tmr/src/sys_tmr.c ../src/relay_app.c ../src/uart_app.c ../src/system_config/pic32mz/framework/system/clk/src/sys_clk_pic32mz.c ../src/system_config/pic32mz/framework/system/devcon/src/sys_devcon.c ../src/system_config/pic32mz/framework/system/devcon/src/sys_devcon_pic32mz.c ../src/system_config/pic32mz/framework/system/devcon/src/sys_devcon_cache_pic32mz.S ../src/system_config/pic32mz/framework/system/ports/src/sys_ports_static.c ../src/system_config/pic32mz/bsp/bsp.c ../src/system_config/pic32mz/system_init.c ../src/system_config/pic32mz/system_interrupt.c ../src/system_config/pic32mz/system_exceptions.c ../src/main.c ../src/system_config/pic32mz/system_tasks.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/709153290/drv_tmr.o ${OBJECTDIR}/_ext/1201400862/drv_usart.o ${OBJECTDIR}/_ext/1201400862/drv_usart_buffer_queue.o ${OBJECTDIR}/_ext/1201400862/drv_usart_read_write.o ${OBJECTDIR}/_ext/23173237/sys_console.o ${OBJECTDIR}/_ext/23173237/sys_console_uart.o ${OBJECTDIR}/_ext/1992574535/sys_debug.o ${OBJECTDIR}/_ext/419932628/sys_dma.o ${OBJECTDIR}/_ext/231205469/sys_int_pic32.o ${OBJECTDIR}/_ext/910924237/sys_tmr.o ${OBJECTDIR}/_ext/1360937237/relay_app.o ${OBJECTDIR}/_ext/1360937237/uart_app.o ${OBJECTDIR}/_ext/1420049864/sys_clk_pic32mz.o ${OBJECTDIR}/_ext/1070083009/sys_devcon.o ${OBJECTDIR}/_ext/1070083009/sys_devcon_pic32mz.o ${OBJECTDIR}/_ext/1070083009/sys_devcon_cache_pic32mz.o ${OBJECTDIR}/_ext/360901400/sys_ports_static.o ${OBJECTDIR}/_ext/1980434993/bsp.o ${OBJECTDIR}/_ext/435813985/system_init.o ${OBJECTDIR}/_ext/435813985/system_interrupt.o ${OBJECTDIR}/_ext/435813985/system_exceptions.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/435813985/system_tasks.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/709153290/drv_tmr.o.d ${OBJECTDIR}/_ext/1201400862/drv_usart.o.d ${OBJECTDIR}/_ext/1201400862/drv_usart_buffer_queue.o.d ${OBJECTDIR}/_ext/1201400862/drv_usart_read_write.o.d ${OBJECTDIR}/_ext/23173237/sys_console.o.d ${OBJECTDIR}/_ext/23173237/sys_console_uart.o.d ${OBJECTDIR}/_ext/1992574535/sys_debug.o.d ${OBJECTDIR}/_ext/419932628/sys_dma.o.d ${OBJECTDIR}/_ext/231205469/sys_int_pic32.o.d ${OBJECTDIR}/_ext/910924237/sys_tmr.o.d ${OBJECTDIR}/_ext/1360937237/relay_app.o.d ${OBJECTDIR}/_ext/1360937237/uart_app.o.d ${OBJECTDIR}/_ext/1420049864/sys_clk_pic32mz.o.d ${OBJECTDIR}/_ext/1070083009/sys_devcon.o.d ${OBJECTDIR}/_ext/1070083009/sys_devcon_pic32mz.o.d ${OBJECTDIR}/_ext/1070083009/sys_devcon_cache_pic32mz.o.d ${OBJECTDIR}/_ext/360901400/sys_ports_static.o.d ${OBJECTDIR}/_ext/1980434993/bsp.o.d ${OBJECTDIR}/_ext/435813985/system_init.o.d ${OBJECTDIR}/_ext/435813985/system_interrupt.o.d ${OBJECTDIR}/_ext/435813985/system_exceptions.o.d ${OBJECTDIR}/_ext/1360937237/main.o.d ${OBJECTDIR}/_ext/435813985/system_tasks.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/709153290/drv_tmr.o ${OBJECTDIR}/_ext/1201400862/drv_usart.o ${OBJECTDIR}/_ext/1201400862/drv_usart_buffer_queue.o ${OBJECTDIR}/_ext/1201400862/drv_usart_read_write.o ${OBJECTDIR}/_ext/23173237/sys_console.o ${OBJECTDIR}/_ext/23173237/sys_console_uart.o ${OBJECTDIR}/_ext/1992574535/sys_debug.o ${OBJECTDIR}/_ext/419932628/sys_dma.o ${OBJECTDIR}/_ext/231205469/sys_int_pic32.o ${OBJECTDIR}/_ext/910924237/sys_tmr.o ${OBJECTDIR}/_ext/1360937237/relay_app.o ${OBJECTDIR}/_ext/1360937237/uart_app.o ${OBJECTDIR}/_ext/1420049864/sys_clk_pic32mz.o ${OBJECTDIR}/_ext/1070083009/sys_devcon.o ${OBJECTDIR}/_ext/1070083009/sys_devcon_pic32mz.o ${OBJECTDIR}/_ext/1070083009/sys_devcon_cache_pic32mz.o ${OBJECTDIR}/_ext/360901400/sys_ports_static.o ${OBJECTDIR}/_ext/1980434993/bsp.o ${OBJECTDIR}/_ext/435813985/system_init.o ${OBJECTDIR}/_ext/435813985/system_interrupt.o ${OBJECTDIR}/_ext/435813985/system_exceptions.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/435813985/system_tasks.o

# Source Files
SOURCEFILES=../../../../../../microchip/harmony/v2_06/framework/driver/tmr/src/dynamic/drv_tmr.c ../../../../../../microchip/harmony/v2_06/framework/driver/usart/src/dynamic/drv_usart.c ../../../../../../microchip/harmony/v2_06/framework/driver/usart/src/dynamic/drv_usart_buffer_queue.c ../../../../../../microchip/harmony/v2_06/framework/driver/usart/src/dynamic/drv_usart_read_write.c ../../../../../../microchip/harmony/v2_06/framework/system/console/src/sys_console.c ../../../../../../microchip/harmony/v2_06/framework/system/console/src/sys_console_uart.c ../../../../../../microchip/harmony/v2_06/framework/system/debug/src/sys_debug.c ../../../../../../microchip/harmony/v2_06/framework/system/dma/src/sys_dma.c ../../../../../../microchip/harmony/v2_06/framework/system/int/src/sys_int_pic32.c ../../../../../../microchip/harmony/v2_06/framework/system/tmr/src/sys_tmr.c ../src/relay_app.c ../src/uart_app.c ../src/system_config/pic32mz/framework/system/clk/src/sys_clk_pic32mz.c ../src/system_config/pic32mz/framework/system/devcon/src/sys_devcon.c ../src/system_config/pic32mz/framework/system/devcon/src/sys_devcon_pic32mz.c ../src/system_config/pic32mz/framework/system/devcon/src/sys_devcon_cache_pic32mz.S ../src/system_config/pic32mz/framework/system/ports/src/sys_ports_static.c ../src/system_config/pic32mz/bsp/bsp.c ../src/system_config/pic32mz/system_init.c ../src/system_config/pic32mz/system_interrupt.c ../src/system_config/pic32mz/system_exceptions.c ../src/main.c ../src/system_config/pic32mz/system_tasks.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-pic32mz.mk dist/${CND_CONF}/${IMAGE_TYPE}/replay_app.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MZ2048EFM100
MP_LINKER_FILE_OPTION=
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1070083009/sys_devcon_cache_pic32mz.o: ../src/system_config/pic32mz/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  .generated_files/flags/pic32mz/c0a892ae5f8f5ad8dc906ff0eac33b4225336c5a .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1070083009" 
	@${RM} ${OBJECTDIR}/_ext/1070083009/sys_devcon_cache_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/1070083009/sys_devcon_cache_pic32mz.o 
	@${RM} ${OBJECTDIR}/_ext/1070083009/sys_devcon_cache_pic32mz.o.ok ${OBJECTDIR}/_ext/1070083009/sys_devcon_cache_pic32mz.o.err 
	${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../../../microchip/harmony/v2_04/third_party/rtos/FreeRTOS/Source/include" -I"../../../../../microchip/harmony/v2_04/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -I"../src/system_config/pic32mz" -MMD -MF "${OBJECTDIR}/_ext/1070083009/sys_devcon_cache_pic32mz.o.d"  -o ${OBJECTDIR}/_ext/1070083009/sys_devcon_cache_pic32mz.o ../src/system_config/pic32mz/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/1070083009/sys_devcon_cache_pic32mz.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1070083009/sys_devcon_cache_pic32mz.o.d" "${OBJECTDIR}/_ext/1070083009/sys_devcon_cache_pic32mz.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/_ext/1070083009/sys_devcon_cache_pic32mz.o: ../src/system_config/pic32mz/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  .generated_files/flags/pic32mz/e3778970f9ede848e832f42f6616f2b975d9b4a8 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1070083009" 
	@${RM} ${OBJECTDIR}/_ext/1070083009/sys_devcon_cache_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/1070083009/sys_devcon_cache_pic32mz.o 
	@${RM} ${OBJECTDIR}/_ext/1070083009/sys_devcon_cache_pic32mz.o.ok ${OBJECTDIR}/_ext/1070083009/sys_devcon_cache_pic32mz.o.err 
	${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"../../../../../microchip/harmony/v2_04/third_party/rtos/FreeRTOS/Source/include" -I"../../../../../microchip/harmony/v2_04/third_party/rtos/FreeRTOS/Source/portable/MPLAB/PIC32MZ" -I"../src/system_config/pic32mz" -MMD -MF "${OBJECTDIR}/_ext/1070083009/sys_devcon_cache_pic32mz.o.d"  -o ${OBJECTDIR}/_ext/1070083009/sys_devcon_cache_pic32mz.o ../src/system_config/pic32mz/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/1070083009/sys_devcon_cache_pic32mz.o.asm.d",--gdwarf-2 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1070083009/sys_devcon_cache_pic32mz.o.d" "${OBJECTDIR}/_ext/1070083009/sys_devcon_cache_pic32mz.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/709153290/drv_tmr.o: ../../../../../../microchip/harmony/v2_06/framework/driver/tmr/src/dynamic/drv_tmr.c  .generated_files/flags/pic32mz/6223971cf76ee510170388790885dc8d4b8a78ef .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/709153290" 
	@${RM} ${OBJECTDIR}/_ext/709153290/drv_tmr.o.d 
	@${RM} ${OBJECTDIR}/_ext/709153290/drv_tmr.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/709153290/drv_tmr.o.d" -o ${OBJECTDIR}/_ext/709153290/drv_tmr.o ../../../../../../microchip/harmony/v2_06/framework/driver/tmr/src/dynamic/drv_tmr.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1201400862/drv_usart.o: ../../../../../../microchip/harmony/v2_06/framework/driver/usart/src/dynamic/drv_usart.c  .generated_files/flags/pic32mz/d96f9b1234749548cd9a37bdf59c780499579fc .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1201400862" 
	@${RM} ${OBJECTDIR}/_ext/1201400862/drv_usart.o.d 
	@${RM} ${OBJECTDIR}/_ext/1201400862/drv_usart.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/1201400862/drv_usart.o.d" -o ${OBJECTDIR}/_ext/1201400862/drv_usart.o ../../../../../../microchip/harmony/v2_06/framework/driver/usart/src/dynamic/drv_usart.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1201400862/drv_usart_buffer_queue.o: ../../../../../../microchip/harmony/v2_06/framework/driver/usart/src/dynamic/drv_usart_buffer_queue.c  .generated_files/flags/pic32mz/32c0940e6d7c5240eab5d37e5f5b4b2bf25e3ac7 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1201400862" 
	@${RM} ${OBJECTDIR}/_ext/1201400862/drv_usart_buffer_queue.o.d 
	@${RM} ${OBJECTDIR}/_ext/1201400862/drv_usart_buffer_queue.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/1201400862/drv_usart_buffer_queue.o.d" -o ${OBJECTDIR}/_ext/1201400862/drv_usart_buffer_queue.o ../../../../../../microchip/harmony/v2_06/framework/driver/usart/src/dynamic/drv_usart_buffer_queue.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1201400862/drv_usart_read_write.o: ../../../../../../microchip/harmony/v2_06/framework/driver/usart/src/dynamic/drv_usart_read_write.c  .generated_files/flags/pic32mz/16cfb6d1080ac6df076829b6692fe77982e456dd .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1201400862" 
	@${RM} ${OBJECTDIR}/_ext/1201400862/drv_usart_read_write.o.d 
	@${RM} ${OBJECTDIR}/_ext/1201400862/drv_usart_read_write.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/1201400862/drv_usart_read_write.o.d" -o ${OBJECTDIR}/_ext/1201400862/drv_usart_read_write.o ../../../../../../microchip/harmony/v2_06/framework/driver/usart/src/dynamic/drv_usart_read_write.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/23173237/sys_console.o: ../../../../../../microchip/harmony/v2_06/framework/system/console/src/sys_console.c  .generated_files/flags/pic32mz/f13c3b3904cc032e8f9b7bc1d2a921b2f49af036 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/23173237" 
	@${RM} ${OBJECTDIR}/_ext/23173237/sys_console.o.d 
	@${RM} ${OBJECTDIR}/_ext/23173237/sys_console.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/23173237/sys_console.o.d" -o ${OBJECTDIR}/_ext/23173237/sys_console.o ../../../../../../microchip/harmony/v2_06/framework/system/console/src/sys_console.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/23173237/sys_console_uart.o: ../../../../../../microchip/harmony/v2_06/framework/system/console/src/sys_console_uart.c  .generated_files/flags/pic32mz/cce3dd5c48ea1b5b8ffe80816b8d80b0162fe1ae .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/23173237" 
	@${RM} ${OBJECTDIR}/_ext/23173237/sys_console_uart.o.d 
	@${RM} ${OBJECTDIR}/_ext/23173237/sys_console_uart.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/23173237/sys_console_uart.o.d" -o ${OBJECTDIR}/_ext/23173237/sys_console_uart.o ../../../../../../microchip/harmony/v2_06/framework/system/console/src/sys_console_uart.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1992574535/sys_debug.o: ../../../../../../microchip/harmony/v2_06/framework/system/debug/src/sys_debug.c  .generated_files/flags/pic32mz/3373ba93e4408de975f166a5305d227993c24419 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1992574535" 
	@${RM} ${OBJECTDIR}/_ext/1992574535/sys_debug.o.d 
	@${RM} ${OBJECTDIR}/_ext/1992574535/sys_debug.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/1992574535/sys_debug.o.d" -o ${OBJECTDIR}/_ext/1992574535/sys_debug.o ../../../../../../microchip/harmony/v2_06/framework/system/debug/src/sys_debug.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/419932628/sys_dma.o: ../../../../../../microchip/harmony/v2_06/framework/system/dma/src/sys_dma.c  .generated_files/flags/pic32mz/befe7a71072aa9081b091c4134d47555dd0635e3 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/419932628" 
	@${RM} ${OBJECTDIR}/_ext/419932628/sys_dma.o.d 
	@${RM} ${OBJECTDIR}/_ext/419932628/sys_dma.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/419932628/sys_dma.o.d" -o ${OBJECTDIR}/_ext/419932628/sys_dma.o ../../../../../../microchip/harmony/v2_06/framework/system/dma/src/sys_dma.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/231205469/sys_int_pic32.o: ../../../../../../microchip/harmony/v2_06/framework/system/int/src/sys_int_pic32.c  .generated_files/flags/pic32mz/ebbda430634dfd5e8bc05c507011af91fc2ab78 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/231205469" 
	@${RM} ${OBJECTDIR}/_ext/231205469/sys_int_pic32.o.d 
	@${RM} ${OBJECTDIR}/_ext/231205469/sys_int_pic32.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/231205469/sys_int_pic32.o.d" -o ${OBJECTDIR}/_ext/231205469/sys_int_pic32.o ../../../../../../microchip/harmony/v2_06/framework/system/int/src/sys_int_pic32.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/910924237/sys_tmr.o: ../../../../../../microchip/harmony/v2_06/framework/system/tmr/src/sys_tmr.c  .generated_files/flags/pic32mz/892a7b3556ff4b941d9861e0ed9807cffadd9f21 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/910924237" 
	@${RM} ${OBJECTDIR}/_ext/910924237/sys_tmr.o.d 
	@${RM} ${OBJECTDIR}/_ext/910924237/sys_tmr.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/910924237/sys_tmr.o.d" -o ${OBJECTDIR}/_ext/910924237/sys_tmr.o ../../../../../../microchip/harmony/v2_06/framework/system/tmr/src/sys_tmr.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1360937237/relay_app.o: ../src/relay_app.c  .generated_files/flags/pic32mz/f622a0968215664c6d6e81f605535265a440130c .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/relay_app.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/relay_app.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/relay_app.o.d" -o ${OBJECTDIR}/_ext/1360937237/relay_app.o ../src/relay_app.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1360937237/uart_app.o: ../src/uart_app.c  .generated_files/flags/pic32mz/caf9196500300cddcfb5e45b17cffe4582347f0a .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/uart_app.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/uart_app.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/uart_app.o.d" -o ${OBJECTDIR}/_ext/1360937237/uart_app.o ../src/uart_app.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1420049864/sys_clk_pic32mz.o: ../src/system_config/pic32mz/framework/system/clk/src/sys_clk_pic32mz.c  .generated_files/flags/pic32mz/88cc8a9ae97998d3c10d9b7a7d547827b3a316ca .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1420049864" 
	@${RM} ${OBJECTDIR}/_ext/1420049864/sys_clk_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/1420049864/sys_clk_pic32mz.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/1420049864/sys_clk_pic32mz.o.d" -o ${OBJECTDIR}/_ext/1420049864/sys_clk_pic32mz.o ../src/system_config/pic32mz/framework/system/clk/src/sys_clk_pic32mz.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1070083009/sys_devcon.o: ../src/system_config/pic32mz/framework/system/devcon/src/sys_devcon.c  .generated_files/flags/pic32mz/ab4968f060c95495c35c94f85e645a031a163cc6 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1070083009" 
	@${RM} ${OBJECTDIR}/_ext/1070083009/sys_devcon.o.d 
	@${RM} ${OBJECTDIR}/_ext/1070083009/sys_devcon.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/1070083009/sys_devcon.o.d" -o ${OBJECTDIR}/_ext/1070083009/sys_devcon.o ../src/system_config/pic32mz/framework/system/devcon/src/sys_devcon.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1070083009/sys_devcon_pic32mz.o: ../src/system_config/pic32mz/framework/system/devcon/src/sys_devcon_pic32mz.c  .generated_files/flags/pic32mz/5a7eaef0a118531493bf0a988caaf304f9a76cbf .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1070083009" 
	@${RM} ${OBJECTDIR}/_ext/1070083009/sys_devcon_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/1070083009/sys_devcon_pic32mz.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/1070083009/sys_devcon_pic32mz.o.d" -o ${OBJECTDIR}/_ext/1070083009/sys_devcon_pic32mz.o ../src/system_config/pic32mz/framework/system/devcon/src/sys_devcon_pic32mz.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/360901400/sys_ports_static.o: ../src/system_config/pic32mz/framework/system/ports/src/sys_ports_static.c  .generated_files/flags/pic32mz/ddfe27562c1328b4bed2dedb759351125c8017b6 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/360901400" 
	@${RM} ${OBJECTDIR}/_ext/360901400/sys_ports_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/360901400/sys_ports_static.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/360901400/sys_ports_static.o.d" -o ${OBJECTDIR}/_ext/360901400/sys_ports_static.o ../src/system_config/pic32mz/framework/system/ports/src/sys_ports_static.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1980434993/bsp.o: ../src/system_config/pic32mz/bsp/bsp.c  .generated_files/flags/pic32mz/7067a8e86d6aa6d02e30330049bd6ac89fb448e9 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1980434993" 
	@${RM} ${OBJECTDIR}/_ext/1980434993/bsp.o.d 
	@${RM} ${OBJECTDIR}/_ext/1980434993/bsp.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/1980434993/bsp.o.d" -o ${OBJECTDIR}/_ext/1980434993/bsp.o ../src/system_config/pic32mz/bsp/bsp.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/435813985/system_init.o: ../src/system_config/pic32mz/system_init.c  .generated_files/flags/pic32mz/f0bb14adac83515248259ef2bbfecee8f3a5a6fd .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/435813985" 
	@${RM} ${OBJECTDIR}/_ext/435813985/system_init.o.d 
	@${RM} ${OBJECTDIR}/_ext/435813985/system_init.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/435813985/system_init.o.d" -o ${OBJECTDIR}/_ext/435813985/system_init.o ../src/system_config/pic32mz/system_init.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/435813985/system_interrupt.o: ../src/system_config/pic32mz/system_interrupt.c  .generated_files/flags/pic32mz/1d88514a6ad6e3c9b1e5bb6f6b88c090d929f58b .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/435813985" 
	@${RM} ${OBJECTDIR}/_ext/435813985/system_interrupt.o.d 
	@${RM} ${OBJECTDIR}/_ext/435813985/system_interrupt.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/435813985/system_interrupt.o.d" -o ${OBJECTDIR}/_ext/435813985/system_interrupt.o ../src/system_config/pic32mz/system_interrupt.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/435813985/system_exceptions.o: ../src/system_config/pic32mz/system_exceptions.c  .generated_files/flags/pic32mz/8449d7978d0d7f413f481964359d977a125182f7 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/435813985" 
	@${RM} ${OBJECTDIR}/_ext/435813985/system_exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/435813985/system_exceptions.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/435813985/system_exceptions.o.d" -o ${OBJECTDIR}/_ext/435813985/system_exceptions.o ../src/system_config/pic32mz/system_exceptions.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  .generated_files/flags/pic32mz/72ee9d86ad7b74b992a76a88784c61bdc6d9b38f .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/435813985/system_tasks.o: ../src/system_config/pic32mz/system_tasks.c  .generated_files/flags/pic32mz/7010cb6ad55217e681017b59eb56a0a40abb521d .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/435813985" 
	@${RM} ${OBJECTDIR}/_ext/435813985/system_tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/435813985/system_tasks.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/435813985/system_tasks.o.d" -o ${OBJECTDIR}/_ext/435813985/system_tasks.o ../src/system_config/pic32mz/system_tasks.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
else
${OBJECTDIR}/_ext/709153290/drv_tmr.o: ../../../../../../microchip/harmony/v2_06/framework/driver/tmr/src/dynamic/drv_tmr.c  .generated_files/flags/pic32mz/7818f9585f0f07ebb28fa828b518831b9dd1f14e .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/709153290" 
	@${RM} ${OBJECTDIR}/_ext/709153290/drv_tmr.o.d 
	@${RM} ${OBJECTDIR}/_ext/709153290/drv_tmr.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/709153290/drv_tmr.o.d" -o ${OBJECTDIR}/_ext/709153290/drv_tmr.o ../../../../../../microchip/harmony/v2_06/framework/driver/tmr/src/dynamic/drv_tmr.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1201400862/drv_usart.o: ../../../../../../microchip/harmony/v2_06/framework/driver/usart/src/dynamic/drv_usart.c  .generated_files/flags/pic32mz/58a02256cc5dbe1b62dc2e9e713233a3ee31fa44 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1201400862" 
	@${RM} ${OBJECTDIR}/_ext/1201400862/drv_usart.o.d 
	@${RM} ${OBJECTDIR}/_ext/1201400862/drv_usart.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/1201400862/drv_usart.o.d" -o ${OBJECTDIR}/_ext/1201400862/drv_usart.o ../../../../../../microchip/harmony/v2_06/framework/driver/usart/src/dynamic/drv_usart.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1201400862/drv_usart_buffer_queue.o: ../../../../../../microchip/harmony/v2_06/framework/driver/usart/src/dynamic/drv_usart_buffer_queue.c  .generated_files/flags/pic32mz/ffecc845d1e7da4b9e82fee3832e85ee18c3588c .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1201400862" 
	@${RM} ${OBJECTDIR}/_ext/1201400862/drv_usart_buffer_queue.o.d 
	@${RM} ${OBJECTDIR}/_ext/1201400862/drv_usart_buffer_queue.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/1201400862/drv_usart_buffer_queue.o.d" -o ${OBJECTDIR}/_ext/1201400862/drv_usart_buffer_queue.o ../../../../../../microchip/harmony/v2_06/framework/driver/usart/src/dynamic/drv_usart_buffer_queue.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1201400862/drv_usart_read_write.o: ../../../../../../microchip/harmony/v2_06/framework/driver/usart/src/dynamic/drv_usart_read_write.c  .generated_files/flags/pic32mz/288e8a007c88f0d35074210195204041c3a9a712 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1201400862" 
	@${RM} ${OBJECTDIR}/_ext/1201400862/drv_usart_read_write.o.d 
	@${RM} ${OBJECTDIR}/_ext/1201400862/drv_usart_read_write.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/1201400862/drv_usart_read_write.o.d" -o ${OBJECTDIR}/_ext/1201400862/drv_usart_read_write.o ../../../../../../microchip/harmony/v2_06/framework/driver/usart/src/dynamic/drv_usart_read_write.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/23173237/sys_console.o: ../../../../../../microchip/harmony/v2_06/framework/system/console/src/sys_console.c  .generated_files/flags/pic32mz/45a990e9abf590ee26588b1c0b56a2d787410de2 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/23173237" 
	@${RM} ${OBJECTDIR}/_ext/23173237/sys_console.o.d 
	@${RM} ${OBJECTDIR}/_ext/23173237/sys_console.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/23173237/sys_console.o.d" -o ${OBJECTDIR}/_ext/23173237/sys_console.o ../../../../../../microchip/harmony/v2_06/framework/system/console/src/sys_console.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/23173237/sys_console_uart.o: ../../../../../../microchip/harmony/v2_06/framework/system/console/src/sys_console_uart.c  .generated_files/flags/pic32mz/4f049eea720c038bd5f4cd5139ab80a0af8f96f0 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/23173237" 
	@${RM} ${OBJECTDIR}/_ext/23173237/sys_console_uart.o.d 
	@${RM} ${OBJECTDIR}/_ext/23173237/sys_console_uart.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/23173237/sys_console_uart.o.d" -o ${OBJECTDIR}/_ext/23173237/sys_console_uart.o ../../../../../../microchip/harmony/v2_06/framework/system/console/src/sys_console_uart.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1992574535/sys_debug.o: ../../../../../../microchip/harmony/v2_06/framework/system/debug/src/sys_debug.c  .generated_files/flags/pic32mz/9207dee65d7883a0a131405944ea7bf61483ce0b .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1992574535" 
	@${RM} ${OBJECTDIR}/_ext/1992574535/sys_debug.o.d 
	@${RM} ${OBJECTDIR}/_ext/1992574535/sys_debug.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/1992574535/sys_debug.o.d" -o ${OBJECTDIR}/_ext/1992574535/sys_debug.o ../../../../../../microchip/harmony/v2_06/framework/system/debug/src/sys_debug.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/419932628/sys_dma.o: ../../../../../../microchip/harmony/v2_06/framework/system/dma/src/sys_dma.c  .generated_files/flags/pic32mz/bb088a21d799c51aba987ed898587e18a12ee284 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/419932628" 
	@${RM} ${OBJECTDIR}/_ext/419932628/sys_dma.o.d 
	@${RM} ${OBJECTDIR}/_ext/419932628/sys_dma.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/419932628/sys_dma.o.d" -o ${OBJECTDIR}/_ext/419932628/sys_dma.o ../../../../../../microchip/harmony/v2_06/framework/system/dma/src/sys_dma.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/231205469/sys_int_pic32.o: ../../../../../../microchip/harmony/v2_06/framework/system/int/src/sys_int_pic32.c  .generated_files/flags/pic32mz/39a0d2b08938e191ef854315b3dc165f5c66f63e .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/231205469" 
	@${RM} ${OBJECTDIR}/_ext/231205469/sys_int_pic32.o.d 
	@${RM} ${OBJECTDIR}/_ext/231205469/sys_int_pic32.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/231205469/sys_int_pic32.o.d" -o ${OBJECTDIR}/_ext/231205469/sys_int_pic32.o ../../../../../../microchip/harmony/v2_06/framework/system/int/src/sys_int_pic32.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/910924237/sys_tmr.o: ../../../../../../microchip/harmony/v2_06/framework/system/tmr/src/sys_tmr.c  .generated_files/flags/pic32mz/20f8bf7b65a772bcf904253ba3443a549e515caf .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/910924237" 
	@${RM} ${OBJECTDIR}/_ext/910924237/sys_tmr.o.d 
	@${RM} ${OBJECTDIR}/_ext/910924237/sys_tmr.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/910924237/sys_tmr.o.d" -o ${OBJECTDIR}/_ext/910924237/sys_tmr.o ../../../../../../microchip/harmony/v2_06/framework/system/tmr/src/sys_tmr.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1360937237/relay_app.o: ../src/relay_app.c  .generated_files/flags/pic32mz/d5d475163567459bd5070f1d9438e10f638c7b0a .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/relay_app.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/relay_app.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/relay_app.o.d" -o ${OBJECTDIR}/_ext/1360937237/relay_app.o ../src/relay_app.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1360937237/uart_app.o: ../src/uart_app.c  .generated_files/flags/pic32mz/ade6ee7fb1e74fb21e55669a259eb69075aa7085 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/uart_app.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/uart_app.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/uart_app.o.d" -o ${OBJECTDIR}/_ext/1360937237/uart_app.o ../src/uart_app.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1420049864/sys_clk_pic32mz.o: ../src/system_config/pic32mz/framework/system/clk/src/sys_clk_pic32mz.c  .generated_files/flags/pic32mz/192565b5dfd5f6c8ca78cc9825061308f6bec69c .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1420049864" 
	@${RM} ${OBJECTDIR}/_ext/1420049864/sys_clk_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/1420049864/sys_clk_pic32mz.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/1420049864/sys_clk_pic32mz.o.d" -o ${OBJECTDIR}/_ext/1420049864/sys_clk_pic32mz.o ../src/system_config/pic32mz/framework/system/clk/src/sys_clk_pic32mz.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1070083009/sys_devcon.o: ../src/system_config/pic32mz/framework/system/devcon/src/sys_devcon.c  .generated_files/flags/pic32mz/d1fb727e07ae5ce5420aefa2c0edc1f8c156bcd8 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1070083009" 
	@${RM} ${OBJECTDIR}/_ext/1070083009/sys_devcon.o.d 
	@${RM} ${OBJECTDIR}/_ext/1070083009/sys_devcon.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/1070083009/sys_devcon.o.d" -o ${OBJECTDIR}/_ext/1070083009/sys_devcon.o ../src/system_config/pic32mz/framework/system/devcon/src/sys_devcon.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1070083009/sys_devcon_pic32mz.o: ../src/system_config/pic32mz/framework/system/devcon/src/sys_devcon_pic32mz.c  .generated_files/flags/pic32mz/db91d6717563e3d15e2b2172937e8153cb74c346 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1070083009" 
	@${RM} ${OBJECTDIR}/_ext/1070083009/sys_devcon_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/1070083009/sys_devcon_pic32mz.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/1070083009/sys_devcon_pic32mz.o.d" -o ${OBJECTDIR}/_ext/1070083009/sys_devcon_pic32mz.o ../src/system_config/pic32mz/framework/system/devcon/src/sys_devcon_pic32mz.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/360901400/sys_ports_static.o: ../src/system_config/pic32mz/framework/system/ports/src/sys_ports_static.c  .generated_files/flags/pic32mz/78e97869ecdc8335c2800e3de1bdad678c29e8de .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/360901400" 
	@${RM} ${OBJECTDIR}/_ext/360901400/sys_ports_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/360901400/sys_ports_static.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/360901400/sys_ports_static.o.d" -o ${OBJECTDIR}/_ext/360901400/sys_ports_static.o ../src/system_config/pic32mz/framework/system/ports/src/sys_ports_static.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1980434993/bsp.o: ../src/system_config/pic32mz/bsp/bsp.c  .generated_files/flags/pic32mz/6926f954745e928bb7a2528477fcefcb6ed13974 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1980434993" 
	@${RM} ${OBJECTDIR}/_ext/1980434993/bsp.o.d 
	@${RM} ${OBJECTDIR}/_ext/1980434993/bsp.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/1980434993/bsp.o.d" -o ${OBJECTDIR}/_ext/1980434993/bsp.o ../src/system_config/pic32mz/bsp/bsp.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/435813985/system_init.o: ../src/system_config/pic32mz/system_init.c  .generated_files/flags/pic32mz/4c77415bf8ae11dcf9c13db076ba9a81b70939e6 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/435813985" 
	@${RM} ${OBJECTDIR}/_ext/435813985/system_init.o.d 
	@${RM} ${OBJECTDIR}/_ext/435813985/system_init.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/435813985/system_init.o.d" -o ${OBJECTDIR}/_ext/435813985/system_init.o ../src/system_config/pic32mz/system_init.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/435813985/system_interrupt.o: ../src/system_config/pic32mz/system_interrupt.c  .generated_files/flags/pic32mz/bc9ae6d82eade5d875ba1380d9946c7a65d0e904 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/435813985" 
	@${RM} ${OBJECTDIR}/_ext/435813985/system_interrupt.o.d 
	@${RM} ${OBJECTDIR}/_ext/435813985/system_interrupt.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/435813985/system_interrupt.o.d" -o ${OBJECTDIR}/_ext/435813985/system_interrupt.o ../src/system_config/pic32mz/system_interrupt.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/435813985/system_exceptions.o: ../src/system_config/pic32mz/system_exceptions.c  .generated_files/flags/pic32mz/a55f3ab3240162581cdb5ac99fa96411b6fccb02 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/435813985" 
	@${RM} ${OBJECTDIR}/_ext/435813985/system_exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/435813985/system_exceptions.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/435813985/system_exceptions.o.d" -o ${OBJECTDIR}/_ext/435813985/system_exceptions.o ../src/system_config/pic32mz/system_exceptions.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  .generated_files/flags/pic32mz/1d530052a1469264e2a6ff2f79dc7a548fb78759 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/435813985/system_tasks.o: ../src/system_config/pic32mz/system_tasks.c  .generated_files/flags/pic32mz/ac0f728c3007fcdc38f267b038edf2599e172f79 .generated_files/flags/pic32mz/23afc7694fdd136f085413f50cd3826a82725351
	@${MKDIR} "${OBJECTDIR}/_ext/435813985" 
	@${RM} ${OBJECTDIR}/_ext/435813985/system_tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/435813985/system_tasks.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/pic32mz" -I"../src/pic32mz" -I"../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/pic32mz/framework" -I"../../../../../../microchip/harmony/v2_06/framework" -I"../src/system_config/pic32mz/bsp" -MP -MMD -MF "${OBJECTDIR}/_ext/435813985/system_tasks.o.d" -o ${OBJECTDIR}/_ext/435813985/system_tasks.o ../src/system_config/pic32mz/system_tasks.c    -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/replay_app.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  ../../../../../../microchip/harmony/v2_06/bin/framework/peripheral/PIC32MZ2048EFM100_peripherals.a  
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -g -mdebugger -D__MPLAB_DEBUGGER_PK3=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/replay_app.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    ../../../../../../microchip/harmony/v2_06/bin/framework/peripheral/PIC32MZ2048EFM100_peripherals.a      -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   -mreserve=data@0x0:0x37F   -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D=__DEBUG_D,--defsym=__MPLAB_DEBUGGER_PK3=1,--defsym=_min_heap_size=2048,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/replay_app.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  ../../../../../../microchip/harmony/v2_06/bin/framework/peripheral/PIC32MZ2048EFM100_peripherals.a 
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/replay_app.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    ../../../../../../microchip/harmony/v2_06/bin/framework/peripheral/PIC32MZ2048EFM100_peripherals.a      -DXPRJ_pic32mz=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=_min_heap_size=2048,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml 
	${MP_CC_DIR}/xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/replay_app.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/pic32mz
	${RM} -r dist/pic32mz

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
