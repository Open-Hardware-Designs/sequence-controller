/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    relay_app.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "relay_app.h"

// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/

RELAY_APP_DATA relay_appData;

// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary callback functions.
*/

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************


/* TODO:  Add any necessary local functions.
*/
static void RELAY_APP_SysTimerCallback1 (  uintptr_t context, uint32_t  alarmCount )
{
        relay_appData.LED1Toggle = true;
}
 
// Callback function sets the flag to toggle LED2
static void RELAY_APP_SysTimerCallback2 (  uintptr_t context, uint32_t  alarmCount )
{
        relay_appData.LED2Toggle = true;
}

static void RELAY_APP_SysTimerCallback_AppTotal(uintptr_t context, uint32_t  alarmCount )
{
    if( relay_appData.sequenceCycleCounter == relay_appData.sequenceCycles - 1)
    {
        relay_appData.sequenceCycleCounter = 0;
        relay_appData.state = RELAY_APP_STATE_LED_TOGGLE;
    }
    else
    {
        relay_appData.sequenceCycleCounter++;
        relay_appData.state = RELAY_APP_STATE_INIT_SEQUENCE;
    }
    
    
    
    
    //RELAY_IN1Off();
    //Relay1Off()
}


static void RELAY_APP_SysTimerCallback_Relay8(uintptr_t context, uint32_t  alarmCount )
{
    RELAY_IN8Off();
    //Relay1Off()
}
static void RELAY_APP_SysTimerCallback_Relay7(uintptr_t context, uint32_t  alarmCount )
{
    RELAY_IN7Off();
    //Relay1Off()
}
static void RELAY_APP_SysTimerCallback_Relay6(uintptr_t context, uint32_t  alarmCount )
{
    RELAY_IN6Off();
    //Relay1Off()
}
static void RELAY_APP_SysTimerCallback_Relay5(uintptr_t context, uint32_t  alarmCount )
{
    RELAY_IN5Off();
    //Relay1Off()
}
static void RELAY_APP_SysTimerCallback_Relay4(uintptr_t context, uint32_t  alarmCount )
{
    RELAY_IN4Off();
    //Relay1Off()
}
static void RELAY_APP_SysTimerCallback_Relay3(uintptr_t context, uint32_t  alarmCount )
{
    RELAY_IN3Off();
    //Relay1Off()
}
static void RELAY_APP_SysTimerCallback_Relay2(uintptr_t context, uint32_t  alarmCount )
{
    RELAY_IN2Off();
    //Relay1Off()
}
static void RELAY_APP_SysTimerCallback_Relay1(uintptr_t context, uint32_t  alarmCount )
{
    RELAY_IN1Off();
    //Relay1Off()
}

void SequenceStart()
{
    
            
    relay_appData.relay_1.callback_set = false;
    relay_appData.relay_2.callback_set = false;
    relay_appData.relay_3.callback_set = false;
    relay_appData.relay_4.callback_set = false;
    relay_appData.relay_5.callback_set = false;
    relay_appData.relay_6.callback_set = false;
    relay_appData.relay_7.callback_set = false;
    relay_appData.relay_8.callback_set = false;
    
    relay_appData.app_total.callback_set = false;
    
    relay_appData.sequenceStartTick = SYS_TMR_TickCountGet();
    
    relay_appData.relay_1.startTick = relay_appData.sequenceStartTick + relay_appData.relay_1.startTime;
    relay_appData.relay_2.startTick = relay_appData.sequenceStartTick + relay_appData.relay_2.startTime;
    relay_appData.relay_3.startTick = relay_appData.sequenceStartTick + relay_appData.relay_3.startTime;
    relay_appData.relay_4.startTick = relay_appData.sequenceStartTick + relay_appData.relay_4.startTime;
    relay_appData.relay_5.startTick = relay_appData.sequenceStartTick + relay_appData.relay_5.startTime;
    relay_appData.relay_6.startTick = relay_appData.sequenceStartTick + relay_appData.relay_6.startTime;
    relay_appData.relay_7.startTick = relay_appData.sequenceStartTick + relay_appData.relay_7.startTime;
    relay_appData.relay_8.startTick = relay_appData.sequenceStartTick + relay_appData.relay_8.startTime;
    
    relay_appData.app_total.startTick = relay_appData.sequenceStartTick + relay_appData.app_total.startTime;
    
}

void SequenceRun()
{
    relay_appData.currentTick = SYS_TMR_TickCountGet();
    
    if((relay_appData.relay_8.startTick <= relay_appData.currentTick) && (relay_appData.relay_8.callback_set == false))
    {
        relay_appData.relay_8.sysTimerHandle = SYS_TMR_CallbackSingle( relay_appData.relay_8.duration, 1, RELAY_APP_SysTimerCallback_Relay8);
        RELAY_IN8On();
        relay_appData.relay_8.callback_set = true;
    }
    if((relay_appData.relay_7.startTick <= relay_appData.currentTick) && (relay_appData.relay_7.callback_set == false))
    {
        relay_appData.relay_7.sysTimerHandle = SYS_TMR_CallbackSingle( relay_appData.relay_7.duration, 1, RELAY_APP_SysTimerCallback_Relay7);
        RELAY_IN7On();
        relay_appData.relay_7.callback_set = true;
    }
    if((relay_appData.relay_6.startTick <= relay_appData.currentTick) && (relay_appData.relay_6.callback_set == false))
    {
        relay_appData.relay_6.sysTimerHandle = SYS_TMR_CallbackSingle( relay_appData.relay_6.duration, 1, RELAY_APP_SysTimerCallback_Relay6);
        RELAY_IN6On();
        relay_appData.relay_6.callback_set = true;
    }
    if((relay_appData.relay_5.startTick <= relay_appData.currentTick) && (relay_appData.relay_5.callback_set == false))
    {
        relay_appData.relay_5.sysTimerHandle = SYS_TMR_CallbackSingle( relay_appData.relay_5.duration, 1, RELAY_APP_SysTimerCallback_Relay5);
        RELAY_IN5On();
        relay_appData.relay_5.callback_set = true;
    }
    if((relay_appData.relay_4.startTick <= relay_appData.currentTick) && (relay_appData.relay_4.callback_set == false))
    {
        relay_appData.relay_4.sysTimerHandle = SYS_TMR_CallbackSingle( relay_appData.relay_4.duration, 1, RELAY_APP_SysTimerCallback_Relay4);
        RELAY_IN4On();
        relay_appData.relay_4.callback_set = true;
    }
    if((relay_appData.relay_3.startTick <= relay_appData.currentTick) && (relay_appData.relay_3.callback_set == false))
    {
        relay_appData.relay_3.sysTimerHandle = SYS_TMR_CallbackSingle( relay_appData.relay_3.duration, 1, RELAY_APP_SysTimerCallback_Relay3);
        RELAY_IN3On();
        relay_appData.relay_3.callback_set = true;
    }
    if((relay_appData.relay_2.startTick <= relay_appData.currentTick) && (relay_appData.relay_2.callback_set == false))
    {
        relay_appData.relay_2.sysTimerHandle = SYS_TMR_CallbackSingle( relay_appData.relay_2.duration, 1, RELAY_APP_SysTimerCallback_Relay2);
        RELAY_IN2On();
        relay_appData.relay_2.callback_set = true;
    }
    if((relay_appData.relay_1.startTick <= relay_appData.currentTick) && (relay_appData.relay_1.callback_set == false))
    {
        relay_appData.relay_1.sysTimerHandle = SYS_TMR_CallbackSingle( relay_appData.relay_1.duration, 1, RELAY_APP_SysTimerCallback_Relay1);
        RELAY_IN1On();
        relay_appData.relay_1.callback_set = true;
    }
    if((relay_appData.app_total.startTick <= relay_appData.currentTick) && (relay_appData.app_total.callback_set == false))
    {
        relay_appData.app_total.sysTimerHandle = SYS_TMR_CallbackSingle( relay_appData.app_total.duration, 1, RELAY_APP_SysTimerCallback_AppTotal);
        //RELAY_IN1On();
        relay_appData.app_total.callback_set = true;
    }
}


void relayAndTimerStop()
{
    SYS_TMR_CallbackStop(relay_appData.relay_1.sysTimerHandle);
    SYS_TMR_CallbackStop(relay_appData.relay_2.sysTimerHandle);
    SYS_TMR_CallbackStop(relay_appData.relay_3.sysTimerHandle);
    SYS_TMR_CallbackStop(relay_appData.relay_4.sysTimerHandle);
    SYS_TMR_CallbackStop(relay_appData.relay_5.sysTimerHandle);
    SYS_TMR_CallbackStop(relay_appData.relay_6.sysTimerHandle);
    SYS_TMR_CallbackStop(relay_appData.relay_7.sysTimerHandle);
    SYS_TMR_CallbackStop(relay_appData.relay_8.sysTimerHandle);
    SYS_TMR_CallbackStop(relay_appData.app_total.sysTimerHandle);

    RELAY_IN1Off();
    RELAY_IN2Off();
    RELAY_IN3Off();
    RELAY_IN4Off();
    RELAY_IN5Off();
    RELAY_IN6Off();
    RELAY_IN7Off();
    RELAY_IN8Off();
}

// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************


/*******************************************************************************
  Function:
    void RELAY_APP_Initialize ( void )

  Remarks:
    See prototype in relay_app.h.
 */

void relaySetDefaultValues()
{
    relay_appData.relay_1.startTime = 6000 / 2;
    relay_appData.relay_2.startTime = 5500 / 2;
    relay_appData.relay_3.startTime = 5000 / 2;
    relay_appData.relay_4.startTime = 4500 / 2;
    relay_appData.relay_5.startTime = 4000 / 2;
    relay_appData.relay_6.startTime = 3500 / 2;
    relay_appData.relay_7.startTime = 3000 / 2;
    relay_appData.relay_8.startTime = 2500 / 2;
    
    relay_appData.relay_1.duration = 500 / 2;
    relay_appData.relay_2.duration = 500 / 2;
    relay_appData.relay_3.duration = 500 / 2;
    relay_appData.relay_4.duration = 500 / 2;
    relay_appData.relay_5.duration = 2500 / 2;
    relay_appData.relay_6.duration = 1500 / 2;
    relay_appData.relay_7.duration = 2000 / 2;
    relay_appData.relay_8.duration = 2500 / 2;
    
    relay_appData.app_total.duration = 4000;
    
    relay_appData.sequenceCycles = 3;
}

void RELAY_APP_Initialize ( void )
{
    /* Place the App state machine in its initial state. */
    relay_appData.state = RELAY_APP_STATE_INIT;
    
    relay_appData.sysTimerClientHandle1  = SYS_TMR_HANDLE_INVALID;
    relay_appData.sysTimerClientHandle2  = SYS_TMR_HANDLE_INVALID;
    relay_appData.LED1Toggle = false;
    relay_appData.LED2Toggle = false;
    relay_appData.startTick = 0; // holds the value of the System Counter at start of period
    BSP_LEDOff( BSP_LED_1 );
    BSP_LEDOff( BSP_LED_2 );
    BSP_LEDOff( BSP_LED_3 );
    
    relay_appData.relay_1.startTime = 6000 / 2;
    relay_appData.relay_2.startTime = 5500 / 2;
    relay_appData.relay_3.startTime = 5000 / 2;
    relay_appData.relay_4.startTime = 4500 / 2;
    relay_appData.relay_5.startTime = 4000 / 2;
    relay_appData.relay_6.startTime = 3500 / 2;
    relay_appData.relay_7.startTime = 3000 / 2;
    relay_appData.relay_8.startTime = 2500 / 2;
    
    relay_appData.app_total.startTime = 0;
    
    relay_appData.relay_1.duration = 500 / 2;
    relay_appData.relay_2.duration = 500 / 2;
    relay_appData.relay_3.duration = 500 / 2;
    relay_appData.relay_4.duration = 500 / 2;
    relay_appData.relay_5.duration = 2500 / 2;
    relay_appData.relay_6.duration = 1500 / 2;
    relay_appData.relay_7.duration = 2000 / 2;
    relay_appData.relay_8.duration = 2500 / 2;
    
    relay_appData.app_total.duration = 10000;
    
    relay_appData.sequenceCycles = 3;
    relay_appData.sequenceCycleCounter = 0;
    
    RELAY_IN8Off();
    RELAY_IN7Off();
    RELAY_IN6Off();
    RELAY_IN5Off();
    RELAY_IN4Off();
    RELAY_IN3Off();
    RELAY_IN2Off();
    RELAY_IN1Off();

    


    
    /* TODO: Initialize your application's state machine and other
     * parameters.
     */
}


/******************************************************************************
  Function:
    void RELAY_APP_Tasks ( void )

  Remarks:
    See prototype in relay_app.h.
 */

void RELAY_APP_Tasks ( void )
{


    /* Check the application's current state. */
    switch ( relay_appData.state )
    {
        /* Application's initial state. */
        case RELAY_APP_STATE_INIT:
        {
            bool appInitialized = true;
            
            
       
        
            if (appInitialized)
            {
                relay_appData.sysTimerClientHandle1 = SYS_TMR_CallbackPeriodic(500, 0, RELAY_APP_SysTimerCallback1);
                relay_appData.sysTimerClientHandle2 = SYS_TMR_CallbackPeriodic(1000, 0, RELAY_APP_SysTimerCallback2);
            
            
                relay_appData.state = RELAY_APP_STATE_SERVICE_TASKS;
                
                //relay_appData.state = RELAY_APP_STATE_RX;
            }
            break;
        }

        case RELAY_APP_STATE_SERVICE_TASKS:
        {
            relay_appData.state = RELAY_APP_STATE_LED_TOGGLE;
            //SYS_PRINT("Tick count: %u\r\n", xTaskGetTickCount());
            //vTaskDelay(2000);
        
            break;
        } 
        
        case RELAY_APP_STATE_LED_TOGGLE:
        {
            if (relay_appData.LED1Toggle == true)  // check if callback changed LED toggle flag
            {
                BSP_LEDToggle( BSP_LED_1 );
                //PLIB_PORTS_PinToggle(PORTS_ID_0, APP_LED_PORT, APP_LED1_PIN); // toggle LED
                relay_appData.LED1Toggle = false;  // reset LED toggle flag
            }

            if (relay_appData.LED2Toggle == true)
            {
                BSP_LEDToggle( BSP_LED_2 );
                //PLIB_PORTS_PinToggle(PORTS_ID_0, APP_LED_PORT, APP_LED2_PIN);
                relay_appData.LED2Toggle = false;
            }

            if (SYS_TMR_TickCountGet() - relay_appData.startTick >= SYS_TMR_TickCounterFrequencyGet())
            {
                BSP_LEDToggle( BSP_LED_3 );
                relay_appData.startTick = SYS_TMR_TickCountGet();    // reset start time
                //PLIB_PORTS_PinToggle(PORTS_ID_0, APP_LED_PORT, APP_LED3_PIN);
                SYS_PRINT("HAHA great %u\r\n", relay_appData.startTick );
            }
            
//            if(Console_UART_Status() == SYS_CONSOLE_STATUS_CONFIGURED)
//            {
//                char uart_read;
//                uart_read = Console_UART_Read(1, uart_read, 1);
//                uart_read++;
//                SYS_PRINT("Uart Read %c \r\n");
//            }
            
            
            if(!BSP_SWITCH_1StateGet())
            {
                while(!BSP_SWITCH_1StateGet());
                relay_appData.sequenceCycleCounter = 0;
                relay_appData.state = RELAY_APP_STATE_INIT_SEQUENCE;
            }
            
            //UART_TaskStates();
            //SYS_TMR_DelayMS( 5000 );
            break;
        }
        
        case RELAY_APP_STATE_INIT_SEQUENCE:
        {
            SYS_PRINT("Sequence Start\r\n");
            relay_appData.state = RELAY_APP_STATE_RUN_SEQUENCE;
            SequenceStart();
            break;
        }
        
        case RELAY_APP_STATE_RUN_SEQUENCE:
        {
            
            SequenceRun();
            if(!BSP_SWITCH_1StateGet())
            {
                while(!BSP_SWITCH_1StateGet());
                SYS_TMR_CallbackStop(relay_appData.relay_1.sysTimerHandle);
                SYS_TMR_CallbackStop(relay_appData.relay_2.sysTimerHandle);
                SYS_TMR_CallbackStop(relay_appData.relay_3.sysTimerHandle);
                SYS_TMR_CallbackStop(relay_appData.relay_4.sysTimerHandle);
                SYS_TMR_CallbackStop(relay_appData.relay_5.sysTimerHandle);
                SYS_TMR_CallbackStop(relay_appData.relay_6.sysTimerHandle);
                SYS_TMR_CallbackStop(relay_appData.relay_7.sysTimerHandle);
                SYS_TMR_CallbackStop(relay_appData.relay_8.sysTimerHandle);
                SYS_TMR_CallbackStop(relay_appData.app_total.sysTimerHandle);
                
                RELAY_IN1Off();
                RELAY_IN2Off();
                RELAY_IN3Off();
                RELAY_IN4Off();
                RELAY_IN5Off();
                RELAY_IN6Off();
                RELAY_IN7Off();
                RELAY_IN8Off();
                //Add possible kill for all timers
                //Reset all the Relays
                
                
                //relay_appData.sequenceCycleCounter = relay_appData.sequenceCycles;
                relay_appData.state = RELAY_APP_STATE_LED_TOGGLE;
            }
            break;
        }

        /* TODO: implement your application state machine.*/
        

        /* The default state should never be executed. */
        default:
        {
            /* TODO: Handle error in application's state machine. */
            break;
        }
    }
}

 

/*******************************************************************************
 End of File
 */
