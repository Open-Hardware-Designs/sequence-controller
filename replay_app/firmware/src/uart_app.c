/*******************************************************************************
  MPLAB Harmony Application Source File
  
  Company:
    Microchip Technology Inc.
  
  File Name:
    uart_app.c

  Summary:
    This file contains the source code for the MPLAB Harmony application.

  Description:
    This file contains the source code for the MPLAB Harmony application.  It 
    implements the logic of the application's state machine and it may call 
    API routines of other MPLAB Harmony modules in the system, such as drivers,
    system services, and middleware.  However, it does not call any of the
    system interfaces (such as the "Initialize" and "Tasks" functions) of any of
    the modules in the system or make any assumptions about when those functions
    are called.  That is the responsibility of the configuration-specific system
    files.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "uart_app.h"
#include <stdio.h>
#include <string.h>
// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/
extern RELAY_APP_DATA relay_appData;
UART_APP_DATA uart_appData;

static RESPONSE_OBJ serial_response;

RELAY_RESPONSE_DATA serial_data;

static uint8_t usartRWTxData[] = "Hello World\r\n";
static enum 
{
    USART_RW_INIT,
    USART_RW_WORKING,
    USART_RW_DONE,
} usartRWState;

// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary callback functions.
*/

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
/**
* Returns true if the response has been successfully parsed and is complete and ready for use
*/
bool serialResponse_isAvailable(){
    return serial_response._complete;
}

void serialResponse_setAvailable(bool complete){
    serial_response._complete = complete;
}
/**
* Returns true if the response contains errors
*/
bool serialResponse_isError(){
    return serial_response._errorCode > 0;
}

void serialResponse_init()
{
    serial_response._complete = false;
    serial_response._errorCode = NO_ERROR;
    serial_response._checksum = 0;
}

void serialResponse_reset()
{
    serialResponse_init();
    serial_response._apiId = 0;
    serial_response._msbLength = 0;
    serial_response._lsbLength = 0;
    serial_response._checksum = 0;
    serial_response._frameLength = 0;

    serial_response._errorCode = NO_ERROR;    
}

void serialResponse_setErrorCode(uint8_t errorCode){
    serial_response._errorCode = errorCode;
}

void serialResponse_setMsbLength(uint8_t msbLength){
    serial_response._msbLength = msbLength;
}

void serialResponse_setLsbLength(uint8_t lsbLength){
    serial_response._lsbLength = lsbLength;
}

void serialResponse_setApiId(uint8_t apiId){
    serial_response._apiId = apiId;
}

uint8_t serialResponse_getApiId(){
    return serial_response._apiId;
}

uint16_t serialResponse_getPacketLength(){
    return ((serial_response._msbLength << 8) & 0xff) + (serial_response._lsbLength & 0xff);
}

void serialResponse_setChecksum(uint8_t checksum){
    serial_response._checksum = checksum;
}

void serialResponse_setFrameLength(uint8_t frameLength){
    serial_response._frameLength = frameLength;
}

uint8_t serialResponse_getFrameDataLength()
{
    return serial_response._frameLength;
}

uint8_t* serialResponse_getFrameData(){
    return serial_data._frameDataPtr;
    
}

//void serialResponse_setFrameData(uint8_t* frameDataPtr){
//    serial_data._frameDataPtr = frameDataPtr;
//}

serial_resetResponse()
{
	serial_data._pos = 0;
	serial_data._escape = false;
	serial_data._checksumTotal = 0;
    serialResponse_reset();    
}

void serial_readPacket()
{
    // reset previous response
    if (serialResponse_isAvailable() || serialResponse_isError())
    {
        // discard previous serial and start over
        serial_resetResponse();
    }
    while( DRV_USART_Read(uart_appData.handleUSART1, &uart_appData.usartRWRxData[0],1) == 1)
    {
        char b = uart_appData.usartRWRxData[0];
        if(serial_data._pos > 0 && b == START_BYTE && ATAP == 2)
        {
            // new packet start before previous packeted completed -- discard previous packet and start over
        	serialResponse_setErrorCode(UNEXPECTED_START_BYTE);
            return;
        }
        if (serial_data._pos > 0 && b == ESCAPE)
        {
            if( DRV_USART_Read(uart_appData.handleUSART1, &uart_appData.usartRWRxData[0],1) == 1)
            {
				b = uart_appData.usartRWRxData[0];
				b = 0x20 ^ b;
			} else {
				// escape byte.  next byte will be
				serial_data._escape = true;
				
            }
        }
        if (serial_data._escape == true)
        {
            b = 0x20 ^ b;
            serial_data._escape = false;
        }
        
        // checksum include all bytes starting with api id
        if (serial_data._pos >= API_ID_INDEX){
            serial_data._checksumTotal+=b;
        }
        switch(serial_data._pos)
        {
            case 0:
                if (b == START_BYTE){
                    serial_data._pos++;
                }
                break;
            case 1:
                // length msb
                serialResponse_setMsbLength(b);
                serial_data._pos++;
                break;
            case 2:
                // length lsb
                serialResponse_setLsbLength(b);
                serial_data._pos++;
                break;
            case 3:
                serialResponse_setApiId(b);
                serial_data._pos++;
                SYS_PRINT("API ID %X\r\n",b);
                break;
            default:
            {
                
                
//                //Starts with fifth byte
                if(serial_data._pos > MAX_FRAME_DATA_SIZE)
                {
                    serialResponse_setErrorCode(PACKET_EXCEEDS_BYTE_ARRAY_LENGTH);
                    
                    return;
                }
                
                
//                SYS_PRINT("11\r\n");
                // check if we're at the end of the packet
                // packet length does not include start, length, or checksum bytes, so add 3 // TWO BEFORE CHECKSUM IS ADDed
                if(serial_data._pos == (serialResponse_getPacketLength() + 3))
                {
                    
                    serialResponse_setFrameLength(serial_data._pos - 4); //-2 AFTER CHECKSUM ADDED
                    serial_data._pos = 0;
                    serialResponse_setAvailable(true);
                    serialResponse_setErrorCode(NO_ERROR);
                    usartRWState = USART_RW_DONE;
                    return;
                } else
                {
                    //serialResponse_getFrameData()[serial_data._pos - 4] = b;
                    serial_data.frameDataArray[serial_data._pos - 4] = b;
                    serial_data._pos++;
                }
            }
        }
    }
}

void serialDataUnpack()
{
    switch(serialResponse_getApiId())
    {
        case CONTROL_REQUEST:
        {
            SYS_PRINT("Control Request\r\n");
            if(serial_data.frameDataArray[0]== START)
            {
                relay_appData.state = RELAY_APP_STATE_INIT_SEQUENCE;
            }else if(serial_data.frameDataArray[0]== STOP)
            {
                relayAndTimerStop();
                relay_appData.state = RELAY_APP_STATE_LED_TOGGLE;
            }else if(serial_data.frameDataArray[0]== RESET)
            {
                relaySetDefaultValues();
                relayAndTimerStop();
                relay_appData.state = RELAY_APP_STATE_LED_TOGGLE;
            }
            break;
        }
        
        case RELAY_CONFIG:
        {
            SYS_PRINT("Relay Configuration\r\n");
            //Add while or for Loop
            int i = 0;
            SYS_PRINT("i %d\r\n",i);
            while(i<serialResponse_getFrameDataLength())
            {
                SYS_PRINT("i %d\r\n",i);
                
                switch(serial_data.frameDataArray[i])
                {
                    case DURATION:
                    {
                        relay_appData.app_total.duration = (serial_data.frameDataArray[i+1] << 24) | (serial_data.frameDataArray[i+2] << 16) | (serial_data.frameDataArray[i+3] << 8) | serial_data.frameDataArray[i+4];
                        i=i+5;
                        break;
                    }
                    case CYCLE_COUNTER:
                    {
                        relay_appData.sequenceCycles = (serial_data.frameDataArray[i+1] << 24) | (serial_data.frameDataArray[i+2] << 16) | (serial_data.frameDataArray[i+3] << 8) | serial_data.frameDataArray[i+4];
                        i=i+5;
                        break;
                    }
                    case RELAY_1:
                    {
                        relay_appData.relay_1.startTime = (serial_data.frameDataArray[i+1] << 24) | (serial_data.frameDataArray[i+2] << 16) | (serial_data.frameDataArray[i+3] << 8) | serial_data.frameDataArray[i+4];
                        relay_appData.relay_1.duration = (serial_data.frameDataArray[i+5] << 24) | (serial_data.frameDataArray[i+6] << 16) | (serial_data.frameDataArray[i+7] << 8) | serial_data.frameDataArray[i+8];
                        SYS_PRINT("Relay: %d, Start time: %d, Duration %d\r\n",serial_data.frameDataArray[i], relay_appData.relay_1.startTime, relay_appData.relay_1.duration);

                        i=i+9;
                        break;
                    }
                    case RELAY_2:
                    {
                        relay_appData.relay_2.startTime = (serial_data.frameDataArray[i+1] << 24) | (serial_data.frameDataArray[i+2] << 16) | (serial_data.frameDataArray[i+3] << 8) | serial_data.frameDataArray[i+4];
                        relay_appData.relay_2.duration = (serial_data.frameDataArray[i+5] << 24) | (serial_data.frameDataArray[i+6] << 16) | (serial_data.frameDataArray[i+7] << 8) | serial_data.frameDataArray[i+8];
                        SYS_PRINT("Relay: %d, Start time: %d, Duration %d\r\n",serial_data.frameDataArray[i], relay_appData.relay_2.startTime, relay_appData.relay_2.duration);
                    
                        i=i+9;
                        break;
                    }
                    case RELAY_3:
                    {
                        relay_appData.relay_3.startTime = (serial_data.frameDataArray[i+1] << 24) | (serial_data.frameDataArray[i+2] << 16) | (serial_data.frameDataArray[i+3] << 8) | serial_data.frameDataArray[i+4];
                        relay_appData.relay_3.duration = (serial_data.frameDataArray[i+5] << 24) | (serial_data.frameDataArray[i+6] << 16) | (serial_data.frameDataArray[i+7] << 8) | serial_data.frameDataArray[i+8];
                        SYS_PRINT("Relay: %d, Start time: %d, Duration %d\r\n",serial_data.frameDataArray[i], relay_appData.relay_3.startTime, relay_appData.relay_3.duration);
                        i=i+9;
                        break;
                    }
                    case RELAY_4:
                    {
                        relay_appData.relay_4.startTime = (serial_data.frameDataArray[i+1] << 24) | (serial_data.frameDataArray[i+2] << 16) | (serial_data.frameDataArray[i+3] << 8) | serial_data.frameDataArray[i+4];
                        relay_appData.relay_4.duration = (serial_data.frameDataArray[i+5] << 24) | (serial_data.frameDataArray[i+6] << 16) | (serial_data.frameDataArray[i+7] << 8) | serial_data.frameDataArray[i+8];
                        SYS_PRINT("Relay: %d, Start time: %d, Duration %d\r\n",serial_data.frameDataArray[i], relay_appData.relay_4.startTime, relay_appData.relay_4.duration);                    
                        i=i+9;
                        break;
                    }
                    case RELAY_5:
                    {
                        relay_appData.relay_5.startTime = (serial_data.frameDataArray[i+1] << 24) | (serial_data.frameDataArray[i+2] << 16) | (serial_data.frameDataArray[i+3] << 8) | serial_data.frameDataArray[i+4];
                        relay_appData.relay_5.duration = (serial_data.frameDataArray[i+5] << 24) | (serial_data.frameDataArray[i+6] << 16) | (serial_data.frameDataArray[i+7] << 8) | serial_data.frameDataArray[i+8];
                        SYS_PRINT("Relay: %d, Start time: %d, Duration %d\r\n",serial_data.frameDataArray[i], relay_appData.relay_5.startTime, relay_appData.relay_5.duration);                    
                        i=i+9;
                        break;
                    }
                    case RELAY_6:
                    {
                        relay_appData.relay_6.startTime = (serial_data.frameDataArray[i+1] << 24) | (serial_data.frameDataArray[i+2] << 16) | (serial_data.frameDataArray[i+3] << 8) | serial_data.frameDataArray[i+4];
                        relay_appData.relay_6.duration = (serial_data.frameDataArray[i+5] << 24) | (serial_data.frameDataArray[i+6] << 16) | (serial_data.frameDataArray[i+7] << 8) | serial_data.frameDataArray[i+8];
                        SYS_PRINT("Relay: %d, Start time: %d, Duration %d\r\n",serial_data.frameDataArray[i], relay_appData.relay_6.startTime, relay_appData.relay_6.duration);                    
                        i=i+9;
                        break;
                    }
                    case RELAY_7:
                    {
                        relay_appData.relay_7.startTime = (serial_data.frameDataArray[i+1] << 24) | (serial_data.frameDataArray[i+2] << 16) | (serial_data.frameDataArray[i+3] << 8) | serial_data.frameDataArray[i+4];
                        relay_appData.relay_7.duration = (serial_data.frameDataArray[i+5] << 24) | (serial_data.frameDataArray[i+6] << 16) | (serial_data.frameDataArray[i+7] << 8) | serial_data.frameDataArray[i+8];
                        SYS_PRINT("Relay: %d, Start time: %d, Duration %d\r\n",serial_data.frameDataArray[i], relay_appData.relay_7.startTime, relay_appData.relay_7.duration);                    
                        i=i+9;
                        break;
                    }
                    case RELAY_8:
                    {
                        relay_appData.relay_8.startTime = (serial_data.frameDataArray[i+1] << 24) | (serial_data.frameDataArray[i+2] << 16) | (serial_data.frameDataArray[i+3] << 8) | serial_data.frameDataArray[i+4];
                        relay_appData.relay_8.duration = (serial_data.frameDataArray[i+5] << 24) | (serial_data.frameDataArray[i+6] << 16) | (serial_data.frameDataArray[i+7] << 8) | serial_data.frameDataArray[i+8];
                        SYS_PRINT("Relay: %d, Start time: %d, Duration %d\r\n",serial_data.frameDataArray[i], relay_appData.relay_8.startTime, relay_appData.relay_8.duration);                    
                        i=i+9;
                        break;
                    }
                    default:
                    {
                        i=serialResponse_getFrameDataLength();
                        break;
                    }
                }
                
                
            }
            break;

//            switch(serial_data.frameDataArray[i])
//            {
//                case RELAY_1:
//                {
//                    relay_appData.relay_1.startTime = (serial_data.frameDataArray[i+1] << 24) | (serial_data.frameDataArray[i+2] << 16) | (serial_data.frameDataArray[i+3] << 8) | serial_data.frameDataArray[4];
//                    relay_appData.relay_1.duration = (serial_data.frameDataArray[i+5] << 24) | (serial_data.frameDataArray[i+6] << 16) | (serial_data.frameDataArray[i+7] << 8) | serial_data.frameDataArray[8];
//                    SYS_PRINT("Relay: %d, Start time: %d, Duration %d\r\n",serial_data.frameDataArray[i], relay_appData.relay_1.startTime, relay_appData.relay_1.duration);
//                    i=i+8;
//                    break;
//                }
//                case RELAY_2:
//                {
//                    relay_appData.relay_2.startTime = (serial_data.frameDataArray[i+1] << 24) | (serial_data.frameDataArray[i+2] << 16) | (serial_data.frameDataArray[i+3] << 8) | serial_data.frameDataArray[4];
//                    relay_appData.relay_2.duration = (serial_data.frameDataArray[i+5] << 24) | (serial_data.frameDataArray[i+6] << 16) | (serial_data.frameDataArray[i+7] << 8) | serial_data.frameDataArray[8];
//                    SYS_PRINT("Relay: %d, Start time: %d, Duration %d\r\n",serial_data.frameDataArray[i], relay_appData.relay_2.startTime, relay_appData.relay_2.duration);
//                    i=i+8;
//                    break;
//                }
//                case RELAY_3:
//                {
//                    relay_appData.relay_3.startTime = (serial_data.frameDataArray[i+1] << 24) | (serial_data.frameDataArray[i+2] << 16) | (serial_data.frameDataArray[i+3] << 8) | serial_data.frameDataArray[4];
//                    relay_appData.relay_3.duration = (serial_data.frameDataArray[i+5] << 24) | (serial_data.frameDataArray[i+6] << 16) | (serial_data.frameDataArray[i+7] << 8) | serial_data.frameDataArray[8];
//                    SYS_PRINT("Relay: %d, Start time: %d, Duration %d\r\n",serial_data.frameDataArray[i], relay_appData.relay_3.startTime, relay_appData.relay_3.duration);
//                    i=i+8;
//                    break;
//                }
//                case RELAY_4:
//                {
//                    relay_appData.relay_4.startTime = (serial_data.frameDataArray[i+1] << 24) | (serial_data.frameDataArray[i+2] << 16) | (serial_data.frameDataArray[i+3] << 8) | serial_data.frameDataArray[4];
//                    relay_appData.relay_4.duration = (serial_data.frameDataArray[i+5] << 24) | (serial_data.frameDataArray[i+6] << 16) | (serial_data.frameDataArray[i+7] << 8) | serial_data.frameDataArray[8];
//                    SYS_PRINT("Relay: %d, Start time: %d, Duration %d\r\n",serial_data.frameDataArray[i], relay_appData.relay_4.startTime, relay_appData.relay_4.duration);
//                    i=i+8;
//                    break;
//                }
//                default:
//                {
//                    break;
//                }
            
//            switch(serial_data.frameDataArray[0])
//            {
//                case RELAY_1:
//                {
//                    relay_appData.relay_1.startTime = (serial_data.frameDataArray[1] << 24) | (serial_data.frameDataArray[2] << 16) | (serial_data.frameDataArray[3] << 8) | serial_data.frameDataArray[4];
//                    relay_appData.relay_1.duration = (serial_data.frameDataArray[5] << 24) | (serial_data.frameDataArray[6] << 16) | (serial_data.frameDataArray[7] << 8) | serial_data.frameDataArray[8];
//                    SYS_PRINT("Start time: %d, Duration %d\r\n",relay_appData.relay_1.startTime, relay_appData.relay_1.duration);
//                    break;
//                }
//                
//                default:
//                {
//                    break;
//                }
//            }
            //if(serial_data.frameDataArray[0]== START)
        }


        default:
        {
            break;
        } 
    }
    
}

//void sendByte(uint8_t b, bool escape)
//{
//    if (escape && (b == START_BYTE || b == ESCAPE || b == XON || b == XOFF)) {
//    UART1_Write(ESCAPE);
//    UART1_Write(b ^ 0x20);
//	} else {
//		UART1_Write(b);
//    }      
//
//}
// *****************************************************************************
//void sendByte(uint8_t b, bool escape){
//    if (escape && (b == START_BYTE || b == ESCAPE || b == XON || b == XOFF)) {
//        DRV_USART_Write(uart_appData.handleUSART1, &ESCAPE, 1);
//        usartRWTxData[0]
//        DRV_USART_Write(uart_appData.handleUSART1, b ^ 0x20, 1);
//	} else {
//		DRV_USART_Write(uart_appData.handleUSART1, &b, 1);
//    }      
//}

static void USART_Task (void)
{
    switch (usartRWState)
    {
        default:
        case USART_RW_INIT:
        {
            uart_appData.usartReadWriteTxIndex = 0;
            uart_appData.usartReadWriteRxIndex = 0;
            usartRWState = USART_RW_WORKING;
            break;
        }

        case USART_RW_WORKING:
        {
//            if(uart_appData.usartReadWriteTxIndex < sizeof(usartRWTxData))
//            {
//                /* Transmit the bytes from the buffer */
//                uart_appData.usartReadWriteTxIndex += 
//                    DRV_USART_Write(uart_appData.handleUSART1,
//                    &usartRWTxData[uart_appData.usartReadWriteTxIndex],
//                    sizeof(usartRWTxData) - uart_appData.usartReadWriteTxIndex);
//            }
//
//            if (uart_appData.usartReadWriteRxIndex < UART_APP_DRV_USART_RW_RX_SIZE)
//            {
//                /* Read bytes into the buffer */
//                uart_appData.usartReadWriteRxIndex += 
//                    DRV_USART_Read(uart_appData.handleUSART1,
//                    &uart_appData.usartRWRxData[uart_appData.usartReadWriteRxIndex],
//                    UART_APP_DRV_USART_RW_RX_SIZE - uart_appData.usartReadWriteRxIndex);
//                SYS_PRINT("Index %d\r\n",uart_appData.usartReadWriteRxIndex);
//            }
//
//            /* have we finished? */
//            if (uart_appData.usartReadWriteTxIndex == sizeof(usartRWTxData) && (uart_appData.usartReadWriteRxIndex ==  UART_APP_DRV_USART_RW_RX_SIZE))
//            {
//                usartRWState = USART_RW_DONE;
//                SYS_PRINT("Some bytes %d %x\r\n", uart_appData.usartRWRxData[0], uart_appData.usartRWRxData[1]);
//            }
            
            serial_readPacket();
//            if( DRV_USART_Read(uart_appData.handleUSART1, &uart_appData.usartRWRxData[0],1) == 1)
//            {
//                SYS_PRINT("FOUND ONE BYTE\r\n");
//                char b = uart_appData.usartRWRxData[0];
//                
//                if( b == START_BYTE )
//                {
//                    SYS_PRINT("START BYTE YEAH\r\n");
//                }
//            }
            break;
        }
        
        case USART_RW_DONE:
        {
            SYS_PRINT("Frame Length %d\r\n",serialResponse_getFrameDataLength() + 1 );
            serialDataUnpack();
            usartRWState = USART_RW_WORKING;
            break;
        }
    }
}

/* TODO:  Add any necessary local functions.
*/


// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

/*******************************************************************************
  Function:
    void UART_APP_Initialize ( void )

  Remarks:
    See prototype in uart_app.h.
 */

void UART_APP_Initialize ( void )
{
    /* Place the App state machine in its initial state. */
    uart_appData.state = UART_APP_STATE_INIT;

    uart_appData.handleUSART1 = DRV_HANDLE_INVALID;

    /* initialize the USART state machine */
    usartRWState = USART_RW_INIT;
    
    /* TODO: Initialize your application's state machine and other
     * parameters.
     */
}


/******************************************************************************
  Function:
    void UART_APP_Tasks ( void )

  Remarks:
    See prototype in uart_app.h.
 */

void UART_APP_Tasks ( void )
{

    /* Check the application's current state. */
    switch ( uart_appData.state )
    {
        /* Application's initial state. */
        case UART_APP_STATE_INIT:
        {
            bool appInitialized = true;
       
            if (uart_appData.handleUSART1 == DRV_HANDLE_INVALID)
            {
                uart_appData.handleUSART1 = DRV_USART_Open(UART_APP_DRV_USART, DRV_IO_INTENT_READWRITE|DRV_IO_INTENT_NONBLOCKING);
                appInitialized &= (DRV_HANDLE_INVALID != uart_appData.handleUSART1);
            }

        
            if (appInitialized)
            {
            
                uart_appData.state = UART_APP_STATE_SERVICE_TASKS;
            }
            break;
        }

        case UART_APP_STATE_SERVICE_TASKS:
        {
            USART_Task();
//            uint32_t u16;
//            char a[sizeof(uint32_t)];
//            a[0] = 0;
//            a[1] = 0;
//            u16 = 200000;
//            memcpy(a, &u16, sizeof(uint32_t));
//            SYS_PRINT("Bytes MSB %X %X %X %X LSB\r\n",a[3],a[2],a[1],a[0]);
        
            break;
        }

        /* TODO: implement your application state machine.*/
        

        /* The default state should never be executed. */
        default:
        {
            /* TODO: Handle error in application's state machine. */
            break;
        }
    }
}

 

/*******************************************************************************
 End of File
 */
