/********************************************************************************
** Form generated from reading UI file 'popupwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_POPUPWINDOW_H
#define UI_POPUPWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_popup_window
{
public:
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *circleBlue;
    QLabel *label_circleBlue;
    QHBoxLayout *horizontalLayout_2;
    QLabel *circleGrey;
    QLabel *label_circleGrey;
    QHBoxLayout *horizontalLayout_3;
    QLabel *circleOrange;
    QLabel *label_circleOrange;
    QHBoxLayout *horizontalLayout_4;
    QLabel *circleGreen;
    QLabel *label_circleGreen;
    QHBoxLayout *horizontalLayout_5;
    QLabel *circleRed;
    QLabel *label_circleRed;
    QSpacerItem *horizontalSpacer;

    void setupUi(QWidget *popup_window)
    {
        if (popup_window->objectName().isEmpty())
            popup_window->setObjectName(QStringLiteral("popup_window"));
        popup_window->resize(800, 600);
        QPalette palette;
        QBrush brush(QColor(25, 46, 71, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(226, 234, 249, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        QBrush brush2(QColor(128, 186, 219, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Light, brush2);
        QBrush brush3(QColor(106, 155, 182, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        QBrush brush4(QColor(42, 62, 73, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush4);
        QBrush brush5(QColor(56, 82, 97, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        QBrush brush6(QColor(255, 255, 255, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush6);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        QBrush brush7(QColor(0, 0, 0, 255));
        brush7.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush7);
        QBrush brush8(QColor(170, 189, 200, 255));
        brush8.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush8);
        QBrush brush9(QColor(255, 255, 220, 255));
        brush9.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush9);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush7);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush7);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush8);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush9);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush7);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        QBrush brush10(QColor(85, 124, 146, 255));
        brush10.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush10);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush7);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush10);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush9);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush7);
        popup_window->setPalette(palette);
        groupBox_2 = new QGroupBox(popup_window);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setGeometry(QRect(60, 80, 341, 265));
        QFont font;
        font.setFamily(QStringLiteral("Titillium Web"));
        font.setPointSize(12);
        groupBox_2->setFont(font);
        groupBox_2->setAlignment(Qt::AlignCenter);
        groupBox_2->setFlat(false);
        gridLayout = new QGridLayout(groupBox_2);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        groupBox = new QGroupBox(groupBox_2);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::Base, brush6);
        QBrush brush11(QColor(194, 201, 215, 255));
        brush11.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush11);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush6);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush11);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush11);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush11);
        groupBox->setPalette(palette1);
        QFont font1;
        font1.setFamily(QStringLiteral("Titillium Web"));
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setWeight(75);
        groupBox->setFont(font1);
        groupBox->setAlignment(Qt::AlignCenter);
        groupBox->setFlat(false);
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        circleBlue = new QLabel(groupBox);
        circleBlue->setObjectName(QStringLiteral("circleBlue"));
        circleBlue->setMaximumSize(QSize(26, 26));
        circleBlue->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleBlue->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout->addWidget(circleBlue);

        label_circleBlue = new QLabel(groupBox);
        label_circleBlue->setObjectName(QStringLiteral("label_circleBlue"));
        QFont font2;
        font2.setFamily(QStringLiteral("Titillium Web"));
        font2.setPointSize(12);
        font2.setBold(false);
        font2.setWeight(50);
        label_circleBlue->setFont(font2);
        label_circleBlue->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_circleBlue->setMargin(3);

        horizontalLayout->addWidget(label_circleBlue);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        circleGrey = new QLabel(groupBox);
        circleGrey->setObjectName(QStringLiteral("circleGrey"));
        circleGrey->setMaximumSize(QSize(26, 26));
        circleGrey->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-01.png")));
        circleGrey->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_2->addWidget(circleGrey);

        label_circleGrey = new QLabel(groupBox);
        label_circleGrey->setObjectName(QStringLiteral("label_circleGrey"));
        label_circleGrey->setFont(font2);
        label_circleGrey->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_circleGrey->setMargin(3);

        horizontalLayout_2->addWidget(label_circleGrey);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        circleOrange = new QLabel(groupBox);
        circleOrange->setObjectName(QStringLiteral("circleOrange"));
        circleOrange->setMaximumSize(QSize(26, 26));
        circleOrange->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-06.png")));
        circleOrange->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_3->addWidget(circleOrange);

        label_circleOrange = new QLabel(groupBox);
        label_circleOrange->setObjectName(QStringLiteral("label_circleOrange"));
        label_circleOrange->setFont(font2);
        label_circleOrange->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_circleOrange->setMargin(3);

        horizontalLayout_3->addWidget(label_circleOrange);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        circleGreen = new QLabel(groupBox);
        circleGreen->setObjectName(QStringLiteral("circleGreen"));
        circleGreen->setMaximumSize(QSize(26, 26));
        circleGreen->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-03.png")));
        circleGreen->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_4->addWidget(circleGreen);

        label_circleGreen = new QLabel(groupBox);
        label_circleGreen->setObjectName(QStringLiteral("label_circleGreen"));
        label_circleGreen->setFont(font2);
        label_circleGreen->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_circleGreen->setMargin(3);

        horizontalLayout_4->addWidget(label_circleGreen);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        circleRed = new QLabel(groupBox);
        circleRed->setObjectName(QStringLiteral("circleRed"));
        circleRed->setMaximumSize(QSize(26, 26));
        circleRed->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-05.png")));
        circleRed->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_5->addWidget(circleRed);

        label_circleRed = new QLabel(groupBox);
        label_circleRed->setObjectName(QStringLiteral("label_circleRed"));
        label_circleRed->setFont(font2);
        label_circleRed->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_circleRed->setMargin(3);

        horizontalLayout_5->addWidget(label_circleRed);


        verticalLayout->addLayout(horizontalLayout_5);


        gridLayout->addWidget(groupBox, 0, 0, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 0, 1, 1, 1);


        retranslateUi(popup_window);

        QMetaObject::connectSlotsByName(popup_window);
    } // setupUi

    void retranslateUi(QWidget *popup_window)
    {
        popup_window->setWindowTitle(QApplication::translate("popup_window", "Form", nullptr));
        groupBox_2->setTitle(QApplication::translate("popup_window", "Feedback Elements", nullptr));
        groupBox->setTitle(QApplication::translate("popup_window", "Legend", nullptr));
        circleBlue->setText(QString());
        label_circleBlue->setText(QApplication::translate("popup_window", "Disarmed", nullptr));
        circleGrey->setText(QString());
        label_circleGrey->setText(QApplication::translate("popup_window", "Before latch", nullptr));
        circleOrange->setText(QString());
        label_circleOrange->setText(QApplication::translate("popup_window", "Latched", nullptr));
        circleGreen->setText(QString());
        label_circleGreen->setText(QApplication::translate("popup_window", "After latch", nullptr));
        circleRed->setText(QString());
        label_circleRed->setText(QApplication::translate("popup_window", "Error", nullptr));
    } // retranslateUi

};

namespace Ui {
    class popup_window: public Ui_popup_window {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_POPUPWINDOW_H
