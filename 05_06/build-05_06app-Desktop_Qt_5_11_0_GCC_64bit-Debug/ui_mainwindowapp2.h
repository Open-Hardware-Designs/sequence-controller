/********************************************************************************
** Form generated from reading UI file 'mainwindowapp2.ui'
**
** Created by: Qt User Interface Compiler version 5.11.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOWAPP2_H
#define UI_MAINWINDOWAPP2_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindowapp2
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout_13;
    QTabWidget *tabWidget_2;
    QWidget *tab_11;
    QGridLayout *gridLayout_70;
    QGridLayout *gridLayout_9;
    QHBoxLayout *horizontalLayout_35;
    QSpacerItem *horizontalSpacer_265;
    QLabel *label_ID_19;
    QSpacerItem *horizontalSpacer_266;
    QLabel *label_start_19;
    QLabel *label_msStart_20;
    QSpacerItem *horizontalSpacer_17;
    QLabel *label_duration_23;
    QLabel *label_msDuration_36;
    QSpacerItem *horizontalSpacer_267;
    QSpacerItem *horizontalSpacer_268;
    QSpacerItem *horizontalSpacer_18;
    QGridLayout *gridLayout_71;
    QSpacerItem *horizontalSpacer_19;
    QLabel *label_duration_24;
    QHBoxLayout *horizontalLayout_36;
    QLineEdit *textFIeld_name1_38;
    QSpacerItem *verticalSpacer_80;
    QSpacerItem *horizontalSpacer_20;
    QSpacerItem *verticalSpacer_81;
    QLabel *label_msDuration_37;
    QSpacerItem *verticalSpacer_82;
    QSpacerItem *verticalSpacer_83;
    QLabel *label_duration_25;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *horizontalSpacer_21;
    QLabel *label_63;
    QProgressBar *progressBar_28;
    QFrame *line_18;
    QScrollArea *scrollArea_14;
    QWidget *scrollAreaWidgetContents_18;
    QGridLayout *gridLayout_72;
    QGridLayout *gridLayout_73;
    QCheckBox *checkBox_312;
    QProgressBar *progressBar_33;
    QLineEdit *textFIeld_name1_48;
    QProgressBar *progressBar_29;
    QLineEdit *textFIeld_name1_42;
    QSpacerItem *horizontalSpacer_274;
    QLabel *label_ID3_33;
    QLabel *label_ID4_230;
    QLabel *label_ID2_33;
    QSpacerItem *horizontalSpacer_272;
    QLabel *circle_inactive3_116;
    QSpacerItem *horizontalSpacer_269;
    QSpacerItem *horizontalSpacer_273;
    QLabel *circle_inactive3_119;
    QLabel *circle_inactive3_117;
    QLabel *label_ID4_231;
    QProgressBar *progressBar_31;
    QLabel *label_ID1_18;
    QLineEdit *textFIeld_name1_40;
    QSpacerItem *horizontalSpacer_271;
    QLineEdit *textFIeld_name1_39;
    QLineEdit *textFIeld_name1_46;
    QCheckBox *checkBox_311;
    QLabel *circle_inactive3_120;
    QProgressBar *progressBar_30;
    QLineEdit *textFIeld_name1_43;
    QCheckBox *checkBox_315;
    QCheckBox *checkBox_313;
    QLabel *circle_inactive3_118;
    QCheckBox *checkBox_314;
    QSpacerItem *horizontalSpacer_270;
    QLineEdit *textFIeld_name1_41;
    QSpacerItem *horizontalSpacer_22;
    QLineEdit *textFIeld_name1_45;
    QSpacerItem *horizontalSpacer_23;
    QLineEdit *textFIeld_name1_44;
    QLineEdit *textFIeld_name1_47;
    QProgressBar *progressBar_32;
    QWidget *Node_1;
    QGridLayout *gridLayout_15;
    QGridLayout *Layout_leftSide;
    QHBoxLayout *Layout_TitleBar;
    QSpacerItem *horizontalSpacer_4;
    QLabel *label_ID_node1;
    QSpacerItem *horizontalSpacer_3;
    QLabel *label_name_node1;
    QSpacerItem *horizontalSpacer_278;
    QSpacerItem *horizontalSpacer_279;
    QLabel *label_start_node1;
    QLabel *label_msStart_node1;
    QSpacerItem *horizontalSpacer_280;
    QLabel *label_duration_node1;
    QLabel *label_msDuration_node1;
    QSpacerItem *horizontalSpacer_281;
    QScrollArea *scrollArea_node1;
    QWidget *scrollAreaWidgetContents_19;
    QGridLayout *gridLayout_75;
    QGridLayout *gridLayout_76;
    QSpinBox *spinBox_start5;
    QCheckBox *checkBox10;
    QSpacerItem *horizontalSpacer_290;
    QCheckBox *checkBox22;
    QSpinBox *spinBox_start3;
    QLineEdit *textField_name14;
    QCheckBox *checkBox18;
    QLabel *label_4;
    QCheckBox *checkBox12;
    QLabel *label_19;
    QLineEdit *textField_name5;
    QCheckBox *checkBox1;
    QLabel *label_15;
    QLineEdit *textField_name16;
    QCheckBox *checkBox14;
    QSpinBox *spinBox_duration10;
    QSpinBox *spinBox_start21;
    QLabel *circleInactive13;
    QCheckBox *checkBox11;
    QLabel *label_14;
    QCheckBox *checkBox5;
    QCheckBox *checkBox4;
    QSpinBox *spinBox_duration11;
    QLabel *label_16;
    QLabel *circleInactive3;
    QLabel *label_6;
    QSpinBox *spinBox_start11;
    QLabel *circleInactive1;
    QLabel *label_7;
    QSpinBox *spinBox_duration6;
    QLineEdit *textField_name8;
    QLineEdit *textField_name2;
    QSpinBox *spinBox_start8;
    QSpinBox *spinBox_duration15;
    QLineEdit *textField_name24;
    QLineEdit *textField_name7;
    QSpinBox *spinBox_start23;
    QLabel *label_18;
    QSpinBox *spinBox_start19;
    QSpinBox *spinBox_duration12;
    QLabel *circleInactive17;
    QLineEdit *textField_name18;
    QCheckBox *checkBox8;
    QSpinBox *spinBox_start13;
    QCheckBox *checkBox21;
    QCheckBox *checkBox2;
    QLabel *label_21;
    QSpinBox *spinBox_duration8;
    QLineEdit *textField_name21;
    QLabel *label_5;
    QLabel *label_23;
    QCheckBox *checkBox20;
    QSpinBox *spinBox_duration19;
    QSpinBox *spinBox_duration16;
    QSpinBox *spinBox_start17;
    QLabel *circleInactive12;
    QLabel *circleInactive15;
    QSpinBox *spinBox_start4;
    QLabel *circleInactive14;
    QSpinBox *spinBox_start20;
    QLabel *label_9;
    QCheckBox *checkBox16;
    QLabel *label_20;
    QLineEdit *textField_name22;
    QLabel *circleInactive21;
    QSpinBox *spinBox_duration3;
    QSpinBox *spinBox_start7;
    QLineEdit *textField_name13;
    QLabel *label_24;
    QCheckBox *checkBox19;
    QLabel *label_8;
    QLabel *circleInactive23;
    QLabel *circleInactive7;
    QSpinBox *spinBox_duration14;
    QLineEdit *textField_name19;
    QLabel *circleInactive16;
    QCheckBox *checkBox17;
    QSpinBox *spinBox_start9;
    QLabel *label_11;
    QCheckBox *checkBox3;
    QSpinBox *spinBox_start16;
    QSpinBox *spinBox_duration5;
    QLabel *circleInactive22;
    QLineEdit *textField_name23;
    QLineEdit *textField_name20;
    QSpinBox *spinBox_duration1;
    QSpinBox *spinBox_duration24;
    QSpinBox *spinBox_duration20;
    QLabel *circleInactive10;
    QSpinBox *spinBox_duration23;
    QLabel *label_10;
    QLabel *circleInactive8;
    QLabel *circleInactive2;
    QLineEdit *textField_name12;
    QLineEdit *textField_name3;
    QSpinBox *spinBox_duration9;
    QSpacerItem *horizontalSpacer_289;
    QLineEdit *textField_name1;
    QSpinBox *spinBox_start2;
    QCheckBox *checkBox15;
    QCheckBox *checkBox24;
    QLineEdit *textField_name15;
    QSpinBox *spinBox_duration18;
    QSpacerItem *horizontalSpacer_287;
    QSpinBox *spinBox_duration22;
    QLabel *circleInactive11;
    QSpinBox *spinBox_start24;
    QSpinBox *spinBox_start14;
    QSpinBox *spinBox_start6;
    QLabel *label_3;
    QSpacerItem *horizontalSpacer_288;
    QCheckBox *checkBox7;
    QSpinBox *spinBox_start10;
    QSpinBox *spinBox_duration4;
    QSpinBox *spinBox_start18;
    QSpacerItem *horizontalSpacer_286;
    QLabel *circleInactive9;
    QCheckBox *checkBox13;
    QSpinBox *spinBox_duration7;
    QLabel *label_12;
    QLabel *label_13;
    QLabel *label_2;
    QLabel *circleInactive6;
    QLineEdit *textField_name10;
    QLabel *circleInactive20;
    QLabel *circleInactive4;
    QLabel *circleInactive19;
    QSpinBox *spinBox_start22;
    QSpinBox *spinBox_start12;
    QSpinBox *spinBox_duration2;
    QSpinBox *spinBox_duration17;
    QLabel *circleInactive5;
    QSpinBox *spinBox_duration21;
    QCheckBox *checkBox9;
    QSpinBox *spinBox_duration13;
    QLineEdit *textField_name17;
    QSpinBox *spinBox_start15;
    QCheckBox *checkBox6;
    QLineEdit *textField_name9;
    QLineEdit *textField_name6;
    QSpinBox *spinBox_start1;
    QLabel *label_1;
    QLineEdit *textField_name11;
    QLineEdit *textField_name4;
    QLabel *circleInactive18;
    QLabel *label_17;
    QLabel *label_22;
    QCheckBox *checkBox23;
    QLabel *circleInactive24;
    QGridLayout *Layout_TopRight;
    QSpacerItem *verticalSpacer_4;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer_11;
    QPushButton *pushButton_Help;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *armButton;
    QSpacerItem *verticalSpacer_7;
    QFrame *line_2;
    QGridLayout *Layout_PreviewNodeButtons;
    QGridLayout *Layout_Preview;
    QGroupBox *groupBox_preview;
    QGridLayout *gridLayout_8;
    QHBoxLayout *horizontalLayout;
    QLabel *label_80;
    QSpinBox *spinBox_SlowMo;
    QPushButton *previewButton;
    QGridLayout *Layout_Node;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_6;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *writeConfigButton;
    QSpacerItem *horizontalSpacer_8;
    QPushButton *readConfigButton;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *startButton;
    QSpacerItem *horizontalSpacer_9;
    QPushButton *stopResetButton;
    QGridLayout *Layout_bottomRight;
    QSpacerItem *horizontalSpacer;
    QGridLayout *gridLayout_4;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *openButton;
    QPushButton *saveButton;
    QFrame *frame;
    QGridLayout *gridLayout_14;
    QLabel *logoPlaceholder;
    QSpacerItem *horizontalSpacer_10;
    QGridLayout *gridLayout_10;
    QSpacerItem *verticalSpacer_2;
    QSpacerItem *verticalSpacer_6;
    QVBoxLayout *verticalLayout;
    QLabel *label_67;
    QSpinBox *spinBox_cycleCounter;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_66;
    QSpinBox *spinBox_Pause;
    QFrame *line;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_81;
    QHBoxLayout *horizontalLayout_38;
    QProgressBar *progressBar_node1;
    QLabel *label_cycleCounter;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_CycleDuration;
    QLabel *label_TotalDuration;
    QSpacerItem *verticalSpacer;
    QSpacerItem *verticalSpacer_5;
    QWidget *tab_12;
    QGridLayout *gridLayout_23;
    QGridLayout *gridLayout_12;
    QHBoxLayout *horizontalLayout_13;
    QSpacerItem *horizontalSpacer_59;
    QSpacerItem *horizontalSpacer_60;
    QLabel *label_ID_8;
    QSpacerItem *horizontalSpacer_95;
    QLabel *label_name_9;
    QSpacerItem *horizontalSpacer_96;
    QSpacerItem *horizontalSpacer_97;
    QLabel *label_start_8;
    QLabel *label_msStart_9;
    QSpacerItem *horizontalSpacer_98;
    QLabel *label_duration_8;
    QLabel *label_msDuration_14;
    QSpacerItem *horizontalSpacer_99;
    QGridLayout *gridLayout_24;
    QHBoxLayout *horizontalLayout_14;
    QProgressBar *progressBar_7;
    QLabel *label_25;
    QSpinBox *spinBox_start5_91;
    QSpinBox *spinBox_start5_92;
    QPushButton *pushButton_27;
    QSpacerItem *horizontalSpacer_100;
    QSpacerItem *verticalSpacer_25;
    QPushButton *pushButton_28;
    QLabel *label_26;
    QLabel *label_27;
    QFrame *line_7;
    QSpacerItem *horizontalSpacer_101;
    QSpacerItem *verticalSpacer_8;
    QPushButton *pushButton_29;
    QLabel *label_msDuration_15;
    QSpacerItem *verticalSpacer_26;
    QPushButton *pushButton_30;
    QSpacerItem *verticalSpacer_27;
    QLabel *label_28;
    QSpacerItem *verticalSpacer_89;
    QScrollArea *scrollArea_16;
    QWidget *scrollAreaWidgetContents_7;
    QGridLayout *gridLayout_25;
    QGridLayout *gridLayout_77;
    QLineEdit *textFIeld_name5_79;
    QSpinBox *spinBox_start2_13;
    QLabel *label_ID4_91;
    QLabel *circle_inactive3_43;
    QSpacerItem *horizontalSpacer_102;
    QCheckBox *checkBox_121;
    QCheckBox *checkBox_122;
    QCheckBox *checkBox_123;
    QSpinBox *spinBox_start1_7;
    QCheckBox *checkBox_124;
    QLabel *circle_inactive3_44;
    QLineEdit *textFIeld_name1_7;
    QSpinBox *spinBox_duration2_13;
    QSpinBox *spinBox_duration4_13;
    QLineEdit *textFIeld_name4_13;
    QSpinBox *spinBox_duration5_79;
    QCheckBox *checkBox_125;
    QLineEdit *textFIeld_name2_13;
    QSpinBox *spinBox_duration5_80;
    QLineEdit *textFIeld_name5_80;
    QSpinBox *spinBox_start5_93;
    QLabel *label_ID4_92;
    QSpinBox *spinBox_start3_13;
    QLabel *label_ID4_93;
    QSpinBox *spinBox_duration4_14;
    QCheckBox *checkBox_126;
    QSpinBox *spinBox_start5_94;
    QSpinBox *spinBox_duration5_81;
    QLineEdit *textFIeld_name5_81;
    QLabel *label_ID4_94;
    QSpinBox *spinBox_start5_95;
    QLineEdit *textFIeld_name4_14;
    QSpinBox *spinBox_start3_14;
    QSpinBox *spinBox_start5_96;
    QSpacerItem *horizontalSpacer_103;
    QSpacerItem *horizontalSpacer_104;
    QSpinBox *spinBox_duration5_82;
    QLineEdit *textFIeld_name5_82;
    QLabel *label_ID2_13;
    QCheckBox *checkBox_127;
    QSpinBox *spinBox_start5_97;
    QLineEdit *textFIeld_name2_14;
    QLineEdit *textFIeld_name5_83;
    QCheckBox *checkBox_128;
    QLabel *label_ID4_95;
    QLabel *circle_inactive3_45;
    QLineEdit *textFIeld_name3_13;
    QSpinBox *spinBox_duration5_83;
    QLineEdit *textFIeld_name3_14;
    QSpinBox *spinBox_duration5_84;
    QCheckBox *checkBox_129;
    QLineEdit *textFIeld_name5_84;
    QLineEdit *textFIeld_name5_85;
    QSpinBox *spinBox_duration5_85;
    QLabel *label_ID4_96;
    QSpacerItem *horizontalSpacer_105;
    QLabel *label_ID4_97;
    QLineEdit *textFIeld_name5_86;
    QSpinBox *spinBox_start2_14;
    QSpinBox *spinBox_duration5_86;
    QLabel *label_ID4_98;
    QSpinBox *spinBox_duration5_87;
    QSpinBox *spinBox_duration5_88;
    QSpinBox *spinBox_start5_98;
    QLineEdit *textFIeld_name5_87;
    QSpinBox *spinBox_start5_99;
    QCheckBox *checkBox_130;
    QLabel *label_ID4_99;
    QLabel *label_ID4_100;
    QSpinBox *spinBox_duration3_13;
    QLabel *label_ID2_14;
    QLabel *label_ID4_101;
    QCheckBox *checkBox_131;
    QLabel *circle_inactive3_46;
    QLabel *label_ID4_102;
    QLabel *label_ID4_103;
    QSpinBox *spinBox_start4_13;
    QLineEdit *textFIeld_name5_88;
    QSpinBox *spinBox_start5_100;
    QSpinBox *spinBox_duration5_89;
    QLabel *label_ID3_13;
    QSpinBox *spinBox_duration1_7;
    QSpinBox *spinBox_duration5_90;
    QSpinBox *spinBox_start5_101;
    QSpinBox *spinBox_start4_14;
    QSpinBox *spinBox_duration2_14;
    QLabel *circle_inactive1_79;
    QCheckBox *checkBox_132;
    QSpinBox *spinBox_duration3_14;
    QCheckBox *checkBox_133;
    QCheckBox *checkBox_134;
    QCheckBox *checkBox_135;
    QLineEdit *textFIeld_name5_89;
    QCheckBox *checkBox_136;
    QSpinBox *spinBox_start5_102;
    QLabel *label_ID3_14;
    QLabel *label_ID1_7;
    QLineEdit *textFIeld_name5_90;
    QSpinBox *spinBox_duration5_91;
    QSpacerItem *horizontalSpacer_106;
    QLabel *label_ID4_104;
    QCheckBox *checkBox_137;
    QCheckBox *checkBox_138;
    QCheckBox *checkBox_139;
    QSpinBox *spinBox_start5_103;
    QCheckBox *checkBox_140;
    QSpinBox *spinBox_start5_104;
    QLabel *label_ID4_105;
    QLabel *circle_inactive3_47;
    QSpinBox *spinBox_start5_105;
    QLabel *circle_inactive1_80;
    QLineEdit *textFIeld_name5_91;
    QLabel *circle_inactive3_48;
    QSpacerItem *horizontalSpacer_107;
    QLabel *circle_inactive3_49;
    QLabel *circle_inactive1_81;
    QLabel *circle_inactive1_82;
    QLabel *circle_inactive1_83;
    QLabel *circle_inactive1_84;
    QLabel *circle_inactive1_85;
    QLabel *circle_inactive1_86;
    QLabel *circle_inactive1_87;
    QLabel *circle_inactive1_88;
    QLabel *circle_inactive1_89;
    QLabel *circle_inactive1_90;
    QLabel *circle_inactive1_91;
    QSpacerItem *horizontalSpacer_108;
    QSpacerItem *horizontalSpacer_109;
    QWidget *tab_13;
    QGridLayout *gridLayout_78;
    QGridLayout *gridLayout_79;
    QHBoxLayout *horizontalLayout_39;
    QSpacerItem *horizontalSpacer_291;
    QSpacerItem *horizontalSpacer_292;
    QLabel *label_ID_21;
    QSpacerItem *horizontalSpacer_293;
    QLabel *label_name_19;
    QSpacerItem *horizontalSpacer_294;
    QSpacerItem *horizontalSpacer_295;
    QLabel *label_start_21;
    QLabel *label_msStart_22;
    QSpacerItem *horizontalSpacer_296;
    QLabel *label_duration_27;
    QLabel *label_msDuration_40;
    QSpacerItem *horizontalSpacer_297;
    QGridLayout *gridLayout_80;
    QSpacerItem *horizontalSpacer_298;
    QSpacerItem *verticalSpacer_90;
    QHBoxLayout *horizontalLayout_40;
    QProgressBar *progressBar_35;
    QLabel *label_68;
    QLabel *label_69;
    QLabel *label_70;
    QSpacerItem *verticalSpacer_91;
    QSpinBox *spinBox_start5_241;
    QSpacerItem *verticalSpacer_92;
    QLabel *label_71;
    QPushButton *pushButton_67;
    QPushButton *pushButton_68;
    QSpacerItem *horizontalSpacer_299;
    QSpinBox *spinBox_start5_242;
    QPushButton *pushButton_69;
    QSpacerItem *verticalSpacer_93;
    QFrame *line_20;
    QPushButton *pushButton_70;
    QLabel *label_msDuration_41;
    QSpacerItem *verticalSpacer_94;
    QScrollArea *scrollArea_17;
    QWidget *scrollAreaWidgetContents_20;
    QGridLayout *gridLayout_81;
    QGridLayout *gridLayout_82;
    QLineEdit *textFIeld_name5_209;
    QSpinBox *spinBox_start2_33;
    QLabel *label_ID4_247;
    QLabel *circle_inactive3_128;
    QSpacerItem *horizontalSpacer_300;
    QCheckBox *checkBox_336;
    QCheckBox *checkBox_337;
    QCheckBox *checkBox_338;
    QSpinBox *spinBox_start1_17;
    QCheckBox *checkBox_339;
    QLabel *circle_inactive3_129;
    QLineEdit *textFIeld_name1_50;
    QSpinBox *spinBox_duration2_33;
    QSpinBox *spinBox_duration4_33;
    QLineEdit *textFIeld_name4_33;
    QSpinBox *spinBox_duration5_209;
    QCheckBox *checkBox_340;
    QLineEdit *textFIeld_name2_33;
    QSpinBox *spinBox_duration5_210;
    QLineEdit *textFIeld_name5_210;
    QSpinBox *spinBox_start5_243;
    QLabel *label_ID4_248;
    QSpinBox *spinBox_start3_33;
    QLabel *label_ID4_249;
    QSpinBox *spinBox_duration4_34;
    QCheckBox *checkBox_341;
    QSpinBox *spinBox_start5_244;
    QSpinBox *spinBox_duration5_211;
    QLineEdit *textFIeld_name5_211;
    QLabel *label_ID4_250;
    QSpinBox *spinBox_start5_245;
    QLineEdit *textFIeld_name4_34;
    QSpinBox *spinBox_start3_34;
    QSpinBox *spinBox_start5_246;
    QSpacerItem *horizontalSpacer_301;
    QSpacerItem *horizontalSpacer_302;
    QSpinBox *spinBox_duration5_212;
    QLineEdit *textFIeld_name5_212;
    QLabel *label_ID2_36;
    QCheckBox *checkBox_342;
    QSpinBox *spinBox_start5_247;
    QLineEdit *textFIeld_name2_34;
    QLineEdit *textFIeld_name5_213;
    QCheckBox *checkBox_343;
    QLabel *label_ID4_251;
    QLabel *circle_inactive3_130;
    QLineEdit *textFIeld_name3_33;
    QSpinBox *spinBox_duration5_213;
    QLineEdit *textFIeld_name3_34;
    QSpinBox *spinBox_duration5_214;
    QCheckBox *checkBox_344;
    QLineEdit *textFIeld_name5_214;
    QLineEdit *textFIeld_name5_215;
    QSpinBox *spinBox_duration5_215;
    QLabel *label_ID4_252;
    QSpacerItem *horizontalSpacer_303;
    QLabel *label_ID4_253;
    QLineEdit *textFIeld_name5_216;
    QSpinBox *spinBox_start2_34;
    QSpinBox *spinBox_duration5_216;
    QLabel *label_ID4_254;
    QSpinBox *spinBox_duration5_217;
    QSpinBox *spinBox_duration5_218;
    QSpinBox *spinBox_start5_248;
    QLineEdit *textFIeld_name5_217;
    QSpinBox *spinBox_start5_249;
    QCheckBox *checkBox_345;
    QLabel *label_ID4_255;
    QLabel *label_ID4_256;
    QSpinBox *spinBox_duration3_33;
    QLabel *label_ID2_37;
    QLabel *label_ID4_257;
    QCheckBox *checkBox_346;
    QLabel *circle_inactive3_131;
    QLabel *label_ID4_258;
    QLabel *label_ID4_259;
    QSpinBox *spinBox_start4_33;
    QLineEdit *textFIeld_name5_218;
    QSpinBox *spinBox_start5_250;
    QSpinBox *spinBox_duration5_219;
    QLabel *label_ID3_36;
    QSpinBox *spinBox_duration1_17;
    QSpinBox *spinBox_duration5_220;
    QSpinBox *spinBox_start5_251;
    QSpinBox *spinBox_start4_34;
    QSpinBox *spinBox_duration2_34;
    QLabel *circle_inactive1_209;
    QCheckBox *checkBox_347;
    QSpinBox *spinBox_duration3_34;
    QCheckBox *checkBox_348;
    QCheckBox *checkBox_349;
    QCheckBox *checkBox_350;
    QLineEdit *textFIeld_name5_219;
    QCheckBox *checkBox_351;
    QSpinBox *spinBox_start5_252;
    QLabel *label_ID3_37;
    QLabel *label_ID1_20;
    QLineEdit *textFIeld_name5_220;
    QSpinBox *spinBox_duration5_221;
    QSpacerItem *horizontalSpacer_304;
    QLabel *label_ID4_260;
    QCheckBox *checkBox_352;
    QCheckBox *checkBox_353;
    QCheckBox *checkBox_354;
    QSpinBox *spinBox_start5_253;
    QCheckBox *checkBox_355;
    QSpinBox *spinBox_start5_254;
    QLabel *label_ID4_261;
    QLabel *circle_inactive3_132;
    QSpinBox *spinBox_start5_255;
    QLabel *circle_inactive1_210;
    QLineEdit *textFIeld_name5_221;
    QLabel *circle_inactive3_133;
    QSpacerItem *horizontalSpacer_305;
    QLabel *circle_inactive3_134;
    QLabel *circle_inactive1_211;
    QLabel *circle_inactive1_212;
    QLabel *circle_inactive1_213;
    QLabel *circle_inactive1_214;
    QLabel *circle_inactive1_215;
    QLabel *circle_inactive1_216;
    QLabel *circle_inactive1_217;
    QLabel *circle_inactive1_218;
    QLabel *circle_inactive1_219;
    QLabel *circle_inactive1_220;
    QLabel *circle_inactive1_221;
    QSpacerItem *horizontalSpacer_306;
    QSpacerItem *horizontalSpacer_307;
    QWidget *tab_14;
    QGridLayout *gridLayout_83;
    QGridLayout *gridLayout_84;
    QHBoxLayout *horizontalLayout_41;
    QSpacerItem *horizontalSpacer_308;
    QSpacerItem *horizontalSpacer_309;
    QLabel *label_ID_22;
    QSpacerItem *horizontalSpacer_310;
    QLabel *label_name_20;
    QSpacerItem *horizontalSpacer_311;
    QSpacerItem *horizontalSpacer_312;
    QLabel *label_start_22;
    QLabel *label_msStart_23;
    QSpacerItem *horizontalSpacer_313;
    QLabel *label_duration_28;
    QLabel *label_msDuration_42;
    QSpacerItem *horizontalSpacer_314;
    QGridLayout *gridLayout_85;
    QHBoxLayout *horizontalLayout_42;
    QProgressBar *progressBar_36;
    QLabel *label_72;
    QSpinBox *spinBox_start5_256;
    QSpinBox *spinBox_start5_257;
    QPushButton *pushButton_71;
    QSpacerItem *horizontalSpacer_315;
    QSpacerItem *verticalSpacer_95;
    QPushButton *pushButton_72;
    QLabel *label_73;
    QLabel *label_74;
    QFrame *line_21;
    QSpacerItem *horizontalSpacer_316;
    QSpacerItem *verticalSpacer_96;
    QPushButton *pushButton_73;
    QLabel *label_msDuration_43;
    QSpacerItem *verticalSpacer_97;
    QPushButton *pushButton_74;
    QSpacerItem *verticalSpacer_98;
    QLabel *label_75;
    QSpacerItem *verticalSpacer_99;
    QScrollArea *scrollArea_18;
    QWidget *scrollAreaWidgetContents_21;
    QGridLayout *gridLayout_86;
    QGridLayout *gridLayout_87;
    QLineEdit *textFIeld_name5_222;
    QSpinBox *spinBox_start2_35;
    QLabel *label_ID4_262;
    QLabel *circle_inactive3_135;
    QSpacerItem *horizontalSpacer_317;
    QCheckBox *checkBox_356;
    QCheckBox *checkBox_357;
    QCheckBox *checkBox_358;
    QSpinBox *spinBox_start1_18;
    QCheckBox *checkBox_359;
    QLabel *circle_inactive3_136;
    QLineEdit *textFIeld_name1_51;
    QSpinBox *spinBox_duration2_35;
    QSpinBox *spinBox_duration4_35;
    QLineEdit *textFIeld_name4_35;
    QSpinBox *spinBox_duration5_222;
    QCheckBox *checkBox_360;
    QLineEdit *textFIeld_name2_35;
    QSpinBox *spinBox_duration5_223;
    QLineEdit *textFIeld_name5_223;
    QSpinBox *spinBox_start5_258;
    QLabel *label_ID4_263;
    QSpinBox *spinBox_start3_35;
    QLabel *label_ID4_264;
    QSpinBox *spinBox_duration4_36;
    QCheckBox *checkBox_361;
    QSpinBox *spinBox_start5_259;
    QSpinBox *spinBox_duration5_224;
    QLineEdit *textFIeld_name5_224;
    QLabel *label_ID4_265;
    QSpinBox *spinBox_start5_260;
    QLineEdit *textFIeld_name4_36;
    QSpinBox *spinBox_start3_36;
    QSpinBox *spinBox_start5_261;
    QSpacerItem *horizontalSpacer_318;
    QSpacerItem *horizontalSpacer_319;
    QSpinBox *spinBox_duration5_225;
    QLineEdit *textFIeld_name5_225;
    QLabel *label_ID2_38;
    QCheckBox *checkBox_362;
    QSpinBox *spinBox_start5_262;
    QLineEdit *textFIeld_name2_36;
    QLineEdit *textFIeld_name5_226;
    QCheckBox *checkBox_363;
    QLabel *label_ID4_266;
    QLabel *circle_inactive3_137;
    QLineEdit *textFIeld_name3_35;
    QSpinBox *spinBox_duration5_226;
    QLineEdit *textFIeld_name3_36;
    QSpinBox *spinBox_duration5_227;
    QCheckBox *checkBox_364;
    QLineEdit *textFIeld_name5_227;
    QLineEdit *textFIeld_name5_228;
    QSpinBox *spinBox_duration5_228;
    QLabel *label_ID4_267;
    QSpacerItem *horizontalSpacer_320;
    QLabel *label_ID4_268;
    QLineEdit *textFIeld_name5_229;
    QSpinBox *spinBox_start2_36;
    QSpinBox *spinBox_duration5_229;
    QLabel *label_ID4_269;
    QSpinBox *spinBox_duration5_230;
    QSpinBox *spinBox_duration5_231;
    QSpinBox *spinBox_start5_263;
    QLineEdit *textFIeld_name5_230;
    QSpinBox *spinBox_start5_264;
    QCheckBox *checkBox_365;
    QLabel *label_ID4_270;
    QLabel *label_ID4_271;
    QSpinBox *spinBox_duration3_35;
    QLabel *label_ID2_39;
    QLabel *label_ID4_272;
    QCheckBox *checkBox_366;
    QLabel *circle_inactive3_138;
    QLabel *label_ID4_273;
    QLabel *label_ID4_274;
    QSpinBox *spinBox_start4_35;
    QLineEdit *textFIeld_name5_231;
    QSpinBox *spinBox_start5_265;
    QSpinBox *spinBox_duration5_232;
    QLabel *label_ID3_38;
    QSpinBox *spinBox_duration1_18;
    QSpinBox *spinBox_duration5_233;
    QSpinBox *spinBox_start5_266;
    QSpinBox *spinBox_start4_36;
    QSpinBox *spinBox_duration2_36;
    QLabel *circle_inactive1_222;
    QCheckBox *checkBox_367;
    QSpinBox *spinBox_duration3_36;
    QCheckBox *checkBox_368;
    QCheckBox *checkBox_369;
    QCheckBox *checkBox_370;
    QLineEdit *textFIeld_name5_232;
    QCheckBox *checkBox_371;
    QSpinBox *spinBox_start5_267;
    QLabel *label_ID3_39;
    QLabel *label_ID1_21;
    QLineEdit *textFIeld_name5_233;
    QSpinBox *spinBox_duration5_234;
    QSpacerItem *horizontalSpacer_321;
    QLabel *label_ID4_275;
    QCheckBox *checkBox_372;
    QCheckBox *checkBox_373;
    QCheckBox *checkBox_374;
    QSpinBox *spinBox_start5_268;
    QCheckBox *checkBox_375;
    QSpinBox *spinBox_start5_269;
    QLabel *label_ID4_276;
    QLabel *circle_inactive3_139;
    QSpinBox *spinBox_start5_270;
    QLabel *circle_inactive1_223;
    QLineEdit *textFIeld_name5_234;
    QLabel *circle_inactive3_140;
    QSpacerItem *horizontalSpacer_322;
    QLabel *circle_inactive3_141;
    QLabel *circle_inactive1_224;
    QLabel *circle_inactive1_225;
    QLabel *circle_inactive1_226;
    QLabel *circle_inactive1_227;
    QLabel *circle_inactive1_228;
    QLabel *circle_inactive1_229;
    QLabel *circle_inactive1_230;
    QLabel *circle_inactive1_231;
    QLabel *circle_inactive1_232;
    QLabel *circle_inactive1_233;
    QLabel *circle_inactive1_234;
    QSpacerItem *horizontalSpacer_323;
    QSpacerItem *horizontalSpacer_324;
    QWidget *tab_15;
    QGridLayout *gridLayout_88;
    QGridLayout *gridLayout_89;
    QHBoxLayout *horizontalLayout_43;
    QSpacerItem *horizontalSpacer_325;
    QSpacerItem *horizontalSpacer_326;
    QLabel *label_ID_23;
    QSpacerItem *horizontalSpacer_327;
    QLabel *label_name_21;
    QSpacerItem *horizontalSpacer_328;
    QSpacerItem *horizontalSpacer_329;
    QLabel *label_start_23;
    QLabel *label_msStart_24;
    QSpacerItem *horizontalSpacer_330;
    QLabel *label_duration_29;
    QLabel *label_msDuration_44;
    QSpacerItem *horizontalSpacer_331;
    QGridLayout *gridLayout_90;
    QHBoxLayout *horizontalLayout_44;
    QProgressBar *progressBar_37;
    QLabel *label_76;
    QSpinBox *spinBox_start5_271;
    QSpinBox *spinBox_start5_272;
    QPushButton *pushButton_75;
    QSpacerItem *horizontalSpacer_332;
    QSpacerItem *verticalSpacer_100;
    QPushButton *pushButton_76;
    QLabel *label_77;
    QLabel *label_78;
    QFrame *line_22;
    QSpacerItem *horizontalSpacer_333;
    QSpacerItem *verticalSpacer_101;
    QPushButton *pushButton_77;
    QLabel *label_msDuration_45;
    QSpacerItem *verticalSpacer_102;
    QPushButton *pushButton_78;
    QSpacerItem *verticalSpacer_103;
    QLabel *label_79;
    QSpacerItem *verticalSpacer_104;
    QScrollArea *scrollArea_19;
    QWidget *scrollAreaWidgetContents_22;
    QGridLayout *gridLayout_91;
    QGridLayout *gridLayout_92;
    QLineEdit *textFIeld_name5_235;
    QSpinBox *spinBox_start2_37;
    QLabel *label_ID4_277;
    QLabel *circle_inactive3_142;
    QSpacerItem *horizontalSpacer_334;
    QCheckBox *checkBox_376;
    QCheckBox *checkBox_377;
    QCheckBox *checkBox_378;
    QSpinBox *spinBox_start1_19;
    QCheckBox *checkBox_379;
    QLabel *circle_inactive3_143;
    QLineEdit *textFIeld_name1_52;
    QSpinBox *spinBox_duration2_37;
    QSpinBox *spinBox_duration4_37;
    QLineEdit *textFIeld_name4_37;
    QSpinBox *spinBox_duration5_235;
    QCheckBox *checkBox_380;
    QLineEdit *textFIeld_name2_37;
    QSpinBox *spinBox_duration5_236;
    QLineEdit *textFIeld_name5_236;
    QSpinBox *spinBox_start5_273;
    QLabel *label_ID4_278;
    QSpinBox *spinBox_start3_37;
    QLabel *label_ID4_279;
    QSpinBox *spinBox_duration4_38;
    QCheckBox *checkBox_381;
    QSpinBox *spinBox_start5_274;
    QSpinBox *spinBox_duration5_237;
    QLineEdit *textFIeld_name5_237;
    QLabel *label_ID4_280;
    QSpinBox *spinBox_start5_275;
    QLineEdit *textFIeld_name4_38;
    QSpinBox *spinBox_start3_38;
    QSpinBox *spinBox_start5_276;
    QSpacerItem *horizontalSpacer_335;
    QSpacerItem *horizontalSpacer_336;
    QSpinBox *spinBox_duration5_238;
    QLineEdit *textFIeld_name5_238;
    QLabel *label_ID2_40;
    QCheckBox *checkBox_382;
    QSpinBox *spinBox_start5_277;
    QLineEdit *textFIeld_name2_38;
    QLineEdit *textFIeld_name5_239;
    QCheckBox *checkBox_383;
    QLabel *label_ID4_281;
    QLabel *circle_inactive3_144;
    QLineEdit *textFIeld_name3_37;
    QSpinBox *spinBox_duration5_239;
    QLineEdit *textFIeld_name3_38;
    QSpinBox *spinBox_duration5_240;
    QCheckBox *checkBox_384;
    QLineEdit *textFIeld_name5_240;
    QLineEdit *textFIeld_name5_241;
    QSpinBox *spinBox_duration5_241;
    QLabel *label_ID4_282;
    QSpacerItem *horizontalSpacer_337;
    QLabel *label_ID4_283;
    QLineEdit *textFIeld_name5_242;
    QSpinBox *spinBox_start2_38;
    QSpinBox *spinBox_duration5_242;
    QLabel *label_ID4_284;
    QSpinBox *spinBox_duration5_243;
    QSpinBox *spinBox_duration5_244;
    QSpinBox *spinBox_start5_278;
    QLineEdit *textFIeld_name5_243;
    QSpinBox *spinBox_start5_279;
    QCheckBox *checkBox_385;
    QLabel *label_ID4_285;
    QLabel *label_ID4_286;
    QSpinBox *spinBox_duration3_37;
    QLabel *label_ID2_41;
    QLabel *label_ID4_287;
    QCheckBox *checkBox_386;
    QLabel *circle_inactive3_145;
    QLabel *label_ID4_288;
    QLabel *label_ID4_289;
    QSpinBox *spinBox_start4_37;
    QLineEdit *textFIeld_name5_244;
    QSpinBox *spinBox_start5_280;
    QSpinBox *spinBox_duration5_245;
    QLabel *label_ID3_40;
    QSpinBox *spinBox_duration1_19;
    QSpinBox *spinBox_duration5_246;
    QSpinBox *spinBox_start5_281;
    QSpinBox *spinBox_start4_38;
    QSpinBox *spinBox_duration2_38;
    QLabel *circle_inactive1_235;
    QCheckBox *checkBox_387;
    QSpinBox *spinBox_duration3_38;
    QCheckBox *checkBox_388;
    QCheckBox *checkBox_389;
    QCheckBox *checkBox_390;
    QLineEdit *textFIeld_name5_245;
    QCheckBox *checkBox_391;
    QSpinBox *spinBox_start5_282;
    QLabel *label_ID3_41;
    QLabel *label_ID1_22;
    QLineEdit *textFIeld_name5_246;
    QSpinBox *spinBox_duration5_247;
    QSpacerItem *horizontalSpacer_338;
    QLabel *label_ID4_290;
    QCheckBox *checkBox_392;
    QCheckBox *checkBox_393;
    QCheckBox *checkBox_394;
    QSpinBox *spinBox_start5_283;
    QCheckBox *checkBox_395;
    QSpinBox *spinBox_start5_284;
    QLabel *label_ID4_291;
    QLabel *circle_inactive3_146;
    QSpinBox *spinBox_start5_285;
    QLabel *circle_inactive1_236;
    QLineEdit *textFIeld_name5_247;
    QLabel *circle_inactive3_147;
    QSpacerItem *horizontalSpacer_339;
    QLabel *circle_inactive3_148;
    QLabel *circle_inactive1_237;
    QLabel *circle_inactive1_238;
    QLabel *circle_inactive1_239;
    QLabel *circle_inactive1_240;
    QLabel *circle_inactive1_241;
    QLabel *circle_inactive1_242;
    QLabel *circle_inactive1_243;
    QLabel *circle_inactive1_244;
    QLabel *circle_inactive1_245;
    QLabel *circle_inactive1_246;
    QLabel *circle_inactive1_247;
    QSpacerItem *horizontalSpacer_340;
    QSpacerItem *horizontalSpacer_341;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindowapp2)
    {
        if (MainWindowapp2->objectName().isEmpty())
            MainWindowapp2->setObjectName(QStringLiteral("MainWindowapp2"));
        MainWindowapp2->resize(1200, 853);
        MainWindowapp2->setMinimumSize(QSize(0, 0));
        MainWindowapp2->setMaximumSize(QSize(1920, 1080));
        QPalette palette;
        QBrush brush(QColor(25, 46, 71, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(85, 124, 146, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        QBrush brush2(QColor(255, 255, 255, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Light, brush2);
        QBrush brush3(QColor(237, 226, 235, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        QBrush brush4(QColor(109, 99, 107, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush4);
        QBrush brush5(QColor(146, 132, 143, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush2);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush2);
        QBrush brush6(QColor(226, 234, 249, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Window, brush6);
        QBrush brush7(QColor(0, 0, 0, 255));
        brush7.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush7);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush3);
        QBrush brush8(QColor(255, 255, 220, 255));
        brush8.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush8);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush7);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        QBrush brush9(QColor(219, 198, 215, 255));
        brush9.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush9);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush7);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush8);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush7);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush6);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush9);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush9);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush9);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush7);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush9);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush8);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush7);
        MainWindowapp2->setPalette(palette);
        centralWidget = new QWidget(MainWindowapp2);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout_13 = new QGridLayout(centralWidget);
        gridLayout_13->setSpacing(6);
        gridLayout_13->setContentsMargins(11, 11, 11, 11);
        gridLayout_13->setObjectName(QStringLiteral("gridLayout_13"));
        tabWidget_2 = new QTabWidget(centralWidget);
        tabWidget_2->setObjectName(QStringLiteral("tabWidget_2"));
        tabWidget_2->setEnabled(true);
        tabWidget_2->setMinimumSize(QSize(150, 300));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush6);
        QBrush brush10(QColor(167, 224, 255, 255));
        brush10.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Light, brush10);
        QBrush brush11(QColor(136, 189, 218, 255));
        brush11.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Midlight, brush11);
        QBrush brush12(QColor(53, 77, 91, 255));
        brush12.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Dark, brush12);
        QBrush brush13(QColor(70, 103, 121, 255));
        brush13.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Mid, brush13);
        palette1.setBrush(QPalette::Active, QPalette::Text, brush7);
        palette1.setBrush(QPalette::Active, QPalette::BrightText, brush2);
        palette1.setBrush(QPalette::Active, QPalette::ButtonText, brush7);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush2);
        QBrush brush14(QColor(106, 155, 182, 255));
        brush14.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush14);
        palette1.setBrush(QPalette::Active, QPalette::Shadow, brush7);
        QBrush brush15(QColor(180, 205, 218, 255));
        brush15.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::AlternateBase, brush15);
        palette1.setBrush(QPalette::Active, QPalette::ToolTipBase, brush8);
        palette1.setBrush(QPalette::Active, QPalette::ToolTipText, brush7);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush14);
        palette1.setBrush(QPalette::Inactive, QPalette::Light, brush10);
        palette1.setBrush(QPalette::Inactive, QPalette::Midlight, brush11);
        palette1.setBrush(QPalette::Inactive, QPalette::Dark, brush12);
        palette1.setBrush(QPalette::Inactive, QPalette::Mid, brush13);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush7);
        palette1.setBrush(QPalette::Inactive, QPalette::BrightText, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::ButtonText, brush7);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush14);
        palette1.setBrush(QPalette::Inactive, QPalette::Shadow, brush7);
        palette1.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush15);
        palette1.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush8);
        palette1.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush7);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush12);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush14);
        palette1.setBrush(QPalette::Disabled, QPalette::Light, brush10);
        palette1.setBrush(QPalette::Disabled, QPalette::Midlight, brush11);
        palette1.setBrush(QPalette::Disabled, QPalette::Dark, brush12);
        palette1.setBrush(QPalette::Disabled, QPalette::Mid, brush13);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush6);
        palette1.setBrush(QPalette::Disabled, QPalette::BrightText, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::ButtonText, brush12);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush14);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush14);
        palette1.setBrush(QPalette::Disabled, QPalette::Shadow, brush7);
        palette1.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush14);
        palette1.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush8);
        palette1.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush7);
        tabWidget_2->setPalette(palette1);
        tab_11 = new QWidget();
        tab_11->setObjectName(QStringLiteral("tab_11"));
        tab_11->setEnabled(false);
        gridLayout_70 = new QGridLayout(tab_11);
        gridLayout_70->setSpacing(6);
        gridLayout_70->setContentsMargins(11, 11, 11, 11);
        gridLayout_70->setObjectName(QStringLiteral("gridLayout_70"));
        gridLayout_9 = new QGridLayout();
        gridLayout_9->setSpacing(6);
        gridLayout_9->setObjectName(QStringLiteral("gridLayout_9"));
        horizontalLayout_35 = new QHBoxLayout();
        horizontalLayout_35->setSpacing(6);
        horizontalLayout_35->setObjectName(QStringLiteral("horizontalLayout_35"));
        horizontalSpacer_265 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_35->addItem(horizontalSpacer_265);

        label_ID_19 = new QLabel(tab_11);
        label_ID_19->setObjectName(QStringLiteral("label_ID_19"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Ignored);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_ID_19->sizePolicy().hasHeightForWidth());
        label_ID_19->setSizePolicy(sizePolicy);
        label_ID_19->setMaximumSize(QSize(16777215, 16777215));
        QFont font;
        font.setFamily(QStringLiteral("Titillium Web"));
        font.setPointSize(20);
        font.setBold(true);
        font.setWeight(75);
        label_ID_19->setFont(font);
        label_ID_19->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_35->addWidget(label_ID_19);

        horizontalSpacer_266 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_35->addItem(horizontalSpacer_266);

        label_start_19 = new QLabel(tab_11);
        label_start_19->setObjectName(QStringLiteral("label_start_19"));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label_start_19->sizePolicy().hasHeightForWidth());
        label_start_19->setSizePolicy(sizePolicy1);
        label_start_19->setFont(font);
        label_start_19->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_35->addWidget(label_start_19);

        label_msStart_20 = new QLabel(tab_11);
        label_msStart_20->setObjectName(QStringLiteral("label_msStart_20"));
        QFont font1;
        font1.setFamily(QStringLiteral("Titillium Web"));
        label_msStart_20->setFont(font1);
        label_msStart_20->setAlignment(Qt::AlignCenter);

        horizontalLayout_35->addWidget(label_msStart_20);

        horizontalSpacer_17 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_35->addItem(horizontalSpacer_17);

        label_duration_23 = new QLabel(tab_11);
        label_duration_23->setObjectName(QStringLiteral("label_duration_23"));
        label_duration_23->setFont(font);
        label_duration_23->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_35->addWidget(label_duration_23);

        label_msDuration_36 = new QLabel(tab_11);
        label_msDuration_36->setObjectName(QStringLiteral("label_msDuration_36"));
        label_msDuration_36->setFont(font1);
        label_msDuration_36->setAlignment(Qt::AlignCenter);

        horizontalLayout_35->addWidget(label_msDuration_36);

        horizontalSpacer_267 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_35->addItem(horizontalSpacer_267);

        horizontalSpacer_268 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_35->addItem(horizontalSpacer_268);

        horizontalSpacer_18 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_35->addItem(horizontalSpacer_18);


        gridLayout_9->addLayout(horizontalLayout_35, 0, 0, 1, 1);

        gridLayout_71 = new QGridLayout();
        gridLayout_71->setSpacing(6);
        gridLayout_71->setObjectName(QStringLiteral("gridLayout_71"));
        horizontalSpacer_19 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_71->addItem(horizontalSpacer_19, 1, 0, 1, 1);

        label_duration_24 = new QLabel(tab_11);
        label_duration_24->setObjectName(QStringLiteral("label_duration_24"));
        label_duration_24->setFont(font);
        label_duration_24->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_71->addWidget(label_duration_24, 1, 2, 1, 1);

        horizontalLayout_36 = new QHBoxLayout();
        horizontalLayout_36->setSpacing(6);
        horizontalLayout_36->setObjectName(QStringLiteral("horizontalLayout_36"));

        gridLayout_71->addLayout(horizontalLayout_36, 3, 0, 1, 7);

        textFIeld_name1_38 = new QLineEdit(tab_11);
        textFIeld_name1_38->setObjectName(QStringLiteral("textFIeld_name1_38"));
        textFIeld_name1_38->setEnabled(false);
        QFont font2;
        font2.setFamily(QStringLiteral("Titillium Web"));
        font2.setPointSize(13);
        textFIeld_name1_38->setFont(font2);

        gridLayout_71->addWidget(textFIeld_name1_38, 2, 2, 1, 2);

        verticalSpacer_80 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_71->addItem(verticalSpacer_80, 6, 1, 1, 1);

        horizontalSpacer_20 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_71->addItem(horizontalSpacer_20, 6, 5, 1, 1);

        verticalSpacer_81 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_71->addItem(verticalSpacer_81, 8, 1, 1, 1);

        label_msDuration_37 = new QLabel(tab_11);
        label_msDuration_37->setObjectName(QStringLiteral("label_msDuration_37"));
        label_msDuration_37->setFont(font1);
        label_msDuration_37->setAlignment(Qt::AlignCenter);

        gridLayout_71->addWidget(label_msDuration_37, 1, 3, 1, 1);

        verticalSpacer_82 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_71->addItem(verticalSpacer_82, 1, 1, 1, 1);

        verticalSpacer_83 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_71->addItem(verticalSpacer_83, 4, 1, 1, 1);

        label_duration_25 = new QLabel(tab_11);
        label_duration_25->setObjectName(QStringLiteral("label_duration_25"));
        label_duration_25->setFont(font);
        label_duration_25->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_71->addWidget(label_duration_25, 6, 2, 1, 1);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_71->addItem(verticalSpacer_3, 0, 2, 1, 1);

        horizontalSpacer_21 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_71->addItem(horizontalSpacer_21, 6, 4, 1, 1);

        label_63 = new QLabel(tab_11);
        label_63->setObjectName(QStringLiteral("label_63"));
        QFont font3;
        font3.setFamily(QStringLiteral("Titillium Web"));
        font3.setItalic(false);
        label_63->setFont(font3);

        gridLayout_71->addWidget(label_63, 7, 4, 1, 1);

        progressBar_28 = new QProgressBar(tab_11);
        progressBar_28->setObjectName(QStringLiteral("progressBar_28"));
        progressBar_28->setValue(24);

        gridLayout_71->addWidget(progressBar_28, 7, 2, 1, 2);

        line_18 = new QFrame(tab_11);
        line_18->setObjectName(QStringLiteral("line_18"));
        line_18->setFrameShape(QFrame::HLine);
        line_18->setFrameShadow(QFrame::Sunken);

        gridLayout_71->addWidget(line_18, 5, 0, 1, 7);


        gridLayout_9->addLayout(gridLayout_71, 0, 2, 2, 1);

        scrollArea_14 = new QScrollArea(tab_11);
        scrollArea_14->setObjectName(QStringLiteral("scrollArea_14"));
        scrollArea_14->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        scrollArea_14->setWidgetResizable(true);
        scrollAreaWidgetContents_18 = new QWidget();
        scrollAreaWidgetContents_18->setObjectName(QStringLiteral("scrollAreaWidgetContents_18"));
        scrollAreaWidgetContents_18->setGeometry(QRect(0, 0, 596, 672));
        gridLayout_72 = new QGridLayout(scrollAreaWidgetContents_18);
        gridLayout_72->setSpacing(6);
        gridLayout_72->setContentsMargins(11, 11, 11, 11);
        gridLayout_72->setObjectName(QStringLiteral("gridLayout_72"));
        gridLayout_73 = new QGridLayout();
        gridLayout_73->setSpacing(1);
        gridLayout_73->setObjectName(QStringLiteral("gridLayout_73"));
        checkBox_312 = new QCheckBox(scrollAreaWidgetContents_18);
        checkBox_312->setObjectName(QStringLiteral("checkBox_312"));
        checkBox_312->setEnabled(false);
        checkBox_312->setBaseSize(QSize(0, 0));
        QFont font4;
        font4.setFamily(QStringLiteral("Titillium Web"));
        font4.setPointSize(14);
        font4.setBold(true);
        font4.setWeight(75);
        checkBox_312->setFont(font4);
        checkBox_312->setTabletTracking(false);
        checkBox_312->setAutoFillBackground(false);
        checkBox_312->setChecked(false);
        checkBox_312->setTristate(false);

        gridLayout_73->addWidget(checkBox_312, 5, 1, 1, 1);

        progressBar_33 = new QProgressBar(scrollAreaWidgetContents_18);
        progressBar_33->setObjectName(QStringLiteral("progressBar_33"));
        progressBar_33->setValue(0);

        gridLayout_73->addWidget(progressBar_33, 1, 12, 1, 1);

        textFIeld_name1_48 = new QLineEdit(scrollAreaWidgetContents_18);
        textFIeld_name1_48->setObjectName(QStringLiteral("textFIeld_name1_48"));
        textFIeld_name1_48->setEnabled(false);
        textFIeld_name1_48->setFont(font2);

        gridLayout_73->addWidget(textFIeld_name1_48, 2, 5, 1, 2);

        progressBar_29 = new QProgressBar(scrollAreaWidgetContents_18);
        progressBar_29->setObjectName(QStringLiteral("progressBar_29"));
        progressBar_29->setValue(0);

        gridLayout_73->addWidget(progressBar_29, 3, 12, 1, 1);

        textFIeld_name1_42 = new QLineEdit(scrollAreaWidgetContents_18);
        textFIeld_name1_42->setObjectName(QStringLiteral("textFIeld_name1_42"));
        textFIeld_name1_42->setEnabled(false);
        textFIeld_name1_42->setFont(font2);

        gridLayout_73->addWidget(textFIeld_name1_42, 5, 8, 1, 2);

        horizontalSpacer_274 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_73->addItem(horizontalSpacer_274, 6, 7, 1, 1);

        label_ID3_33 = new QLabel(scrollAreaWidgetContents_18);
        label_ID3_33->setObjectName(QStringLiteral("label_ID3_33"));
        QFont font5;
        font5.setFamily(QStringLiteral("Titillium Web"));
        font5.setPointSize(15);
        font5.setBold(true);
        font5.setWeight(75);
        label_ID3_33->setFont(font5);
        label_ID3_33->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_73->addWidget(label_ID3_33, 2, 2, 1, 1);

        label_ID4_230 = new QLabel(scrollAreaWidgetContents_18);
        label_ID4_230->setObjectName(QStringLiteral("label_ID4_230"));
        label_ID4_230->setFont(font5);
        label_ID4_230->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_73->addWidget(label_ID4_230, 3, 2, 1, 1);

        label_ID2_33 = new QLabel(scrollAreaWidgetContents_18);
        label_ID2_33->setObjectName(QStringLiteral("label_ID2_33"));
        label_ID2_33->setFont(font5);
        label_ID2_33->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_73->addWidget(label_ID2_33, 1, 2, 1, 1);

        horizontalSpacer_272 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_73->addItem(horizontalSpacer_272, 6, 0, 1, 1);

        circle_inactive3_116 = new QLabel(scrollAreaWidgetContents_18);
        circle_inactive3_116->setObjectName(QStringLiteral("circle_inactive3_116"));
        circle_inactive3_116->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_116->setAlignment(Qt::AlignCenter);

        gridLayout_73->addWidget(circle_inactive3_116, 3, 3, 1, 1);

        horizontalSpacer_269 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_73->addItem(horizontalSpacer_269, 6, 3, 1, 1);

        horizontalSpacer_273 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_73->addItem(horizontalSpacer_273, 0, 11, 1, 1);

        circle_inactive3_119 = new QLabel(scrollAreaWidgetContents_18);
        circle_inactive3_119->setObjectName(QStringLiteral("circle_inactive3_119"));
        circle_inactive3_119->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_119->setAlignment(Qt::AlignCenter);

        gridLayout_73->addWidget(circle_inactive3_119, 2, 3, 1, 1);

        circle_inactive3_117 = new QLabel(scrollAreaWidgetContents_18);
        circle_inactive3_117->setObjectName(QStringLiteral("circle_inactive3_117"));
        circle_inactive3_117->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-03.png")));
        circle_inactive3_117->setAlignment(Qt::AlignCenter);

        gridLayout_73->addWidget(circle_inactive3_117, 0, 3, 1, 1);

        label_ID4_231 = new QLabel(scrollAreaWidgetContents_18);
        label_ID4_231->setObjectName(QStringLiteral("label_ID4_231"));
        label_ID4_231->setFont(font5);
        label_ID4_231->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_73->addWidget(label_ID4_231, 5, 2, 1, 1);

        progressBar_31 = new QProgressBar(scrollAreaWidgetContents_18);
        progressBar_31->setObjectName(QStringLiteral("progressBar_31"));
        progressBar_31->setValue(0);

        gridLayout_73->addWidget(progressBar_31, 2, 12, 1, 1);

        label_ID1_18 = new QLabel(scrollAreaWidgetContents_18);
        label_ID1_18->setObjectName(QStringLiteral("label_ID1_18"));
        label_ID1_18->setFont(font5);
        label_ID1_18->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_73->addWidget(label_ID1_18, 0, 2, 1, 1);

        textFIeld_name1_40 = new QLineEdit(scrollAreaWidgetContents_18);
        textFIeld_name1_40->setObjectName(QStringLiteral("textFIeld_name1_40"));
        textFIeld_name1_40->setEnabled(false);
        textFIeld_name1_40->setFont(font2);

        gridLayout_73->addWidget(textFIeld_name1_40, 0, 8, 1, 2);

        horizontalSpacer_271 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_73->addItem(horizontalSpacer_271, 6, 4, 1, 1);

        textFIeld_name1_39 = new QLineEdit(scrollAreaWidgetContents_18);
        textFIeld_name1_39->setObjectName(QStringLiteral("textFIeld_name1_39"));
        textFIeld_name1_39->setEnabled(false);
        textFIeld_name1_39->setFont(font2);

        gridLayout_73->addWidget(textFIeld_name1_39, 0, 5, 1, 2);

        textFIeld_name1_46 = new QLineEdit(scrollAreaWidgetContents_18);
        textFIeld_name1_46->setObjectName(QStringLiteral("textFIeld_name1_46"));
        textFIeld_name1_46->setEnabled(false);
        textFIeld_name1_46->setFont(font2);

        gridLayout_73->addWidget(textFIeld_name1_46, 1, 5, 1, 2);

        checkBox_311 = new QCheckBox(scrollAreaWidgetContents_18);
        checkBox_311->setObjectName(QStringLiteral("checkBox_311"));
        checkBox_311->setEnabled(false);
        checkBox_311->setBaseSize(QSize(0, 0));
        checkBox_311->setFont(font4);
        checkBox_311->setTabletTracking(false);
        checkBox_311->setAutoFillBackground(false);
        checkBox_311->setChecked(false);
        checkBox_311->setTristate(false);

        gridLayout_73->addWidget(checkBox_311, 2, 1, 1, 1);

        circle_inactive3_120 = new QLabel(scrollAreaWidgetContents_18);
        circle_inactive3_120->setObjectName(QStringLiteral("circle_inactive3_120"));
        circle_inactive3_120->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_120->setAlignment(Qt::AlignCenter);

        gridLayout_73->addWidget(circle_inactive3_120, 1, 3, 1, 1);

        progressBar_30 = new QProgressBar(scrollAreaWidgetContents_18);
        progressBar_30->setObjectName(QStringLiteral("progressBar_30"));
        progressBar_30->setValue(100);

        gridLayout_73->addWidget(progressBar_30, 0, 12, 1, 1);

        textFIeld_name1_43 = new QLineEdit(scrollAreaWidgetContents_18);
        textFIeld_name1_43->setObjectName(QStringLiteral("textFIeld_name1_43"));
        textFIeld_name1_43->setEnabled(false);
        textFIeld_name1_43->setFont(font2);

        gridLayout_73->addWidget(textFIeld_name1_43, 1, 8, 1, 2);

        checkBox_315 = new QCheckBox(scrollAreaWidgetContents_18);
        checkBox_315->setObjectName(QStringLiteral("checkBox_315"));
        checkBox_315->setEnabled(false);
        checkBox_315->setBaseSize(QSize(0, 0));
        checkBox_315->setFont(font4);
        checkBox_315->setTabletTracking(false);
        checkBox_315->setAutoFillBackground(false);
        checkBox_315->setChecked(false);
        checkBox_315->setTristate(false);

        gridLayout_73->addWidget(checkBox_315, 1, 1, 1, 1);

        checkBox_313 = new QCheckBox(scrollAreaWidgetContents_18);
        checkBox_313->setObjectName(QStringLiteral("checkBox_313"));
        checkBox_313->setEnabled(false);
        checkBox_313->setBaseSize(QSize(0, 0));
        checkBox_313->setFont(font4);
        checkBox_313->setTabletTracking(false);
        checkBox_313->setAutoFillBackground(false);
        checkBox_313->setChecked(true);
        checkBox_313->setTristate(false);

        gridLayout_73->addWidget(checkBox_313, 0, 1, 1, 1);

        circle_inactive3_118 = new QLabel(scrollAreaWidgetContents_18);
        circle_inactive3_118->setObjectName(QStringLiteral("circle_inactive3_118"));
        circle_inactive3_118->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_118->setAlignment(Qt::AlignCenter);

        gridLayout_73->addWidget(circle_inactive3_118, 5, 3, 1, 1);

        checkBox_314 = new QCheckBox(scrollAreaWidgetContents_18);
        checkBox_314->setObjectName(QStringLiteral("checkBox_314"));
        checkBox_314->setEnabled(false);
        checkBox_314->setBaseSize(QSize(0, 0));
        checkBox_314->setFont(font4);
        checkBox_314->setTabletTracking(false);
        checkBox_314->setAutoFillBackground(false);
        checkBox_314->setChecked(false);
        checkBox_314->setTristate(false);

        gridLayout_73->addWidget(checkBox_314, 3, 1, 1, 1);

        horizontalSpacer_270 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_73->addItem(horizontalSpacer_270, 0, 7, 1, 1);

        textFIeld_name1_41 = new QLineEdit(scrollAreaWidgetContents_18);
        textFIeld_name1_41->setObjectName(QStringLiteral("textFIeld_name1_41"));
        textFIeld_name1_41->setEnabled(false);
        textFIeld_name1_41->setFont(font2);

        gridLayout_73->addWidget(textFIeld_name1_41, 5, 5, 1, 2);

        horizontalSpacer_22 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_73->addItem(horizontalSpacer_22, 6, 2, 1, 1);

        textFIeld_name1_45 = new QLineEdit(scrollAreaWidgetContents_18);
        textFIeld_name1_45->setObjectName(QStringLiteral("textFIeld_name1_45"));
        textFIeld_name1_45->setEnabled(false);
        textFIeld_name1_45->setFont(font2);

        gridLayout_73->addWidget(textFIeld_name1_45, 3, 5, 1, 2);

        horizontalSpacer_23 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_73->addItem(horizontalSpacer_23, 0, 4, 1, 1);

        textFIeld_name1_44 = new QLineEdit(scrollAreaWidgetContents_18);
        textFIeld_name1_44->setObjectName(QStringLiteral("textFIeld_name1_44"));
        textFIeld_name1_44->setEnabled(false);
        textFIeld_name1_44->setFont(font2);

        gridLayout_73->addWidget(textFIeld_name1_44, 2, 8, 1, 2);

        textFIeld_name1_47 = new QLineEdit(scrollAreaWidgetContents_18);
        textFIeld_name1_47->setObjectName(QStringLiteral("textFIeld_name1_47"));
        textFIeld_name1_47->setEnabled(false);
        textFIeld_name1_47->setFont(font2);

        gridLayout_73->addWidget(textFIeld_name1_47, 3, 8, 1, 2);

        progressBar_32 = new QProgressBar(scrollAreaWidgetContents_18);
        progressBar_32->setObjectName(QStringLiteral("progressBar_32"));
        progressBar_32->setValue(0);

        gridLayout_73->addWidget(progressBar_32, 4, 12, 1, 1);


        gridLayout_72->addLayout(gridLayout_73, 1, 0, 1, 1);

        scrollArea_14->setWidget(scrollAreaWidgetContents_18);

        gridLayout_9->addWidget(scrollArea_14, 1, 0, 1, 2);


        gridLayout_70->addLayout(gridLayout_9, 0, 0, 1, 1);

        tabWidget_2->addTab(tab_11, QString());
        Node_1 = new QWidget();
        Node_1->setObjectName(QStringLiteral("Node_1"));
        QPalette palette2;
        palette2.setBrush(QPalette::Active, QPalette::Text, brush);
        palette2.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette2.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette2.setBrush(QPalette::Disabled, QPalette::Text, brush6);
        palette2.setBrush(QPalette::Disabled, QPalette::ButtonText, brush12);
        Node_1->setPalette(palette2);
        gridLayout_15 = new QGridLayout(Node_1);
        gridLayout_15->setSpacing(6);
        gridLayout_15->setContentsMargins(11, 11, 11, 11);
        gridLayout_15->setObjectName(QStringLiteral("gridLayout_15"));
        Layout_leftSide = new QGridLayout();
        Layout_leftSide->setSpacing(6);
        Layout_leftSide->setObjectName(QStringLiteral("Layout_leftSide"));
        Layout_TitleBar = new QHBoxLayout();
        Layout_TitleBar->setSpacing(6);
        Layout_TitleBar->setObjectName(QStringLiteral("Layout_TitleBar"));
        horizontalSpacer_4 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        Layout_TitleBar->addItem(horizontalSpacer_4);

        label_ID_node1 = new QLabel(Node_1);
        label_ID_node1->setObjectName(QStringLiteral("label_ID_node1"));
        sizePolicy.setHeightForWidth(label_ID_node1->sizePolicy().hasHeightForWidth());
        label_ID_node1->setSizePolicy(sizePolicy);
        label_ID_node1->setMaximumSize(QSize(16777215, 16777215));
        label_ID_node1->setFont(font);
        label_ID_node1->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        Layout_TitleBar->addWidget(label_ID_node1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        Layout_TitleBar->addItem(horizontalSpacer_3);

        label_name_node1 = new QLabel(Node_1);
        label_name_node1->setObjectName(QStringLiteral("label_name_node1"));
        label_name_node1->setFont(font);
        label_name_node1->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        Layout_TitleBar->addWidget(label_name_node1);

        horizontalSpacer_278 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        Layout_TitleBar->addItem(horizontalSpacer_278);

        horizontalSpacer_279 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        Layout_TitleBar->addItem(horizontalSpacer_279);

        label_start_node1 = new QLabel(Node_1);
        label_start_node1->setObjectName(QStringLiteral("label_start_node1"));
        sizePolicy1.setHeightForWidth(label_start_node1->sizePolicy().hasHeightForWidth());
        label_start_node1->setSizePolicy(sizePolicy1);
        label_start_node1->setFont(font);
        label_start_node1->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        Layout_TitleBar->addWidget(label_start_node1);

        label_msStart_node1 = new QLabel(Node_1);
        label_msStart_node1->setObjectName(QStringLiteral("label_msStart_node1"));
        label_msStart_node1->setFont(font1);
        label_msStart_node1->setAlignment(Qt::AlignCenter);

        Layout_TitleBar->addWidget(label_msStart_node1);

        horizontalSpacer_280 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        Layout_TitleBar->addItem(horizontalSpacer_280);

        label_duration_node1 = new QLabel(Node_1);
        label_duration_node1->setObjectName(QStringLiteral("label_duration_node1"));
        label_duration_node1->setFont(font);
        label_duration_node1->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        Layout_TitleBar->addWidget(label_duration_node1);

        label_msDuration_node1 = new QLabel(Node_1);
        label_msDuration_node1->setObjectName(QStringLiteral("label_msDuration_node1"));
        label_msDuration_node1->setFont(font1);
        label_msDuration_node1->setAlignment(Qt::AlignCenter);

        Layout_TitleBar->addWidget(label_msDuration_node1);

        horizontalSpacer_281 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        Layout_TitleBar->addItem(horizontalSpacer_281);


        Layout_leftSide->addLayout(Layout_TitleBar, 0, 0, 1, 1);

        scrollArea_node1 = new QScrollArea(Node_1);
        scrollArea_node1->setObjectName(QStringLiteral("scrollArea_node1"));
        QPalette palette3;
        palette3.setBrush(QPalette::Active, QPalette::Window, brush6);
        palette3.setBrush(QPalette::Inactive, QPalette::Window, brush14);
        palette3.setBrush(QPalette::Disabled, QPalette::Window, brush14);
        scrollArea_node1->setPalette(palette3);
        scrollArea_node1->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        scrollArea_node1->setWidgetResizable(true);
        scrollAreaWidgetContents_19 = new QWidget();
        scrollAreaWidgetContents_19->setObjectName(QStringLiteral("scrollAreaWidgetContents_19"));
        scrollAreaWidgetContents_19->setGeometry(QRect(0, 0, 558, 880));
        gridLayout_75 = new QGridLayout(scrollAreaWidgetContents_19);
        gridLayout_75->setSpacing(6);
        gridLayout_75->setContentsMargins(11, 11, 11, 11);
        gridLayout_75->setObjectName(QStringLiteral("gridLayout_75"));
        gridLayout_76 = new QGridLayout();
        gridLayout_76->setSpacing(1);
        gridLayout_76->setObjectName(QStringLiteral("gridLayout_76"));
        spinBox_start5 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start5->setObjectName(QStringLiteral("spinBox_start5"));
        spinBox_start5->setEnabled(false);
        QFont font6;
        font6.setFamily(QStringLiteral("Titillium Web"));
        font6.setPointSize(12);
        spinBox_start5->setFont(font6);
        spinBox_start5->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start5, 4, 5, 1, 2);

        checkBox10 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox10->setObjectName(QStringLiteral("checkBox10"));
        checkBox10->setEnabled(true);
        checkBox10->setBaseSize(QSize(0, 0));
        checkBox10->setFont(font4);
        checkBox10->setTabletTracking(false);
        checkBox10->setAutoFillBackground(false);
        checkBox10->setChecked(false);
        checkBox10->setTristate(false);

        gridLayout_76->addWidget(checkBox10, 9, 0, 1, 1);

        horizontalSpacer_290 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_76->addItem(horizontalSpacer_290, 0, 7, 1, 1);

        checkBox22 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox22->setObjectName(QStringLiteral("checkBox22"));
        checkBox22->setEnabled(true);
        checkBox22->setBaseSize(QSize(0, 0));
        checkBox22->setFont(font4);
        checkBox22->setTabletTracking(false);
        checkBox22->setAutoFillBackground(false);
        checkBox22->setChecked(false);
        checkBox22->setTristate(false);

        gridLayout_76->addWidget(checkBox22, 22, 0, 1, 1);

        spinBox_start3 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start3->setObjectName(QStringLiteral("spinBox_start3"));
        spinBox_start3->setEnabled(false);
        spinBox_start3->setFont(font6);
        spinBox_start3->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start3, 2, 5, 1, 2);

        textField_name14 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name14->setObjectName(QStringLiteral("textField_name14"));
        textField_name14->setEnabled(false);
        textField_name14->setFont(font2);

        gridLayout_76->addWidget(textField_name14, 13, 3, 1, 1);

        checkBox18 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox18->setObjectName(QStringLiteral("checkBox18"));
        checkBox18->setEnabled(true);
        checkBox18->setBaseSize(QSize(0, 0));
        checkBox18->setFont(font4);
        checkBox18->setTabletTracking(false);
        checkBox18->setAutoFillBackground(false);
        checkBox18->setChecked(false);
        checkBox18->setTristate(false);

        gridLayout_76->addWidget(checkBox18, 17, 0, 1, 1);

        label_4 = new QLabel(scrollAreaWidgetContents_19);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setFont(font5);
        label_4->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_4, 3, 1, 1, 1);

        checkBox12 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox12->setObjectName(QStringLiteral("checkBox12"));
        checkBox12->setEnabled(true);
        checkBox12->setBaseSize(QSize(0, 0));
        checkBox12->setFont(font4);
        checkBox12->setTabletTracking(false);
        checkBox12->setAutoFillBackground(false);
        checkBox12->setChecked(false);
        checkBox12->setTristate(false);

        gridLayout_76->addWidget(checkBox12, 11, 0, 1, 1);

        label_19 = new QLabel(scrollAreaWidgetContents_19);
        label_19->setObjectName(QStringLiteral("label_19"));
        label_19->setFont(font5);
        label_19->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_19, 18, 1, 1, 1);

        textField_name5 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name5->setObjectName(QStringLiteral("textField_name5"));
        textField_name5->setEnabled(false);
        textField_name5->setFont(font2);

        gridLayout_76->addWidget(textField_name5, 4, 3, 1, 1);

        checkBox1 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox1->setObjectName(QStringLiteral("checkBox1"));
        checkBox1->setEnabled(true);
        checkBox1->setBaseSize(QSize(0, 0));
        checkBox1->setFont(font4);
        checkBox1->setTabletTracking(false);
        checkBox1->setAutoFillBackground(false);
        checkBox1->setChecked(false);
        checkBox1->setTristate(false);

        gridLayout_76->addWidget(checkBox1, 0, 0, 1, 1);

        label_15 = new QLabel(scrollAreaWidgetContents_19);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setFont(font5);
        label_15->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_15, 14, 1, 1, 1);

        textField_name16 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name16->setObjectName(QStringLiteral("textField_name16"));
        textField_name16->setEnabled(false);
        textField_name16->setFont(font2);

        gridLayout_76->addWidget(textField_name16, 15, 3, 1, 1);

        checkBox14 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox14->setObjectName(QStringLiteral("checkBox14"));
        checkBox14->setEnabled(true);
        checkBox14->setBaseSize(QSize(0, 0));
        checkBox14->setFont(font4);
        checkBox14->setTabletTracking(false);
        checkBox14->setAutoFillBackground(false);
        checkBox14->setChecked(false);
        checkBox14->setTristate(false);

        gridLayout_76->addWidget(checkBox14, 13, 0, 1, 1);

        spinBox_duration10 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration10->setObjectName(QStringLiteral("spinBox_duration10"));
        spinBox_duration10->setEnabled(false);
        spinBox_duration10->setFont(font6);
        spinBox_duration10->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration10, 9, 8, 1, 2);

        spinBox_start21 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start21->setObjectName(QStringLiteral("spinBox_start21"));
        spinBox_start21->setEnabled(false);
        spinBox_start21->setFont(font6);
        spinBox_start21->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start21, 21, 5, 1, 2);

        circleInactive13 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive13->setObjectName(QStringLiteral("circleInactive13"));
        circleInactive13->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive13->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive13, 12, 2, 1, 1);

        checkBox11 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox11->setObjectName(QStringLiteral("checkBox11"));
        checkBox11->setEnabled(true);
        checkBox11->setBaseSize(QSize(0, 0));
        checkBox11->setFont(font4);
        checkBox11->setTabletTracking(false);
        checkBox11->setAutoFillBackground(false);
        checkBox11->setChecked(false);
        checkBox11->setTristate(false);

        gridLayout_76->addWidget(checkBox11, 10, 0, 1, 1);

        label_14 = new QLabel(scrollAreaWidgetContents_19);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setFont(font5);
        label_14->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_14, 13, 1, 1, 1);

        checkBox5 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox5->setObjectName(QStringLiteral("checkBox5"));
        checkBox5->setEnabled(true);
        checkBox5->setBaseSize(QSize(0, 0));
        checkBox5->setFont(font4);
        checkBox5->setTabletTracking(false);
        checkBox5->setAutoFillBackground(false);
        checkBox5->setChecked(false);
        checkBox5->setTristate(false);

        gridLayout_76->addWidget(checkBox5, 4, 0, 1, 1);

        checkBox4 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox4->setObjectName(QStringLiteral("checkBox4"));
        checkBox4->setEnabled(true);
        checkBox4->setBaseSize(QSize(0, 0));
        checkBox4->setFont(font4);
        checkBox4->setTabletTracking(false);
        checkBox4->setAutoFillBackground(false);
        checkBox4->setChecked(false);
        checkBox4->setTristate(false);

        gridLayout_76->addWidget(checkBox4, 3, 0, 1, 1);

        spinBox_duration11 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration11->setObjectName(QStringLiteral("spinBox_duration11"));
        spinBox_duration11->setEnabled(false);
        spinBox_duration11->setFont(font6);
        spinBox_duration11->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration11, 10, 8, 1, 2);

        label_16 = new QLabel(scrollAreaWidgetContents_19);
        label_16->setObjectName(QStringLiteral("label_16"));
        label_16->setFont(font5);
        label_16->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_16, 15, 1, 1, 1);

        circleInactive3 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive3->setObjectName(QStringLiteral("circleInactive3"));
        circleInactive3->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive3->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive3, 2, 2, 1, 1);

        label_6 = new QLabel(scrollAreaWidgetContents_19);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setFont(font5);
        label_6->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_6, 5, 1, 1, 1);

        spinBox_start11 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start11->setObjectName(QStringLiteral("spinBox_start11"));
        spinBox_start11->setEnabled(false);
        spinBox_start11->setFont(font6);
        spinBox_start11->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start11, 10, 5, 1, 2);

        circleInactive1 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive1->setObjectName(QStringLiteral("circleInactive1"));
        circleInactive1->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive1->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive1, 0, 2, 1, 1);

        label_7 = new QLabel(scrollAreaWidgetContents_19);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setFont(font5);
        label_7->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_7, 6, 1, 1, 1);

        spinBox_duration6 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration6->setObjectName(QStringLiteral("spinBox_duration6"));
        spinBox_duration6->setEnabled(false);
        spinBox_duration6->setFont(font6);
        spinBox_duration6->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration6, 5, 8, 1, 2);

        textField_name8 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name8->setObjectName(QStringLiteral("textField_name8"));
        textField_name8->setEnabled(false);
        textField_name8->setFont(font2);

        gridLayout_76->addWidget(textField_name8, 7, 3, 1, 1);

        textField_name2 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name2->setObjectName(QStringLiteral("textField_name2"));
        textField_name2->setEnabled(false);
        textField_name2->setFont(font2);

        gridLayout_76->addWidget(textField_name2, 1, 3, 1, 1);

        spinBox_start8 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start8->setObjectName(QStringLiteral("spinBox_start8"));
        spinBox_start8->setEnabled(false);
        spinBox_start8->setFont(font6);
        spinBox_start8->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start8, 7, 5, 1, 2);

        spinBox_duration15 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration15->setObjectName(QStringLiteral("spinBox_duration15"));
        spinBox_duration15->setEnabled(false);
        spinBox_duration15->setFont(font6);
        spinBox_duration15->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration15, 14, 8, 1, 2);

        textField_name24 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name24->setObjectName(QStringLiteral("textField_name24"));
        textField_name24->setEnabled(false);
        textField_name24->setFont(font2);

        gridLayout_76->addWidget(textField_name24, 24, 3, 1, 1);

        textField_name7 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name7->setObjectName(QStringLiteral("textField_name7"));
        textField_name7->setEnabled(false);
        textField_name7->setFont(font2);

        gridLayout_76->addWidget(textField_name7, 6, 3, 1, 1);

        spinBox_start23 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start23->setObjectName(QStringLiteral("spinBox_start23"));
        spinBox_start23->setEnabled(false);
        spinBox_start23->setFont(font6);
        spinBox_start23->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start23, 23, 5, 1, 2);

        label_18 = new QLabel(scrollAreaWidgetContents_19);
        label_18->setObjectName(QStringLiteral("label_18"));
        label_18->setFont(font5);
        label_18->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_18, 17, 1, 1, 1);

        spinBox_start19 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start19->setObjectName(QStringLiteral("spinBox_start19"));
        spinBox_start19->setEnabled(false);
        spinBox_start19->setFont(font6);
        spinBox_start19->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start19, 18, 5, 1, 2);

        spinBox_duration12 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration12->setObjectName(QStringLiteral("spinBox_duration12"));
        spinBox_duration12->setEnabled(false);
        spinBox_duration12->setFont(font6);
        spinBox_duration12->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration12, 11, 8, 1, 2);

        circleInactive17 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive17->setObjectName(QStringLiteral("circleInactive17"));
        circleInactive17->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive17->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive17, 16, 2, 1, 1);

        textField_name18 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name18->setObjectName(QStringLiteral("textField_name18"));
        textField_name18->setEnabled(false);
        textField_name18->setFont(font2);

        gridLayout_76->addWidget(textField_name18, 17, 3, 1, 1);

        checkBox8 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox8->setObjectName(QStringLiteral("checkBox8"));
        checkBox8->setEnabled(true);
        checkBox8->setBaseSize(QSize(0, 0));
        checkBox8->setFont(font4);
        checkBox8->setTabletTracking(false);
        checkBox8->setAutoFillBackground(false);
        checkBox8->setChecked(false);
        checkBox8->setTristate(false);

        gridLayout_76->addWidget(checkBox8, 7, 0, 1, 1);

        spinBox_start13 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start13->setObjectName(QStringLiteral("spinBox_start13"));
        spinBox_start13->setEnabled(false);
        spinBox_start13->setFont(font6);
        spinBox_start13->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start13, 12, 5, 1, 2);

        checkBox21 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox21->setObjectName(QStringLiteral("checkBox21"));
        checkBox21->setEnabled(true);
        checkBox21->setBaseSize(QSize(0, 0));
        checkBox21->setFont(font4);
        checkBox21->setTabletTracking(false);
        checkBox21->setAutoFillBackground(false);
        checkBox21->setChecked(false);
        checkBox21->setTristate(false);

        gridLayout_76->addWidget(checkBox21, 21, 0, 1, 1);

        checkBox2 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox2->setObjectName(QStringLiteral("checkBox2"));
        checkBox2->setEnabled(true);
        checkBox2->setBaseSize(QSize(0, 0));
        checkBox2->setFont(font4);
        checkBox2->setTabletTracking(false);
        checkBox2->setAutoFillBackground(false);
        checkBox2->setChecked(false);
        checkBox2->setTristate(false);

        gridLayout_76->addWidget(checkBox2, 1, 0, 1, 1);

        label_21 = new QLabel(scrollAreaWidgetContents_19);
        label_21->setObjectName(QStringLiteral("label_21"));
        label_21->setFont(font5);
        label_21->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_21, 21, 1, 1, 1);

        spinBox_duration8 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration8->setObjectName(QStringLiteral("spinBox_duration8"));
        spinBox_duration8->setEnabled(false);
        spinBox_duration8->setFont(font6);
        spinBox_duration8->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration8, 7, 8, 1, 2);

        textField_name21 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name21->setObjectName(QStringLiteral("textField_name21"));
        textField_name21->setEnabled(false);
        textField_name21->setFont(font2);

        gridLayout_76->addWidget(textField_name21, 21, 3, 1, 1);

        label_5 = new QLabel(scrollAreaWidgetContents_19);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setFont(font5);
        label_5->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_5, 4, 1, 1, 1);

        label_23 = new QLabel(scrollAreaWidgetContents_19);
        label_23->setObjectName(QStringLiteral("label_23"));
        label_23->setFont(font5);
        label_23->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_23, 23, 1, 1, 1);

        checkBox20 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox20->setObjectName(QStringLiteral("checkBox20"));
        checkBox20->setEnabled(true);
        checkBox20->setBaseSize(QSize(0, 0));
        checkBox20->setFont(font4);
        checkBox20->setTabletTracking(false);
        checkBox20->setAutoFillBackground(false);
        checkBox20->setChecked(false);
        checkBox20->setTristate(false);

        gridLayout_76->addWidget(checkBox20, 20, 0, 1, 1);

        spinBox_duration19 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration19->setObjectName(QStringLiteral("spinBox_duration19"));
        spinBox_duration19->setEnabled(false);
        spinBox_duration19->setFont(font6);
        spinBox_duration19->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration19, 18, 8, 1, 2);

        spinBox_duration16 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration16->setObjectName(QStringLiteral("spinBox_duration16"));
        spinBox_duration16->setEnabled(false);
        spinBox_duration16->setFont(font6);
        spinBox_duration16->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration16, 15, 8, 1, 2);

        spinBox_start17 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start17->setObjectName(QStringLiteral("spinBox_start17"));
        spinBox_start17->setEnabled(false);
        spinBox_start17->setFont(font6);
        spinBox_start17->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start17, 16, 5, 1, 2);

        circleInactive12 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive12->setObjectName(QStringLiteral("circleInactive12"));
        circleInactive12->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive12->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive12, 11, 2, 1, 1);

        circleInactive15 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive15->setObjectName(QStringLiteral("circleInactive15"));
        circleInactive15->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive15->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive15, 14, 2, 1, 1);

        spinBox_start4 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start4->setObjectName(QStringLiteral("spinBox_start4"));
        spinBox_start4->setEnabled(false);
        spinBox_start4->setFont(font6);
        spinBox_start4->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start4, 3, 5, 1, 2);

        circleInactive14 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive14->setObjectName(QStringLiteral("circleInactive14"));
        circleInactive14->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive14->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive14, 13, 2, 1, 1);

        spinBox_start20 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start20->setObjectName(QStringLiteral("spinBox_start20"));
        spinBox_start20->setEnabled(false);
        spinBox_start20->setFont(font6);
        spinBox_start20->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start20, 20, 5, 1, 2);

        label_9 = new QLabel(scrollAreaWidgetContents_19);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setFont(font5);
        label_9->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_9, 8, 1, 1, 1);

        checkBox16 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox16->setObjectName(QStringLiteral("checkBox16"));
        checkBox16->setEnabled(true);
        checkBox16->setBaseSize(QSize(0, 0));
        checkBox16->setFont(font4);
        checkBox16->setTabletTracking(false);
        checkBox16->setAutoFillBackground(false);
        checkBox16->setChecked(false);
        checkBox16->setTristate(false);

        gridLayout_76->addWidget(checkBox16, 15, 0, 1, 1);

        label_20 = new QLabel(scrollAreaWidgetContents_19);
        label_20->setObjectName(QStringLiteral("label_20"));
        label_20->setFont(font5);
        label_20->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_20, 20, 1, 1, 1);

        textField_name22 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name22->setObjectName(QStringLiteral("textField_name22"));
        textField_name22->setEnabled(false);
        textField_name22->setFont(font2);

        gridLayout_76->addWidget(textField_name22, 22, 3, 1, 1);

        circleInactive21 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive21->setObjectName(QStringLiteral("circleInactive21"));
        circleInactive21->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive21->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive21, 21, 2, 1, 1);

        spinBox_duration3 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration3->setObjectName(QStringLiteral("spinBox_duration3"));
        spinBox_duration3->setEnabled(false);
        spinBox_duration3->setFont(font6);
        spinBox_duration3->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration3, 2, 8, 1, 2);

        spinBox_start7 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start7->setObjectName(QStringLiteral("spinBox_start7"));
        spinBox_start7->setEnabled(false);
        spinBox_start7->setFont(font6);
        spinBox_start7->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start7, 6, 5, 1, 2);

        textField_name13 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name13->setObjectName(QStringLiteral("textField_name13"));
        textField_name13->setEnabled(false);
        textField_name13->setFont(font2);

        gridLayout_76->addWidget(textField_name13, 12, 3, 1, 1);

        label_24 = new QLabel(scrollAreaWidgetContents_19);
        label_24->setObjectName(QStringLiteral("label_24"));
        label_24->setFont(font5);
        label_24->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_24, 24, 1, 1, 1);

        checkBox19 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox19->setObjectName(QStringLiteral("checkBox19"));
        checkBox19->setEnabled(true);
        checkBox19->setBaseSize(QSize(0, 0));
        checkBox19->setFont(font4);
        checkBox19->setTabletTracking(false);
        checkBox19->setAutoFillBackground(false);
        checkBox19->setChecked(false);
        checkBox19->setTristate(false);

        gridLayout_76->addWidget(checkBox19, 18, 0, 1, 1);

        label_8 = new QLabel(scrollAreaWidgetContents_19);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setFont(font5);
        label_8->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_8, 7, 1, 1, 1);

        circleInactive23 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive23->setObjectName(QStringLiteral("circleInactive23"));
        circleInactive23->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive23->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive23, 23, 2, 1, 1);

        circleInactive7 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive7->setObjectName(QStringLiteral("circleInactive7"));
        circleInactive7->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive7->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive7, 6, 2, 1, 1);

        spinBox_duration14 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration14->setObjectName(QStringLiteral("spinBox_duration14"));
        spinBox_duration14->setEnabled(false);
        spinBox_duration14->setFont(font6);
        spinBox_duration14->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration14, 13, 8, 1, 2);

        textField_name19 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name19->setObjectName(QStringLiteral("textField_name19"));
        textField_name19->setEnabled(false);
        textField_name19->setFont(font2);

        gridLayout_76->addWidget(textField_name19, 18, 3, 1, 1);

        circleInactive16 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive16->setObjectName(QStringLiteral("circleInactive16"));
        circleInactive16->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive16->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive16, 15, 2, 1, 1);

        checkBox17 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox17->setObjectName(QStringLiteral("checkBox17"));
        checkBox17->setEnabled(true);
        checkBox17->setBaseSize(QSize(0, 0));
        checkBox17->setFont(font4);
        checkBox17->setTabletTracking(false);
        checkBox17->setAutoFillBackground(false);
        checkBox17->setChecked(false);
        checkBox17->setTristate(false);

        gridLayout_76->addWidget(checkBox17, 16, 0, 1, 1);

        spinBox_start9 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start9->setObjectName(QStringLiteral("spinBox_start9"));
        spinBox_start9->setEnabled(false);
        spinBox_start9->setFont(font6);
        spinBox_start9->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start9, 8, 5, 1, 2);

        label_11 = new QLabel(scrollAreaWidgetContents_19);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setFont(font5);
        label_11->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_11, 10, 1, 1, 1);

        checkBox3 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox3->setObjectName(QStringLiteral("checkBox3"));
        checkBox3->setEnabled(true);
        checkBox3->setBaseSize(QSize(0, 0));
        checkBox3->setFont(font4);
        checkBox3->setTabletTracking(false);
        checkBox3->setAutoFillBackground(false);
        checkBox3->setChecked(false);
        checkBox3->setTristate(false);

        gridLayout_76->addWidget(checkBox3, 2, 0, 1, 1);

        spinBox_start16 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start16->setObjectName(QStringLiteral("spinBox_start16"));
        spinBox_start16->setEnabled(false);
        spinBox_start16->setFont(font6);
        spinBox_start16->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start16, 15, 5, 1, 2);

        spinBox_duration5 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration5->setObjectName(QStringLiteral("spinBox_duration5"));
        spinBox_duration5->setEnabled(false);
        spinBox_duration5->setFont(font6);
        spinBox_duration5->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration5, 4, 8, 1, 2);

        circleInactive22 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive22->setObjectName(QStringLiteral("circleInactive22"));
        circleInactive22->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive22->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive22, 22, 2, 1, 1);

        textField_name23 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name23->setObjectName(QStringLiteral("textField_name23"));
        textField_name23->setEnabled(false);
        textField_name23->setFont(font2);

        gridLayout_76->addWidget(textField_name23, 23, 3, 1, 1);

        textField_name20 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name20->setObjectName(QStringLiteral("textField_name20"));
        textField_name20->setEnabled(false);
        textField_name20->setFont(font2);

        gridLayout_76->addWidget(textField_name20, 20, 3, 1, 1);

        spinBox_duration1 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration1->setObjectName(QStringLiteral("spinBox_duration1"));
        spinBox_duration1->setEnabled(false);
        spinBox_duration1->setFont(font6);
        spinBox_duration1->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration1, 0, 8, 1, 2);

        spinBox_duration24 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration24->setObjectName(QStringLiteral("spinBox_duration24"));
        spinBox_duration24->setEnabled(false);
        spinBox_duration24->setFont(font6);
        spinBox_duration24->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration24, 24, 8, 1, 2);

        spinBox_duration20 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration20->setObjectName(QStringLiteral("spinBox_duration20"));
        spinBox_duration20->setEnabled(false);
        spinBox_duration20->setFont(font6);
        spinBox_duration20->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration20, 20, 8, 1, 2);

        circleInactive10 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive10->setObjectName(QStringLiteral("circleInactive10"));
        circleInactive10->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive10->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive10, 9, 2, 1, 1);

        spinBox_duration23 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration23->setObjectName(QStringLiteral("spinBox_duration23"));
        spinBox_duration23->setEnabled(false);
        spinBox_duration23->setFont(font6);
        spinBox_duration23->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration23, 23, 8, 1, 2);

        label_10 = new QLabel(scrollAreaWidgetContents_19);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setFont(font5);
        label_10->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_10, 9, 1, 1, 1);

        circleInactive8 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive8->setObjectName(QStringLiteral("circleInactive8"));
        circleInactive8->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive8->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive8, 7, 2, 1, 1);

        circleInactive2 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive2->setObjectName(QStringLiteral("circleInactive2"));
        circleInactive2->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive2->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive2, 1, 2, 1, 1);

        textField_name12 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name12->setObjectName(QStringLiteral("textField_name12"));
        textField_name12->setEnabled(false);
        textField_name12->setFont(font2);

        gridLayout_76->addWidget(textField_name12, 11, 3, 1, 1);

        textField_name3 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name3->setObjectName(QStringLiteral("textField_name3"));
        textField_name3->setEnabled(false);
        textField_name3->setFont(font2);

        gridLayout_76->addWidget(textField_name3, 2, 3, 1, 1);

        spinBox_duration9 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration9->setObjectName(QStringLiteral("spinBox_duration9"));
        spinBox_duration9->setEnabled(false);
        spinBox_duration9->setFont(font6);
        spinBox_duration9->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration9, 8, 8, 1, 2);

        horizontalSpacer_289 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_76->addItem(horizontalSpacer_289, 29, 2, 1, 1);

        textField_name1 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name1->setObjectName(QStringLiteral("textField_name1"));
        textField_name1->setEnabled(false);
        textField_name1->setFont(font2);

        gridLayout_76->addWidget(textField_name1, 0, 3, 1, 1);

        spinBox_start2 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start2->setObjectName(QStringLiteral("spinBox_start2"));
        spinBox_start2->setEnabled(false);
        spinBox_start2->setFont(font6);
        spinBox_start2->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start2, 1, 5, 1, 2);

        checkBox15 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox15->setObjectName(QStringLiteral("checkBox15"));
        checkBox15->setEnabled(true);
        checkBox15->setBaseSize(QSize(0, 0));
        checkBox15->setFont(font4);
        checkBox15->setTabletTracking(false);
        checkBox15->setAutoFillBackground(false);
        checkBox15->setChecked(false);
        checkBox15->setTristate(false);

        gridLayout_76->addWidget(checkBox15, 14, 0, 1, 1);

        checkBox24 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox24->setObjectName(QStringLiteral("checkBox24"));
        checkBox24->setEnabled(true);
        checkBox24->setBaseSize(QSize(0, 0));
        checkBox24->setFont(font4);
        checkBox24->setTabletTracking(false);
        checkBox24->setAutoFillBackground(false);
        checkBox24->setChecked(false);
        checkBox24->setTristate(false);

        gridLayout_76->addWidget(checkBox24, 24, 0, 1, 1);

        textField_name15 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name15->setObjectName(QStringLiteral("textField_name15"));
        textField_name15->setEnabled(false);
        textField_name15->setFont(font2);

        gridLayout_76->addWidget(textField_name15, 14, 3, 1, 1);

        spinBox_duration18 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration18->setObjectName(QStringLiteral("spinBox_duration18"));
        spinBox_duration18->setEnabled(false);
        spinBox_duration18->setFont(font6);
        spinBox_duration18->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration18, 17, 8, 1, 2);

        horizontalSpacer_287 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_76->addItem(horizontalSpacer_287, 10, 4, 1, 1);

        spinBox_duration22 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration22->setObjectName(QStringLiteral("spinBox_duration22"));
        spinBox_duration22->setEnabled(false);
        spinBox_duration22->setFont(font6);
        spinBox_duration22->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration22, 22, 8, 1, 2);

        circleInactive11 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive11->setObjectName(QStringLiteral("circleInactive11"));
        circleInactive11->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive11->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive11, 10, 2, 1, 1);

        spinBox_start24 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start24->setObjectName(QStringLiteral("spinBox_start24"));
        spinBox_start24->setEnabled(false);
        spinBox_start24->setFont(font6);
        spinBox_start24->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start24, 24, 5, 1, 2);

        spinBox_start14 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start14->setObjectName(QStringLiteral("spinBox_start14"));
        spinBox_start14->setEnabled(false);
        spinBox_start14->setFont(font6);
        spinBox_start14->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start14, 13, 5, 1, 2);

        spinBox_start6 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start6->setObjectName(QStringLiteral("spinBox_start6"));
        spinBox_start6->setEnabled(false);
        spinBox_start6->setFont(font6);
        spinBox_start6->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start6, 5, 5, 1, 2);

        label_3 = new QLabel(scrollAreaWidgetContents_19);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setFont(font5);
        label_3->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_3, 2, 1, 1, 1);

        horizontalSpacer_288 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_76->addItem(horizontalSpacer_288, 10, 7, 1, 1);

        checkBox7 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox7->setObjectName(QStringLiteral("checkBox7"));
        checkBox7->setEnabled(true);
        checkBox7->setBaseSize(QSize(0, 0));
        checkBox7->setFont(font4);
        checkBox7->setTabletTracking(false);
        checkBox7->setAutoFillBackground(false);
        checkBox7->setChecked(false);
        checkBox7->setTristate(false);

        gridLayout_76->addWidget(checkBox7, 6, 0, 1, 1);

        spinBox_start10 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start10->setObjectName(QStringLiteral("spinBox_start10"));
        spinBox_start10->setEnabled(false);
        spinBox_start10->setFont(font6);
        spinBox_start10->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start10, 9, 5, 1, 2);

        spinBox_duration4 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration4->setObjectName(QStringLiteral("spinBox_duration4"));
        spinBox_duration4->setEnabled(false);
        spinBox_duration4->setFont(font6);
        spinBox_duration4->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration4, 3, 8, 1, 2);

        spinBox_start18 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start18->setObjectName(QStringLiteral("spinBox_start18"));
        spinBox_start18->setEnabled(false);
        spinBox_start18->setFont(font6);
        spinBox_start18->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start18, 17, 5, 1, 2);

        horizontalSpacer_286 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_76->addItem(horizontalSpacer_286, 0, 11, 1, 1);

        circleInactive9 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive9->setObjectName(QStringLiteral("circleInactive9"));
        QPalette palette4;
        palette4.setBrush(QPalette::Active, QPalette::Button, brush6);
        palette4.setBrush(QPalette::Inactive, QPalette::Button, brush6);
        palette4.setBrush(QPalette::Disabled, QPalette::Button, brush6);
        circleInactive9->setPalette(palette4);
        circleInactive9->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive9->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive9, 8, 2, 1, 1);

        checkBox13 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox13->setObjectName(QStringLiteral("checkBox13"));
        checkBox13->setEnabled(true);
        checkBox13->setBaseSize(QSize(0, 0));
        checkBox13->setFont(font4);
        checkBox13->setTabletTracking(false);
        checkBox13->setAutoFillBackground(false);
        checkBox13->setChecked(false);
        checkBox13->setTristate(false);

        gridLayout_76->addWidget(checkBox13, 12, 0, 1, 1);

        spinBox_duration7 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration7->setObjectName(QStringLiteral("spinBox_duration7"));
        spinBox_duration7->setEnabled(false);
        spinBox_duration7->setFont(font6);
        spinBox_duration7->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration7, 6, 8, 1, 2);

        label_12 = new QLabel(scrollAreaWidgetContents_19);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setFont(font5);
        label_12->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_12, 11, 1, 1, 1);

        label_13 = new QLabel(scrollAreaWidgetContents_19);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setFont(font5);
        label_13->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_13, 12, 1, 1, 1);

        label_2 = new QLabel(scrollAreaWidgetContents_19);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setFont(font5);
        label_2->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_2, 1, 1, 1, 1);

        circleInactive6 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive6->setObjectName(QStringLiteral("circleInactive6"));
        circleInactive6->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive6->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive6, 5, 2, 1, 1);

        textField_name10 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name10->setObjectName(QStringLiteral("textField_name10"));
        textField_name10->setEnabled(false);
        textField_name10->setFont(font2);

        gridLayout_76->addWidget(textField_name10, 9, 3, 1, 1);

        circleInactive20 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive20->setObjectName(QStringLiteral("circleInactive20"));
        circleInactive20->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive20->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive20, 20, 2, 1, 1);

        circleInactive4 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive4->setObjectName(QStringLiteral("circleInactive4"));
        circleInactive4->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive4->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive4, 3, 2, 1, 1);

        circleInactive19 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive19->setObjectName(QStringLiteral("circleInactive19"));
        circleInactive19->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive19->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive19, 18, 2, 1, 1);

        spinBox_start22 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start22->setObjectName(QStringLiteral("spinBox_start22"));
        spinBox_start22->setEnabled(false);
        spinBox_start22->setFont(font6);
        spinBox_start22->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start22, 22, 5, 1, 2);

        spinBox_start12 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start12->setObjectName(QStringLiteral("spinBox_start12"));
        spinBox_start12->setEnabled(false);
        spinBox_start12->setFont(font6);
        spinBox_start12->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start12, 11, 5, 1, 2);

        spinBox_duration2 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration2->setObjectName(QStringLiteral("spinBox_duration2"));
        spinBox_duration2->setEnabled(false);
        spinBox_duration2->setFont(font6);
        spinBox_duration2->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration2, 1, 8, 1, 2);

        spinBox_duration17 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration17->setObjectName(QStringLiteral("spinBox_duration17"));
        spinBox_duration17->setEnabled(false);
        spinBox_duration17->setFont(font6);
        spinBox_duration17->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration17, 16, 8, 1, 2);

        circleInactive5 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive5->setObjectName(QStringLiteral("circleInactive5"));
        circleInactive5->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive5->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive5, 4, 2, 1, 1);

        spinBox_duration21 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration21->setObjectName(QStringLiteral("spinBox_duration21"));
        spinBox_duration21->setEnabled(false);
        spinBox_duration21->setFont(font6);
        spinBox_duration21->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration21, 21, 8, 1, 2);

        checkBox9 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox9->setObjectName(QStringLiteral("checkBox9"));
        checkBox9->setEnabled(true);
        checkBox9->setBaseSize(QSize(0, 0));
        checkBox9->setFont(font4);
        checkBox9->setTabletTracking(false);
        checkBox9->setAutoFillBackground(false);
        checkBox9->setChecked(false);
        checkBox9->setTristate(false);

        gridLayout_76->addWidget(checkBox9, 8, 0, 1, 1);

        spinBox_duration13 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_duration13->setObjectName(QStringLiteral("spinBox_duration13"));
        spinBox_duration13->setEnabled(false);
        spinBox_duration13->setFont(font6);
        spinBox_duration13->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_duration13, 12, 8, 1, 2);

        textField_name17 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name17->setObjectName(QStringLiteral("textField_name17"));
        textField_name17->setEnabled(false);
        textField_name17->setFont(font2);

        gridLayout_76->addWidget(textField_name17, 16, 3, 1, 1);

        spinBox_start15 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start15->setObjectName(QStringLiteral("spinBox_start15"));
        spinBox_start15->setEnabled(false);
        spinBox_start15->setFont(font6);
        spinBox_start15->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start15, 14, 5, 1, 2);

        checkBox6 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox6->setObjectName(QStringLiteral("checkBox6"));
        checkBox6->setEnabled(true);
        checkBox6->setBaseSize(QSize(0, 0));
        checkBox6->setFont(font4);
        checkBox6->setTabletTracking(false);
        checkBox6->setAutoFillBackground(false);
        checkBox6->setChecked(false);
        checkBox6->setTristate(false);

        gridLayout_76->addWidget(checkBox6, 5, 0, 1, 1);

        textField_name9 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name9->setObjectName(QStringLiteral("textField_name9"));
        textField_name9->setEnabled(false);
        textField_name9->setFont(font2);

        gridLayout_76->addWidget(textField_name9, 8, 3, 1, 1);

        textField_name6 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name6->setObjectName(QStringLiteral("textField_name6"));
        textField_name6->setEnabled(false);
        textField_name6->setFont(font2);

        gridLayout_76->addWidget(textField_name6, 5, 3, 1, 1);

        spinBox_start1 = new QSpinBox(scrollAreaWidgetContents_19);
        spinBox_start1->setObjectName(QStringLiteral("spinBox_start1"));
        spinBox_start1->setEnabled(false);
        spinBox_start1->setFont(font6);
        spinBox_start1->setMaximum(10000000);

        gridLayout_76->addWidget(spinBox_start1, 0, 5, 1, 2);

        label_1 = new QLabel(scrollAreaWidgetContents_19);
        label_1->setObjectName(QStringLiteral("label_1"));
        label_1->setFont(font5);
        label_1->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_1, 0, 1, 1, 1);

        textField_name11 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name11->setObjectName(QStringLiteral("textField_name11"));
        textField_name11->setEnabled(false);
        textField_name11->setFont(font2);

        gridLayout_76->addWidget(textField_name11, 10, 3, 1, 1);

        textField_name4 = new QLineEdit(scrollAreaWidgetContents_19);
        textField_name4->setObjectName(QStringLiteral("textField_name4"));
        textField_name4->setEnabled(false);
        textField_name4->setFont(font2);

        gridLayout_76->addWidget(textField_name4, 3, 3, 1, 1);

        circleInactive18 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive18->setObjectName(QStringLiteral("circleInactive18"));
        circleInactive18->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive18->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive18, 17, 2, 1, 1);

        label_17 = new QLabel(scrollAreaWidgetContents_19);
        label_17->setObjectName(QStringLiteral("label_17"));
        label_17->setFont(font5);
        label_17->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_17, 16, 1, 1, 1);

        label_22 = new QLabel(scrollAreaWidgetContents_19);
        label_22->setObjectName(QStringLiteral("label_22"));
        label_22->setFont(font5);
        label_22->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_76->addWidget(label_22, 22, 1, 1, 1);

        checkBox23 = new QCheckBox(scrollAreaWidgetContents_19);
        checkBox23->setObjectName(QStringLiteral("checkBox23"));
        checkBox23->setEnabled(true);
        checkBox23->setBaseSize(QSize(0, 0));
        checkBox23->setFont(font4);
        checkBox23->setTabletTracking(false);
        checkBox23->setAutoFillBackground(false);
        checkBox23->setChecked(false);
        checkBox23->setTristate(false);

        gridLayout_76->addWidget(checkBox23, 23, 0, 1, 1);

        circleInactive24 = new QLabel(scrollAreaWidgetContents_19);
        circleInactive24->setObjectName(QStringLiteral("circleInactive24"));
        circleInactive24->setPixmap(QPixmap(QString::fromUtf8("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/FeedbackCircles-02.png")));
        circleInactive24->setAlignment(Qt::AlignCenter);

        gridLayout_76->addWidget(circleInactive24, 24, 2, 1, 1);


        gridLayout_75->addLayout(gridLayout_76, 1, 0, 1, 1);

        scrollArea_node1->setWidget(scrollAreaWidgetContents_19);

        Layout_leftSide->addWidget(scrollArea_node1, 1, 0, 1, 1);


        gridLayout_15->addLayout(Layout_leftSide, 0, 0, 3, 1);

        Layout_TopRight = new QGridLayout();
        Layout_TopRight->setSpacing(6);
        Layout_TopRight->setObjectName(QStringLiteral("Layout_TopRight"));
        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        Layout_TopRight->addItem(verticalSpacer_4, 0, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        Layout_TopRight->addItem(horizontalSpacer_2, 2, 0, 1, 1);

        verticalSpacer_11 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        Layout_TopRight->addItem(verticalSpacer_11, 4, 1, 1, 1);

        pushButton_Help = new QPushButton(Node_1);
        pushButton_Help->setObjectName(QStringLiteral("pushButton_Help"));
        pushButton_Help->setMinimumSize(QSize(30, 30));
        QIcon icon;
        icon.addFile(QStringLiteral("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/questionMarkButton-11.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButton_Help->setIcon(icon);
        pushButton_Help->setIconSize(QSize(20, 20));

        Layout_TopRight->addWidget(pushButton_Help, 0, 3, 2, 1);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        Layout_TopRight->addItem(horizontalSpacer_5, 2, 2, 1, 1);

        armButton = new QPushButton(Node_1);
        armButton->setObjectName(QStringLiteral("armButton"));
        armButton->setEnabled(true);
        armButton->setText(QStringLiteral(""));
        QIcon icon1;
        icon1.addFile(QStringLiteral("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/disarmedButton.png"), QSize(), QIcon::Normal, QIcon::Off);
        icon1.addFile(QStringLiteral("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/armedButton.png"), QSize(), QIcon::Normal, QIcon::On);
        armButton->setIcon(icon1);
        armButton->setIconSize(QSize(299, 159));
        armButton->setCheckable(true);
        armButton->setChecked(false);

        Layout_TopRight->addWidget(armButton, 2, 1, 1, 1);

        verticalSpacer_7 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        Layout_TopRight->addItem(verticalSpacer_7, 3, 1, 1, 1);


        gridLayout_15->addLayout(Layout_TopRight, 0, 1, 1, 2);

        line_2 = new QFrame(Node_1);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        gridLayout_15->addWidget(line_2, 1, 1, 1, 2);

        Layout_PreviewNodeButtons = new QGridLayout();
        Layout_PreviewNodeButtons->setSpacing(6);
        Layout_PreviewNodeButtons->setObjectName(QStringLiteral("Layout_PreviewNodeButtons"));
        Layout_Preview = new QGridLayout();
        Layout_Preview->setSpacing(6);
        Layout_Preview->setObjectName(QStringLiteral("Layout_Preview"));
        groupBox_preview = new QGroupBox(Node_1);
        groupBox_preview->setObjectName(QStringLiteral("groupBox_preview"));
        QFont font7;
        font7.setFamily(QStringLiteral("Titillium Web"));
        font7.setPointSize(12);
        font7.setBold(false);
        font7.setWeight(50);
        groupBox_preview->setFont(font7);
        groupBox_preview->setAlignment(Qt::AlignCenter);
        groupBox_preview->setFlat(false);
        groupBox_preview->setCheckable(false);
        gridLayout_8 = new QGridLayout(groupBox_preview);
        gridLayout_8->setSpacing(6);
        gridLayout_8->setContentsMargins(11, 11, 11, 11);
        gridLayout_8->setObjectName(QStringLiteral("gridLayout_8"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label_80 = new QLabel(groupBox_preview);
        label_80->setObjectName(QStringLiteral("label_80"));
        QFont font8;
        font8.setFamily(QStringLiteral("Titillium Web"));
        font8.setItalic(true);
        label_80->setFont(font8);

        horizontalLayout->addWidget(label_80);

        spinBox_SlowMo = new QSpinBox(groupBox_preview);
        spinBox_SlowMo->setObjectName(QStringLiteral("spinBox_SlowMo"));
        spinBox_SlowMo->setMinimum(1);
        spinBox_SlowMo->setMaximum(10000);
        spinBox_SlowMo->setValue(1);

        horizontalLayout->addWidget(spinBox_SlowMo);


        gridLayout_8->addLayout(horizontalLayout, 0, 1, 1, 1);

        previewButton = new QPushButton(groupBox_preview);
        previewButton->setObjectName(QStringLiteral("previewButton"));
        previewButton->setMinimumSize(QSize(0, 80));
        QPalette palette5;
        palette5.setBrush(QPalette::Active, QPalette::WindowText, brush7);
        palette5.setBrush(QPalette::Active, QPalette::Button, brush6);
        palette5.setBrush(QPalette::Active, QPalette::Light, brush2);
        QBrush brush16(QColor(240, 244, 252, 255));
        brush16.setStyle(Qt::SolidPattern);
        palette5.setBrush(QPalette::Active, QPalette::Midlight, brush16);
        QBrush brush17(QColor(113, 117, 124, 255));
        brush17.setStyle(Qt::SolidPattern);
        palette5.setBrush(QPalette::Active, QPalette::Dark, brush17);
        QBrush brush18(QColor(151, 156, 166, 255));
        brush18.setStyle(Qt::SolidPattern);
        palette5.setBrush(QPalette::Active, QPalette::Mid, brush18);
        palette5.setBrush(QPalette::Active, QPalette::Text, brush7);
        palette5.setBrush(QPalette::Active, QPalette::BrightText, brush2);
        palette5.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette5.setBrush(QPalette::Active, QPalette::Base, brush2);
        palette5.setBrush(QPalette::Active, QPalette::Window, brush6);
        palette5.setBrush(QPalette::Active, QPalette::Shadow, brush7);
        palette5.setBrush(QPalette::Active, QPalette::AlternateBase, brush16);
        palette5.setBrush(QPalette::Active, QPalette::ToolTipBase, brush8);
        palette5.setBrush(QPalette::Active, QPalette::ToolTipText, brush7);
        palette5.setBrush(QPalette::Inactive, QPalette::WindowText, brush7);
        palette5.setBrush(QPalette::Inactive, QPalette::Button, brush6);
        palette5.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette5.setBrush(QPalette::Inactive, QPalette::Midlight, brush16);
        palette5.setBrush(QPalette::Inactive, QPalette::Dark, brush17);
        palette5.setBrush(QPalette::Inactive, QPalette::Mid, brush18);
        palette5.setBrush(QPalette::Inactive, QPalette::Text, brush7);
        palette5.setBrush(QPalette::Inactive, QPalette::BrightText, brush2);
        palette5.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette5.setBrush(QPalette::Inactive, QPalette::Base, brush2);
        palette5.setBrush(QPalette::Inactive, QPalette::Window, brush6);
        palette5.setBrush(QPalette::Inactive, QPalette::Shadow, brush7);
        palette5.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush16);
        palette5.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush8);
        palette5.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush7);
        palette5.setBrush(QPalette::Disabled, QPalette::WindowText, brush17);
        palette5.setBrush(QPalette::Disabled, QPalette::Button, brush6);
        palette5.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette5.setBrush(QPalette::Disabled, QPalette::Midlight, brush16);
        palette5.setBrush(QPalette::Disabled, QPalette::Dark, brush17);
        palette5.setBrush(QPalette::Disabled, QPalette::Mid, brush18);
        palette5.setBrush(QPalette::Disabled, QPalette::Text, brush17);
        palette5.setBrush(QPalette::Disabled, QPalette::BrightText, brush2);
        palette5.setBrush(QPalette::Disabled, QPalette::ButtonText, brush17);
        palette5.setBrush(QPalette::Disabled, QPalette::Base, brush6);
        palette5.setBrush(QPalette::Disabled, QPalette::Window, brush6);
        palette5.setBrush(QPalette::Disabled, QPalette::Shadow, brush7);
        palette5.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush6);
        palette5.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush8);
        palette5.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush7);
        previewButton->setPalette(palette5);
        previewButton->setFont(font2);

        gridLayout_8->addWidget(previewButton, 1, 1, 1, 1);


        Layout_Preview->addWidget(groupBox_preview, 0, 0, 1, 1);


        Layout_PreviewNodeButtons->addLayout(Layout_Preview, 1, 0, 1, 1);

        Layout_Node = new QGridLayout();
        Layout_Node->setSpacing(6);
        Layout_Node->setObjectName(QStringLiteral("Layout_Node"));
        groupBox_2 = new QGroupBox(Node_1);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setFont(font7);
        groupBox_2->setAlignment(Qt::AlignCenter);
        groupBox_2->setFlat(false);
        groupBox_2->setCheckable(false);
        gridLayout_6 = new QGridLayout(groupBox_2);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        writeConfigButton = new QPushButton(groupBox_2);
        writeConfigButton->setObjectName(QStringLiteral("writeConfigButton"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(writeConfigButton->sizePolicy().hasHeightForWidth());
        writeConfigButton->setSizePolicy(sizePolicy2);
        writeConfigButton->setMinimumSize(QSize(95, 60));
        writeConfigButton->setMaximumSize(QSize(16777215, 60));
        writeConfigButton->setFont(font2);

        horizontalLayout_2->addWidget(writeConfigButton);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_8);

        readConfigButton = new QPushButton(groupBox_2);
        readConfigButton->setObjectName(QStringLiteral("readConfigButton"));
        readConfigButton->setMinimumSize(QSize(95, 60));
        readConfigButton->setFont(font2);

        horizontalLayout_2->addWidget(readConfigButton);


        gridLayout_6->addLayout(horizontalLayout_2, 1, 1, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        startButton = new QPushButton(groupBox_2);
        startButton->setObjectName(QStringLiteral("startButton"));
        startButton->setEnabled(false);
        startButton->setMinimumSize(QSize(95, 60));
        startButton->setFont(font2);

        horizontalLayout_3->addWidget(startButton);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_9);

        stopResetButton = new QPushButton(groupBox_2);
        stopResetButton->setObjectName(QStringLiteral("stopResetButton"));
        stopResetButton->setMinimumSize(QSize(95, 60));
        stopResetButton->setFont(font2);

        horizontalLayout_3->addWidget(stopResetButton);


        gridLayout_6->addLayout(horizontalLayout_3, 0, 1, 1, 1);


        Layout_Node->addWidget(groupBox_2, 0, 0, 1, 1);


        Layout_PreviewNodeButtons->addLayout(Layout_Node, 2, 0, 1, 1);


        gridLayout_15->addLayout(Layout_PreviewNodeButtons, 2, 1, 1, 1);

        Layout_bottomRight = new QGridLayout();
        Layout_bottomRight->setSpacing(6);
        Layout_bottomRight->setObjectName(QStringLiteral("Layout_bottomRight"));
        horizontalSpacer = new QSpacerItem(13, 327, QSizePolicy::Expanding, QSizePolicy::Minimum);

        Layout_bottomRight->addItem(horizontalSpacer, 0, 0, 1, 1);

        gridLayout_4 = new QGridLayout();
        gridLayout_4->setSpacing(6);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        openButton = new QPushButton(Node_1);
        openButton->setObjectName(QStringLiteral("openButton"));
        openButton->setMinimumSize(QSize(0, 80));
        openButton->setFont(font2);
        QIcon icon2;
        icon2.addFile(QStringLiteral("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/openButton.png"), QSize(), QIcon::Normal, QIcon::On);
        openButton->setIcon(icon2);

        horizontalLayout_5->addWidget(openButton);

        saveButton = new QPushButton(Node_1);
        saveButton->setObjectName(QStringLiteral("saveButton"));
        saveButton->setMinimumSize(QSize(0, 80));
        saveButton->setFont(font2);
        QIcon icon3;
        icon3.addFile(QStringLiteral("../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/saveButton.png"), QSize(), QIcon::Normal, QIcon::On);
        saveButton->setIcon(icon3);

        horizontalLayout_5->addWidget(saveButton);


        gridLayout_4->addLayout(horizontalLayout_5, 0, 1, 1, 1);

        frame = new QFrame(Node_1);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayout_14 = new QGridLayout(frame);
        gridLayout_14->setSpacing(6);
        gridLayout_14->setContentsMargins(11, 11, 11, 11);
        gridLayout_14->setObjectName(QStringLiteral("gridLayout_14"));
        logoPlaceholder = new QLabel(frame);
        logoPlaceholder->setObjectName(QStringLiteral("logoPlaceholder"));

        gridLayout_14->addWidget(logoPlaceholder, 0, 0, 1, 1);


        gridLayout_4->addWidget(frame, 0, 3, 1, 1);

        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_10, 0, 2, 1, 1);


        Layout_bottomRight->addLayout(gridLayout_4, 4, 1, 2, 1);

        gridLayout_10 = new QGridLayout();
        gridLayout_10->setSpacing(6);
        gridLayout_10->setObjectName(QStringLiteral("gridLayout_10"));
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_10->addItem(verticalSpacer_2, 2, 0, 1, 1);

        verticalSpacer_6 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_10->addItem(verticalSpacer_6, 4, 0, 1, 1);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label_67 = new QLabel(Node_1);
        label_67->setObjectName(QStringLiteral("label_67"));
        label_67->setFont(font8);
        label_67->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        verticalLayout->addWidget(label_67);

        spinBox_cycleCounter = new QSpinBox(Node_1);
        spinBox_cycleCounter->setObjectName(QStringLiteral("spinBox_cycleCounter"));
        spinBox_cycleCounter->setFont(font6);
        spinBox_cycleCounter->setMaximum(10000000);
        spinBox_cycleCounter->setValue(5);

        verticalLayout->addWidget(spinBox_cycleCounter);


        gridLayout_10->addLayout(verticalLayout, 1, 0, 1, 1);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label_66 = new QLabel(Node_1);
        label_66->setObjectName(QStringLiteral("label_66"));
        label_66->setFont(font8);
        label_66->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        verticalLayout_2->addWidget(label_66);

        spinBox_Pause = new QSpinBox(Node_1);
        spinBox_Pause->setObjectName(QStringLiteral("spinBox_Pause"));
        spinBox_Pause->setFont(font6);
        spinBox_Pause->setMaximum(10000000);
        spinBox_Pause->setValue(0);

        verticalLayout_2->addWidget(spinBox_Pause);


        gridLayout_10->addLayout(verticalLayout_2, 3, 0, 1, 1);

        line = new QFrame(Node_1);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        gridLayout_10->addWidget(line, 6, 0, 1, 1);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label_81 = new QLabel(Node_1);
        label_81->setObjectName(QStringLiteral("label_81"));
        label_81->setFont(font8);

        horizontalLayout_4->addWidget(label_81);


        verticalLayout_3->addLayout(horizontalLayout_4);

        horizontalLayout_38 = new QHBoxLayout();
        horizontalLayout_38->setSpacing(6);
        horizontalLayout_38->setObjectName(QStringLiteral("horizontalLayout_38"));
        progressBar_node1 = new QProgressBar(Node_1);
        progressBar_node1->setObjectName(QStringLiteral("progressBar_node1"));
        progressBar_node1->setValue(48);

        horizontalLayout_38->addWidget(progressBar_node1);

        label_cycleCounter = new QLabel(Node_1);
        label_cycleCounter->setObjectName(QStringLiteral("label_cycleCounter"));
        label_cycleCounter->setFont(font3);

        horizontalLayout_38->addWidget(label_cycleCounter);


        verticalLayout_3->addLayout(horizontalLayout_38);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        label_CycleDuration = new QLabel(Node_1);
        label_CycleDuration->setObjectName(QStringLiteral("label_CycleDuration"));
        QFont font9;
        font9.setFamily(QStringLiteral("Titillium Web"));
        font9.setPointSize(11);
        font9.setItalic(true);
        label_CycleDuration->setFont(font9);
        label_CycleDuration->setLayoutDirection(Qt::RightToLeft);
        label_CycleDuration->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_6->addWidget(label_CycleDuration);

        label_TotalDuration = new QLabel(Node_1);
        label_TotalDuration->setObjectName(QStringLiteral("label_TotalDuration"));
        label_TotalDuration->setFont(font9);
        label_TotalDuration->setLayoutDirection(Qt::RightToLeft);
        label_TotalDuration->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_6->addWidget(label_TotalDuration);


        verticalLayout_3->addLayout(horizontalLayout_6);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);


        gridLayout_10->addLayout(verticalLayout_3, 5, 0, 1, 1);


        Layout_bottomRight->addLayout(gridLayout_10, 0, 1, 1, 1);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        Layout_bottomRight->addItem(verticalSpacer_5, 1, 1, 1, 1);


        gridLayout_15->addLayout(Layout_bottomRight, 2, 2, 1, 1);

        tabWidget_2->addTab(Node_1, QString());
        tab_12 = new QWidget();
        tab_12->setObjectName(QStringLiteral("tab_12"));
        tab_12->setEnabled(false);
        gridLayout_23 = new QGridLayout(tab_12);
        gridLayout_23->setSpacing(6);
        gridLayout_23->setContentsMargins(11, 11, 11, 11);
        gridLayout_23->setObjectName(QStringLiteral("gridLayout_23"));
        gridLayout_12 = new QGridLayout();
        gridLayout_12->setSpacing(6);
        gridLayout_12->setObjectName(QStringLiteral("gridLayout_12"));
        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setSpacing(6);
        horizontalLayout_13->setObjectName(QStringLiteral("horizontalLayout_13"));
        horizontalSpacer_59 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_59);

        horizontalSpacer_60 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_60);

        label_ID_8 = new QLabel(tab_12);
        label_ID_8->setObjectName(QStringLiteral("label_ID_8"));
        sizePolicy.setHeightForWidth(label_ID_8->sizePolicy().hasHeightForWidth());
        label_ID_8->setSizePolicy(sizePolicy);
        label_ID_8->setMaximumSize(QSize(16777215, 16777215));
        label_ID_8->setFont(font);
        label_ID_8->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_13->addWidget(label_ID_8);

        horizontalSpacer_95 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_95);

        label_name_9 = new QLabel(tab_12);
        label_name_9->setObjectName(QStringLiteral("label_name_9"));
        label_name_9->setFont(font);
        label_name_9->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_13->addWidget(label_name_9);

        horizontalSpacer_96 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_96);

        horizontalSpacer_97 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_97);

        label_start_8 = new QLabel(tab_12);
        label_start_8->setObjectName(QStringLiteral("label_start_8"));
        sizePolicy1.setHeightForWidth(label_start_8->sizePolicy().hasHeightForWidth());
        label_start_8->setSizePolicy(sizePolicy1);
        label_start_8->setFont(font);
        label_start_8->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_13->addWidget(label_start_8);

        label_msStart_9 = new QLabel(tab_12);
        label_msStart_9->setObjectName(QStringLiteral("label_msStart_9"));
        label_msStart_9->setFont(font1);
        label_msStart_9->setAlignment(Qt::AlignCenter);

        horizontalLayout_13->addWidget(label_msStart_9);

        horizontalSpacer_98 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_98);

        label_duration_8 = new QLabel(tab_12);
        label_duration_8->setObjectName(QStringLiteral("label_duration_8"));
        label_duration_8->setFont(font);
        label_duration_8->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_13->addWidget(label_duration_8);

        label_msDuration_14 = new QLabel(tab_12);
        label_msDuration_14->setObjectName(QStringLiteral("label_msDuration_14"));
        label_msDuration_14->setFont(font1);
        label_msDuration_14->setAlignment(Qt::AlignCenter);

        horizontalLayout_13->addWidget(label_msDuration_14);

        horizontalSpacer_99 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_99);


        gridLayout_12->addLayout(horizontalLayout_13, 0, 0, 1, 1);

        gridLayout_24 = new QGridLayout();
        gridLayout_24->setSpacing(6);
        gridLayout_24->setObjectName(QStringLiteral("gridLayout_24"));
        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setSpacing(6);
        horizontalLayout_14->setObjectName(QStringLiteral("horizontalLayout_14"));
        progressBar_7 = new QProgressBar(tab_12);
        progressBar_7->setObjectName(QStringLiteral("progressBar_7"));
        progressBar_7->setValue(0);

        horizontalLayout_14->addWidget(progressBar_7);

        label_25 = new QLabel(tab_12);
        label_25->setObjectName(QStringLiteral("label_25"));
        label_25->setFont(font3);

        horizontalLayout_14->addWidget(label_25);


        gridLayout_24->addLayout(horizontalLayout_14, 4, 0, 1, 5);

        spinBox_start5_91 = new QSpinBox(tab_12);
        spinBox_start5_91->setObjectName(QStringLiteral("spinBox_start5_91"));
        spinBox_start5_91->setFont(font6);
        spinBox_start5_91->setMaximum(10000000);
        spinBox_start5_91->setValue(0);

        gridLayout_24->addWidget(spinBox_start5_91, 2, 3, 1, 2);

        spinBox_start5_92 = new QSpinBox(tab_12);
        spinBox_start5_92->setObjectName(QStringLiteral("spinBox_start5_92"));
        spinBox_start5_92->setFont(font6);
        spinBox_start5_92->setMaximum(10000000);
        spinBox_start5_92->setValue(0);

        gridLayout_24->addWidget(spinBox_start5_92, 2, 0, 1, 2);

        pushButton_27 = new QPushButton(tab_12);
        pushButton_27->setObjectName(QStringLiteral("pushButton_27"));
        pushButton_27->setMinimumSize(QSize(0, 80));
        pushButton_27->setFont(font2);

        gridLayout_24->addWidget(pushButton_27, 10, 3, 1, 2);

        horizontalSpacer_100 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_24->addItem(horizontalSpacer_100, 8, 2, 1, 1);

        verticalSpacer_25 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_24->addItem(verticalSpacer_25, 7, 0, 1, 1);

        pushButton_28 = new QPushButton(tab_12);
        pushButton_28->setObjectName(QStringLiteral("pushButton_28"));
        pushButton_28->setMinimumSize(QSize(0, 80));
        pushButton_28->setFont(font2);

        gridLayout_24->addWidget(pushButton_28, 8, 0, 1, 2);

        label_26 = new QLabel(tab_12);
        label_26->setObjectName(QStringLiteral("label_26"));
        QFont font10;
        font10.setFamily(QStringLiteral("Titillium Web"));
        font10.setPointSize(9);
        font10.setItalic(true);
        label_26->setFont(font10);
        label_26->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_24->addWidget(label_26, 3, 0, 1, 5);

        label_27 = new QLabel(tab_12);
        label_27->setObjectName(QStringLiteral("label_27"));
        label_27->setFont(font8);

        gridLayout_24->addWidget(label_27, 1, 3, 1, 1);

        line_7 = new QFrame(tab_12);
        line_7->setObjectName(QStringLiteral("line_7"));
        line_7->setFrameShape(QFrame::HLine);
        line_7->setFrameShadow(QFrame::Sunken);

        gridLayout_24->addWidget(line_7, 6, 0, 1, 5);

        horizontalSpacer_101 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_24->addItem(horizontalSpacer_101, 0, 1, 1, 1);

        verticalSpacer_8 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_24->addItem(verticalSpacer_8, 0, 0, 1, 1);

        pushButton_29 = new QPushButton(tab_12);
        pushButton_29->setObjectName(QStringLiteral("pushButton_29"));
        pushButton_29->setMinimumSize(QSize(0, 80));
        pushButton_29->setFont(font2);

        gridLayout_24->addWidget(pushButton_29, 10, 0, 1, 2);

        label_msDuration_15 = new QLabel(tab_12);
        label_msDuration_15->setObjectName(QStringLiteral("label_msDuration_15"));
        label_msDuration_15->setFont(font1);
        label_msDuration_15->setAlignment(Qt::AlignCenter);

        gridLayout_24->addWidget(label_msDuration_15, 1, 4, 1, 1);

        verticalSpacer_26 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_24->addItem(verticalSpacer_26, 5, 0, 1, 1);

        pushButton_30 = new QPushButton(tab_12);
        pushButton_30->setObjectName(QStringLiteral("pushButton_30"));
        pushButton_30->setMinimumSize(QSize(0, 80));
        pushButton_30->setFont(font2);

        gridLayout_24->addWidget(pushButton_30, 8, 3, 1, 2);

        verticalSpacer_27 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_24->addItem(verticalSpacer_27, 9, 0, 1, 1);

        label_28 = new QLabel(tab_12);
        label_28->setObjectName(QStringLiteral("label_28"));
        label_28->setFont(font8);

        gridLayout_24->addWidget(label_28, 1, 0, 1, 1);

        verticalSpacer_89 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_24->addItem(verticalSpacer_89, 11, 0, 1, 1);


        gridLayout_12->addLayout(gridLayout_24, 0, 2, 2, 1);

        scrollArea_16 = new QScrollArea(tab_12);
        scrollArea_16->setObjectName(QStringLiteral("scrollArea_16"));
        scrollArea_16->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        scrollArea_16->setWidgetResizable(true);
        scrollAreaWidgetContents_7 = new QWidget();
        scrollAreaWidgetContents_7->setObjectName(QStringLiteral("scrollAreaWidgetContents_7"));
        scrollAreaWidgetContents_7->setGeometry(QRect(0, 0, 665, 740));
        gridLayout_25 = new QGridLayout(scrollAreaWidgetContents_7);
        gridLayout_25->setSpacing(6);
        gridLayout_25->setContentsMargins(11, 11, 11, 11);
        gridLayout_25->setObjectName(QStringLiteral("gridLayout_25"));
        gridLayout_77 = new QGridLayout();
        gridLayout_77->setSpacing(1);
        gridLayout_77->setObjectName(QStringLiteral("gridLayout_77"));
        textFIeld_name5_79 = new QLineEdit(scrollAreaWidgetContents_7);
        textFIeld_name5_79->setObjectName(QStringLiteral("textFIeld_name5_79"));
        textFIeld_name5_79->setFont(font2);

        gridLayout_77->addWidget(textFIeld_name5_79, 12, 4, 1, 1);

        spinBox_start2_13 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_start2_13->setObjectName(QStringLiteral("spinBox_start2_13"));
        spinBox_start2_13->setFont(font6);
        spinBox_start2_13->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_start2_13, 7, 6, 1, 2);

        label_ID4_91 = new QLabel(scrollAreaWidgetContents_7);
        label_ID4_91->setObjectName(QStringLiteral("label_ID4_91"));
        label_ID4_91->setFont(font5);
        label_ID4_91->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_77->addWidget(label_ID4_91, 5, 2, 1, 1);

        circle_inactive3_43 = new QLabel(scrollAreaWidgetContents_7);
        circle_inactive3_43->setObjectName(QStringLiteral("circle_inactive3_43"));
        circle_inactive3_43->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_43->setAlignment(Qt::AlignCenter);

        gridLayout_77->addWidget(circle_inactive3_43, 5, 3, 1, 1);

        horizontalSpacer_102 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_77->addItem(horizontalSpacer_102, 6, 0, 1, 1);

        checkBox_121 = new QCheckBox(scrollAreaWidgetContents_7);
        checkBox_121->setObjectName(QStringLiteral("checkBox_121"));
        checkBox_121->setEnabled(false);
        checkBox_121->setBaseSize(QSize(0, 0));
        checkBox_121->setFont(font4);
        checkBox_121->setTabletTracking(false);
        checkBox_121->setAutoFillBackground(false);
        checkBox_121->setChecked(false);
        checkBox_121->setTristate(false);

        gridLayout_77->addWidget(checkBox_121, 3, 1, 1, 1);

        checkBox_122 = new QCheckBox(scrollAreaWidgetContents_7);
        checkBox_122->setObjectName(QStringLiteral("checkBox_122"));
        checkBox_122->setEnabled(false);
        checkBox_122->setBaseSize(QSize(0, 0));
        checkBox_122->setFont(font4);
        checkBox_122->setTabletTracking(false);
        checkBox_122->setAutoFillBackground(false);
        checkBox_122->setChecked(false);
        checkBox_122->setTristate(false);

        gridLayout_77->addWidget(checkBox_122, 5, 1, 1, 1);

        checkBox_123 = new QCheckBox(scrollAreaWidgetContents_7);
        checkBox_123->setObjectName(QStringLiteral("checkBox_123"));
        checkBox_123->setEnabled(false);
        checkBox_123->setBaseSize(QSize(0, 0));
        checkBox_123->setFont(font4);
        checkBox_123->setTabletTracking(false);
        checkBox_123->setAutoFillBackground(false);
        checkBox_123->setChecked(false);
        checkBox_123->setTristate(false);

        gridLayout_77->addWidget(checkBox_123, 8, 1, 1, 1);

        spinBox_start1_7 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_start1_7->setObjectName(QStringLiteral("spinBox_start1_7"));
        spinBox_start1_7->setFont(font6);
        spinBox_start1_7->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_start1_7, 0, 6, 1, 2);

        checkBox_124 = new QCheckBox(scrollAreaWidgetContents_7);
        checkBox_124->setObjectName(QStringLiteral("checkBox_124"));
        checkBox_124->setEnabled(false);
        checkBox_124->setBaseSize(QSize(0, 0));
        checkBox_124->setFont(font4);
        checkBox_124->setTabletTracking(false);
        checkBox_124->setAutoFillBackground(false);
        checkBox_124->setChecked(false);
        checkBox_124->setTristate(false);

        gridLayout_77->addWidget(checkBox_124, 4, 1, 1, 1);

        circle_inactive3_44 = new QLabel(scrollAreaWidgetContents_7);
        circle_inactive3_44->setObjectName(QStringLiteral("circle_inactive3_44"));
        circle_inactive3_44->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_44->setAlignment(Qt::AlignCenter);

        gridLayout_77->addWidget(circle_inactive3_44, 3, 3, 1, 1);

        textFIeld_name1_7 = new QLineEdit(scrollAreaWidgetContents_7);
        textFIeld_name1_7->setObjectName(QStringLiteral("textFIeld_name1_7"));
        textFIeld_name1_7->setFont(font2);

        gridLayout_77->addWidget(textFIeld_name1_7, 0, 4, 1, 1);

        spinBox_duration2_13 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_duration2_13->setObjectName(QStringLiteral("spinBox_duration2_13"));
        spinBox_duration2_13->setFont(font6);
        spinBox_duration2_13->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_duration2_13, 7, 9, 1, 2);

        spinBox_duration4_13 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_duration4_13->setObjectName(QStringLiteral("spinBox_duration4_13"));
        spinBox_duration4_13->setFont(font6);
        spinBox_duration4_13->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_duration4_13, 3, 9, 1, 2);

        textFIeld_name4_13 = new QLineEdit(scrollAreaWidgetContents_7);
        textFIeld_name4_13->setObjectName(QStringLiteral("textFIeld_name4_13"));
        textFIeld_name4_13->setFont(font2);

        gridLayout_77->addWidget(textFIeld_name4_13, 3, 4, 1, 1);

        spinBox_duration5_79 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_duration5_79->setObjectName(QStringLiteral("spinBox_duration5_79"));
        spinBox_duration5_79->setFont(font6);
        spinBox_duration5_79->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_duration5_79, 6, 9, 1, 2);

        checkBox_125 = new QCheckBox(scrollAreaWidgetContents_7);
        checkBox_125->setObjectName(QStringLiteral("checkBox_125"));
        checkBox_125->setEnabled(false);
        checkBox_125->setBaseSize(QSize(0, 0));
        checkBox_125->setFont(font4);
        checkBox_125->setTabletTracking(false);
        checkBox_125->setAutoFillBackground(false);
        checkBox_125->setChecked(false);
        checkBox_125->setTristate(false);

        gridLayout_77->addWidget(checkBox_125, 14, 1, 1, 1);

        textFIeld_name2_13 = new QLineEdit(scrollAreaWidgetContents_7);
        textFIeld_name2_13->setObjectName(QStringLiteral("textFIeld_name2_13"));
        textFIeld_name2_13->setFont(font2);

        gridLayout_77->addWidget(textFIeld_name2_13, 7, 4, 1, 1);

        spinBox_duration5_80 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_duration5_80->setObjectName(QStringLiteral("spinBox_duration5_80"));
        spinBox_duration5_80->setFont(font6);
        spinBox_duration5_80->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_duration5_80, 10, 9, 1, 2);

        textFIeld_name5_80 = new QLineEdit(scrollAreaWidgetContents_7);
        textFIeld_name5_80->setObjectName(QStringLiteral("textFIeld_name5_80"));
        textFIeld_name5_80->setFont(font2);

        gridLayout_77->addWidget(textFIeld_name5_80, 4, 4, 1, 1);

        spinBox_start5_93 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_start5_93->setObjectName(QStringLiteral("spinBox_start5_93"));
        spinBox_start5_93->setFont(font6);
        spinBox_start5_93->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_start5_93, 4, 6, 1, 2);

        label_ID4_92 = new QLabel(scrollAreaWidgetContents_7);
        label_ID4_92->setObjectName(QStringLiteral("label_ID4_92"));
        label_ID4_92->setFont(font5);
        label_ID4_92->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_77->addWidget(label_ID4_92, 3, 2, 1, 1);

        spinBox_start3_13 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_start3_13->setObjectName(QStringLiteral("spinBox_start3_13"));
        spinBox_start3_13->setFont(font6);
        spinBox_start3_13->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_start3_13, 8, 6, 1, 2);

        label_ID4_93 = new QLabel(scrollAreaWidgetContents_7);
        label_ID4_93->setObjectName(QStringLiteral("label_ID4_93"));
        label_ID4_93->setFont(font5);
        label_ID4_93->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_77->addWidget(label_ID4_93, 9, 2, 1, 1);

        spinBox_duration4_14 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_duration4_14->setObjectName(QStringLiteral("spinBox_duration4_14"));
        spinBox_duration4_14->setFont(font6);
        spinBox_duration4_14->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_duration4_14, 9, 9, 1, 2);

        checkBox_126 = new QCheckBox(scrollAreaWidgetContents_7);
        checkBox_126->setObjectName(QStringLiteral("checkBox_126"));
        checkBox_126->setEnabled(false);
        checkBox_126->setBaseSize(QSize(0, 0));
        checkBox_126->setFont(font4);
        checkBox_126->setTabletTracking(false);
        checkBox_126->setAutoFillBackground(false);
        checkBox_126->setChecked(false);
        checkBox_126->setTristate(false);

        gridLayout_77->addWidget(checkBox_126, 13, 1, 1, 1);

        spinBox_start5_94 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_start5_94->setObjectName(QStringLiteral("spinBox_start5_94"));
        spinBox_start5_94->setFont(font6);
        spinBox_start5_94->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_start5_94, 15, 6, 1, 2);

        spinBox_duration5_81 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_duration5_81->setObjectName(QStringLiteral("spinBox_duration5_81"));
        spinBox_duration5_81->setFont(font6);
        spinBox_duration5_81->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_duration5_81, 15, 9, 1, 2);

        textFIeld_name5_81 = new QLineEdit(scrollAreaWidgetContents_7);
        textFIeld_name5_81->setObjectName(QStringLiteral("textFIeld_name5_81"));
        textFIeld_name5_81->setFont(font2);

        gridLayout_77->addWidget(textFIeld_name5_81, 10, 4, 1, 1);

        label_ID4_94 = new QLabel(scrollAreaWidgetContents_7);
        label_ID4_94->setObjectName(QStringLiteral("label_ID4_94"));
        label_ID4_94->setFont(font5);
        label_ID4_94->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_77->addWidget(label_ID4_94, 14, 2, 1, 1);

        spinBox_start5_95 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_start5_95->setObjectName(QStringLiteral("spinBox_start5_95"));
        spinBox_start5_95->setFont(font6);
        spinBox_start5_95->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_start5_95, 14, 6, 1, 2);

        textFIeld_name4_14 = new QLineEdit(scrollAreaWidgetContents_7);
        textFIeld_name4_14->setObjectName(QStringLiteral("textFIeld_name4_14"));
        textFIeld_name4_14->setFont(font2);

        gridLayout_77->addWidget(textFIeld_name4_14, 9, 4, 1, 1);

        spinBox_start3_14 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_start3_14->setObjectName(QStringLiteral("spinBox_start3_14"));
        spinBox_start3_14->setFont(font6);
        spinBox_start3_14->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_start3_14, 2, 6, 1, 2);

        spinBox_start5_96 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_start5_96->setObjectName(QStringLiteral("spinBox_start5_96"));
        spinBox_start5_96->setFont(font6);
        spinBox_start5_96->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_start5_96, 6, 6, 1, 2);

        horizontalSpacer_103 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_77->addItem(horizontalSpacer_103, 0, 12, 1, 1);

        horizontalSpacer_104 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_77->addItem(horizontalSpacer_104, 10, 5, 1, 1);

        spinBox_duration5_82 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_duration5_82->setObjectName(QStringLiteral("spinBox_duration5_82"));
        spinBox_duration5_82->setFont(font6);
        spinBox_duration5_82->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_duration5_82, 11, 9, 1, 2);

        textFIeld_name5_82 = new QLineEdit(scrollAreaWidgetContents_7);
        textFIeld_name5_82->setObjectName(QStringLiteral("textFIeld_name5_82"));
        textFIeld_name5_82->setFont(font2);

        gridLayout_77->addWidget(textFIeld_name5_82, 5, 4, 1, 1);

        label_ID2_13 = new QLabel(scrollAreaWidgetContents_7);
        label_ID2_13->setObjectName(QStringLiteral("label_ID2_13"));
        label_ID2_13->setFont(font5);
        label_ID2_13->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_77->addWidget(label_ID2_13, 1, 2, 1, 1);

        checkBox_127 = new QCheckBox(scrollAreaWidgetContents_7);
        checkBox_127->setObjectName(QStringLiteral("checkBox_127"));
        checkBox_127->setEnabled(false);
        checkBox_127->setBaseSize(QSize(0, 0));
        checkBox_127->setFont(font4);
        checkBox_127->setTabletTracking(false);
        checkBox_127->setAutoFillBackground(false);
        checkBox_127->setChecked(false);
        checkBox_127->setTristate(false);

        gridLayout_77->addWidget(checkBox_127, 6, 1, 1, 1);

        spinBox_start5_97 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_start5_97->setObjectName(QStringLiteral("spinBox_start5_97"));
        spinBox_start5_97->setFont(font6);
        spinBox_start5_97->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_start5_97, 17, 6, 1, 2);

        textFIeld_name2_14 = new QLineEdit(scrollAreaWidgetContents_7);
        textFIeld_name2_14->setObjectName(QStringLiteral("textFIeld_name2_14"));
        textFIeld_name2_14->setFont(font2);

        gridLayout_77->addWidget(textFIeld_name2_14, 1, 4, 1, 1);

        textFIeld_name5_83 = new QLineEdit(scrollAreaWidgetContents_7);
        textFIeld_name5_83->setObjectName(QStringLiteral("textFIeld_name5_83"));
        textFIeld_name5_83->setFont(font2);

        gridLayout_77->addWidget(textFIeld_name5_83, 6, 4, 1, 1);

        checkBox_128 = new QCheckBox(scrollAreaWidgetContents_7);
        checkBox_128->setObjectName(QStringLiteral("checkBox_128"));
        checkBox_128->setEnabled(false);
        checkBox_128->setBaseSize(QSize(0, 0));
        checkBox_128->setFont(font4);
        checkBox_128->setTabletTracking(false);
        checkBox_128->setAutoFillBackground(false);
        checkBox_128->setChecked(false);
        checkBox_128->setTristate(false);

        gridLayout_77->addWidget(checkBox_128, 7, 1, 1, 1);

        label_ID4_95 = new QLabel(scrollAreaWidgetContents_7);
        label_ID4_95->setObjectName(QStringLiteral("label_ID4_95"));
        label_ID4_95->setFont(font5);
        label_ID4_95->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_77->addWidget(label_ID4_95, 6, 2, 1, 1);

        circle_inactive3_45 = new QLabel(scrollAreaWidgetContents_7);
        circle_inactive3_45->setObjectName(QStringLiteral("circle_inactive3_45"));
        QPalette palette6;
        palette6.setBrush(QPalette::Active, QPalette::Button, brush6);
        palette6.setBrush(QPalette::Inactive, QPalette::Button, brush6);
        palette6.setBrush(QPalette::Disabled, QPalette::Button, brush6);
        circle_inactive3_45->setPalette(palette6);
        circle_inactive3_45->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_45->setAlignment(Qt::AlignCenter);

        gridLayout_77->addWidget(circle_inactive3_45, 8, 3, 1, 1);

        textFIeld_name3_13 = new QLineEdit(scrollAreaWidgetContents_7);
        textFIeld_name3_13->setObjectName(QStringLiteral("textFIeld_name3_13"));
        textFIeld_name3_13->setFont(font2);

        gridLayout_77->addWidget(textFIeld_name3_13, 2, 4, 1, 1);

        spinBox_duration5_83 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_duration5_83->setObjectName(QStringLiteral("spinBox_duration5_83"));
        spinBox_duration5_83->setFont(font6);
        spinBox_duration5_83->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_duration5_83, 5, 9, 1, 2);

        textFIeld_name3_14 = new QLineEdit(scrollAreaWidgetContents_7);
        textFIeld_name3_14->setObjectName(QStringLiteral("textFIeld_name3_14"));
        textFIeld_name3_14->setFont(font2);

        gridLayout_77->addWidget(textFIeld_name3_14, 8, 4, 1, 1);

        spinBox_duration5_84 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_duration5_84->setObjectName(QStringLiteral("spinBox_duration5_84"));
        spinBox_duration5_84->setFont(font6);
        spinBox_duration5_84->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_duration5_84, 14, 9, 1, 2);

        checkBox_129 = new QCheckBox(scrollAreaWidgetContents_7);
        checkBox_129->setObjectName(QStringLiteral("checkBox_129"));
        checkBox_129->setEnabled(false);
        checkBox_129->setBaseSize(QSize(0, 0));
        checkBox_129->setFont(font4);
        checkBox_129->setTabletTracking(false);
        checkBox_129->setAutoFillBackground(false);
        checkBox_129->setChecked(false);
        checkBox_129->setTristate(false);

        gridLayout_77->addWidget(checkBox_129, 12, 1, 1, 1);

        textFIeld_name5_84 = new QLineEdit(scrollAreaWidgetContents_7);
        textFIeld_name5_84->setObjectName(QStringLiteral("textFIeld_name5_84"));
        textFIeld_name5_84->setFont(font2);

        gridLayout_77->addWidget(textFIeld_name5_84, 18, 4, 1, 1);

        textFIeld_name5_85 = new QLineEdit(scrollAreaWidgetContents_7);
        textFIeld_name5_85->setObjectName(QStringLiteral("textFIeld_name5_85"));
        textFIeld_name5_85->setFont(font2);

        gridLayout_77->addWidget(textFIeld_name5_85, 20, 4, 1, 1);

        spinBox_duration5_85 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_duration5_85->setObjectName(QStringLiteral("spinBox_duration5_85"));
        spinBox_duration5_85->setFont(font6);
        spinBox_duration5_85->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_duration5_85, 4, 9, 1, 2);

        label_ID4_96 = new QLabel(scrollAreaWidgetContents_7);
        label_ID4_96->setObjectName(QStringLiteral("label_ID4_96"));
        label_ID4_96->setFont(font5);
        label_ID4_96->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_77->addWidget(label_ID4_96, 20, 2, 1, 1);

        horizontalSpacer_105 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_77->addItem(horizontalSpacer_105, 10, 8, 1, 1);

        label_ID4_97 = new QLabel(scrollAreaWidgetContents_7);
        label_ID4_97->setObjectName(QStringLiteral("label_ID4_97"));
        label_ID4_97->setFont(font5);
        label_ID4_97->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_77->addWidget(label_ID4_97, 4, 2, 1, 1);

        textFIeld_name5_86 = new QLineEdit(scrollAreaWidgetContents_7);
        textFIeld_name5_86->setObjectName(QStringLiteral("textFIeld_name5_86"));
        textFIeld_name5_86->setFont(font2);

        gridLayout_77->addWidget(textFIeld_name5_86, 13, 4, 1, 1);

        spinBox_start2_14 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_start2_14->setObjectName(QStringLiteral("spinBox_start2_14"));
        spinBox_start2_14->setFont(font6);
        spinBox_start2_14->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_start2_14, 1, 6, 1, 2);

        spinBox_duration5_86 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_duration5_86->setObjectName(QStringLiteral("spinBox_duration5_86"));
        spinBox_duration5_86->setFont(font6);
        spinBox_duration5_86->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_duration5_86, 17, 9, 1, 2);

        label_ID4_98 = new QLabel(scrollAreaWidgetContents_7);
        label_ID4_98->setObjectName(QStringLiteral("label_ID4_98"));
        label_ID4_98->setFont(font5);
        label_ID4_98->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_77->addWidget(label_ID4_98, 10, 2, 1, 1);

        spinBox_duration5_87 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_duration5_87->setObjectName(QStringLiteral("spinBox_duration5_87"));
        spinBox_duration5_87->setFont(font6);
        spinBox_duration5_87->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_duration5_87, 12, 9, 1, 2);

        spinBox_duration5_88 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_duration5_88->setObjectName(QStringLiteral("spinBox_duration5_88"));
        spinBox_duration5_88->setFont(font6);
        spinBox_duration5_88->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_duration5_88, 20, 9, 1, 2);

        spinBox_start5_98 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_start5_98->setObjectName(QStringLiteral("spinBox_start5_98"));
        spinBox_start5_98->setFont(font6);
        spinBox_start5_98->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_start5_98, 20, 6, 1, 2);

        textFIeld_name5_87 = new QLineEdit(scrollAreaWidgetContents_7);
        textFIeld_name5_87->setObjectName(QStringLiteral("textFIeld_name5_87"));
        textFIeld_name5_87->setFont(font2);

        gridLayout_77->addWidget(textFIeld_name5_87, 11, 4, 1, 1);

        spinBox_start5_99 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_start5_99->setObjectName(QStringLiteral("spinBox_start5_99"));
        spinBox_start5_99->setFont(font6);
        spinBox_start5_99->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_start5_99, 13, 6, 1, 2);

        checkBox_130 = new QCheckBox(scrollAreaWidgetContents_7);
        checkBox_130->setObjectName(QStringLiteral("checkBox_130"));
        checkBox_130->setEnabled(false);
        checkBox_130->setBaseSize(QSize(0, 0));
        checkBox_130->setFont(font4);
        checkBox_130->setTabletTracking(false);
        checkBox_130->setAutoFillBackground(false);
        checkBox_130->setChecked(false);
        checkBox_130->setTristate(false);

        gridLayout_77->addWidget(checkBox_130, 15, 1, 1, 1);

        label_ID4_99 = new QLabel(scrollAreaWidgetContents_7);
        label_ID4_99->setObjectName(QStringLiteral("label_ID4_99"));
        label_ID4_99->setFont(font5);
        label_ID4_99->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_77->addWidget(label_ID4_99, 12, 2, 1, 1);

        label_ID4_100 = new QLabel(scrollAreaWidgetContents_7);
        label_ID4_100->setObjectName(QStringLiteral("label_ID4_100"));
        label_ID4_100->setFont(font5);
        label_ID4_100->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_77->addWidget(label_ID4_100, 16, 2, 1, 1);

        spinBox_duration3_13 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_duration3_13->setObjectName(QStringLiteral("spinBox_duration3_13"));
        spinBox_duration3_13->setFont(font6);
        spinBox_duration3_13->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_duration3_13, 8, 9, 1, 2);

        label_ID2_14 = new QLabel(scrollAreaWidgetContents_7);
        label_ID2_14->setObjectName(QStringLiteral("label_ID2_14"));
        label_ID2_14->setFont(font5);
        label_ID2_14->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_77->addWidget(label_ID2_14, 7, 2, 1, 1);

        label_ID4_101 = new QLabel(scrollAreaWidgetContents_7);
        label_ID4_101->setObjectName(QStringLiteral("label_ID4_101"));
        label_ID4_101->setFont(font5);
        label_ID4_101->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_77->addWidget(label_ID4_101, 15, 2, 1, 1);

        checkBox_131 = new QCheckBox(scrollAreaWidgetContents_7);
        checkBox_131->setObjectName(QStringLiteral("checkBox_131"));
        checkBox_131->setEnabled(false);
        checkBox_131->setBaseSize(QSize(0, 0));
        checkBox_131->setFont(font4);
        checkBox_131->setTabletTracking(false);
        checkBox_131->setAutoFillBackground(false);
        checkBox_131->setChecked(false);
        checkBox_131->setTristate(false);

        gridLayout_77->addWidget(checkBox_131, 0, 1, 1, 1);

        circle_inactive3_46 = new QLabel(scrollAreaWidgetContents_7);
        circle_inactive3_46->setObjectName(QStringLiteral("circle_inactive3_46"));
        circle_inactive3_46->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_46->setAlignment(Qt::AlignCenter);

        gridLayout_77->addWidget(circle_inactive3_46, 2, 3, 1, 1);

        label_ID4_102 = new QLabel(scrollAreaWidgetContents_7);
        label_ID4_102->setObjectName(QStringLiteral("label_ID4_102"));
        label_ID4_102->setFont(font5);
        label_ID4_102->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_77->addWidget(label_ID4_102, 13, 2, 1, 1);

        label_ID4_103 = new QLabel(scrollAreaWidgetContents_7);
        label_ID4_103->setObjectName(QStringLiteral("label_ID4_103"));
        label_ID4_103->setFont(font5);
        label_ID4_103->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_77->addWidget(label_ID4_103, 11, 2, 1, 1);

        spinBox_start4_13 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_start4_13->setObjectName(QStringLiteral("spinBox_start4_13"));
        spinBox_start4_13->setFont(font6);
        spinBox_start4_13->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_start4_13, 3, 6, 1, 2);

        textFIeld_name5_88 = new QLineEdit(scrollAreaWidgetContents_7);
        textFIeld_name5_88->setObjectName(QStringLiteral("textFIeld_name5_88"));
        textFIeld_name5_88->setFont(font2);

        gridLayout_77->addWidget(textFIeld_name5_88, 16, 4, 1, 1);

        spinBox_start5_100 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_start5_100->setObjectName(QStringLiteral("spinBox_start5_100"));
        spinBox_start5_100->setFont(font6);
        spinBox_start5_100->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_start5_100, 12, 6, 1, 2);

        spinBox_duration5_89 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_duration5_89->setObjectName(QStringLiteral("spinBox_duration5_89"));
        spinBox_duration5_89->setFont(font6);
        spinBox_duration5_89->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_duration5_89, 18, 9, 1, 2);

        label_ID3_13 = new QLabel(scrollAreaWidgetContents_7);
        label_ID3_13->setObjectName(QStringLiteral("label_ID3_13"));
        label_ID3_13->setFont(font5);
        label_ID3_13->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_77->addWidget(label_ID3_13, 2, 2, 1, 1);

        spinBox_duration1_7 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_duration1_7->setObjectName(QStringLiteral("spinBox_duration1_7"));
        spinBox_duration1_7->setFont(font6);
        spinBox_duration1_7->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_duration1_7, 0, 9, 1, 2);

        spinBox_duration5_90 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_duration5_90->setObjectName(QStringLiteral("spinBox_duration5_90"));
        spinBox_duration5_90->setFont(font6);
        spinBox_duration5_90->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_duration5_90, 16, 9, 1, 2);

        spinBox_start5_101 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_start5_101->setObjectName(QStringLiteral("spinBox_start5_101"));
        spinBox_start5_101->setFont(font6);
        spinBox_start5_101->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_start5_101, 10, 6, 1, 2);

        spinBox_start4_14 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_start4_14->setObjectName(QStringLiteral("spinBox_start4_14"));
        spinBox_start4_14->setFont(font6);
        spinBox_start4_14->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_start4_14, 9, 6, 1, 2);

        spinBox_duration2_14 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_duration2_14->setObjectName(QStringLiteral("spinBox_duration2_14"));
        spinBox_duration2_14->setFont(font6);
        spinBox_duration2_14->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_duration2_14, 1, 9, 1, 2);

        circle_inactive1_79 = new QLabel(scrollAreaWidgetContents_7);
        circle_inactive1_79->setObjectName(QStringLiteral("circle_inactive1_79"));
        circle_inactive1_79->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_79->setAlignment(Qt::AlignCenter);

        gridLayout_77->addWidget(circle_inactive1_79, 0, 3, 1, 1);

        checkBox_132 = new QCheckBox(scrollAreaWidgetContents_7);
        checkBox_132->setObjectName(QStringLiteral("checkBox_132"));
        checkBox_132->setEnabled(false);
        checkBox_132->setBaseSize(QSize(0, 0));
        checkBox_132->setFont(font4);
        checkBox_132->setTabletTracking(false);
        checkBox_132->setAutoFillBackground(false);
        checkBox_132->setChecked(false);
        checkBox_132->setTristate(false);

        gridLayout_77->addWidget(checkBox_132, 1, 1, 1, 1);

        spinBox_duration3_14 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_duration3_14->setObjectName(QStringLiteral("spinBox_duration3_14"));
        spinBox_duration3_14->setFont(font6);
        spinBox_duration3_14->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_duration3_14, 2, 9, 1, 2);

        checkBox_133 = new QCheckBox(scrollAreaWidgetContents_7);
        checkBox_133->setObjectName(QStringLiteral("checkBox_133"));
        checkBox_133->setEnabled(false);
        checkBox_133->setBaseSize(QSize(0, 0));
        checkBox_133->setFont(font4);
        checkBox_133->setTabletTracking(false);
        checkBox_133->setAutoFillBackground(false);
        checkBox_133->setChecked(false);
        checkBox_133->setTristate(false);

        gridLayout_77->addWidget(checkBox_133, 11, 1, 1, 1);

        checkBox_134 = new QCheckBox(scrollAreaWidgetContents_7);
        checkBox_134->setObjectName(QStringLiteral("checkBox_134"));
        checkBox_134->setEnabled(false);
        checkBox_134->setBaseSize(QSize(0, 0));
        checkBox_134->setFont(font4);
        checkBox_134->setTabletTracking(false);
        checkBox_134->setAutoFillBackground(false);
        checkBox_134->setChecked(false);
        checkBox_134->setTristate(false);

        gridLayout_77->addWidget(checkBox_134, 9, 1, 1, 1);

        checkBox_135 = new QCheckBox(scrollAreaWidgetContents_7);
        checkBox_135->setObjectName(QStringLiteral("checkBox_135"));
        checkBox_135->setEnabled(false);
        checkBox_135->setBaseSize(QSize(0, 0));
        checkBox_135->setFont(font4);
        checkBox_135->setTabletTracking(false);
        checkBox_135->setAutoFillBackground(false);
        checkBox_135->setChecked(false);
        checkBox_135->setTristate(false);

        gridLayout_77->addWidget(checkBox_135, 2, 1, 1, 1);

        textFIeld_name5_89 = new QLineEdit(scrollAreaWidgetContents_7);
        textFIeld_name5_89->setObjectName(QStringLiteral("textFIeld_name5_89"));
        textFIeld_name5_89->setFont(font2);

        gridLayout_77->addWidget(textFIeld_name5_89, 15, 4, 1, 1);

        checkBox_136 = new QCheckBox(scrollAreaWidgetContents_7);
        checkBox_136->setObjectName(QStringLiteral("checkBox_136"));
        checkBox_136->setEnabled(false);
        checkBox_136->setBaseSize(QSize(0, 0));
        checkBox_136->setFont(font4);
        checkBox_136->setTabletTracking(false);
        checkBox_136->setAutoFillBackground(false);
        checkBox_136->setChecked(false);
        checkBox_136->setTristate(false);

        gridLayout_77->addWidget(checkBox_136, 10, 1, 1, 1);

        spinBox_start5_102 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_start5_102->setObjectName(QStringLiteral("spinBox_start5_102"));
        spinBox_start5_102->setFont(font6);
        spinBox_start5_102->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_start5_102, 5, 6, 1, 2);

        label_ID3_14 = new QLabel(scrollAreaWidgetContents_7);
        label_ID3_14->setObjectName(QStringLiteral("label_ID3_14"));
        label_ID3_14->setFont(font5);
        label_ID3_14->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_77->addWidget(label_ID3_14, 8, 2, 1, 1);

        label_ID1_7 = new QLabel(scrollAreaWidgetContents_7);
        label_ID1_7->setObjectName(QStringLiteral("label_ID1_7"));
        label_ID1_7->setFont(font5);
        label_ID1_7->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_77->addWidget(label_ID1_7, 0, 2, 1, 1);

        textFIeld_name5_90 = new QLineEdit(scrollAreaWidgetContents_7);
        textFIeld_name5_90->setObjectName(QStringLiteral("textFIeld_name5_90"));
        textFIeld_name5_90->setFont(font2);

        gridLayout_77->addWidget(textFIeld_name5_90, 14, 4, 1, 1);

        spinBox_duration5_91 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_duration5_91->setObjectName(QStringLiteral("spinBox_duration5_91"));
        spinBox_duration5_91->setFont(font6);
        spinBox_duration5_91->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_duration5_91, 13, 9, 1, 2);

        horizontalSpacer_106 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_77->addItem(horizontalSpacer_106, 21, 3, 1, 1);

        label_ID4_104 = new QLabel(scrollAreaWidgetContents_7);
        label_ID4_104->setObjectName(QStringLiteral("label_ID4_104"));
        label_ID4_104->setFont(font5);
        label_ID4_104->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_77->addWidget(label_ID4_104, 18, 2, 1, 1);

        checkBox_137 = new QCheckBox(scrollAreaWidgetContents_7);
        checkBox_137->setObjectName(QStringLiteral("checkBox_137"));
        checkBox_137->setEnabled(false);
        checkBox_137->setBaseSize(QSize(0, 0));
        checkBox_137->setFont(font4);
        checkBox_137->setTabletTracking(false);
        checkBox_137->setAutoFillBackground(false);
        checkBox_137->setChecked(false);
        checkBox_137->setTristate(false);

        gridLayout_77->addWidget(checkBox_137, 18, 1, 1, 1);

        checkBox_138 = new QCheckBox(scrollAreaWidgetContents_7);
        checkBox_138->setObjectName(QStringLiteral("checkBox_138"));
        checkBox_138->setEnabled(false);
        checkBox_138->setBaseSize(QSize(0, 0));
        checkBox_138->setFont(font4);
        checkBox_138->setTabletTracking(false);
        checkBox_138->setAutoFillBackground(false);
        checkBox_138->setChecked(false);
        checkBox_138->setTristate(false);

        gridLayout_77->addWidget(checkBox_138, 20, 1, 1, 1);

        checkBox_139 = new QCheckBox(scrollAreaWidgetContents_7);
        checkBox_139->setObjectName(QStringLiteral("checkBox_139"));
        checkBox_139->setEnabled(false);
        checkBox_139->setBaseSize(QSize(0, 0));
        checkBox_139->setFont(font4);
        checkBox_139->setTabletTracking(false);
        checkBox_139->setAutoFillBackground(false);
        checkBox_139->setChecked(false);
        checkBox_139->setTristate(false);

        gridLayout_77->addWidget(checkBox_139, 17, 1, 1, 1);

        spinBox_start5_103 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_start5_103->setObjectName(QStringLiteral("spinBox_start5_103"));
        spinBox_start5_103->setFont(font6);
        spinBox_start5_103->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_start5_103, 11, 6, 1, 2);

        checkBox_140 = new QCheckBox(scrollAreaWidgetContents_7);
        checkBox_140->setObjectName(QStringLiteral("checkBox_140"));
        checkBox_140->setEnabled(false);
        checkBox_140->setBaseSize(QSize(0, 0));
        checkBox_140->setFont(font4);
        checkBox_140->setTabletTracking(false);
        checkBox_140->setAutoFillBackground(false);
        checkBox_140->setChecked(false);
        checkBox_140->setTristate(false);

        gridLayout_77->addWidget(checkBox_140, 16, 1, 1, 1);

        spinBox_start5_104 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_start5_104->setObjectName(QStringLiteral("spinBox_start5_104"));
        spinBox_start5_104->setFont(font6);
        spinBox_start5_104->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_start5_104, 16, 6, 1, 2);

        label_ID4_105 = new QLabel(scrollAreaWidgetContents_7);
        label_ID4_105->setObjectName(QStringLiteral("label_ID4_105"));
        label_ID4_105->setFont(font5);
        label_ID4_105->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_77->addWidget(label_ID4_105, 17, 2, 1, 1);

        circle_inactive3_47 = new QLabel(scrollAreaWidgetContents_7);
        circle_inactive3_47->setObjectName(QStringLiteral("circle_inactive3_47"));
        circle_inactive3_47->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_47->setAlignment(Qt::AlignCenter);

        gridLayout_77->addWidget(circle_inactive3_47, 4, 3, 1, 1);

        spinBox_start5_105 = new QSpinBox(scrollAreaWidgetContents_7);
        spinBox_start5_105->setObjectName(QStringLiteral("spinBox_start5_105"));
        spinBox_start5_105->setFont(font6);
        spinBox_start5_105->setMaximum(10000000);

        gridLayout_77->addWidget(spinBox_start5_105, 18, 6, 1, 2);

        circle_inactive1_80 = new QLabel(scrollAreaWidgetContents_7);
        circle_inactive1_80->setObjectName(QStringLiteral("circle_inactive1_80"));
        circle_inactive1_80->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_80->setAlignment(Qt::AlignCenter);

        gridLayout_77->addWidget(circle_inactive1_80, 1, 3, 1, 1);

        textFIeld_name5_91 = new QLineEdit(scrollAreaWidgetContents_7);
        textFIeld_name5_91->setObjectName(QStringLiteral("textFIeld_name5_91"));
        textFIeld_name5_91->setFont(font2);

        gridLayout_77->addWidget(textFIeld_name5_91, 17, 4, 1, 1);

        circle_inactive3_48 = new QLabel(scrollAreaWidgetContents_7);
        circle_inactive3_48->setObjectName(QStringLiteral("circle_inactive3_48"));
        circle_inactive3_48->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_48->setAlignment(Qt::AlignCenter);

        gridLayout_77->addWidget(circle_inactive3_48, 6, 3, 1, 1);

        horizontalSpacer_107 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_77->addItem(horizontalSpacer_107, 0, 8, 1, 1);

        circle_inactive3_49 = new QLabel(scrollAreaWidgetContents_7);
        circle_inactive3_49->setObjectName(QStringLiteral("circle_inactive3_49"));
        circle_inactive3_49->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_49->setAlignment(Qt::AlignCenter);

        gridLayout_77->addWidget(circle_inactive3_49, 7, 3, 1, 1);

        circle_inactive1_81 = new QLabel(scrollAreaWidgetContents_7);
        circle_inactive1_81->setObjectName(QStringLiteral("circle_inactive1_81"));
        circle_inactive1_81->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_81->setAlignment(Qt::AlignCenter);

        gridLayout_77->addWidget(circle_inactive1_81, 9, 3, 1, 1);

        circle_inactive1_82 = new QLabel(scrollAreaWidgetContents_7);
        circle_inactive1_82->setObjectName(QStringLiteral("circle_inactive1_82"));
        circle_inactive1_82->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_82->setAlignment(Qt::AlignCenter);

        gridLayout_77->addWidget(circle_inactive1_82, 10, 3, 1, 1);

        circle_inactive1_83 = new QLabel(scrollAreaWidgetContents_7);
        circle_inactive1_83->setObjectName(QStringLiteral("circle_inactive1_83"));
        circle_inactive1_83->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_83->setAlignment(Qt::AlignCenter);

        gridLayout_77->addWidget(circle_inactive1_83, 11, 3, 1, 1);

        circle_inactive1_84 = new QLabel(scrollAreaWidgetContents_7);
        circle_inactive1_84->setObjectName(QStringLiteral("circle_inactive1_84"));
        circle_inactive1_84->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_84->setAlignment(Qt::AlignCenter);

        gridLayout_77->addWidget(circle_inactive1_84, 12, 3, 1, 1);

        circle_inactive1_85 = new QLabel(scrollAreaWidgetContents_7);
        circle_inactive1_85->setObjectName(QStringLiteral("circle_inactive1_85"));
        circle_inactive1_85->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_85->setAlignment(Qt::AlignCenter);

        gridLayout_77->addWidget(circle_inactive1_85, 13, 3, 1, 1);

        circle_inactive1_86 = new QLabel(scrollAreaWidgetContents_7);
        circle_inactive1_86->setObjectName(QStringLiteral("circle_inactive1_86"));
        circle_inactive1_86->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_86->setAlignment(Qt::AlignCenter);

        gridLayout_77->addWidget(circle_inactive1_86, 14, 3, 1, 1);

        circle_inactive1_87 = new QLabel(scrollAreaWidgetContents_7);
        circle_inactive1_87->setObjectName(QStringLiteral("circle_inactive1_87"));
        circle_inactive1_87->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_87->setAlignment(Qt::AlignCenter);

        gridLayout_77->addWidget(circle_inactive1_87, 15, 3, 1, 1);

        circle_inactive1_88 = new QLabel(scrollAreaWidgetContents_7);
        circle_inactive1_88->setObjectName(QStringLiteral("circle_inactive1_88"));
        circle_inactive1_88->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_88->setAlignment(Qt::AlignCenter);

        gridLayout_77->addWidget(circle_inactive1_88, 16, 3, 1, 1);

        circle_inactive1_89 = new QLabel(scrollAreaWidgetContents_7);
        circle_inactive1_89->setObjectName(QStringLiteral("circle_inactive1_89"));
        circle_inactive1_89->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_89->setAlignment(Qt::AlignCenter);

        gridLayout_77->addWidget(circle_inactive1_89, 17, 3, 1, 1);

        circle_inactive1_90 = new QLabel(scrollAreaWidgetContents_7);
        circle_inactive1_90->setObjectName(QStringLiteral("circle_inactive1_90"));
        circle_inactive1_90->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_90->setAlignment(Qt::AlignCenter);

        gridLayout_77->addWidget(circle_inactive1_90, 18, 3, 1, 1);

        circle_inactive1_91 = new QLabel(scrollAreaWidgetContents_7);
        circle_inactive1_91->setObjectName(QStringLiteral("circle_inactive1_91"));
        circle_inactive1_91->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_91->setAlignment(Qt::AlignCenter);

        gridLayout_77->addWidget(circle_inactive1_91, 20, 3, 1, 1);


        gridLayout_25->addLayout(gridLayout_77, 1, 0, 1, 1);

        scrollArea_16->setWidget(scrollAreaWidgetContents_7);

        gridLayout_12->addWidget(scrollArea_16, 1, 0, 1, 1);

        horizontalSpacer_108 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_12->addItem(horizontalSpacer_108, 1, 1, 1, 1);


        gridLayout_23->addLayout(gridLayout_12, 0, 0, 1, 1);

        horizontalSpacer_109 = new QSpacerItem(8, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_23->addItem(horizontalSpacer_109, 0, 1, 1, 1);

        tabWidget_2->addTab(tab_12, QString());
        tab_13 = new QWidget();
        tab_13->setObjectName(QStringLiteral("tab_13"));
        tab_13->setEnabled(false);
        gridLayout_78 = new QGridLayout(tab_13);
        gridLayout_78->setSpacing(6);
        gridLayout_78->setContentsMargins(11, 11, 11, 11);
        gridLayout_78->setObjectName(QStringLiteral("gridLayout_78"));
        gridLayout_79 = new QGridLayout();
        gridLayout_79->setSpacing(6);
        gridLayout_79->setObjectName(QStringLiteral("gridLayout_79"));
        horizontalLayout_39 = new QHBoxLayout();
        horizontalLayout_39->setSpacing(6);
        horizontalLayout_39->setObjectName(QStringLiteral("horizontalLayout_39"));
        horizontalSpacer_291 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_39->addItem(horizontalSpacer_291);

        horizontalSpacer_292 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_39->addItem(horizontalSpacer_292);

        label_ID_21 = new QLabel(tab_13);
        label_ID_21->setObjectName(QStringLiteral("label_ID_21"));
        sizePolicy.setHeightForWidth(label_ID_21->sizePolicy().hasHeightForWidth());
        label_ID_21->setSizePolicy(sizePolicy);
        label_ID_21->setMaximumSize(QSize(16777215, 16777215));
        label_ID_21->setFont(font);
        label_ID_21->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_39->addWidget(label_ID_21);

        horizontalSpacer_293 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_39->addItem(horizontalSpacer_293);

        label_name_19 = new QLabel(tab_13);
        label_name_19->setObjectName(QStringLiteral("label_name_19"));
        label_name_19->setFont(font);
        label_name_19->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_39->addWidget(label_name_19);

        horizontalSpacer_294 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_39->addItem(horizontalSpacer_294);

        horizontalSpacer_295 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_39->addItem(horizontalSpacer_295);

        label_start_21 = new QLabel(tab_13);
        label_start_21->setObjectName(QStringLiteral("label_start_21"));
        sizePolicy1.setHeightForWidth(label_start_21->sizePolicy().hasHeightForWidth());
        label_start_21->setSizePolicy(sizePolicy1);
        label_start_21->setFont(font);
        label_start_21->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_39->addWidget(label_start_21);

        label_msStart_22 = new QLabel(tab_13);
        label_msStart_22->setObjectName(QStringLiteral("label_msStart_22"));
        label_msStart_22->setFont(font1);
        label_msStart_22->setAlignment(Qt::AlignCenter);

        horizontalLayout_39->addWidget(label_msStart_22);

        horizontalSpacer_296 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_39->addItem(horizontalSpacer_296);

        label_duration_27 = new QLabel(tab_13);
        label_duration_27->setObjectName(QStringLiteral("label_duration_27"));
        label_duration_27->setFont(font);
        label_duration_27->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_39->addWidget(label_duration_27);

        label_msDuration_40 = new QLabel(tab_13);
        label_msDuration_40->setObjectName(QStringLiteral("label_msDuration_40"));
        label_msDuration_40->setFont(font1);
        label_msDuration_40->setAlignment(Qt::AlignCenter);

        horizontalLayout_39->addWidget(label_msDuration_40);

        horizontalSpacer_297 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_39->addItem(horizontalSpacer_297);


        gridLayout_79->addLayout(horizontalLayout_39, 0, 0, 1, 1);

        gridLayout_80 = new QGridLayout();
        gridLayout_80->setSpacing(6);
        gridLayout_80->setObjectName(QStringLiteral("gridLayout_80"));
        horizontalSpacer_298 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_80->addItem(horizontalSpacer_298, 8, 2, 1, 1);

        verticalSpacer_90 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_80->addItem(verticalSpacer_90, 9, 0, 1, 1);

        horizontalLayout_40 = new QHBoxLayout();
        horizontalLayout_40->setSpacing(6);
        horizontalLayout_40->setObjectName(QStringLiteral("horizontalLayout_40"));
        progressBar_35 = new QProgressBar(tab_13);
        progressBar_35->setObjectName(QStringLiteral("progressBar_35"));
        progressBar_35->setValue(0);

        horizontalLayout_40->addWidget(progressBar_35);

        label_68 = new QLabel(tab_13);
        label_68->setObjectName(QStringLiteral("label_68"));
        label_68->setFont(font3);

        horizontalLayout_40->addWidget(label_68);


        gridLayout_80->addLayout(horizontalLayout_40, 4, 0, 1, 5);

        label_69 = new QLabel(tab_13);
        label_69->setObjectName(QStringLiteral("label_69"));
        label_69->setFont(font8);

        gridLayout_80->addWidget(label_69, 1, 0, 1, 1);

        label_70 = new QLabel(tab_13);
        label_70->setObjectName(QStringLiteral("label_70"));
        label_70->setFont(font8);

        gridLayout_80->addWidget(label_70, 1, 3, 1, 1);

        verticalSpacer_91 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_80->addItem(verticalSpacer_91, 5, 0, 1, 1);

        spinBox_start5_241 = new QSpinBox(tab_13);
        spinBox_start5_241->setObjectName(QStringLiteral("spinBox_start5_241"));
        spinBox_start5_241->setFont(font6);
        spinBox_start5_241->setMaximum(10000000);
        spinBox_start5_241->setValue(0);

        gridLayout_80->addWidget(spinBox_start5_241, 2, 3, 1, 2);

        verticalSpacer_92 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_80->addItem(verticalSpacer_92, 7, 0, 1, 1);

        label_71 = new QLabel(tab_13);
        label_71->setObjectName(QStringLiteral("label_71"));
        label_71->setFont(font10);
        label_71->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_80->addWidget(label_71, 3, 0, 1, 5);

        pushButton_67 = new QPushButton(tab_13);
        pushButton_67->setObjectName(QStringLiteral("pushButton_67"));
        pushButton_67->setMinimumSize(QSize(0, 80));
        pushButton_67->setFont(font2);

        gridLayout_80->addWidget(pushButton_67, 8, 0, 1, 2);

        pushButton_68 = new QPushButton(tab_13);
        pushButton_68->setObjectName(QStringLiteral("pushButton_68"));
        pushButton_68->setMinimumSize(QSize(0, 80));
        pushButton_68->setFont(font2);

        gridLayout_80->addWidget(pushButton_68, 10, 3, 1, 2);

        horizontalSpacer_299 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_80->addItem(horizontalSpacer_299, 0, 1, 1, 1);

        spinBox_start5_242 = new QSpinBox(tab_13);
        spinBox_start5_242->setObjectName(QStringLiteral("spinBox_start5_242"));
        spinBox_start5_242->setFont(font6);
        spinBox_start5_242->setMaximum(10000000);
        spinBox_start5_242->setValue(0);

        gridLayout_80->addWidget(spinBox_start5_242, 2, 0, 1, 2);

        pushButton_69 = new QPushButton(tab_13);
        pushButton_69->setObjectName(QStringLiteral("pushButton_69"));
        pushButton_69->setMinimumSize(QSize(0, 80));
        pushButton_69->setFont(font2);

        gridLayout_80->addWidget(pushButton_69, 10, 0, 1, 2);

        verticalSpacer_93 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_80->addItem(verticalSpacer_93, 0, 0, 1, 1);

        line_20 = new QFrame(tab_13);
        line_20->setObjectName(QStringLiteral("line_20"));
        line_20->setFrameShape(QFrame::HLine);
        line_20->setFrameShadow(QFrame::Sunken);

        gridLayout_80->addWidget(line_20, 6, 0, 1, 5);

        pushButton_70 = new QPushButton(tab_13);
        pushButton_70->setObjectName(QStringLiteral("pushButton_70"));
        pushButton_70->setMinimumSize(QSize(0, 80));
        pushButton_70->setFont(font2);

        gridLayout_80->addWidget(pushButton_70, 8, 3, 1, 2);

        label_msDuration_41 = new QLabel(tab_13);
        label_msDuration_41->setObjectName(QStringLiteral("label_msDuration_41"));
        label_msDuration_41->setFont(font1);
        label_msDuration_41->setAlignment(Qt::AlignCenter);

        gridLayout_80->addWidget(label_msDuration_41, 1, 4, 1, 1);

        verticalSpacer_94 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_80->addItem(verticalSpacer_94, 11, 0, 1, 1);


        gridLayout_79->addLayout(gridLayout_80, 0, 2, 2, 1);

        scrollArea_17 = new QScrollArea(tab_13);
        scrollArea_17->setObjectName(QStringLiteral("scrollArea_17"));
        scrollArea_17->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        scrollArea_17->setWidgetResizable(true);
        scrollAreaWidgetContents_20 = new QWidget();
        scrollAreaWidgetContents_20->setObjectName(QStringLiteral("scrollAreaWidgetContents_20"));
        scrollAreaWidgetContents_20->setGeometry(QRect(0, 0, 665, 740));
        gridLayout_81 = new QGridLayout(scrollAreaWidgetContents_20);
        gridLayout_81->setSpacing(6);
        gridLayout_81->setContentsMargins(11, 11, 11, 11);
        gridLayout_81->setObjectName(QStringLiteral("gridLayout_81"));
        gridLayout_82 = new QGridLayout();
        gridLayout_82->setSpacing(1);
        gridLayout_82->setObjectName(QStringLiteral("gridLayout_82"));
        textFIeld_name5_209 = new QLineEdit(scrollAreaWidgetContents_20);
        textFIeld_name5_209->setObjectName(QStringLiteral("textFIeld_name5_209"));
        textFIeld_name5_209->setFont(font2);

        gridLayout_82->addWidget(textFIeld_name5_209, 12, 4, 1, 1);

        spinBox_start2_33 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_start2_33->setObjectName(QStringLiteral("spinBox_start2_33"));
        spinBox_start2_33->setFont(font6);
        spinBox_start2_33->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_start2_33, 7, 6, 1, 2);

        label_ID4_247 = new QLabel(scrollAreaWidgetContents_20);
        label_ID4_247->setObjectName(QStringLiteral("label_ID4_247"));
        label_ID4_247->setFont(font5);
        label_ID4_247->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_82->addWidget(label_ID4_247, 5, 2, 1, 1);

        circle_inactive3_128 = new QLabel(scrollAreaWidgetContents_20);
        circle_inactive3_128->setObjectName(QStringLiteral("circle_inactive3_128"));
        circle_inactive3_128->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_128->setAlignment(Qt::AlignCenter);

        gridLayout_82->addWidget(circle_inactive3_128, 5, 3, 1, 1);

        horizontalSpacer_300 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_82->addItem(horizontalSpacer_300, 6, 0, 1, 1);

        checkBox_336 = new QCheckBox(scrollAreaWidgetContents_20);
        checkBox_336->setObjectName(QStringLiteral("checkBox_336"));
        checkBox_336->setEnabled(false);
        checkBox_336->setBaseSize(QSize(0, 0));
        checkBox_336->setFont(font4);
        checkBox_336->setTabletTracking(false);
        checkBox_336->setAutoFillBackground(false);
        checkBox_336->setChecked(false);
        checkBox_336->setTristate(false);

        gridLayout_82->addWidget(checkBox_336, 3, 1, 1, 1);

        checkBox_337 = new QCheckBox(scrollAreaWidgetContents_20);
        checkBox_337->setObjectName(QStringLiteral("checkBox_337"));
        checkBox_337->setEnabled(false);
        checkBox_337->setBaseSize(QSize(0, 0));
        checkBox_337->setFont(font4);
        checkBox_337->setTabletTracking(false);
        checkBox_337->setAutoFillBackground(false);
        checkBox_337->setChecked(false);
        checkBox_337->setTristate(false);

        gridLayout_82->addWidget(checkBox_337, 5, 1, 1, 1);

        checkBox_338 = new QCheckBox(scrollAreaWidgetContents_20);
        checkBox_338->setObjectName(QStringLiteral("checkBox_338"));
        checkBox_338->setEnabled(false);
        checkBox_338->setBaseSize(QSize(0, 0));
        checkBox_338->setFont(font4);
        checkBox_338->setTabletTracking(false);
        checkBox_338->setAutoFillBackground(false);
        checkBox_338->setChecked(false);
        checkBox_338->setTristate(false);

        gridLayout_82->addWidget(checkBox_338, 8, 1, 1, 1);

        spinBox_start1_17 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_start1_17->setObjectName(QStringLiteral("spinBox_start1_17"));
        spinBox_start1_17->setFont(font6);
        spinBox_start1_17->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_start1_17, 0, 6, 1, 2);

        checkBox_339 = new QCheckBox(scrollAreaWidgetContents_20);
        checkBox_339->setObjectName(QStringLiteral("checkBox_339"));
        checkBox_339->setEnabled(false);
        checkBox_339->setBaseSize(QSize(0, 0));
        checkBox_339->setFont(font4);
        checkBox_339->setTabletTracking(false);
        checkBox_339->setAutoFillBackground(false);
        checkBox_339->setChecked(false);
        checkBox_339->setTristate(false);

        gridLayout_82->addWidget(checkBox_339, 4, 1, 1, 1);

        circle_inactive3_129 = new QLabel(scrollAreaWidgetContents_20);
        circle_inactive3_129->setObjectName(QStringLiteral("circle_inactive3_129"));
        circle_inactive3_129->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_129->setAlignment(Qt::AlignCenter);

        gridLayout_82->addWidget(circle_inactive3_129, 3, 3, 1, 1);

        textFIeld_name1_50 = new QLineEdit(scrollAreaWidgetContents_20);
        textFIeld_name1_50->setObjectName(QStringLiteral("textFIeld_name1_50"));
        textFIeld_name1_50->setFont(font2);

        gridLayout_82->addWidget(textFIeld_name1_50, 0, 4, 1, 1);

        spinBox_duration2_33 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_duration2_33->setObjectName(QStringLiteral("spinBox_duration2_33"));
        spinBox_duration2_33->setFont(font6);
        spinBox_duration2_33->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_duration2_33, 7, 9, 1, 2);

        spinBox_duration4_33 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_duration4_33->setObjectName(QStringLiteral("spinBox_duration4_33"));
        spinBox_duration4_33->setFont(font6);
        spinBox_duration4_33->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_duration4_33, 3, 9, 1, 2);

        textFIeld_name4_33 = new QLineEdit(scrollAreaWidgetContents_20);
        textFIeld_name4_33->setObjectName(QStringLiteral("textFIeld_name4_33"));
        textFIeld_name4_33->setFont(font2);

        gridLayout_82->addWidget(textFIeld_name4_33, 3, 4, 1, 1);

        spinBox_duration5_209 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_duration5_209->setObjectName(QStringLiteral("spinBox_duration5_209"));
        spinBox_duration5_209->setFont(font6);
        spinBox_duration5_209->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_duration5_209, 6, 9, 1, 2);

        checkBox_340 = new QCheckBox(scrollAreaWidgetContents_20);
        checkBox_340->setObjectName(QStringLiteral("checkBox_340"));
        checkBox_340->setEnabled(false);
        checkBox_340->setBaseSize(QSize(0, 0));
        checkBox_340->setFont(font4);
        checkBox_340->setTabletTracking(false);
        checkBox_340->setAutoFillBackground(false);
        checkBox_340->setChecked(false);
        checkBox_340->setTristate(false);

        gridLayout_82->addWidget(checkBox_340, 14, 1, 1, 1);

        textFIeld_name2_33 = new QLineEdit(scrollAreaWidgetContents_20);
        textFIeld_name2_33->setObjectName(QStringLiteral("textFIeld_name2_33"));
        textFIeld_name2_33->setFont(font2);

        gridLayout_82->addWidget(textFIeld_name2_33, 7, 4, 1, 1);

        spinBox_duration5_210 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_duration5_210->setObjectName(QStringLiteral("spinBox_duration5_210"));
        spinBox_duration5_210->setFont(font6);
        spinBox_duration5_210->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_duration5_210, 10, 9, 1, 2);

        textFIeld_name5_210 = new QLineEdit(scrollAreaWidgetContents_20);
        textFIeld_name5_210->setObjectName(QStringLiteral("textFIeld_name5_210"));
        textFIeld_name5_210->setFont(font2);

        gridLayout_82->addWidget(textFIeld_name5_210, 4, 4, 1, 1);

        spinBox_start5_243 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_start5_243->setObjectName(QStringLiteral("spinBox_start5_243"));
        spinBox_start5_243->setFont(font6);
        spinBox_start5_243->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_start5_243, 4, 6, 1, 2);

        label_ID4_248 = new QLabel(scrollAreaWidgetContents_20);
        label_ID4_248->setObjectName(QStringLiteral("label_ID4_248"));
        label_ID4_248->setFont(font5);
        label_ID4_248->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_82->addWidget(label_ID4_248, 3, 2, 1, 1);

        spinBox_start3_33 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_start3_33->setObjectName(QStringLiteral("spinBox_start3_33"));
        spinBox_start3_33->setFont(font6);
        spinBox_start3_33->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_start3_33, 8, 6, 1, 2);

        label_ID4_249 = new QLabel(scrollAreaWidgetContents_20);
        label_ID4_249->setObjectName(QStringLiteral("label_ID4_249"));
        label_ID4_249->setFont(font5);
        label_ID4_249->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_82->addWidget(label_ID4_249, 9, 2, 1, 1);

        spinBox_duration4_34 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_duration4_34->setObjectName(QStringLiteral("spinBox_duration4_34"));
        spinBox_duration4_34->setFont(font6);
        spinBox_duration4_34->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_duration4_34, 9, 9, 1, 2);

        checkBox_341 = new QCheckBox(scrollAreaWidgetContents_20);
        checkBox_341->setObjectName(QStringLiteral("checkBox_341"));
        checkBox_341->setEnabled(false);
        checkBox_341->setBaseSize(QSize(0, 0));
        checkBox_341->setFont(font4);
        checkBox_341->setTabletTracking(false);
        checkBox_341->setAutoFillBackground(false);
        checkBox_341->setChecked(false);
        checkBox_341->setTristate(false);

        gridLayout_82->addWidget(checkBox_341, 13, 1, 1, 1);

        spinBox_start5_244 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_start5_244->setObjectName(QStringLiteral("spinBox_start5_244"));
        spinBox_start5_244->setFont(font6);
        spinBox_start5_244->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_start5_244, 15, 6, 1, 2);

        spinBox_duration5_211 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_duration5_211->setObjectName(QStringLiteral("spinBox_duration5_211"));
        spinBox_duration5_211->setFont(font6);
        spinBox_duration5_211->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_duration5_211, 15, 9, 1, 2);

        textFIeld_name5_211 = new QLineEdit(scrollAreaWidgetContents_20);
        textFIeld_name5_211->setObjectName(QStringLiteral("textFIeld_name5_211"));
        textFIeld_name5_211->setFont(font2);

        gridLayout_82->addWidget(textFIeld_name5_211, 10, 4, 1, 1);

        label_ID4_250 = new QLabel(scrollAreaWidgetContents_20);
        label_ID4_250->setObjectName(QStringLiteral("label_ID4_250"));
        label_ID4_250->setFont(font5);
        label_ID4_250->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_82->addWidget(label_ID4_250, 14, 2, 1, 1);

        spinBox_start5_245 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_start5_245->setObjectName(QStringLiteral("spinBox_start5_245"));
        spinBox_start5_245->setFont(font6);
        spinBox_start5_245->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_start5_245, 14, 6, 1, 2);

        textFIeld_name4_34 = new QLineEdit(scrollAreaWidgetContents_20);
        textFIeld_name4_34->setObjectName(QStringLiteral("textFIeld_name4_34"));
        textFIeld_name4_34->setFont(font2);

        gridLayout_82->addWidget(textFIeld_name4_34, 9, 4, 1, 1);

        spinBox_start3_34 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_start3_34->setObjectName(QStringLiteral("spinBox_start3_34"));
        spinBox_start3_34->setFont(font6);
        spinBox_start3_34->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_start3_34, 2, 6, 1, 2);

        spinBox_start5_246 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_start5_246->setObjectName(QStringLiteral("spinBox_start5_246"));
        spinBox_start5_246->setFont(font6);
        spinBox_start5_246->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_start5_246, 6, 6, 1, 2);

        horizontalSpacer_301 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_82->addItem(horizontalSpacer_301, 0, 12, 1, 1);

        horizontalSpacer_302 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_82->addItem(horizontalSpacer_302, 10, 5, 1, 1);

        spinBox_duration5_212 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_duration5_212->setObjectName(QStringLiteral("spinBox_duration5_212"));
        spinBox_duration5_212->setFont(font6);
        spinBox_duration5_212->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_duration5_212, 11, 9, 1, 2);

        textFIeld_name5_212 = new QLineEdit(scrollAreaWidgetContents_20);
        textFIeld_name5_212->setObjectName(QStringLiteral("textFIeld_name5_212"));
        textFIeld_name5_212->setFont(font2);

        gridLayout_82->addWidget(textFIeld_name5_212, 5, 4, 1, 1);

        label_ID2_36 = new QLabel(scrollAreaWidgetContents_20);
        label_ID2_36->setObjectName(QStringLiteral("label_ID2_36"));
        label_ID2_36->setFont(font5);
        label_ID2_36->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_82->addWidget(label_ID2_36, 1, 2, 1, 1);

        checkBox_342 = new QCheckBox(scrollAreaWidgetContents_20);
        checkBox_342->setObjectName(QStringLiteral("checkBox_342"));
        checkBox_342->setEnabled(false);
        checkBox_342->setBaseSize(QSize(0, 0));
        checkBox_342->setFont(font4);
        checkBox_342->setTabletTracking(false);
        checkBox_342->setAutoFillBackground(false);
        checkBox_342->setChecked(false);
        checkBox_342->setTristate(false);

        gridLayout_82->addWidget(checkBox_342, 6, 1, 1, 1);

        spinBox_start5_247 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_start5_247->setObjectName(QStringLiteral("spinBox_start5_247"));
        spinBox_start5_247->setFont(font6);
        spinBox_start5_247->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_start5_247, 17, 6, 1, 2);

        textFIeld_name2_34 = new QLineEdit(scrollAreaWidgetContents_20);
        textFIeld_name2_34->setObjectName(QStringLiteral("textFIeld_name2_34"));
        textFIeld_name2_34->setFont(font2);

        gridLayout_82->addWidget(textFIeld_name2_34, 1, 4, 1, 1);

        textFIeld_name5_213 = new QLineEdit(scrollAreaWidgetContents_20);
        textFIeld_name5_213->setObjectName(QStringLiteral("textFIeld_name5_213"));
        textFIeld_name5_213->setFont(font2);

        gridLayout_82->addWidget(textFIeld_name5_213, 6, 4, 1, 1);

        checkBox_343 = new QCheckBox(scrollAreaWidgetContents_20);
        checkBox_343->setObjectName(QStringLiteral("checkBox_343"));
        checkBox_343->setEnabled(false);
        checkBox_343->setBaseSize(QSize(0, 0));
        checkBox_343->setFont(font4);
        checkBox_343->setTabletTracking(false);
        checkBox_343->setAutoFillBackground(false);
        checkBox_343->setChecked(false);
        checkBox_343->setTristate(false);

        gridLayout_82->addWidget(checkBox_343, 7, 1, 1, 1);

        label_ID4_251 = new QLabel(scrollAreaWidgetContents_20);
        label_ID4_251->setObjectName(QStringLiteral("label_ID4_251"));
        label_ID4_251->setFont(font5);
        label_ID4_251->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_82->addWidget(label_ID4_251, 6, 2, 1, 1);

        circle_inactive3_130 = new QLabel(scrollAreaWidgetContents_20);
        circle_inactive3_130->setObjectName(QStringLiteral("circle_inactive3_130"));
        QPalette palette7;
        palette7.setBrush(QPalette::Active, QPalette::Button, brush6);
        palette7.setBrush(QPalette::Inactive, QPalette::Button, brush6);
        palette7.setBrush(QPalette::Disabled, QPalette::Button, brush6);
        circle_inactive3_130->setPalette(palette7);
        circle_inactive3_130->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_130->setAlignment(Qt::AlignCenter);

        gridLayout_82->addWidget(circle_inactive3_130, 8, 3, 1, 1);

        textFIeld_name3_33 = new QLineEdit(scrollAreaWidgetContents_20);
        textFIeld_name3_33->setObjectName(QStringLiteral("textFIeld_name3_33"));
        textFIeld_name3_33->setFont(font2);

        gridLayout_82->addWidget(textFIeld_name3_33, 2, 4, 1, 1);

        spinBox_duration5_213 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_duration5_213->setObjectName(QStringLiteral("spinBox_duration5_213"));
        spinBox_duration5_213->setFont(font6);
        spinBox_duration5_213->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_duration5_213, 5, 9, 1, 2);

        textFIeld_name3_34 = new QLineEdit(scrollAreaWidgetContents_20);
        textFIeld_name3_34->setObjectName(QStringLiteral("textFIeld_name3_34"));
        textFIeld_name3_34->setFont(font2);

        gridLayout_82->addWidget(textFIeld_name3_34, 8, 4, 1, 1);

        spinBox_duration5_214 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_duration5_214->setObjectName(QStringLiteral("spinBox_duration5_214"));
        spinBox_duration5_214->setFont(font6);
        spinBox_duration5_214->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_duration5_214, 14, 9, 1, 2);

        checkBox_344 = new QCheckBox(scrollAreaWidgetContents_20);
        checkBox_344->setObjectName(QStringLiteral("checkBox_344"));
        checkBox_344->setEnabled(false);
        checkBox_344->setBaseSize(QSize(0, 0));
        checkBox_344->setFont(font4);
        checkBox_344->setTabletTracking(false);
        checkBox_344->setAutoFillBackground(false);
        checkBox_344->setChecked(false);
        checkBox_344->setTristate(false);

        gridLayout_82->addWidget(checkBox_344, 12, 1, 1, 1);

        textFIeld_name5_214 = new QLineEdit(scrollAreaWidgetContents_20);
        textFIeld_name5_214->setObjectName(QStringLiteral("textFIeld_name5_214"));
        textFIeld_name5_214->setFont(font2);

        gridLayout_82->addWidget(textFIeld_name5_214, 18, 4, 1, 1);

        textFIeld_name5_215 = new QLineEdit(scrollAreaWidgetContents_20);
        textFIeld_name5_215->setObjectName(QStringLiteral("textFIeld_name5_215"));
        textFIeld_name5_215->setFont(font2);

        gridLayout_82->addWidget(textFIeld_name5_215, 20, 4, 1, 1);

        spinBox_duration5_215 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_duration5_215->setObjectName(QStringLiteral("spinBox_duration5_215"));
        spinBox_duration5_215->setFont(font6);
        spinBox_duration5_215->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_duration5_215, 4, 9, 1, 2);

        label_ID4_252 = new QLabel(scrollAreaWidgetContents_20);
        label_ID4_252->setObjectName(QStringLiteral("label_ID4_252"));
        label_ID4_252->setFont(font5);
        label_ID4_252->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_82->addWidget(label_ID4_252, 20, 2, 1, 1);

        horizontalSpacer_303 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_82->addItem(horizontalSpacer_303, 10, 8, 1, 1);

        label_ID4_253 = new QLabel(scrollAreaWidgetContents_20);
        label_ID4_253->setObjectName(QStringLiteral("label_ID4_253"));
        label_ID4_253->setFont(font5);
        label_ID4_253->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_82->addWidget(label_ID4_253, 4, 2, 1, 1);

        textFIeld_name5_216 = new QLineEdit(scrollAreaWidgetContents_20);
        textFIeld_name5_216->setObjectName(QStringLiteral("textFIeld_name5_216"));
        textFIeld_name5_216->setFont(font2);

        gridLayout_82->addWidget(textFIeld_name5_216, 13, 4, 1, 1);

        spinBox_start2_34 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_start2_34->setObjectName(QStringLiteral("spinBox_start2_34"));
        spinBox_start2_34->setFont(font6);
        spinBox_start2_34->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_start2_34, 1, 6, 1, 2);

        spinBox_duration5_216 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_duration5_216->setObjectName(QStringLiteral("spinBox_duration5_216"));
        spinBox_duration5_216->setFont(font6);
        spinBox_duration5_216->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_duration5_216, 17, 9, 1, 2);

        label_ID4_254 = new QLabel(scrollAreaWidgetContents_20);
        label_ID4_254->setObjectName(QStringLiteral("label_ID4_254"));
        label_ID4_254->setFont(font5);
        label_ID4_254->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_82->addWidget(label_ID4_254, 10, 2, 1, 1);

        spinBox_duration5_217 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_duration5_217->setObjectName(QStringLiteral("spinBox_duration5_217"));
        spinBox_duration5_217->setFont(font6);
        spinBox_duration5_217->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_duration5_217, 12, 9, 1, 2);

        spinBox_duration5_218 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_duration5_218->setObjectName(QStringLiteral("spinBox_duration5_218"));
        spinBox_duration5_218->setFont(font6);
        spinBox_duration5_218->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_duration5_218, 20, 9, 1, 2);

        spinBox_start5_248 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_start5_248->setObjectName(QStringLiteral("spinBox_start5_248"));
        spinBox_start5_248->setFont(font6);
        spinBox_start5_248->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_start5_248, 20, 6, 1, 2);

        textFIeld_name5_217 = new QLineEdit(scrollAreaWidgetContents_20);
        textFIeld_name5_217->setObjectName(QStringLiteral("textFIeld_name5_217"));
        textFIeld_name5_217->setFont(font2);

        gridLayout_82->addWidget(textFIeld_name5_217, 11, 4, 1, 1);

        spinBox_start5_249 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_start5_249->setObjectName(QStringLiteral("spinBox_start5_249"));
        spinBox_start5_249->setFont(font6);
        spinBox_start5_249->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_start5_249, 13, 6, 1, 2);

        checkBox_345 = new QCheckBox(scrollAreaWidgetContents_20);
        checkBox_345->setObjectName(QStringLiteral("checkBox_345"));
        checkBox_345->setEnabled(false);
        checkBox_345->setBaseSize(QSize(0, 0));
        checkBox_345->setFont(font4);
        checkBox_345->setTabletTracking(false);
        checkBox_345->setAutoFillBackground(false);
        checkBox_345->setChecked(false);
        checkBox_345->setTristate(false);

        gridLayout_82->addWidget(checkBox_345, 15, 1, 1, 1);

        label_ID4_255 = new QLabel(scrollAreaWidgetContents_20);
        label_ID4_255->setObjectName(QStringLiteral("label_ID4_255"));
        label_ID4_255->setFont(font5);
        label_ID4_255->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_82->addWidget(label_ID4_255, 12, 2, 1, 1);

        label_ID4_256 = new QLabel(scrollAreaWidgetContents_20);
        label_ID4_256->setObjectName(QStringLiteral("label_ID4_256"));
        label_ID4_256->setFont(font5);
        label_ID4_256->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_82->addWidget(label_ID4_256, 16, 2, 1, 1);

        spinBox_duration3_33 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_duration3_33->setObjectName(QStringLiteral("spinBox_duration3_33"));
        spinBox_duration3_33->setFont(font6);
        spinBox_duration3_33->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_duration3_33, 8, 9, 1, 2);

        label_ID2_37 = new QLabel(scrollAreaWidgetContents_20);
        label_ID2_37->setObjectName(QStringLiteral("label_ID2_37"));
        label_ID2_37->setFont(font5);
        label_ID2_37->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_82->addWidget(label_ID2_37, 7, 2, 1, 1);

        label_ID4_257 = new QLabel(scrollAreaWidgetContents_20);
        label_ID4_257->setObjectName(QStringLiteral("label_ID4_257"));
        label_ID4_257->setFont(font5);
        label_ID4_257->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_82->addWidget(label_ID4_257, 15, 2, 1, 1);

        checkBox_346 = new QCheckBox(scrollAreaWidgetContents_20);
        checkBox_346->setObjectName(QStringLiteral("checkBox_346"));
        checkBox_346->setEnabled(false);
        checkBox_346->setBaseSize(QSize(0, 0));
        checkBox_346->setFont(font4);
        checkBox_346->setTabletTracking(false);
        checkBox_346->setAutoFillBackground(false);
        checkBox_346->setChecked(false);
        checkBox_346->setTristate(false);

        gridLayout_82->addWidget(checkBox_346, 0, 1, 1, 1);

        circle_inactive3_131 = new QLabel(scrollAreaWidgetContents_20);
        circle_inactive3_131->setObjectName(QStringLiteral("circle_inactive3_131"));
        circle_inactive3_131->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_131->setAlignment(Qt::AlignCenter);

        gridLayout_82->addWidget(circle_inactive3_131, 2, 3, 1, 1);

        label_ID4_258 = new QLabel(scrollAreaWidgetContents_20);
        label_ID4_258->setObjectName(QStringLiteral("label_ID4_258"));
        label_ID4_258->setFont(font5);
        label_ID4_258->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_82->addWidget(label_ID4_258, 13, 2, 1, 1);

        label_ID4_259 = new QLabel(scrollAreaWidgetContents_20);
        label_ID4_259->setObjectName(QStringLiteral("label_ID4_259"));
        label_ID4_259->setFont(font5);
        label_ID4_259->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_82->addWidget(label_ID4_259, 11, 2, 1, 1);

        spinBox_start4_33 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_start4_33->setObjectName(QStringLiteral("spinBox_start4_33"));
        spinBox_start4_33->setFont(font6);
        spinBox_start4_33->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_start4_33, 3, 6, 1, 2);

        textFIeld_name5_218 = new QLineEdit(scrollAreaWidgetContents_20);
        textFIeld_name5_218->setObjectName(QStringLiteral("textFIeld_name5_218"));
        textFIeld_name5_218->setFont(font2);

        gridLayout_82->addWidget(textFIeld_name5_218, 16, 4, 1, 1);

        spinBox_start5_250 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_start5_250->setObjectName(QStringLiteral("spinBox_start5_250"));
        spinBox_start5_250->setFont(font6);
        spinBox_start5_250->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_start5_250, 12, 6, 1, 2);

        spinBox_duration5_219 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_duration5_219->setObjectName(QStringLiteral("spinBox_duration5_219"));
        spinBox_duration5_219->setFont(font6);
        spinBox_duration5_219->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_duration5_219, 18, 9, 1, 2);

        label_ID3_36 = new QLabel(scrollAreaWidgetContents_20);
        label_ID3_36->setObjectName(QStringLiteral("label_ID3_36"));
        label_ID3_36->setFont(font5);
        label_ID3_36->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_82->addWidget(label_ID3_36, 2, 2, 1, 1);

        spinBox_duration1_17 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_duration1_17->setObjectName(QStringLiteral("spinBox_duration1_17"));
        spinBox_duration1_17->setFont(font6);
        spinBox_duration1_17->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_duration1_17, 0, 9, 1, 2);

        spinBox_duration5_220 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_duration5_220->setObjectName(QStringLiteral("spinBox_duration5_220"));
        spinBox_duration5_220->setFont(font6);
        spinBox_duration5_220->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_duration5_220, 16, 9, 1, 2);

        spinBox_start5_251 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_start5_251->setObjectName(QStringLiteral("spinBox_start5_251"));
        spinBox_start5_251->setFont(font6);
        spinBox_start5_251->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_start5_251, 10, 6, 1, 2);

        spinBox_start4_34 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_start4_34->setObjectName(QStringLiteral("spinBox_start4_34"));
        spinBox_start4_34->setFont(font6);
        spinBox_start4_34->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_start4_34, 9, 6, 1, 2);

        spinBox_duration2_34 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_duration2_34->setObjectName(QStringLiteral("spinBox_duration2_34"));
        spinBox_duration2_34->setFont(font6);
        spinBox_duration2_34->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_duration2_34, 1, 9, 1, 2);

        circle_inactive1_209 = new QLabel(scrollAreaWidgetContents_20);
        circle_inactive1_209->setObjectName(QStringLiteral("circle_inactive1_209"));
        circle_inactive1_209->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_209->setAlignment(Qt::AlignCenter);

        gridLayout_82->addWidget(circle_inactive1_209, 0, 3, 1, 1);

        checkBox_347 = new QCheckBox(scrollAreaWidgetContents_20);
        checkBox_347->setObjectName(QStringLiteral("checkBox_347"));
        checkBox_347->setEnabled(false);
        checkBox_347->setBaseSize(QSize(0, 0));
        checkBox_347->setFont(font4);
        checkBox_347->setTabletTracking(false);
        checkBox_347->setAutoFillBackground(false);
        checkBox_347->setChecked(false);
        checkBox_347->setTristate(false);

        gridLayout_82->addWidget(checkBox_347, 1, 1, 1, 1);

        spinBox_duration3_34 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_duration3_34->setObjectName(QStringLiteral("spinBox_duration3_34"));
        spinBox_duration3_34->setFont(font6);
        spinBox_duration3_34->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_duration3_34, 2, 9, 1, 2);

        checkBox_348 = new QCheckBox(scrollAreaWidgetContents_20);
        checkBox_348->setObjectName(QStringLiteral("checkBox_348"));
        checkBox_348->setEnabled(false);
        checkBox_348->setBaseSize(QSize(0, 0));
        checkBox_348->setFont(font4);
        checkBox_348->setTabletTracking(false);
        checkBox_348->setAutoFillBackground(false);
        checkBox_348->setChecked(false);
        checkBox_348->setTristate(false);

        gridLayout_82->addWidget(checkBox_348, 11, 1, 1, 1);

        checkBox_349 = new QCheckBox(scrollAreaWidgetContents_20);
        checkBox_349->setObjectName(QStringLiteral("checkBox_349"));
        checkBox_349->setEnabled(false);
        checkBox_349->setBaseSize(QSize(0, 0));
        checkBox_349->setFont(font4);
        checkBox_349->setTabletTracking(false);
        checkBox_349->setAutoFillBackground(false);
        checkBox_349->setChecked(false);
        checkBox_349->setTristate(false);

        gridLayout_82->addWidget(checkBox_349, 9, 1, 1, 1);

        checkBox_350 = new QCheckBox(scrollAreaWidgetContents_20);
        checkBox_350->setObjectName(QStringLiteral("checkBox_350"));
        checkBox_350->setEnabled(false);
        checkBox_350->setBaseSize(QSize(0, 0));
        checkBox_350->setFont(font4);
        checkBox_350->setTabletTracking(false);
        checkBox_350->setAutoFillBackground(false);
        checkBox_350->setChecked(false);
        checkBox_350->setTristate(false);

        gridLayout_82->addWidget(checkBox_350, 2, 1, 1, 1);

        textFIeld_name5_219 = new QLineEdit(scrollAreaWidgetContents_20);
        textFIeld_name5_219->setObjectName(QStringLiteral("textFIeld_name5_219"));
        textFIeld_name5_219->setFont(font2);

        gridLayout_82->addWidget(textFIeld_name5_219, 15, 4, 1, 1);

        checkBox_351 = new QCheckBox(scrollAreaWidgetContents_20);
        checkBox_351->setObjectName(QStringLiteral("checkBox_351"));
        checkBox_351->setEnabled(false);
        checkBox_351->setBaseSize(QSize(0, 0));
        checkBox_351->setFont(font4);
        checkBox_351->setTabletTracking(false);
        checkBox_351->setAutoFillBackground(false);
        checkBox_351->setChecked(false);
        checkBox_351->setTristate(false);

        gridLayout_82->addWidget(checkBox_351, 10, 1, 1, 1);

        spinBox_start5_252 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_start5_252->setObjectName(QStringLiteral("spinBox_start5_252"));
        spinBox_start5_252->setFont(font6);
        spinBox_start5_252->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_start5_252, 5, 6, 1, 2);

        label_ID3_37 = new QLabel(scrollAreaWidgetContents_20);
        label_ID3_37->setObjectName(QStringLiteral("label_ID3_37"));
        label_ID3_37->setFont(font5);
        label_ID3_37->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_82->addWidget(label_ID3_37, 8, 2, 1, 1);

        label_ID1_20 = new QLabel(scrollAreaWidgetContents_20);
        label_ID1_20->setObjectName(QStringLiteral("label_ID1_20"));
        label_ID1_20->setFont(font5);
        label_ID1_20->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_82->addWidget(label_ID1_20, 0, 2, 1, 1);

        textFIeld_name5_220 = new QLineEdit(scrollAreaWidgetContents_20);
        textFIeld_name5_220->setObjectName(QStringLiteral("textFIeld_name5_220"));
        textFIeld_name5_220->setFont(font2);

        gridLayout_82->addWidget(textFIeld_name5_220, 14, 4, 1, 1);

        spinBox_duration5_221 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_duration5_221->setObjectName(QStringLiteral("spinBox_duration5_221"));
        spinBox_duration5_221->setFont(font6);
        spinBox_duration5_221->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_duration5_221, 13, 9, 1, 2);

        horizontalSpacer_304 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_82->addItem(horizontalSpacer_304, 21, 3, 1, 1);

        label_ID4_260 = new QLabel(scrollAreaWidgetContents_20);
        label_ID4_260->setObjectName(QStringLiteral("label_ID4_260"));
        label_ID4_260->setFont(font5);
        label_ID4_260->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_82->addWidget(label_ID4_260, 18, 2, 1, 1);

        checkBox_352 = new QCheckBox(scrollAreaWidgetContents_20);
        checkBox_352->setObjectName(QStringLiteral("checkBox_352"));
        checkBox_352->setEnabled(false);
        checkBox_352->setBaseSize(QSize(0, 0));
        checkBox_352->setFont(font4);
        checkBox_352->setTabletTracking(false);
        checkBox_352->setAutoFillBackground(false);
        checkBox_352->setChecked(false);
        checkBox_352->setTristate(false);

        gridLayout_82->addWidget(checkBox_352, 18, 1, 1, 1);

        checkBox_353 = new QCheckBox(scrollAreaWidgetContents_20);
        checkBox_353->setObjectName(QStringLiteral("checkBox_353"));
        checkBox_353->setEnabled(false);
        checkBox_353->setBaseSize(QSize(0, 0));
        checkBox_353->setFont(font4);
        checkBox_353->setTabletTracking(false);
        checkBox_353->setAutoFillBackground(false);
        checkBox_353->setChecked(false);
        checkBox_353->setTristate(false);

        gridLayout_82->addWidget(checkBox_353, 20, 1, 1, 1);

        checkBox_354 = new QCheckBox(scrollAreaWidgetContents_20);
        checkBox_354->setObjectName(QStringLiteral("checkBox_354"));
        checkBox_354->setEnabled(false);
        checkBox_354->setBaseSize(QSize(0, 0));
        checkBox_354->setFont(font4);
        checkBox_354->setTabletTracking(false);
        checkBox_354->setAutoFillBackground(false);
        checkBox_354->setChecked(false);
        checkBox_354->setTristate(false);

        gridLayout_82->addWidget(checkBox_354, 17, 1, 1, 1);

        spinBox_start5_253 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_start5_253->setObjectName(QStringLiteral("spinBox_start5_253"));
        spinBox_start5_253->setFont(font6);
        spinBox_start5_253->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_start5_253, 11, 6, 1, 2);

        checkBox_355 = new QCheckBox(scrollAreaWidgetContents_20);
        checkBox_355->setObjectName(QStringLiteral("checkBox_355"));
        checkBox_355->setEnabled(false);
        checkBox_355->setBaseSize(QSize(0, 0));
        checkBox_355->setFont(font4);
        checkBox_355->setTabletTracking(false);
        checkBox_355->setAutoFillBackground(false);
        checkBox_355->setChecked(false);
        checkBox_355->setTristate(false);

        gridLayout_82->addWidget(checkBox_355, 16, 1, 1, 1);

        spinBox_start5_254 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_start5_254->setObjectName(QStringLiteral("spinBox_start5_254"));
        spinBox_start5_254->setFont(font6);
        spinBox_start5_254->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_start5_254, 16, 6, 1, 2);

        label_ID4_261 = new QLabel(scrollAreaWidgetContents_20);
        label_ID4_261->setObjectName(QStringLiteral("label_ID4_261"));
        label_ID4_261->setFont(font5);
        label_ID4_261->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_82->addWidget(label_ID4_261, 17, 2, 1, 1);

        circle_inactive3_132 = new QLabel(scrollAreaWidgetContents_20);
        circle_inactive3_132->setObjectName(QStringLiteral("circle_inactive3_132"));
        circle_inactive3_132->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_132->setAlignment(Qt::AlignCenter);

        gridLayout_82->addWidget(circle_inactive3_132, 4, 3, 1, 1);

        spinBox_start5_255 = new QSpinBox(scrollAreaWidgetContents_20);
        spinBox_start5_255->setObjectName(QStringLiteral("spinBox_start5_255"));
        spinBox_start5_255->setFont(font6);
        spinBox_start5_255->setMaximum(10000000);

        gridLayout_82->addWidget(spinBox_start5_255, 18, 6, 1, 2);

        circle_inactive1_210 = new QLabel(scrollAreaWidgetContents_20);
        circle_inactive1_210->setObjectName(QStringLiteral("circle_inactive1_210"));
        circle_inactive1_210->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_210->setAlignment(Qt::AlignCenter);

        gridLayout_82->addWidget(circle_inactive1_210, 1, 3, 1, 1);

        textFIeld_name5_221 = new QLineEdit(scrollAreaWidgetContents_20);
        textFIeld_name5_221->setObjectName(QStringLiteral("textFIeld_name5_221"));
        textFIeld_name5_221->setFont(font2);

        gridLayout_82->addWidget(textFIeld_name5_221, 17, 4, 1, 1);

        circle_inactive3_133 = new QLabel(scrollAreaWidgetContents_20);
        circle_inactive3_133->setObjectName(QStringLiteral("circle_inactive3_133"));
        circle_inactive3_133->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_133->setAlignment(Qt::AlignCenter);

        gridLayout_82->addWidget(circle_inactive3_133, 6, 3, 1, 1);

        horizontalSpacer_305 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_82->addItem(horizontalSpacer_305, 0, 8, 1, 1);

        circle_inactive3_134 = new QLabel(scrollAreaWidgetContents_20);
        circle_inactive3_134->setObjectName(QStringLiteral("circle_inactive3_134"));
        circle_inactive3_134->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_134->setAlignment(Qt::AlignCenter);

        gridLayout_82->addWidget(circle_inactive3_134, 7, 3, 1, 1);

        circle_inactive1_211 = new QLabel(scrollAreaWidgetContents_20);
        circle_inactive1_211->setObjectName(QStringLiteral("circle_inactive1_211"));
        circle_inactive1_211->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_211->setAlignment(Qt::AlignCenter);

        gridLayout_82->addWidget(circle_inactive1_211, 9, 3, 1, 1);

        circle_inactive1_212 = new QLabel(scrollAreaWidgetContents_20);
        circle_inactive1_212->setObjectName(QStringLiteral("circle_inactive1_212"));
        circle_inactive1_212->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_212->setAlignment(Qt::AlignCenter);

        gridLayout_82->addWidget(circle_inactive1_212, 10, 3, 1, 1);

        circle_inactive1_213 = new QLabel(scrollAreaWidgetContents_20);
        circle_inactive1_213->setObjectName(QStringLiteral("circle_inactive1_213"));
        circle_inactive1_213->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_213->setAlignment(Qt::AlignCenter);

        gridLayout_82->addWidget(circle_inactive1_213, 11, 3, 1, 1);

        circle_inactive1_214 = new QLabel(scrollAreaWidgetContents_20);
        circle_inactive1_214->setObjectName(QStringLiteral("circle_inactive1_214"));
        circle_inactive1_214->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_214->setAlignment(Qt::AlignCenter);

        gridLayout_82->addWidget(circle_inactive1_214, 12, 3, 1, 1);

        circle_inactive1_215 = new QLabel(scrollAreaWidgetContents_20);
        circle_inactive1_215->setObjectName(QStringLiteral("circle_inactive1_215"));
        circle_inactive1_215->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_215->setAlignment(Qt::AlignCenter);

        gridLayout_82->addWidget(circle_inactive1_215, 13, 3, 1, 1);

        circle_inactive1_216 = new QLabel(scrollAreaWidgetContents_20);
        circle_inactive1_216->setObjectName(QStringLiteral("circle_inactive1_216"));
        circle_inactive1_216->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_216->setAlignment(Qt::AlignCenter);

        gridLayout_82->addWidget(circle_inactive1_216, 14, 3, 1, 1);

        circle_inactive1_217 = new QLabel(scrollAreaWidgetContents_20);
        circle_inactive1_217->setObjectName(QStringLiteral("circle_inactive1_217"));
        circle_inactive1_217->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_217->setAlignment(Qt::AlignCenter);

        gridLayout_82->addWidget(circle_inactive1_217, 15, 3, 1, 1);

        circle_inactive1_218 = new QLabel(scrollAreaWidgetContents_20);
        circle_inactive1_218->setObjectName(QStringLiteral("circle_inactive1_218"));
        circle_inactive1_218->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_218->setAlignment(Qt::AlignCenter);

        gridLayout_82->addWidget(circle_inactive1_218, 16, 3, 1, 1);

        circle_inactive1_219 = new QLabel(scrollAreaWidgetContents_20);
        circle_inactive1_219->setObjectName(QStringLiteral("circle_inactive1_219"));
        circle_inactive1_219->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_219->setAlignment(Qt::AlignCenter);

        gridLayout_82->addWidget(circle_inactive1_219, 17, 3, 1, 1);

        circle_inactive1_220 = new QLabel(scrollAreaWidgetContents_20);
        circle_inactive1_220->setObjectName(QStringLiteral("circle_inactive1_220"));
        circle_inactive1_220->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_220->setAlignment(Qt::AlignCenter);

        gridLayout_82->addWidget(circle_inactive1_220, 18, 3, 1, 1);

        circle_inactive1_221 = new QLabel(scrollAreaWidgetContents_20);
        circle_inactive1_221->setObjectName(QStringLiteral("circle_inactive1_221"));
        circle_inactive1_221->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_221->setAlignment(Qt::AlignCenter);

        gridLayout_82->addWidget(circle_inactive1_221, 20, 3, 1, 1);


        gridLayout_81->addLayout(gridLayout_82, 1, 0, 1, 1);

        scrollArea_17->setWidget(scrollAreaWidgetContents_20);

        gridLayout_79->addWidget(scrollArea_17, 1, 0, 1, 1);

        horizontalSpacer_306 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_79->addItem(horizontalSpacer_306, 1, 1, 1, 1);


        gridLayout_78->addLayout(gridLayout_79, 0, 0, 1, 1);

        horizontalSpacer_307 = new QSpacerItem(8, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_78->addItem(horizontalSpacer_307, 0, 1, 1, 1);

        tabWidget_2->addTab(tab_13, QString());
        tab_14 = new QWidget();
        tab_14->setObjectName(QStringLiteral("tab_14"));
        tab_14->setEnabled(false);
        gridLayout_83 = new QGridLayout(tab_14);
        gridLayout_83->setSpacing(6);
        gridLayout_83->setContentsMargins(11, 11, 11, 11);
        gridLayout_83->setObjectName(QStringLiteral("gridLayout_83"));
        gridLayout_84 = new QGridLayout();
        gridLayout_84->setSpacing(6);
        gridLayout_84->setObjectName(QStringLiteral("gridLayout_84"));
        horizontalLayout_41 = new QHBoxLayout();
        horizontalLayout_41->setSpacing(6);
        horizontalLayout_41->setObjectName(QStringLiteral("horizontalLayout_41"));
        horizontalSpacer_308 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_41->addItem(horizontalSpacer_308);

        horizontalSpacer_309 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_41->addItem(horizontalSpacer_309);

        label_ID_22 = new QLabel(tab_14);
        label_ID_22->setObjectName(QStringLiteral("label_ID_22"));
        sizePolicy.setHeightForWidth(label_ID_22->sizePolicy().hasHeightForWidth());
        label_ID_22->setSizePolicy(sizePolicy);
        label_ID_22->setMaximumSize(QSize(16777215, 16777215));
        label_ID_22->setFont(font);
        label_ID_22->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_41->addWidget(label_ID_22);

        horizontalSpacer_310 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_41->addItem(horizontalSpacer_310);

        label_name_20 = new QLabel(tab_14);
        label_name_20->setObjectName(QStringLiteral("label_name_20"));
        label_name_20->setFont(font);
        label_name_20->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_41->addWidget(label_name_20);

        horizontalSpacer_311 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_41->addItem(horizontalSpacer_311);

        horizontalSpacer_312 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_41->addItem(horizontalSpacer_312);

        label_start_22 = new QLabel(tab_14);
        label_start_22->setObjectName(QStringLiteral("label_start_22"));
        sizePolicy1.setHeightForWidth(label_start_22->sizePolicy().hasHeightForWidth());
        label_start_22->setSizePolicy(sizePolicy1);
        label_start_22->setFont(font);
        label_start_22->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_41->addWidget(label_start_22);

        label_msStart_23 = new QLabel(tab_14);
        label_msStart_23->setObjectName(QStringLiteral("label_msStart_23"));
        label_msStart_23->setFont(font1);
        label_msStart_23->setAlignment(Qt::AlignCenter);

        horizontalLayout_41->addWidget(label_msStart_23);

        horizontalSpacer_313 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_41->addItem(horizontalSpacer_313);

        label_duration_28 = new QLabel(tab_14);
        label_duration_28->setObjectName(QStringLiteral("label_duration_28"));
        label_duration_28->setFont(font);
        label_duration_28->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_41->addWidget(label_duration_28);

        label_msDuration_42 = new QLabel(tab_14);
        label_msDuration_42->setObjectName(QStringLiteral("label_msDuration_42"));
        label_msDuration_42->setFont(font1);
        label_msDuration_42->setAlignment(Qt::AlignCenter);

        horizontalLayout_41->addWidget(label_msDuration_42);

        horizontalSpacer_314 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_41->addItem(horizontalSpacer_314);


        gridLayout_84->addLayout(horizontalLayout_41, 0, 0, 1, 1);

        gridLayout_85 = new QGridLayout();
        gridLayout_85->setSpacing(6);
        gridLayout_85->setObjectName(QStringLiteral("gridLayout_85"));
        horizontalLayout_42 = new QHBoxLayout();
        horizontalLayout_42->setSpacing(6);
        horizontalLayout_42->setObjectName(QStringLiteral("horizontalLayout_42"));
        progressBar_36 = new QProgressBar(tab_14);
        progressBar_36->setObjectName(QStringLiteral("progressBar_36"));
        progressBar_36->setValue(0);

        horizontalLayout_42->addWidget(progressBar_36);

        label_72 = new QLabel(tab_14);
        label_72->setObjectName(QStringLiteral("label_72"));
        label_72->setFont(font3);

        horizontalLayout_42->addWidget(label_72);


        gridLayout_85->addLayout(horizontalLayout_42, 4, 0, 1, 5);

        spinBox_start5_256 = new QSpinBox(tab_14);
        spinBox_start5_256->setObjectName(QStringLiteral("spinBox_start5_256"));
        spinBox_start5_256->setFont(font6);
        spinBox_start5_256->setMaximum(10000000);
        spinBox_start5_256->setValue(0);

        gridLayout_85->addWidget(spinBox_start5_256, 2, 3, 1, 2);

        spinBox_start5_257 = new QSpinBox(tab_14);
        spinBox_start5_257->setObjectName(QStringLiteral("spinBox_start5_257"));
        spinBox_start5_257->setFont(font6);
        spinBox_start5_257->setMaximum(10000000);
        spinBox_start5_257->setValue(0);

        gridLayout_85->addWidget(spinBox_start5_257, 2, 0, 1, 2);

        pushButton_71 = new QPushButton(tab_14);
        pushButton_71->setObjectName(QStringLiteral("pushButton_71"));
        pushButton_71->setMinimumSize(QSize(0, 80));
        pushButton_71->setFont(font2);

        gridLayout_85->addWidget(pushButton_71, 10, 3, 1, 2);

        horizontalSpacer_315 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_85->addItem(horizontalSpacer_315, 8, 2, 1, 1);

        verticalSpacer_95 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_85->addItem(verticalSpacer_95, 7, 0, 1, 1);

        pushButton_72 = new QPushButton(tab_14);
        pushButton_72->setObjectName(QStringLiteral("pushButton_72"));
        pushButton_72->setMinimumSize(QSize(0, 80));
        pushButton_72->setFont(font2);

        gridLayout_85->addWidget(pushButton_72, 8, 0, 1, 2);

        label_73 = new QLabel(tab_14);
        label_73->setObjectName(QStringLiteral("label_73"));
        label_73->setFont(font10);
        label_73->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_85->addWidget(label_73, 3, 0, 1, 5);

        label_74 = new QLabel(tab_14);
        label_74->setObjectName(QStringLiteral("label_74"));
        label_74->setFont(font8);

        gridLayout_85->addWidget(label_74, 1, 3, 1, 1);

        line_21 = new QFrame(tab_14);
        line_21->setObjectName(QStringLiteral("line_21"));
        line_21->setFrameShape(QFrame::HLine);
        line_21->setFrameShadow(QFrame::Sunken);

        gridLayout_85->addWidget(line_21, 6, 0, 1, 5);

        horizontalSpacer_316 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_85->addItem(horizontalSpacer_316, 0, 1, 1, 1);

        verticalSpacer_96 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_85->addItem(verticalSpacer_96, 0, 0, 1, 1);

        pushButton_73 = new QPushButton(tab_14);
        pushButton_73->setObjectName(QStringLiteral("pushButton_73"));
        pushButton_73->setMinimumSize(QSize(0, 80));
        pushButton_73->setFont(font2);

        gridLayout_85->addWidget(pushButton_73, 10, 0, 1, 2);

        label_msDuration_43 = new QLabel(tab_14);
        label_msDuration_43->setObjectName(QStringLiteral("label_msDuration_43"));
        label_msDuration_43->setFont(font1);
        label_msDuration_43->setAlignment(Qt::AlignCenter);

        gridLayout_85->addWidget(label_msDuration_43, 1, 4, 1, 1);

        verticalSpacer_97 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_85->addItem(verticalSpacer_97, 5, 0, 1, 1);

        pushButton_74 = new QPushButton(tab_14);
        pushButton_74->setObjectName(QStringLiteral("pushButton_74"));
        pushButton_74->setMinimumSize(QSize(0, 80));
        pushButton_74->setFont(font2);

        gridLayout_85->addWidget(pushButton_74, 8, 3, 1, 2);

        verticalSpacer_98 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_85->addItem(verticalSpacer_98, 9, 0, 1, 1);

        label_75 = new QLabel(tab_14);
        label_75->setObjectName(QStringLiteral("label_75"));
        label_75->setFont(font8);

        gridLayout_85->addWidget(label_75, 1, 0, 1, 1);

        verticalSpacer_99 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_85->addItem(verticalSpacer_99, 11, 0, 1, 1);


        gridLayout_84->addLayout(gridLayout_85, 0, 2, 2, 1);

        scrollArea_18 = new QScrollArea(tab_14);
        scrollArea_18->setObjectName(QStringLiteral("scrollArea_18"));
        scrollArea_18->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        scrollArea_18->setWidgetResizable(true);
        scrollAreaWidgetContents_21 = new QWidget();
        scrollAreaWidgetContents_21->setObjectName(QStringLiteral("scrollAreaWidgetContents_21"));
        scrollAreaWidgetContents_21->setGeometry(QRect(0, 0, 665, 740));
        gridLayout_86 = new QGridLayout(scrollAreaWidgetContents_21);
        gridLayout_86->setSpacing(6);
        gridLayout_86->setContentsMargins(11, 11, 11, 11);
        gridLayout_86->setObjectName(QStringLiteral("gridLayout_86"));
        gridLayout_87 = new QGridLayout();
        gridLayout_87->setSpacing(1);
        gridLayout_87->setObjectName(QStringLiteral("gridLayout_87"));
        textFIeld_name5_222 = new QLineEdit(scrollAreaWidgetContents_21);
        textFIeld_name5_222->setObjectName(QStringLiteral("textFIeld_name5_222"));
        textFIeld_name5_222->setFont(font2);

        gridLayout_87->addWidget(textFIeld_name5_222, 12, 4, 1, 1);

        spinBox_start2_35 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_start2_35->setObjectName(QStringLiteral("spinBox_start2_35"));
        spinBox_start2_35->setFont(font6);
        spinBox_start2_35->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_start2_35, 7, 6, 1, 2);

        label_ID4_262 = new QLabel(scrollAreaWidgetContents_21);
        label_ID4_262->setObjectName(QStringLiteral("label_ID4_262"));
        label_ID4_262->setFont(font5);
        label_ID4_262->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_87->addWidget(label_ID4_262, 5, 2, 1, 1);

        circle_inactive3_135 = new QLabel(scrollAreaWidgetContents_21);
        circle_inactive3_135->setObjectName(QStringLiteral("circle_inactive3_135"));
        circle_inactive3_135->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_135->setAlignment(Qt::AlignCenter);

        gridLayout_87->addWidget(circle_inactive3_135, 5, 3, 1, 1);

        horizontalSpacer_317 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_87->addItem(horizontalSpacer_317, 6, 0, 1, 1);

        checkBox_356 = new QCheckBox(scrollAreaWidgetContents_21);
        checkBox_356->setObjectName(QStringLiteral("checkBox_356"));
        checkBox_356->setEnabled(false);
        checkBox_356->setBaseSize(QSize(0, 0));
        checkBox_356->setFont(font4);
        checkBox_356->setTabletTracking(false);
        checkBox_356->setAutoFillBackground(false);
        checkBox_356->setChecked(false);
        checkBox_356->setTristate(false);

        gridLayout_87->addWidget(checkBox_356, 3, 1, 1, 1);

        checkBox_357 = new QCheckBox(scrollAreaWidgetContents_21);
        checkBox_357->setObjectName(QStringLiteral("checkBox_357"));
        checkBox_357->setEnabled(false);
        checkBox_357->setBaseSize(QSize(0, 0));
        checkBox_357->setFont(font4);
        checkBox_357->setTabletTracking(false);
        checkBox_357->setAutoFillBackground(false);
        checkBox_357->setChecked(false);
        checkBox_357->setTristate(false);

        gridLayout_87->addWidget(checkBox_357, 5, 1, 1, 1);

        checkBox_358 = new QCheckBox(scrollAreaWidgetContents_21);
        checkBox_358->setObjectName(QStringLiteral("checkBox_358"));
        checkBox_358->setEnabled(false);
        checkBox_358->setBaseSize(QSize(0, 0));
        checkBox_358->setFont(font4);
        checkBox_358->setTabletTracking(false);
        checkBox_358->setAutoFillBackground(false);
        checkBox_358->setChecked(false);
        checkBox_358->setTristate(false);

        gridLayout_87->addWidget(checkBox_358, 8, 1, 1, 1);

        spinBox_start1_18 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_start1_18->setObjectName(QStringLiteral("spinBox_start1_18"));
        spinBox_start1_18->setFont(font6);
        spinBox_start1_18->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_start1_18, 0, 6, 1, 2);

        checkBox_359 = new QCheckBox(scrollAreaWidgetContents_21);
        checkBox_359->setObjectName(QStringLiteral("checkBox_359"));
        checkBox_359->setEnabled(false);
        checkBox_359->setBaseSize(QSize(0, 0));
        checkBox_359->setFont(font4);
        checkBox_359->setTabletTracking(false);
        checkBox_359->setAutoFillBackground(false);
        checkBox_359->setChecked(false);
        checkBox_359->setTristate(false);

        gridLayout_87->addWidget(checkBox_359, 4, 1, 1, 1);

        circle_inactive3_136 = new QLabel(scrollAreaWidgetContents_21);
        circle_inactive3_136->setObjectName(QStringLiteral("circle_inactive3_136"));
        circle_inactive3_136->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_136->setAlignment(Qt::AlignCenter);

        gridLayout_87->addWidget(circle_inactive3_136, 3, 3, 1, 1);

        textFIeld_name1_51 = new QLineEdit(scrollAreaWidgetContents_21);
        textFIeld_name1_51->setObjectName(QStringLiteral("textFIeld_name1_51"));
        textFIeld_name1_51->setFont(font2);

        gridLayout_87->addWidget(textFIeld_name1_51, 0, 4, 1, 1);

        spinBox_duration2_35 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_duration2_35->setObjectName(QStringLiteral("spinBox_duration2_35"));
        spinBox_duration2_35->setFont(font6);
        spinBox_duration2_35->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_duration2_35, 7, 9, 1, 2);

        spinBox_duration4_35 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_duration4_35->setObjectName(QStringLiteral("spinBox_duration4_35"));
        spinBox_duration4_35->setFont(font6);
        spinBox_duration4_35->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_duration4_35, 3, 9, 1, 2);

        textFIeld_name4_35 = new QLineEdit(scrollAreaWidgetContents_21);
        textFIeld_name4_35->setObjectName(QStringLiteral("textFIeld_name4_35"));
        textFIeld_name4_35->setFont(font2);

        gridLayout_87->addWidget(textFIeld_name4_35, 3, 4, 1, 1);

        spinBox_duration5_222 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_duration5_222->setObjectName(QStringLiteral("spinBox_duration5_222"));
        spinBox_duration5_222->setFont(font6);
        spinBox_duration5_222->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_duration5_222, 6, 9, 1, 2);

        checkBox_360 = new QCheckBox(scrollAreaWidgetContents_21);
        checkBox_360->setObjectName(QStringLiteral("checkBox_360"));
        checkBox_360->setEnabled(false);
        checkBox_360->setBaseSize(QSize(0, 0));
        checkBox_360->setFont(font4);
        checkBox_360->setTabletTracking(false);
        checkBox_360->setAutoFillBackground(false);
        checkBox_360->setChecked(false);
        checkBox_360->setTristate(false);

        gridLayout_87->addWidget(checkBox_360, 14, 1, 1, 1);

        textFIeld_name2_35 = new QLineEdit(scrollAreaWidgetContents_21);
        textFIeld_name2_35->setObjectName(QStringLiteral("textFIeld_name2_35"));
        textFIeld_name2_35->setFont(font2);

        gridLayout_87->addWidget(textFIeld_name2_35, 7, 4, 1, 1);

        spinBox_duration5_223 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_duration5_223->setObjectName(QStringLiteral("spinBox_duration5_223"));
        spinBox_duration5_223->setFont(font6);
        spinBox_duration5_223->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_duration5_223, 10, 9, 1, 2);

        textFIeld_name5_223 = new QLineEdit(scrollAreaWidgetContents_21);
        textFIeld_name5_223->setObjectName(QStringLiteral("textFIeld_name5_223"));
        textFIeld_name5_223->setFont(font2);

        gridLayout_87->addWidget(textFIeld_name5_223, 4, 4, 1, 1);

        spinBox_start5_258 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_start5_258->setObjectName(QStringLiteral("spinBox_start5_258"));
        spinBox_start5_258->setFont(font6);
        spinBox_start5_258->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_start5_258, 4, 6, 1, 2);

        label_ID4_263 = new QLabel(scrollAreaWidgetContents_21);
        label_ID4_263->setObjectName(QStringLiteral("label_ID4_263"));
        label_ID4_263->setFont(font5);
        label_ID4_263->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_87->addWidget(label_ID4_263, 3, 2, 1, 1);

        spinBox_start3_35 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_start3_35->setObjectName(QStringLiteral("spinBox_start3_35"));
        spinBox_start3_35->setFont(font6);
        spinBox_start3_35->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_start3_35, 8, 6, 1, 2);

        label_ID4_264 = new QLabel(scrollAreaWidgetContents_21);
        label_ID4_264->setObjectName(QStringLiteral("label_ID4_264"));
        label_ID4_264->setFont(font5);
        label_ID4_264->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_87->addWidget(label_ID4_264, 9, 2, 1, 1);

        spinBox_duration4_36 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_duration4_36->setObjectName(QStringLiteral("spinBox_duration4_36"));
        spinBox_duration4_36->setFont(font6);
        spinBox_duration4_36->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_duration4_36, 9, 9, 1, 2);

        checkBox_361 = new QCheckBox(scrollAreaWidgetContents_21);
        checkBox_361->setObjectName(QStringLiteral("checkBox_361"));
        checkBox_361->setEnabled(false);
        checkBox_361->setBaseSize(QSize(0, 0));
        checkBox_361->setFont(font4);
        checkBox_361->setTabletTracking(false);
        checkBox_361->setAutoFillBackground(false);
        checkBox_361->setChecked(false);
        checkBox_361->setTristate(false);

        gridLayout_87->addWidget(checkBox_361, 13, 1, 1, 1);

        spinBox_start5_259 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_start5_259->setObjectName(QStringLiteral("spinBox_start5_259"));
        spinBox_start5_259->setFont(font6);
        spinBox_start5_259->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_start5_259, 15, 6, 1, 2);

        spinBox_duration5_224 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_duration5_224->setObjectName(QStringLiteral("spinBox_duration5_224"));
        spinBox_duration5_224->setFont(font6);
        spinBox_duration5_224->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_duration5_224, 15, 9, 1, 2);

        textFIeld_name5_224 = new QLineEdit(scrollAreaWidgetContents_21);
        textFIeld_name5_224->setObjectName(QStringLiteral("textFIeld_name5_224"));
        textFIeld_name5_224->setFont(font2);

        gridLayout_87->addWidget(textFIeld_name5_224, 10, 4, 1, 1);

        label_ID4_265 = new QLabel(scrollAreaWidgetContents_21);
        label_ID4_265->setObjectName(QStringLiteral("label_ID4_265"));
        label_ID4_265->setFont(font5);
        label_ID4_265->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_87->addWidget(label_ID4_265, 14, 2, 1, 1);

        spinBox_start5_260 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_start5_260->setObjectName(QStringLiteral("spinBox_start5_260"));
        spinBox_start5_260->setFont(font6);
        spinBox_start5_260->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_start5_260, 14, 6, 1, 2);

        textFIeld_name4_36 = new QLineEdit(scrollAreaWidgetContents_21);
        textFIeld_name4_36->setObjectName(QStringLiteral("textFIeld_name4_36"));
        textFIeld_name4_36->setFont(font2);

        gridLayout_87->addWidget(textFIeld_name4_36, 9, 4, 1, 1);

        spinBox_start3_36 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_start3_36->setObjectName(QStringLiteral("spinBox_start3_36"));
        spinBox_start3_36->setFont(font6);
        spinBox_start3_36->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_start3_36, 2, 6, 1, 2);

        spinBox_start5_261 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_start5_261->setObjectName(QStringLiteral("spinBox_start5_261"));
        spinBox_start5_261->setFont(font6);
        spinBox_start5_261->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_start5_261, 6, 6, 1, 2);

        horizontalSpacer_318 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_87->addItem(horizontalSpacer_318, 0, 12, 1, 1);

        horizontalSpacer_319 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_87->addItem(horizontalSpacer_319, 10, 5, 1, 1);

        spinBox_duration5_225 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_duration5_225->setObjectName(QStringLiteral("spinBox_duration5_225"));
        spinBox_duration5_225->setFont(font6);
        spinBox_duration5_225->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_duration5_225, 11, 9, 1, 2);

        textFIeld_name5_225 = new QLineEdit(scrollAreaWidgetContents_21);
        textFIeld_name5_225->setObjectName(QStringLiteral("textFIeld_name5_225"));
        textFIeld_name5_225->setFont(font2);

        gridLayout_87->addWidget(textFIeld_name5_225, 5, 4, 1, 1);

        label_ID2_38 = new QLabel(scrollAreaWidgetContents_21);
        label_ID2_38->setObjectName(QStringLiteral("label_ID2_38"));
        label_ID2_38->setFont(font5);
        label_ID2_38->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_87->addWidget(label_ID2_38, 1, 2, 1, 1);

        checkBox_362 = new QCheckBox(scrollAreaWidgetContents_21);
        checkBox_362->setObjectName(QStringLiteral("checkBox_362"));
        checkBox_362->setEnabled(false);
        checkBox_362->setBaseSize(QSize(0, 0));
        checkBox_362->setFont(font4);
        checkBox_362->setTabletTracking(false);
        checkBox_362->setAutoFillBackground(false);
        checkBox_362->setChecked(false);
        checkBox_362->setTristate(false);

        gridLayout_87->addWidget(checkBox_362, 6, 1, 1, 1);

        spinBox_start5_262 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_start5_262->setObjectName(QStringLiteral("spinBox_start5_262"));
        spinBox_start5_262->setFont(font6);
        spinBox_start5_262->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_start5_262, 17, 6, 1, 2);

        textFIeld_name2_36 = new QLineEdit(scrollAreaWidgetContents_21);
        textFIeld_name2_36->setObjectName(QStringLiteral("textFIeld_name2_36"));
        textFIeld_name2_36->setFont(font2);

        gridLayout_87->addWidget(textFIeld_name2_36, 1, 4, 1, 1);

        textFIeld_name5_226 = new QLineEdit(scrollAreaWidgetContents_21);
        textFIeld_name5_226->setObjectName(QStringLiteral("textFIeld_name5_226"));
        textFIeld_name5_226->setFont(font2);

        gridLayout_87->addWidget(textFIeld_name5_226, 6, 4, 1, 1);

        checkBox_363 = new QCheckBox(scrollAreaWidgetContents_21);
        checkBox_363->setObjectName(QStringLiteral("checkBox_363"));
        checkBox_363->setEnabled(false);
        checkBox_363->setBaseSize(QSize(0, 0));
        checkBox_363->setFont(font4);
        checkBox_363->setTabletTracking(false);
        checkBox_363->setAutoFillBackground(false);
        checkBox_363->setChecked(false);
        checkBox_363->setTristate(false);

        gridLayout_87->addWidget(checkBox_363, 7, 1, 1, 1);

        label_ID4_266 = new QLabel(scrollAreaWidgetContents_21);
        label_ID4_266->setObjectName(QStringLiteral("label_ID4_266"));
        label_ID4_266->setFont(font5);
        label_ID4_266->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_87->addWidget(label_ID4_266, 6, 2, 1, 1);

        circle_inactive3_137 = new QLabel(scrollAreaWidgetContents_21);
        circle_inactive3_137->setObjectName(QStringLiteral("circle_inactive3_137"));
        QPalette palette8;
        palette8.setBrush(QPalette::Active, QPalette::Button, brush6);
        palette8.setBrush(QPalette::Inactive, QPalette::Button, brush6);
        palette8.setBrush(QPalette::Disabled, QPalette::Button, brush6);
        circle_inactive3_137->setPalette(palette8);
        circle_inactive3_137->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_137->setAlignment(Qt::AlignCenter);

        gridLayout_87->addWidget(circle_inactive3_137, 8, 3, 1, 1);

        textFIeld_name3_35 = new QLineEdit(scrollAreaWidgetContents_21);
        textFIeld_name3_35->setObjectName(QStringLiteral("textFIeld_name3_35"));
        textFIeld_name3_35->setFont(font2);

        gridLayout_87->addWidget(textFIeld_name3_35, 2, 4, 1, 1);

        spinBox_duration5_226 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_duration5_226->setObjectName(QStringLiteral("spinBox_duration5_226"));
        spinBox_duration5_226->setFont(font6);
        spinBox_duration5_226->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_duration5_226, 5, 9, 1, 2);

        textFIeld_name3_36 = new QLineEdit(scrollAreaWidgetContents_21);
        textFIeld_name3_36->setObjectName(QStringLiteral("textFIeld_name3_36"));
        textFIeld_name3_36->setFont(font2);

        gridLayout_87->addWidget(textFIeld_name3_36, 8, 4, 1, 1);

        spinBox_duration5_227 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_duration5_227->setObjectName(QStringLiteral("spinBox_duration5_227"));
        spinBox_duration5_227->setFont(font6);
        spinBox_duration5_227->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_duration5_227, 14, 9, 1, 2);

        checkBox_364 = new QCheckBox(scrollAreaWidgetContents_21);
        checkBox_364->setObjectName(QStringLiteral("checkBox_364"));
        checkBox_364->setEnabled(false);
        checkBox_364->setBaseSize(QSize(0, 0));
        checkBox_364->setFont(font4);
        checkBox_364->setTabletTracking(false);
        checkBox_364->setAutoFillBackground(false);
        checkBox_364->setChecked(false);
        checkBox_364->setTristate(false);

        gridLayout_87->addWidget(checkBox_364, 12, 1, 1, 1);

        textFIeld_name5_227 = new QLineEdit(scrollAreaWidgetContents_21);
        textFIeld_name5_227->setObjectName(QStringLiteral("textFIeld_name5_227"));
        textFIeld_name5_227->setFont(font2);

        gridLayout_87->addWidget(textFIeld_name5_227, 18, 4, 1, 1);

        textFIeld_name5_228 = new QLineEdit(scrollAreaWidgetContents_21);
        textFIeld_name5_228->setObjectName(QStringLiteral("textFIeld_name5_228"));
        textFIeld_name5_228->setFont(font2);

        gridLayout_87->addWidget(textFIeld_name5_228, 20, 4, 1, 1);

        spinBox_duration5_228 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_duration5_228->setObjectName(QStringLiteral("spinBox_duration5_228"));
        spinBox_duration5_228->setFont(font6);
        spinBox_duration5_228->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_duration5_228, 4, 9, 1, 2);

        label_ID4_267 = new QLabel(scrollAreaWidgetContents_21);
        label_ID4_267->setObjectName(QStringLiteral("label_ID4_267"));
        label_ID4_267->setFont(font5);
        label_ID4_267->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_87->addWidget(label_ID4_267, 20, 2, 1, 1);

        horizontalSpacer_320 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_87->addItem(horizontalSpacer_320, 10, 8, 1, 1);

        label_ID4_268 = new QLabel(scrollAreaWidgetContents_21);
        label_ID4_268->setObjectName(QStringLiteral("label_ID4_268"));
        label_ID4_268->setFont(font5);
        label_ID4_268->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_87->addWidget(label_ID4_268, 4, 2, 1, 1);

        textFIeld_name5_229 = new QLineEdit(scrollAreaWidgetContents_21);
        textFIeld_name5_229->setObjectName(QStringLiteral("textFIeld_name5_229"));
        textFIeld_name5_229->setFont(font2);

        gridLayout_87->addWidget(textFIeld_name5_229, 13, 4, 1, 1);

        spinBox_start2_36 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_start2_36->setObjectName(QStringLiteral("spinBox_start2_36"));
        spinBox_start2_36->setFont(font6);
        spinBox_start2_36->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_start2_36, 1, 6, 1, 2);

        spinBox_duration5_229 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_duration5_229->setObjectName(QStringLiteral("spinBox_duration5_229"));
        spinBox_duration5_229->setFont(font6);
        spinBox_duration5_229->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_duration5_229, 17, 9, 1, 2);

        label_ID4_269 = new QLabel(scrollAreaWidgetContents_21);
        label_ID4_269->setObjectName(QStringLiteral("label_ID4_269"));
        label_ID4_269->setFont(font5);
        label_ID4_269->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_87->addWidget(label_ID4_269, 10, 2, 1, 1);

        spinBox_duration5_230 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_duration5_230->setObjectName(QStringLiteral("spinBox_duration5_230"));
        spinBox_duration5_230->setFont(font6);
        spinBox_duration5_230->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_duration5_230, 12, 9, 1, 2);

        spinBox_duration5_231 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_duration5_231->setObjectName(QStringLiteral("spinBox_duration5_231"));
        spinBox_duration5_231->setFont(font6);
        spinBox_duration5_231->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_duration5_231, 20, 9, 1, 2);

        spinBox_start5_263 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_start5_263->setObjectName(QStringLiteral("spinBox_start5_263"));
        spinBox_start5_263->setFont(font6);
        spinBox_start5_263->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_start5_263, 20, 6, 1, 2);

        textFIeld_name5_230 = new QLineEdit(scrollAreaWidgetContents_21);
        textFIeld_name5_230->setObjectName(QStringLiteral("textFIeld_name5_230"));
        textFIeld_name5_230->setFont(font2);

        gridLayout_87->addWidget(textFIeld_name5_230, 11, 4, 1, 1);

        spinBox_start5_264 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_start5_264->setObjectName(QStringLiteral("spinBox_start5_264"));
        spinBox_start5_264->setFont(font6);
        spinBox_start5_264->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_start5_264, 13, 6, 1, 2);

        checkBox_365 = new QCheckBox(scrollAreaWidgetContents_21);
        checkBox_365->setObjectName(QStringLiteral("checkBox_365"));
        checkBox_365->setEnabled(false);
        checkBox_365->setBaseSize(QSize(0, 0));
        checkBox_365->setFont(font4);
        checkBox_365->setTabletTracking(false);
        checkBox_365->setAutoFillBackground(false);
        checkBox_365->setChecked(false);
        checkBox_365->setTristate(false);

        gridLayout_87->addWidget(checkBox_365, 15, 1, 1, 1);

        label_ID4_270 = new QLabel(scrollAreaWidgetContents_21);
        label_ID4_270->setObjectName(QStringLiteral("label_ID4_270"));
        label_ID4_270->setFont(font5);
        label_ID4_270->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_87->addWidget(label_ID4_270, 12, 2, 1, 1);

        label_ID4_271 = new QLabel(scrollAreaWidgetContents_21);
        label_ID4_271->setObjectName(QStringLiteral("label_ID4_271"));
        label_ID4_271->setFont(font5);
        label_ID4_271->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_87->addWidget(label_ID4_271, 16, 2, 1, 1);

        spinBox_duration3_35 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_duration3_35->setObjectName(QStringLiteral("spinBox_duration3_35"));
        spinBox_duration3_35->setFont(font6);
        spinBox_duration3_35->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_duration3_35, 8, 9, 1, 2);

        label_ID2_39 = new QLabel(scrollAreaWidgetContents_21);
        label_ID2_39->setObjectName(QStringLiteral("label_ID2_39"));
        label_ID2_39->setFont(font5);
        label_ID2_39->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_87->addWidget(label_ID2_39, 7, 2, 1, 1);

        label_ID4_272 = new QLabel(scrollAreaWidgetContents_21);
        label_ID4_272->setObjectName(QStringLiteral("label_ID4_272"));
        label_ID4_272->setFont(font5);
        label_ID4_272->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_87->addWidget(label_ID4_272, 15, 2, 1, 1);

        checkBox_366 = new QCheckBox(scrollAreaWidgetContents_21);
        checkBox_366->setObjectName(QStringLiteral("checkBox_366"));
        checkBox_366->setEnabled(false);
        checkBox_366->setBaseSize(QSize(0, 0));
        checkBox_366->setFont(font4);
        checkBox_366->setTabletTracking(false);
        checkBox_366->setAutoFillBackground(false);
        checkBox_366->setChecked(false);
        checkBox_366->setTristate(false);

        gridLayout_87->addWidget(checkBox_366, 0, 1, 1, 1);

        circle_inactive3_138 = new QLabel(scrollAreaWidgetContents_21);
        circle_inactive3_138->setObjectName(QStringLiteral("circle_inactive3_138"));
        circle_inactive3_138->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_138->setAlignment(Qt::AlignCenter);

        gridLayout_87->addWidget(circle_inactive3_138, 2, 3, 1, 1);

        label_ID4_273 = new QLabel(scrollAreaWidgetContents_21);
        label_ID4_273->setObjectName(QStringLiteral("label_ID4_273"));
        label_ID4_273->setFont(font5);
        label_ID4_273->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_87->addWidget(label_ID4_273, 13, 2, 1, 1);

        label_ID4_274 = new QLabel(scrollAreaWidgetContents_21);
        label_ID4_274->setObjectName(QStringLiteral("label_ID4_274"));
        label_ID4_274->setFont(font5);
        label_ID4_274->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_87->addWidget(label_ID4_274, 11, 2, 1, 1);

        spinBox_start4_35 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_start4_35->setObjectName(QStringLiteral("spinBox_start4_35"));
        spinBox_start4_35->setFont(font6);
        spinBox_start4_35->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_start4_35, 3, 6, 1, 2);

        textFIeld_name5_231 = new QLineEdit(scrollAreaWidgetContents_21);
        textFIeld_name5_231->setObjectName(QStringLiteral("textFIeld_name5_231"));
        textFIeld_name5_231->setFont(font2);

        gridLayout_87->addWidget(textFIeld_name5_231, 16, 4, 1, 1);

        spinBox_start5_265 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_start5_265->setObjectName(QStringLiteral("spinBox_start5_265"));
        spinBox_start5_265->setFont(font6);
        spinBox_start5_265->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_start5_265, 12, 6, 1, 2);

        spinBox_duration5_232 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_duration5_232->setObjectName(QStringLiteral("spinBox_duration5_232"));
        spinBox_duration5_232->setFont(font6);
        spinBox_duration5_232->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_duration5_232, 18, 9, 1, 2);

        label_ID3_38 = new QLabel(scrollAreaWidgetContents_21);
        label_ID3_38->setObjectName(QStringLiteral("label_ID3_38"));
        label_ID3_38->setFont(font5);
        label_ID3_38->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_87->addWidget(label_ID3_38, 2, 2, 1, 1);

        spinBox_duration1_18 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_duration1_18->setObjectName(QStringLiteral("spinBox_duration1_18"));
        spinBox_duration1_18->setFont(font6);
        spinBox_duration1_18->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_duration1_18, 0, 9, 1, 2);

        spinBox_duration5_233 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_duration5_233->setObjectName(QStringLiteral("spinBox_duration5_233"));
        spinBox_duration5_233->setFont(font6);
        spinBox_duration5_233->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_duration5_233, 16, 9, 1, 2);

        spinBox_start5_266 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_start5_266->setObjectName(QStringLiteral("spinBox_start5_266"));
        spinBox_start5_266->setFont(font6);
        spinBox_start5_266->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_start5_266, 10, 6, 1, 2);

        spinBox_start4_36 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_start4_36->setObjectName(QStringLiteral("spinBox_start4_36"));
        spinBox_start4_36->setFont(font6);
        spinBox_start4_36->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_start4_36, 9, 6, 1, 2);

        spinBox_duration2_36 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_duration2_36->setObjectName(QStringLiteral("spinBox_duration2_36"));
        spinBox_duration2_36->setFont(font6);
        spinBox_duration2_36->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_duration2_36, 1, 9, 1, 2);

        circle_inactive1_222 = new QLabel(scrollAreaWidgetContents_21);
        circle_inactive1_222->setObjectName(QStringLiteral("circle_inactive1_222"));
        circle_inactive1_222->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_222->setAlignment(Qt::AlignCenter);

        gridLayout_87->addWidget(circle_inactive1_222, 0, 3, 1, 1);

        checkBox_367 = new QCheckBox(scrollAreaWidgetContents_21);
        checkBox_367->setObjectName(QStringLiteral("checkBox_367"));
        checkBox_367->setEnabled(false);
        checkBox_367->setBaseSize(QSize(0, 0));
        checkBox_367->setFont(font4);
        checkBox_367->setTabletTracking(false);
        checkBox_367->setAutoFillBackground(false);
        checkBox_367->setChecked(false);
        checkBox_367->setTristate(false);

        gridLayout_87->addWidget(checkBox_367, 1, 1, 1, 1);

        spinBox_duration3_36 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_duration3_36->setObjectName(QStringLiteral("spinBox_duration3_36"));
        spinBox_duration3_36->setFont(font6);
        spinBox_duration3_36->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_duration3_36, 2, 9, 1, 2);

        checkBox_368 = new QCheckBox(scrollAreaWidgetContents_21);
        checkBox_368->setObjectName(QStringLiteral("checkBox_368"));
        checkBox_368->setEnabled(false);
        checkBox_368->setBaseSize(QSize(0, 0));
        checkBox_368->setFont(font4);
        checkBox_368->setTabletTracking(false);
        checkBox_368->setAutoFillBackground(false);
        checkBox_368->setChecked(false);
        checkBox_368->setTristate(false);

        gridLayout_87->addWidget(checkBox_368, 11, 1, 1, 1);

        checkBox_369 = new QCheckBox(scrollAreaWidgetContents_21);
        checkBox_369->setObjectName(QStringLiteral("checkBox_369"));
        checkBox_369->setEnabled(false);
        checkBox_369->setBaseSize(QSize(0, 0));
        checkBox_369->setFont(font4);
        checkBox_369->setTabletTracking(false);
        checkBox_369->setAutoFillBackground(false);
        checkBox_369->setChecked(false);
        checkBox_369->setTristate(false);

        gridLayout_87->addWidget(checkBox_369, 9, 1, 1, 1);

        checkBox_370 = new QCheckBox(scrollAreaWidgetContents_21);
        checkBox_370->setObjectName(QStringLiteral("checkBox_370"));
        checkBox_370->setEnabled(false);
        checkBox_370->setBaseSize(QSize(0, 0));
        checkBox_370->setFont(font4);
        checkBox_370->setTabletTracking(false);
        checkBox_370->setAutoFillBackground(false);
        checkBox_370->setChecked(false);
        checkBox_370->setTristate(false);

        gridLayout_87->addWidget(checkBox_370, 2, 1, 1, 1);

        textFIeld_name5_232 = new QLineEdit(scrollAreaWidgetContents_21);
        textFIeld_name5_232->setObjectName(QStringLiteral("textFIeld_name5_232"));
        textFIeld_name5_232->setFont(font2);

        gridLayout_87->addWidget(textFIeld_name5_232, 15, 4, 1, 1);

        checkBox_371 = new QCheckBox(scrollAreaWidgetContents_21);
        checkBox_371->setObjectName(QStringLiteral("checkBox_371"));
        checkBox_371->setEnabled(false);
        checkBox_371->setBaseSize(QSize(0, 0));
        checkBox_371->setFont(font4);
        checkBox_371->setTabletTracking(false);
        checkBox_371->setAutoFillBackground(false);
        checkBox_371->setChecked(false);
        checkBox_371->setTristate(false);

        gridLayout_87->addWidget(checkBox_371, 10, 1, 1, 1);

        spinBox_start5_267 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_start5_267->setObjectName(QStringLiteral("spinBox_start5_267"));
        spinBox_start5_267->setFont(font6);
        spinBox_start5_267->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_start5_267, 5, 6, 1, 2);

        label_ID3_39 = new QLabel(scrollAreaWidgetContents_21);
        label_ID3_39->setObjectName(QStringLiteral("label_ID3_39"));
        label_ID3_39->setFont(font5);
        label_ID3_39->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_87->addWidget(label_ID3_39, 8, 2, 1, 1);

        label_ID1_21 = new QLabel(scrollAreaWidgetContents_21);
        label_ID1_21->setObjectName(QStringLiteral("label_ID1_21"));
        label_ID1_21->setFont(font5);
        label_ID1_21->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_87->addWidget(label_ID1_21, 0, 2, 1, 1);

        textFIeld_name5_233 = new QLineEdit(scrollAreaWidgetContents_21);
        textFIeld_name5_233->setObjectName(QStringLiteral("textFIeld_name5_233"));
        textFIeld_name5_233->setFont(font2);

        gridLayout_87->addWidget(textFIeld_name5_233, 14, 4, 1, 1);

        spinBox_duration5_234 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_duration5_234->setObjectName(QStringLiteral("spinBox_duration5_234"));
        spinBox_duration5_234->setFont(font6);
        spinBox_duration5_234->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_duration5_234, 13, 9, 1, 2);

        horizontalSpacer_321 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_87->addItem(horizontalSpacer_321, 21, 3, 1, 1);

        label_ID4_275 = new QLabel(scrollAreaWidgetContents_21);
        label_ID4_275->setObjectName(QStringLiteral("label_ID4_275"));
        label_ID4_275->setFont(font5);
        label_ID4_275->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_87->addWidget(label_ID4_275, 18, 2, 1, 1);

        checkBox_372 = new QCheckBox(scrollAreaWidgetContents_21);
        checkBox_372->setObjectName(QStringLiteral("checkBox_372"));
        checkBox_372->setEnabled(false);
        checkBox_372->setBaseSize(QSize(0, 0));
        checkBox_372->setFont(font4);
        checkBox_372->setTabletTracking(false);
        checkBox_372->setAutoFillBackground(false);
        checkBox_372->setChecked(false);
        checkBox_372->setTristate(false);

        gridLayout_87->addWidget(checkBox_372, 18, 1, 1, 1);

        checkBox_373 = new QCheckBox(scrollAreaWidgetContents_21);
        checkBox_373->setObjectName(QStringLiteral("checkBox_373"));
        checkBox_373->setEnabled(false);
        checkBox_373->setBaseSize(QSize(0, 0));
        checkBox_373->setFont(font4);
        checkBox_373->setTabletTracking(false);
        checkBox_373->setAutoFillBackground(false);
        checkBox_373->setChecked(false);
        checkBox_373->setTristate(false);

        gridLayout_87->addWidget(checkBox_373, 20, 1, 1, 1);

        checkBox_374 = new QCheckBox(scrollAreaWidgetContents_21);
        checkBox_374->setObjectName(QStringLiteral("checkBox_374"));
        checkBox_374->setEnabled(false);
        checkBox_374->setBaseSize(QSize(0, 0));
        checkBox_374->setFont(font4);
        checkBox_374->setTabletTracking(false);
        checkBox_374->setAutoFillBackground(false);
        checkBox_374->setChecked(false);
        checkBox_374->setTristate(false);

        gridLayout_87->addWidget(checkBox_374, 17, 1, 1, 1);

        spinBox_start5_268 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_start5_268->setObjectName(QStringLiteral("spinBox_start5_268"));
        spinBox_start5_268->setFont(font6);
        spinBox_start5_268->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_start5_268, 11, 6, 1, 2);

        checkBox_375 = new QCheckBox(scrollAreaWidgetContents_21);
        checkBox_375->setObjectName(QStringLiteral("checkBox_375"));
        checkBox_375->setEnabled(false);
        checkBox_375->setBaseSize(QSize(0, 0));
        checkBox_375->setFont(font4);
        checkBox_375->setTabletTracking(false);
        checkBox_375->setAutoFillBackground(false);
        checkBox_375->setChecked(false);
        checkBox_375->setTristate(false);

        gridLayout_87->addWidget(checkBox_375, 16, 1, 1, 1);

        spinBox_start5_269 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_start5_269->setObjectName(QStringLiteral("spinBox_start5_269"));
        spinBox_start5_269->setFont(font6);
        spinBox_start5_269->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_start5_269, 16, 6, 1, 2);

        label_ID4_276 = new QLabel(scrollAreaWidgetContents_21);
        label_ID4_276->setObjectName(QStringLiteral("label_ID4_276"));
        label_ID4_276->setFont(font5);
        label_ID4_276->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_87->addWidget(label_ID4_276, 17, 2, 1, 1);

        circle_inactive3_139 = new QLabel(scrollAreaWidgetContents_21);
        circle_inactive3_139->setObjectName(QStringLiteral("circle_inactive3_139"));
        circle_inactive3_139->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_139->setAlignment(Qt::AlignCenter);

        gridLayout_87->addWidget(circle_inactive3_139, 4, 3, 1, 1);

        spinBox_start5_270 = new QSpinBox(scrollAreaWidgetContents_21);
        spinBox_start5_270->setObjectName(QStringLiteral("spinBox_start5_270"));
        spinBox_start5_270->setFont(font6);
        spinBox_start5_270->setMaximum(10000000);

        gridLayout_87->addWidget(spinBox_start5_270, 18, 6, 1, 2);

        circle_inactive1_223 = new QLabel(scrollAreaWidgetContents_21);
        circle_inactive1_223->setObjectName(QStringLiteral("circle_inactive1_223"));
        circle_inactive1_223->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_223->setAlignment(Qt::AlignCenter);

        gridLayout_87->addWidget(circle_inactive1_223, 1, 3, 1, 1);

        textFIeld_name5_234 = new QLineEdit(scrollAreaWidgetContents_21);
        textFIeld_name5_234->setObjectName(QStringLiteral("textFIeld_name5_234"));
        textFIeld_name5_234->setFont(font2);

        gridLayout_87->addWidget(textFIeld_name5_234, 17, 4, 1, 1);

        circle_inactive3_140 = new QLabel(scrollAreaWidgetContents_21);
        circle_inactive3_140->setObjectName(QStringLiteral("circle_inactive3_140"));
        circle_inactive3_140->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_140->setAlignment(Qt::AlignCenter);

        gridLayout_87->addWidget(circle_inactive3_140, 6, 3, 1, 1);

        horizontalSpacer_322 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_87->addItem(horizontalSpacer_322, 0, 8, 1, 1);

        circle_inactive3_141 = new QLabel(scrollAreaWidgetContents_21);
        circle_inactive3_141->setObjectName(QStringLiteral("circle_inactive3_141"));
        circle_inactive3_141->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_141->setAlignment(Qt::AlignCenter);

        gridLayout_87->addWidget(circle_inactive3_141, 7, 3, 1, 1);

        circle_inactive1_224 = new QLabel(scrollAreaWidgetContents_21);
        circle_inactive1_224->setObjectName(QStringLiteral("circle_inactive1_224"));
        circle_inactive1_224->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_224->setAlignment(Qt::AlignCenter);

        gridLayout_87->addWidget(circle_inactive1_224, 9, 3, 1, 1);

        circle_inactive1_225 = new QLabel(scrollAreaWidgetContents_21);
        circle_inactive1_225->setObjectName(QStringLiteral("circle_inactive1_225"));
        circle_inactive1_225->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_225->setAlignment(Qt::AlignCenter);

        gridLayout_87->addWidget(circle_inactive1_225, 10, 3, 1, 1);

        circle_inactive1_226 = new QLabel(scrollAreaWidgetContents_21);
        circle_inactive1_226->setObjectName(QStringLiteral("circle_inactive1_226"));
        circle_inactive1_226->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_226->setAlignment(Qt::AlignCenter);

        gridLayout_87->addWidget(circle_inactive1_226, 11, 3, 1, 1);

        circle_inactive1_227 = new QLabel(scrollAreaWidgetContents_21);
        circle_inactive1_227->setObjectName(QStringLiteral("circle_inactive1_227"));
        circle_inactive1_227->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_227->setAlignment(Qt::AlignCenter);

        gridLayout_87->addWidget(circle_inactive1_227, 12, 3, 1, 1);

        circle_inactive1_228 = new QLabel(scrollAreaWidgetContents_21);
        circle_inactive1_228->setObjectName(QStringLiteral("circle_inactive1_228"));
        circle_inactive1_228->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_228->setAlignment(Qt::AlignCenter);

        gridLayout_87->addWidget(circle_inactive1_228, 13, 3, 1, 1);

        circle_inactive1_229 = new QLabel(scrollAreaWidgetContents_21);
        circle_inactive1_229->setObjectName(QStringLiteral("circle_inactive1_229"));
        circle_inactive1_229->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_229->setAlignment(Qt::AlignCenter);

        gridLayout_87->addWidget(circle_inactive1_229, 14, 3, 1, 1);

        circle_inactive1_230 = new QLabel(scrollAreaWidgetContents_21);
        circle_inactive1_230->setObjectName(QStringLiteral("circle_inactive1_230"));
        circle_inactive1_230->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_230->setAlignment(Qt::AlignCenter);

        gridLayout_87->addWidget(circle_inactive1_230, 15, 3, 1, 1);

        circle_inactive1_231 = new QLabel(scrollAreaWidgetContents_21);
        circle_inactive1_231->setObjectName(QStringLiteral("circle_inactive1_231"));
        circle_inactive1_231->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_231->setAlignment(Qt::AlignCenter);

        gridLayout_87->addWidget(circle_inactive1_231, 16, 3, 1, 1);

        circle_inactive1_232 = new QLabel(scrollAreaWidgetContents_21);
        circle_inactive1_232->setObjectName(QStringLiteral("circle_inactive1_232"));
        circle_inactive1_232->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_232->setAlignment(Qt::AlignCenter);

        gridLayout_87->addWidget(circle_inactive1_232, 17, 3, 1, 1);

        circle_inactive1_233 = new QLabel(scrollAreaWidgetContents_21);
        circle_inactive1_233->setObjectName(QStringLiteral("circle_inactive1_233"));
        circle_inactive1_233->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_233->setAlignment(Qt::AlignCenter);

        gridLayout_87->addWidget(circle_inactive1_233, 18, 3, 1, 1);

        circle_inactive1_234 = new QLabel(scrollAreaWidgetContents_21);
        circle_inactive1_234->setObjectName(QStringLiteral("circle_inactive1_234"));
        circle_inactive1_234->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_234->setAlignment(Qt::AlignCenter);

        gridLayout_87->addWidget(circle_inactive1_234, 20, 3, 1, 1);


        gridLayout_86->addLayout(gridLayout_87, 1, 0, 1, 1);

        scrollArea_18->setWidget(scrollAreaWidgetContents_21);

        gridLayout_84->addWidget(scrollArea_18, 1, 0, 1, 1);

        horizontalSpacer_323 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_84->addItem(horizontalSpacer_323, 1, 1, 1, 1);


        gridLayout_83->addLayout(gridLayout_84, 0, 0, 1, 1);

        horizontalSpacer_324 = new QSpacerItem(8, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_83->addItem(horizontalSpacer_324, 0, 1, 1, 1);

        tabWidget_2->addTab(tab_14, QString());
        tab_15 = new QWidget();
        tab_15->setObjectName(QStringLiteral("tab_15"));
        tab_15->setEnabled(false);
        gridLayout_88 = new QGridLayout(tab_15);
        gridLayout_88->setSpacing(6);
        gridLayout_88->setContentsMargins(11, 11, 11, 11);
        gridLayout_88->setObjectName(QStringLiteral("gridLayout_88"));
        gridLayout_89 = new QGridLayout();
        gridLayout_89->setSpacing(6);
        gridLayout_89->setObjectName(QStringLiteral("gridLayout_89"));
        horizontalLayout_43 = new QHBoxLayout();
        horizontalLayout_43->setSpacing(6);
        horizontalLayout_43->setObjectName(QStringLiteral("horizontalLayout_43"));
        horizontalSpacer_325 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_43->addItem(horizontalSpacer_325);

        horizontalSpacer_326 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_43->addItem(horizontalSpacer_326);

        label_ID_23 = new QLabel(tab_15);
        label_ID_23->setObjectName(QStringLiteral("label_ID_23"));
        sizePolicy.setHeightForWidth(label_ID_23->sizePolicy().hasHeightForWidth());
        label_ID_23->setSizePolicy(sizePolicy);
        label_ID_23->setMaximumSize(QSize(16777215, 16777215));
        label_ID_23->setFont(font);
        label_ID_23->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_43->addWidget(label_ID_23);

        horizontalSpacer_327 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_43->addItem(horizontalSpacer_327);

        label_name_21 = new QLabel(tab_15);
        label_name_21->setObjectName(QStringLiteral("label_name_21"));
        label_name_21->setFont(font);
        label_name_21->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_43->addWidget(label_name_21);

        horizontalSpacer_328 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_43->addItem(horizontalSpacer_328);

        horizontalSpacer_329 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_43->addItem(horizontalSpacer_329);

        label_start_23 = new QLabel(tab_15);
        label_start_23->setObjectName(QStringLiteral("label_start_23"));
        sizePolicy1.setHeightForWidth(label_start_23->sizePolicy().hasHeightForWidth());
        label_start_23->setSizePolicy(sizePolicy1);
        label_start_23->setFont(font);
        label_start_23->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_43->addWidget(label_start_23);

        label_msStart_24 = new QLabel(tab_15);
        label_msStart_24->setObjectName(QStringLiteral("label_msStart_24"));
        label_msStart_24->setFont(font1);
        label_msStart_24->setAlignment(Qt::AlignCenter);

        horizontalLayout_43->addWidget(label_msStart_24);

        horizontalSpacer_330 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_43->addItem(horizontalSpacer_330);

        label_duration_29 = new QLabel(tab_15);
        label_duration_29->setObjectName(QStringLiteral("label_duration_29"));
        label_duration_29->setFont(font);
        label_duration_29->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_43->addWidget(label_duration_29);

        label_msDuration_44 = new QLabel(tab_15);
        label_msDuration_44->setObjectName(QStringLiteral("label_msDuration_44"));
        label_msDuration_44->setFont(font1);
        label_msDuration_44->setAlignment(Qt::AlignCenter);

        horizontalLayout_43->addWidget(label_msDuration_44);

        horizontalSpacer_331 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_43->addItem(horizontalSpacer_331);


        gridLayout_89->addLayout(horizontalLayout_43, 0, 0, 1, 1);

        gridLayout_90 = new QGridLayout();
        gridLayout_90->setSpacing(6);
        gridLayout_90->setObjectName(QStringLiteral("gridLayout_90"));
        horizontalLayout_44 = new QHBoxLayout();
        horizontalLayout_44->setSpacing(6);
        horizontalLayout_44->setObjectName(QStringLiteral("horizontalLayout_44"));
        progressBar_37 = new QProgressBar(tab_15);
        progressBar_37->setObjectName(QStringLiteral("progressBar_37"));
        progressBar_37->setValue(0);

        horizontalLayout_44->addWidget(progressBar_37);

        label_76 = new QLabel(tab_15);
        label_76->setObjectName(QStringLiteral("label_76"));
        label_76->setFont(font3);

        horizontalLayout_44->addWidget(label_76);


        gridLayout_90->addLayout(horizontalLayout_44, 4, 0, 1, 5);

        spinBox_start5_271 = new QSpinBox(tab_15);
        spinBox_start5_271->setObjectName(QStringLiteral("spinBox_start5_271"));
        spinBox_start5_271->setFont(font6);
        spinBox_start5_271->setMaximum(10000000);
        spinBox_start5_271->setValue(0);

        gridLayout_90->addWidget(spinBox_start5_271, 2, 3, 1, 2);

        spinBox_start5_272 = new QSpinBox(tab_15);
        spinBox_start5_272->setObjectName(QStringLiteral("spinBox_start5_272"));
        spinBox_start5_272->setFont(font6);
        spinBox_start5_272->setMaximum(10000000);
        spinBox_start5_272->setValue(0);

        gridLayout_90->addWidget(spinBox_start5_272, 2, 0, 1, 2);

        pushButton_75 = new QPushButton(tab_15);
        pushButton_75->setObjectName(QStringLiteral("pushButton_75"));
        pushButton_75->setMinimumSize(QSize(0, 80));
        pushButton_75->setFont(font2);

        gridLayout_90->addWidget(pushButton_75, 10, 3, 1, 2);

        horizontalSpacer_332 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_90->addItem(horizontalSpacer_332, 8, 2, 1, 1);

        verticalSpacer_100 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_90->addItem(verticalSpacer_100, 7, 0, 1, 1);

        pushButton_76 = new QPushButton(tab_15);
        pushButton_76->setObjectName(QStringLiteral("pushButton_76"));
        pushButton_76->setMinimumSize(QSize(0, 80));
        pushButton_76->setFont(font2);

        gridLayout_90->addWidget(pushButton_76, 8, 0, 1, 2);

        label_77 = new QLabel(tab_15);
        label_77->setObjectName(QStringLiteral("label_77"));
        label_77->setFont(font10);
        label_77->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_90->addWidget(label_77, 3, 0, 1, 5);

        label_78 = new QLabel(tab_15);
        label_78->setObjectName(QStringLiteral("label_78"));
        label_78->setFont(font8);

        gridLayout_90->addWidget(label_78, 1, 3, 1, 1);

        line_22 = new QFrame(tab_15);
        line_22->setObjectName(QStringLiteral("line_22"));
        line_22->setFrameShape(QFrame::HLine);
        line_22->setFrameShadow(QFrame::Sunken);

        gridLayout_90->addWidget(line_22, 6, 0, 1, 5);

        horizontalSpacer_333 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_90->addItem(horizontalSpacer_333, 0, 1, 1, 1);

        verticalSpacer_101 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_90->addItem(verticalSpacer_101, 0, 0, 1, 1);

        pushButton_77 = new QPushButton(tab_15);
        pushButton_77->setObjectName(QStringLiteral("pushButton_77"));
        pushButton_77->setMinimumSize(QSize(0, 80));
        pushButton_77->setFont(font2);

        gridLayout_90->addWidget(pushButton_77, 10, 0, 1, 2);

        label_msDuration_45 = new QLabel(tab_15);
        label_msDuration_45->setObjectName(QStringLiteral("label_msDuration_45"));
        label_msDuration_45->setFont(font1);
        label_msDuration_45->setAlignment(Qt::AlignCenter);

        gridLayout_90->addWidget(label_msDuration_45, 1, 4, 1, 1);

        verticalSpacer_102 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_90->addItem(verticalSpacer_102, 5, 0, 1, 1);

        pushButton_78 = new QPushButton(tab_15);
        pushButton_78->setObjectName(QStringLiteral("pushButton_78"));
        pushButton_78->setMinimumSize(QSize(0, 80));
        pushButton_78->setFont(font2);

        gridLayout_90->addWidget(pushButton_78, 8, 3, 1, 2);

        verticalSpacer_103 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_90->addItem(verticalSpacer_103, 9, 0, 1, 1);

        label_79 = new QLabel(tab_15);
        label_79->setObjectName(QStringLiteral("label_79"));
        label_79->setFont(font8);

        gridLayout_90->addWidget(label_79, 1, 0, 1, 1);

        verticalSpacer_104 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_90->addItem(verticalSpacer_104, 11, 0, 1, 1);


        gridLayout_89->addLayout(gridLayout_90, 0, 2, 2, 1);

        scrollArea_19 = new QScrollArea(tab_15);
        scrollArea_19->setObjectName(QStringLiteral("scrollArea_19"));
        scrollArea_19->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        scrollArea_19->setWidgetResizable(true);
        scrollAreaWidgetContents_22 = new QWidget();
        scrollAreaWidgetContents_22->setObjectName(QStringLiteral("scrollAreaWidgetContents_22"));
        scrollAreaWidgetContents_22->setGeometry(QRect(0, 0, 665, 740));
        gridLayout_91 = new QGridLayout(scrollAreaWidgetContents_22);
        gridLayout_91->setSpacing(6);
        gridLayout_91->setContentsMargins(11, 11, 11, 11);
        gridLayout_91->setObjectName(QStringLiteral("gridLayout_91"));
        gridLayout_92 = new QGridLayout();
        gridLayout_92->setSpacing(1);
        gridLayout_92->setObjectName(QStringLiteral("gridLayout_92"));
        textFIeld_name5_235 = new QLineEdit(scrollAreaWidgetContents_22);
        textFIeld_name5_235->setObjectName(QStringLiteral("textFIeld_name5_235"));
        textFIeld_name5_235->setFont(font2);

        gridLayout_92->addWidget(textFIeld_name5_235, 12, 4, 1, 1);

        spinBox_start2_37 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_start2_37->setObjectName(QStringLiteral("spinBox_start2_37"));
        spinBox_start2_37->setFont(font6);
        spinBox_start2_37->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_start2_37, 7, 6, 1, 2);

        label_ID4_277 = new QLabel(scrollAreaWidgetContents_22);
        label_ID4_277->setObjectName(QStringLiteral("label_ID4_277"));
        label_ID4_277->setFont(font5);
        label_ID4_277->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_92->addWidget(label_ID4_277, 5, 2, 1, 1);

        circle_inactive3_142 = new QLabel(scrollAreaWidgetContents_22);
        circle_inactive3_142->setObjectName(QStringLiteral("circle_inactive3_142"));
        circle_inactive3_142->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_142->setAlignment(Qt::AlignCenter);

        gridLayout_92->addWidget(circle_inactive3_142, 5, 3, 1, 1);

        horizontalSpacer_334 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_92->addItem(horizontalSpacer_334, 6, 0, 1, 1);

        checkBox_376 = new QCheckBox(scrollAreaWidgetContents_22);
        checkBox_376->setObjectName(QStringLiteral("checkBox_376"));
        checkBox_376->setEnabled(false);
        checkBox_376->setBaseSize(QSize(0, 0));
        checkBox_376->setFont(font4);
        checkBox_376->setTabletTracking(false);
        checkBox_376->setAutoFillBackground(false);
        checkBox_376->setChecked(false);
        checkBox_376->setTristate(false);

        gridLayout_92->addWidget(checkBox_376, 3, 1, 1, 1);

        checkBox_377 = new QCheckBox(scrollAreaWidgetContents_22);
        checkBox_377->setObjectName(QStringLiteral("checkBox_377"));
        checkBox_377->setEnabled(false);
        checkBox_377->setBaseSize(QSize(0, 0));
        checkBox_377->setFont(font4);
        checkBox_377->setTabletTracking(false);
        checkBox_377->setAutoFillBackground(false);
        checkBox_377->setChecked(false);
        checkBox_377->setTristate(false);

        gridLayout_92->addWidget(checkBox_377, 5, 1, 1, 1);

        checkBox_378 = new QCheckBox(scrollAreaWidgetContents_22);
        checkBox_378->setObjectName(QStringLiteral("checkBox_378"));
        checkBox_378->setEnabled(false);
        checkBox_378->setBaseSize(QSize(0, 0));
        checkBox_378->setFont(font4);
        checkBox_378->setTabletTracking(false);
        checkBox_378->setAutoFillBackground(false);
        checkBox_378->setChecked(false);
        checkBox_378->setTristate(false);

        gridLayout_92->addWidget(checkBox_378, 8, 1, 1, 1);

        spinBox_start1_19 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_start1_19->setObjectName(QStringLiteral("spinBox_start1_19"));
        spinBox_start1_19->setFont(font6);
        spinBox_start1_19->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_start1_19, 0, 6, 1, 2);

        checkBox_379 = new QCheckBox(scrollAreaWidgetContents_22);
        checkBox_379->setObjectName(QStringLiteral("checkBox_379"));
        checkBox_379->setEnabled(false);
        checkBox_379->setBaseSize(QSize(0, 0));
        checkBox_379->setFont(font4);
        checkBox_379->setTabletTracking(false);
        checkBox_379->setAutoFillBackground(false);
        checkBox_379->setChecked(false);
        checkBox_379->setTristate(false);

        gridLayout_92->addWidget(checkBox_379, 4, 1, 1, 1);

        circle_inactive3_143 = new QLabel(scrollAreaWidgetContents_22);
        circle_inactive3_143->setObjectName(QStringLiteral("circle_inactive3_143"));
        circle_inactive3_143->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_143->setAlignment(Qt::AlignCenter);

        gridLayout_92->addWidget(circle_inactive3_143, 3, 3, 1, 1);

        textFIeld_name1_52 = new QLineEdit(scrollAreaWidgetContents_22);
        textFIeld_name1_52->setObjectName(QStringLiteral("textFIeld_name1_52"));
        textFIeld_name1_52->setFont(font2);

        gridLayout_92->addWidget(textFIeld_name1_52, 0, 4, 1, 1);

        spinBox_duration2_37 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_duration2_37->setObjectName(QStringLiteral("spinBox_duration2_37"));
        spinBox_duration2_37->setFont(font6);
        spinBox_duration2_37->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_duration2_37, 7, 9, 1, 2);

        spinBox_duration4_37 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_duration4_37->setObjectName(QStringLiteral("spinBox_duration4_37"));
        spinBox_duration4_37->setFont(font6);
        spinBox_duration4_37->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_duration4_37, 3, 9, 1, 2);

        textFIeld_name4_37 = new QLineEdit(scrollAreaWidgetContents_22);
        textFIeld_name4_37->setObjectName(QStringLiteral("textFIeld_name4_37"));
        textFIeld_name4_37->setFont(font2);

        gridLayout_92->addWidget(textFIeld_name4_37, 3, 4, 1, 1);

        spinBox_duration5_235 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_duration5_235->setObjectName(QStringLiteral("spinBox_duration5_235"));
        spinBox_duration5_235->setFont(font6);
        spinBox_duration5_235->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_duration5_235, 6, 9, 1, 2);

        checkBox_380 = new QCheckBox(scrollAreaWidgetContents_22);
        checkBox_380->setObjectName(QStringLiteral("checkBox_380"));
        checkBox_380->setEnabled(false);
        checkBox_380->setBaseSize(QSize(0, 0));
        checkBox_380->setFont(font4);
        checkBox_380->setTabletTracking(false);
        checkBox_380->setAutoFillBackground(false);
        checkBox_380->setChecked(false);
        checkBox_380->setTristate(false);

        gridLayout_92->addWidget(checkBox_380, 14, 1, 1, 1);

        textFIeld_name2_37 = new QLineEdit(scrollAreaWidgetContents_22);
        textFIeld_name2_37->setObjectName(QStringLiteral("textFIeld_name2_37"));
        textFIeld_name2_37->setFont(font2);

        gridLayout_92->addWidget(textFIeld_name2_37, 7, 4, 1, 1);

        spinBox_duration5_236 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_duration5_236->setObjectName(QStringLiteral("spinBox_duration5_236"));
        spinBox_duration5_236->setFont(font6);
        spinBox_duration5_236->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_duration5_236, 10, 9, 1, 2);

        textFIeld_name5_236 = new QLineEdit(scrollAreaWidgetContents_22);
        textFIeld_name5_236->setObjectName(QStringLiteral("textFIeld_name5_236"));
        textFIeld_name5_236->setFont(font2);

        gridLayout_92->addWidget(textFIeld_name5_236, 4, 4, 1, 1);

        spinBox_start5_273 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_start5_273->setObjectName(QStringLiteral("spinBox_start5_273"));
        spinBox_start5_273->setFont(font6);
        spinBox_start5_273->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_start5_273, 4, 6, 1, 2);

        label_ID4_278 = new QLabel(scrollAreaWidgetContents_22);
        label_ID4_278->setObjectName(QStringLiteral("label_ID4_278"));
        label_ID4_278->setFont(font5);
        label_ID4_278->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_92->addWidget(label_ID4_278, 3, 2, 1, 1);

        spinBox_start3_37 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_start3_37->setObjectName(QStringLiteral("spinBox_start3_37"));
        spinBox_start3_37->setFont(font6);
        spinBox_start3_37->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_start3_37, 8, 6, 1, 2);

        label_ID4_279 = new QLabel(scrollAreaWidgetContents_22);
        label_ID4_279->setObjectName(QStringLiteral("label_ID4_279"));
        label_ID4_279->setFont(font5);
        label_ID4_279->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_92->addWidget(label_ID4_279, 9, 2, 1, 1);

        spinBox_duration4_38 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_duration4_38->setObjectName(QStringLiteral("spinBox_duration4_38"));
        spinBox_duration4_38->setFont(font6);
        spinBox_duration4_38->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_duration4_38, 9, 9, 1, 2);

        checkBox_381 = new QCheckBox(scrollAreaWidgetContents_22);
        checkBox_381->setObjectName(QStringLiteral("checkBox_381"));
        checkBox_381->setEnabled(false);
        checkBox_381->setBaseSize(QSize(0, 0));
        checkBox_381->setFont(font4);
        checkBox_381->setTabletTracking(false);
        checkBox_381->setAutoFillBackground(false);
        checkBox_381->setChecked(false);
        checkBox_381->setTristate(false);

        gridLayout_92->addWidget(checkBox_381, 13, 1, 1, 1);

        spinBox_start5_274 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_start5_274->setObjectName(QStringLiteral("spinBox_start5_274"));
        spinBox_start5_274->setFont(font6);
        spinBox_start5_274->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_start5_274, 15, 6, 1, 2);

        spinBox_duration5_237 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_duration5_237->setObjectName(QStringLiteral("spinBox_duration5_237"));
        spinBox_duration5_237->setFont(font6);
        spinBox_duration5_237->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_duration5_237, 15, 9, 1, 2);

        textFIeld_name5_237 = new QLineEdit(scrollAreaWidgetContents_22);
        textFIeld_name5_237->setObjectName(QStringLiteral("textFIeld_name5_237"));
        textFIeld_name5_237->setFont(font2);

        gridLayout_92->addWidget(textFIeld_name5_237, 10, 4, 1, 1);

        label_ID4_280 = new QLabel(scrollAreaWidgetContents_22);
        label_ID4_280->setObjectName(QStringLiteral("label_ID4_280"));
        label_ID4_280->setFont(font5);
        label_ID4_280->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_92->addWidget(label_ID4_280, 14, 2, 1, 1);

        spinBox_start5_275 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_start5_275->setObjectName(QStringLiteral("spinBox_start5_275"));
        spinBox_start5_275->setFont(font6);
        spinBox_start5_275->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_start5_275, 14, 6, 1, 2);

        textFIeld_name4_38 = new QLineEdit(scrollAreaWidgetContents_22);
        textFIeld_name4_38->setObjectName(QStringLiteral("textFIeld_name4_38"));
        textFIeld_name4_38->setFont(font2);

        gridLayout_92->addWidget(textFIeld_name4_38, 9, 4, 1, 1);

        spinBox_start3_38 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_start3_38->setObjectName(QStringLiteral("spinBox_start3_38"));
        spinBox_start3_38->setFont(font6);
        spinBox_start3_38->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_start3_38, 2, 6, 1, 2);

        spinBox_start5_276 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_start5_276->setObjectName(QStringLiteral("spinBox_start5_276"));
        spinBox_start5_276->setFont(font6);
        spinBox_start5_276->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_start5_276, 6, 6, 1, 2);

        horizontalSpacer_335 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_92->addItem(horizontalSpacer_335, 0, 12, 1, 1);

        horizontalSpacer_336 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_92->addItem(horizontalSpacer_336, 10, 5, 1, 1);

        spinBox_duration5_238 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_duration5_238->setObjectName(QStringLiteral("spinBox_duration5_238"));
        spinBox_duration5_238->setFont(font6);
        spinBox_duration5_238->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_duration5_238, 11, 9, 1, 2);

        textFIeld_name5_238 = new QLineEdit(scrollAreaWidgetContents_22);
        textFIeld_name5_238->setObjectName(QStringLiteral("textFIeld_name5_238"));
        textFIeld_name5_238->setFont(font2);

        gridLayout_92->addWidget(textFIeld_name5_238, 5, 4, 1, 1);

        label_ID2_40 = new QLabel(scrollAreaWidgetContents_22);
        label_ID2_40->setObjectName(QStringLiteral("label_ID2_40"));
        label_ID2_40->setFont(font5);
        label_ID2_40->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_92->addWidget(label_ID2_40, 1, 2, 1, 1);

        checkBox_382 = new QCheckBox(scrollAreaWidgetContents_22);
        checkBox_382->setObjectName(QStringLiteral("checkBox_382"));
        checkBox_382->setEnabled(false);
        checkBox_382->setBaseSize(QSize(0, 0));
        checkBox_382->setFont(font4);
        checkBox_382->setTabletTracking(false);
        checkBox_382->setAutoFillBackground(false);
        checkBox_382->setChecked(false);
        checkBox_382->setTristate(false);

        gridLayout_92->addWidget(checkBox_382, 6, 1, 1, 1);

        spinBox_start5_277 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_start5_277->setObjectName(QStringLiteral("spinBox_start5_277"));
        spinBox_start5_277->setFont(font6);
        spinBox_start5_277->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_start5_277, 17, 6, 1, 2);

        textFIeld_name2_38 = new QLineEdit(scrollAreaWidgetContents_22);
        textFIeld_name2_38->setObjectName(QStringLiteral("textFIeld_name2_38"));
        textFIeld_name2_38->setFont(font2);

        gridLayout_92->addWidget(textFIeld_name2_38, 1, 4, 1, 1);

        textFIeld_name5_239 = new QLineEdit(scrollAreaWidgetContents_22);
        textFIeld_name5_239->setObjectName(QStringLiteral("textFIeld_name5_239"));
        textFIeld_name5_239->setFont(font2);

        gridLayout_92->addWidget(textFIeld_name5_239, 6, 4, 1, 1);

        checkBox_383 = new QCheckBox(scrollAreaWidgetContents_22);
        checkBox_383->setObjectName(QStringLiteral("checkBox_383"));
        checkBox_383->setEnabled(false);
        checkBox_383->setBaseSize(QSize(0, 0));
        checkBox_383->setFont(font4);
        checkBox_383->setTabletTracking(false);
        checkBox_383->setAutoFillBackground(false);
        checkBox_383->setChecked(false);
        checkBox_383->setTristate(false);

        gridLayout_92->addWidget(checkBox_383, 7, 1, 1, 1);

        label_ID4_281 = new QLabel(scrollAreaWidgetContents_22);
        label_ID4_281->setObjectName(QStringLiteral("label_ID4_281"));
        label_ID4_281->setFont(font5);
        label_ID4_281->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_92->addWidget(label_ID4_281, 6, 2, 1, 1);

        circle_inactive3_144 = new QLabel(scrollAreaWidgetContents_22);
        circle_inactive3_144->setObjectName(QStringLiteral("circle_inactive3_144"));
        QPalette palette9;
        palette9.setBrush(QPalette::Active, QPalette::Button, brush6);
        palette9.setBrush(QPalette::Inactive, QPalette::Button, brush6);
        palette9.setBrush(QPalette::Disabled, QPalette::Button, brush6);
        circle_inactive3_144->setPalette(palette9);
        circle_inactive3_144->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_144->setAlignment(Qt::AlignCenter);

        gridLayout_92->addWidget(circle_inactive3_144, 8, 3, 1, 1);

        textFIeld_name3_37 = new QLineEdit(scrollAreaWidgetContents_22);
        textFIeld_name3_37->setObjectName(QStringLiteral("textFIeld_name3_37"));
        textFIeld_name3_37->setFont(font2);

        gridLayout_92->addWidget(textFIeld_name3_37, 2, 4, 1, 1);

        spinBox_duration5_239 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_duration5_239->setObjectName(QStringLiteral("spinBox_duration5_239"));
        spinBox_duration5_239->setFont(font6);
        spinBox_duration5_239->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_duration5_239, 5, 9, 1, 2);

        textFIeld_name3_38 = new QLineEdit(scrollAreaWidgetContents_22);
        textFIeld_name3_38->setObjectName(QStringLiteral("textFIeld_name3_38"));
        textFIeld_name3_38->setFont(font2);

        gridLayout_92->addWidget(textFIeld_name3_38, 8, 4, 1, 1);

        spinBox_duration5_240 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_duration5_240->setObjectName(QStringLiteral("spinBox_duration5_240"));
        spinBox_duration5_240->setFont(font6);
        spinBox_duration5_240->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_duration5_240, 14, 9, 1, 2);

        checkBox_384 = new QCheckBox(scrollAreaWidgetContents_22);
        checkBox_384->setObjectName(QStringLiteral("checkBox_384"));
        checkBox_384->setEnabled(false);
        checkBox_384->setBaseSize(QSize(0, 0));
        checkBox_384->setFont(font4);
        checkBox_384->setTabletTracking(false);
        checkBox_384->setAutoFillBackground(false);
        checkBox_384->setChecked(false);
        checkBox_384->setTristate(false);

        gridLayout_92->addWidget(checkBox_384, 12, 1, 1, 1);

        textFIeld_name5_240 = new QLineEdit(scrollAreaWidgetContents_22);
        textFIeld_name5_240->setObjectName(QStringLiteral("textFIeld_name5_240"));
        textFIeld_name5_240->setFont(font2);

        gridLayout_92->addWidget(textFIeld_name5_240, 18, 4, 1, 1);

        textFIeld_name5_241 = new QLineEdit(scrollAreaWidgetContents_22);
        textFIeld_name5_241->setObjectName(QStringLiteral("textFIeld_name5_241"));
        textFIeld_name5_241->setFont(font2);

        gridLayout_92->addWidget(textFIeld_name5_241, 20, 4, 1, 1);

        spinBox_duration5_241 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_duration5_241->setObjectName(QStringLiteral("spinBox_duration5_241"));
        spinBox_duration5_241->setFont(font6);
        spinBox_duration5_241->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_duration5_241, 4, 9, 1, 2);

        label_ID4_282 = new QLabel(scrollAreaWidgetContents_22);
        label_ID4_282->setObjectName(QStringLiteral("label_ID4_282"));
        label_ID4_282->setFont(font5);
        label_ID4_282->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_92->addWidget(label_ID4_282, 20, 2, 1, 1);

        horizontalSpacer_337 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_92->addItem(horizontalSpacer_337, 10, 8, 1, 1);

        label_ID4_283 = new QLabel(scrollAreaWidgetContents_22);
        label_ID4_283->setObjectName(QStringLiteral("label_ID4_283"));
        label_ID4_283->setFont(font5);
        label_ID4_283->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_92->addWidget(label_ID4_283, 4, 2, 1, 1);

        textFIeld_name5_242 = new QLineEdit(scrollAreaWidgetContents_22);
        textFIeld_name5_242->setObjectName(QStringLiteral("textFIeld_name5_242"));
        textFIeld_name5_242->setFont(font2);

        gridLayout_92->addWidget(textFIeld_name5_242, 13, 4, 1, 1);

        spinBox_start2_38 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_start2_38->setObjectName(QStringLiteral("spinBox_start2_38"));
        spinBox_start2_38->setFont(font6);
        spinBox_start2_38->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_start2_38, 1, 6, 1, 2);

        spinBox_duration5_242 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_duration5_242->setObjectName(QStringLiteral("spinBox_duration5_242"));
        spinBox_duration5_242->setFont(font6);
        spinBox_duration5_242->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_duration5_242, 17, 9, 1, 2);

        label_ID4_284 = new QLabel(scrollAreaWidgetContents_22);
        label_ID4_284->setObjectName(QStringLiteral("label_ID4_284"));
        label_ID4_284->setFont(font5);
        label_ID4_284->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_92->addWidget(label_ID4_284, 10, 2, 1, 1);

        spinBox_duration5_243 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_duration5_243->setObjectName(QStringLiteral("spinBox_duration5_243"));
        spinBox_duration5_243->setFont(font6);
        spinBox_duration5_243->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_duration5_243, 12, 9, 1, 2);

        spinBox_duration5_244 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_duration5_244->setObjectName(QStringLiteral("spinBox_duration5_244"));
        spinBox_duration5_244->setFont(font6);
        spinBox_duration5_244->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_duration5_244, 20, 9, 1, 2);

        spinBox_start5_278 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_start5_278->setObjectName(QStringLiteral("spinBox_start5_278"));
        spinBox_start5_278->setFont(font6);
        spinBox_start5_278->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_start5_278, 20, 6, 1, 2);

        textFIeld_name5_243 = new QLineEdit(scrollAreaWidgetContents_22);
        textFIeld_name5_243->setObjectName(QStringLiteral("textFIeld_name5_243"));
        textFIeld_name5_243->setFont(font2);

        gridLayout_92->addWidget(textFIeld_name5_243, 11, 4, 1, 1);

        spinBox_start5_279 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_start5_279->setObjectName(QStringLiteral("spinBox_start5_279"));
        spinBox_start5_279->setFont(font6);
        spinBox_start5_279->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_start5_279, 13, 6, 1, 2);

        checkBox_385 = new QCheckBox(scrollAreaWidgetContents_22);
        checkBox_385->setObjectName(QStringLiteral("checkBox_385"));
        checkBox_385->setEnabled(false);
        checkBox_385->setBaseSize(QSize(0, 0));
        checkBox_385->setFont(font4);
        checkBox_385->setTabletTracking(false);
        checkBox_385->setAutoFillBackground(false);
        checkBox_385->setChecked(false);
        checkBox_385->setTristate(false);

        gridLayout_92->addWidget(checkBox_385, 15, 1, 1, 1);

        label_ID4_285 = new QLabel(scrollAreaWidgetContents_22);
        label_ID4_285->setObjectName(QStringLiteral("label_ID4_285"));
        label_ID4_285->setFont(font5);
        label_ID4_285->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_92->addWidget(label_ID4_285, 12, 2, 1, 1);

        label_ID4_286 = new QLabel(scrollAreaWidgetContents_22);
        label_ID4_286->setObjectName(QStringLiteral("label_ID4_286"));
        label_ID4_286->setFont(font5);
        label_ID4_286->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_92->addWidget(label_ID4_286, 16, 2, 1, 1);

        spinBox_duration3_37 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_duration3_37->setObjectName(QStringLiteral("spinBox_duration3_37"));
        spinBox_duration3_37->setFont(font6);
        spinBox_duration3_37->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_duration3_37, 8, 9, 1, 2);

        label_ID2_41 = new QLabel(scrollAreaWidgetContents_22);
        label_ID2_41->setObjectName(QStringLiteral("label_ID2_41"));
        label_ID2_41->setFont(font5);
        label_ID2_41->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_92->addWidget(label_ID2_41, 7, 2, 1, 1);

        label_ID4_287 = new QLabel(scrollAreaWidgetContents_22);
        label_ID4_287->setObjectName(QStringLiteral("label_ID4_287"));
        label_ID4_287->setFont(font5);
        label_ID4_287->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_92->addWidget(label_ID4_287, 15, 2, 1, 1);

        checkBox_386 = new QCheckBox(scrollAreaWidgetContents_22);
        checkBox_386->setObjectName(QStringLiteral("checkBox_386"));
        checkBox_386->setEnabled(false);
        checkBox_386->setBaseSize(QSize(0, 0));
        checkBox_386->setFont(font4);
        checkBox_386->setTabletTracking(false);
        checkBox_386->setAutoFillBackground(false);
        checkBox_386->setChecked(false);
        checkBox_386->setTristate(false);

        gridLayout_92->addWidget(checkBox_386, 0, 1, 1, 1);

        circle_inactive3_145 = new QLabel(scrollAreaWidgetContents_22);
        circle_inactive3_145->setObjectName(QStringLiteral("circle_inactive3_145"));
        circle_inactive3_145->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_145->setAlignment(Qt::AlignCenter);

        gridLayout_92->addWidget(circle_inactive3_145, 2, 3, 1, 1);

        label_ID4_288 = new QLabel(scrollAreaWidgetContents_22);
        label_ID4_288->setObjectName(QStringLiteral("label_ID4_288"));
        label_ID4_288->setFont(font5);
        label_ID4_288->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_92->addWidget(label_ID4_288, 13, 2, 1, 1);

        label_ID4_289 = new QLabel(scrollAreaWidgetContents_22);
        label_ID4_289->setObjectName(QStringLiteral("label_ID4_289"));
        label_ID4_289->setFont(font5);
        label_ID4_289->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_92->addWidget(label_ID4_289, 11, 2, 1, 1);

        spinBox_start4_37 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_start4_37->setObjectName(QStringLiteral("spinBox_start4_37"));
        spinBox_start4_37->setFont(font6);
        spinBox_start4_37->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_start4_37, 3, 6, 1, 2);

        textFIeld_name5_244 = new QLineEdit(scrollAreaWidgetContents_22);
        textFIeld_name5_244->setObjectName(QStringLiteral("textFIeld_name5_244"));
        textFIeld_name5_244->setFont(font2);

        gridLayout_92->addWidget(textFIeld_name5_244, 16, 4, 1, 1);

        spinBox_start5_280 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_start5_280->setObjectName(QStringLiteral("spinBox_start5_280"));
        spinBox_start5_280->setFont(font6);
        spinBox_start5_280->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_start5_280, 12, 6, 1, 2);

        spinBox_duration5_245 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_duration5_245->setObjectName(QStringLiteral("spinBox_duration5_245"));
        spinBox_duration5_245->setFont(font6);
        spinBox_duration5_245->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_duration5_245, 18, 9, 1, 2);

        label_ID3_40 = new QLabel(scrollAreaWidgetContents_22);
        label_ID3_40->setObjectName(QStringLiteral("label_ID3_40"));
        label_ID3_40->setFont(font5);
        label_ID3_40->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_92->addWidget(label_ID3_40, 2, 2, 1, 1);

        spinBox_duration1_19 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_duration1_19->setObjectName(QStringLiteral("spinBox_duration1_19"));
        spinBox_duration1_19->setFont(font6);
        spinBox_duration1_19->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_duration1_19, 0, 9, 1, 2);

        spinBox_duration5_246 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_duration5_246->setObjectName(QStringLiteral("spinBox_duration5_246"));
        spinBox_duration5_246->setFont(font6);
        spinBox_duration5_246->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_duration5_246, 16, 9, 1, 2);

        spinBox_start5_281 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_start5_281->setObjectName(QStringLiteral("spinBox_start5_281"));
        spinBox_start5_281->setFont(font6);
        spinBox_start5_281->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_start5_281, 10, 6, 1, 2);

        spinBox_start4_38 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_start4_38->setObjectName(QStringLiteral("spinBox_start4_38"));
        spinBox_start4_38->setFont(font6);
        spinBox_start4_38->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_start4_38, 9, 6, 1, 2);

        spinBox_duration2_38 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_duration2_38->setObjectName(QStringLiteral("spinBox_duration2_38"));
        spinBox_duration2_38->setFont(font6);
        spinBox_duration2_38->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_duration2_38, 1, 9, 1, 2);

        circle_inactive1_235 = new QLabel(scrollAreaWidgetContents_22);
        circle_inactive1_235->setObjectName(QStringLiteral("circle_inactive1_235"));
        circle_inactive1_235->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_235->setAlignment(Qt::AlignCenter);

        gridLayout_92->addWidget(circle_inactive1_235, 0, 3, 1, 1);

        checkBox_387 = new QCheckBox(scrollAreaWidgetContents_22);
        checkBox_387->setObjectName(QStringLiteral("checkBox_387"));
        checkBox_387->setEnabled(false);
        checkBox_387->setBaseSize(QSize(0, 0));
        checkBox_387->setFont(font4);
        checkBox_387->setTabletTracking(false);
        checkBox_387->setAutoFillBackground(false);
        checkBox_387->setChecked(false);
        checkBox_387->setTristate(false);

        gridLayout_92->addWidget(checkBox_387, 1, 1, 1, 1);

        spinBox_duration3_38 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_duration3_38->setObjectName(QStringLiteral("spinBox_duration3_38"));
        spinBox_duration3_38->setFont(font6);
        spinBox_duration3_38->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_duration3_38, 2, 9, 1, 2);

        checkBox_388 = new QCheckBox(scrollAreaWidgetContents_22);
        checkBox_388->setObjectName(QStringLiteral("checkBox_388"));
        checkBox_388->setEnabled(false);
        checkBox_388->setBaseSize(QSize(0, 0));
        checkBox_388->setFont(font4);
        checkBox_388->setTabletTracking(false);
        checkBox_388->setAutoFillBackground(false);
        checkBox_388->setChecked(false);
        checkBox_388->setTristate(false);

        gridLayout_92->addWidget(checkBox_388, 11, 1, 1, 1);

        checkBox_389 = new QCheckBox(scrollAreaWidgetContents_22);
        checkBox_389->setObjectName(QStringLiteral("checkBox_389"));
        checkBox_389->setEnabled(false);
        checkBox_389->setBaseSize(QSize(0, 0));
        checkBox_389->setFont(font4);
        checkBox_389->setTabletTracking(false);
        checkBox_389->setAutoFillBackground(false);
        checkBox_389->setChecked(false);
        checkBox_389->setTristate(false);

        gridLayout_92->addWidget(checkBox_389, 9, 1, 1, 1);

        checkBox_390 = new QCheckBox(scrollAreaWidgetContents_22);
        checkBox_390->setObjectName(QStringLiteral("checkBox_390"));
        checkBox_390->setEnabled(false);
        checkBox_390->setBaseSize(QSize(0, 0));
        checkBox_390->setFont(font4);
        checkBox_390->setTabletTracking(false);
        checkBox_390->setAutoFillBackground(false);
        checkBox_390->setChecked(false);
        checkBox_390->setTristate(false);

        gridLayout_92->addWidget(checkBox_390, 2, 1, 1, 1);

        textFIeld_name5_245 = new QLineEdit(scrollAreaWidgetContents_22);
        textFIeld_name5_245->setObjectName(QStringLiteral("textFIeld_name5_245"));
        textFIeld_name5_245->setFont(font2);

        gridLayout_92->addWidget(textFIeld_name5_245, 15, 4, 1, 1);

        checkBox_391 = new QCheckBox(scrollAreaWidgetContents_22);
        checkBox_391->setObjectName(QStringLiteral("checkBox_391"));
        checkBox_391->setEnabled(false);
        checkBox_391->setBaseSize(QSize(0, 0));
        checkBox_391->setFont(font4);
        checkBox_391->setTabletTracking(false);
        checkBox_391->setAutoFillBackground(false);
        checkBox_391->setChecked(false);
        checkBox_391->setTristate(false);

        gridLayout_92->addWidget(checkBox_391, 10, 1, 1, 1);

        spinBox_start5_282 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_start5_282->setObjectName(QStringLiteral("spinBox_start5_282"));
        spinBox_start5_282->setFont(font6);
        spinBox_start5_282->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_start5_282, 5, 6, 1, 2);

        label_ID3_41 = new QLabel(scrollAreaWidgetContents_22);
        label_ID3_41->setObjectName(QStringLiteral("label_ID3_41"));
        label_ID3_41->setFont(font5);
        label_ID3_41->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_92->addWidget(label_ID3_41, 8, 2, 1, 1);

        label_ID1_22 = new QLabel(scrollAreaWidgetContents_22);
        label_ID1_22->setObjectName(QStringLiteral("label_ID1_22"));
        label_ID1_22->setFont(font5);
        label_ID1_22->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_92->addWidget(label_ID1_22, 0, 2, 1, 1);

        textFIeld_name5_246 = new QLineEdit(scrollAreaWidgetContents_22);
        textFIeld_name5_246->setObjectName(QStringLiteral("textFIeld_name5_246"));
        textFIeld_name5_246->setFont(font2);

        gridLayout_92->addWidget(textFIeld_name5_246, 14, 4, 1, 1);

        spinBox_duration5_247 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_duration5_247->setObjectName(QStringLiteral("spinBox_duration5_247"));
        spinBox_duration5_247->setFont(font6);
        spinBox_duration5_247->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_duration5_247, 13, 9, 1, 2);

        horizontalSpacer_338 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_92->addItem(horizontalSpacer_338, 21, 3, 1, 1);

        label_ID4_290 = new QLabel(scrollAreaWidgetContents_22);
        label_ID4_290->setObjectName(QStringLiteral("label_ID4_290"));
        label_ID4_290->setFont(font5);
        label_ID4_290->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_92->addWidget(label_ID4_290, 18, 2, 1, 1);

        checkBox_392 = new QCheckBox(scrollAreaWidgetContents_22);
        checkBox_392->setObjectName(QStringLiteral("checkBox_392"));
        checkBox_392->setEnabled(false);
        checkBox_392->setBaseSize(QSize(0, 0));
        checkBox_392->setFont(font4);
        checkBox_392->setTabletTracking(false);
        checkBox_392->setAutoFillBackground(false);
        checkBox_392->setChecked(false);
        checkBox_392->setTristate(false);

        gridLayout_92->addWidget(checkBox_392, 18, 1, 1, 1);

        checkBox_393 = new QCheckBox(scrollAreaWidgetContents_22);
        checkBox_393->setObjectName(QStringLiteral("checkBox_393"));
        checkBox_393->setEnabled(false);
        checkBox_393->setBaseSize(QSize(0, 0));
        checkBox_393->setFont(font4);
        checkBox_393->setTabletTracking(false);
        checkBox_393->setAutoFillBackground(false);
        checkBox_393->setChecked(false);
        checkBox_393->setTristate(false);

        gridLayout_92->addWidget(checkBox_393, 20, 1, 1, 1);

        checkBox_394 = new QCheckBox(scrollAreaWidgetContents_22);
        checkBox_394->setObjectName(QStringLiteral("checkBox_394"));
        checkBox_394->setEnabled(false);
        checkBox_394->setBaseSize(QSize(0, 0));
        checkBox_394->setFont(font4);
        checkBox_394->setTabletTracking(false);
        checkBox_394->setAutoFillBackground(false);
        checkBox_394->setChecked(false);
        checkBox_394->setTristate(false);

        gridLayout_92->addWidget(checkBox_394, 17, 1, 1, 1);

        spinBox_start5_283 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_start5_283->setObjectName(QStringLiteral("spinBox_start5_283"));
        spinBox_start5_283->setFont(font6);
        spinBox_start5_283->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_start5_283, 11, 6, 1, 2);

        checkBox_395 = new QCheckBox(scrollAreaWidgetContents_22);
        checkBox_395->setObjectName(QStringLiteral("checkBox_395"));
        checkBox_395->setEnabled(false);
        checkBox_395->setBaseSize(QSize(0, 0));
        checkBox_395->setFont(font4);
        checkBox_395->setTabletTracking(false);
        checkBox_395->setAutoFillBackground(false);
        checkBox_395->setChecked(false);
        checkBox_395->setTristate(false);

        gridLayout_92->addWidget(checkBox_395, 16, 1, 1, 1);

        spinBox_start5_284 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_start5_284->setObjectName(QStringLiteral("spinBox_start5_284"));
        spinBox_start5_284->setFont(font6);
        spinBox_start5_284->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_start5_284, 16, 6, 1, 2);

        label_ID4_291 = new QLabel(scrollAreaWidgetContents_22);
        label_ID4_291->setObjectName(QStringLiteral("label_ID4_291"));
        label_ID4_291->setFont(font5);
        label_ID4_291->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_92->addWidget(label_ID4_291, 17, 2, 1, 1);

        circle_inactive3_146 = new QLabel(scrollAreaWidgetContents_22);
        circle_inactive3_146->setObjectName(QStringLiteral("circle_inactive3_146"));
        circle_inactive3_146->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_146->setAlignment(Qt::AlignCenter);

        gridLayout_92->addWidget(circle_inactive3_146, 4, 3, 1, 1);

        spinBox_start5_285 = new QSpinBox(scrollAreaWidgetContents_22);
        spinBox_start5_285->setObjectName(QStringLiteral("spinBox_start5_285"));
        spinBox_start5_285->setFont(font6);
        spinBox_start5_285->setMaximum(10000000);

        gridLayout_92->addWidget(spinBox_start5_285, 18, 6, 1, 2);

        circle_inactive1_236 = new QLabel(scrollAreaWidgetContents_22);
        circle_inactive1_236->setObjectName(QStringLiteral("circle_inactive1_236"));
        circle_inactive1_236->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_236->setAlignment(Qt::AlignCenter);

        gridLayout_92->addWidget(circle_inactive1_236, 1, 3, 1, 1);

        textFIeld_name5_247 = new QLineEdit(scrollAreaWidgetContents_22);
        textFIeld_name5_247->setObjectName(QStringLiteral("textFIeld_name5_247"));
        textFIeld_name5_247->setFont(font2);

        gridLayout_92->addWidget(textFIeld_name5_247, 17, 4, 1, 1);

        circle_inactive3_147 = new QLabel(scrollAreaWidgetContents_22);
        circle_inactive3_147->setObjectName(QStringLiteral("circle_inactive3_147"));
        circle_inactive3_147->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_147->setAlignment(Qt::AlignCenter);

        gridLayout_92->addWidget(circle_inactive3_147, 6, 3, 1, 1);

        horizontalSpacer_339 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_92->addItem(horizontalSpacer_339, 0, 8, 1, 1);

        circle_inactive3_148 = new QLabel(scrollAreaWidgetContents_22);
        circle_inactive3_148->setObjectName(QStringLiteral("circle_inactive3_148"));
        circle_inactive3_148->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_148->setAlignment(Qt::AlignCenter);

        gridLayout_92->addWidget(circle_inactive3_148, 7, 3, 1, 1);

        circle_inactive1_237 = new QLabel(scrollAreaWidgetContents_22);
        circle_inactive1_237->setObjectName(QStringLiteral("circle_inactive1_237"));
        circle_inactive1_237->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_237->setAlignment(Qt::AlignCenter);

        gridLayout_92->addWidget(circle_inactive1_237, 9, 3, 1, 1);

        circle_inactive1_238 = new QLabel(scrollAreaWidgetContents_22);
        circle_inactive1_238->setObjectName(QStringLiteral("circle_inactive1_238"));
        circle_inactive1_238->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_238->setAlignment(Qt::AlignCenter);

        gridLayout_92->addWidget(circle_inactive1_238, 10, 3, 1, 1);

        circle_inactive1_239 = new QLabel(scrollAreaWidgetContents_22);
        circle_inactive1_239->setObjectName(QStringLiteral("circle_inactive1_239"));
        circle_inactive1_239->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_239->setAlignment(Qt::AlignCenter);

        gridLayout_92->addWidget(circle_inactive1_239, 11, 3, 1, 1);

        circle_inactive1_240 = new QLabel(scrollAreaWidgetContents_22);
        circle_inactive1_240->setObjectName(QStringLiteral("circle_inactive1_240"));
        circle_inactive1_240->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_240->setAlignment(Qt::AlignCenter);

        gridLayout_92->addWidget(circle_inactive1_240, 12, 3, 1, 1);

        circle_inactive1_241 = new QLabel(scrollAreaWidgetContents_22);
        circle_inactive1_241->setObjectName(QStringLiteral("circle_inactive1_241"));
        circle_inactive1_241->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_241->setAlignment(Qt::AlignCenter);

        gridLayout_92->addWidget(circle_inactive1_241, 13, 3, 1, 1);

        circle_inactive1_242 = new QLabel(scrollAreaWidgetContents_22);
        circle_inactive1_242->setObjectName(QStringLiteral("circle_inactive1_242"));
        circle_inactive1_242->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_242->setAlignment(Qt::AlignCenter);

        gridLayout_92->addWidget(circle_inactive1_242, 14, 3, 1, 1);

        circle_inactive1_243 = new QLabel(scrollAreaWidgetContents_22);
        circle_inactive1_243->setObjectName(QStringLiteral("circle_inactive1_243"));
        circle_inactive1_243->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_243->setAlignment(Qt::AlignCenter);

        gridLayout_92->addWidget(circle_inactive1_243, 15, 3, 1, 1);

        circle_inactive1_244 = new QLabel(scrollAreaWidgetContents_22);
        circle_inactive1_244->setObjectName(QStringLiteral("circle_inactive1_244"));
        circle_inactive1_244->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_244->setAlignment(Qt::AlignCenter);

        gridLayout_92->addWidget(circle_inactive1_244, 16, 3, 1, 1);

        circle_inactive1_245 = new QLabel(scrollAreaWidgetContents_22);
        circle_inactive1_245->setObjectName(QStringLiteral("circle_inactive1_245"));
        circle_inactive1_245->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_245->setAlignment(Qt::AlignCenter);

        gridLayout_92->addWidget(circle_inactive1_245, 17, 3, 1, 1);

        circle_inactive1_246 = new QLabel(scrollAreaWidgetContents_22);
        circle_inactive1_246->setObjectName(QStringLiteral("circle_inactive1_246"));
        circle_inactive1_246->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_246->setAlignment(Qt::AlignCenter);

        gridLayout_92->addWidget(circle_inactive1_246, 18, 3, 1, 1);

        circle_inactive1_247 = new QLabel(scrollAreaWidgetContents_22);
        circle_inactive1_247->setObjectName(QStringLiteral("circle_inactive1_247"));
        circle_inactive1_247->setPixmap(QPixmap(QString::fromUtf8("../../03_06/06_03_app/UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_247->setAlignment(Qt::AlignCenter);

        gridLayout_92->addWidget(circle_inactive1_247, 20, 3, 1, 1);


        gridLayout_91->addLayout(gridLayout_92, 1, 0, 1, 1);

        scrollArea_19->setWidget(scrollAreaWidgetContents_22);

        gridLayout_89->addWidget(scrollArea_19, 1, 0, 1, 1);

        horizontalSpacer_340 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_89->addItem(horizontalSpacer_340, 1, 1, 1, 1);


        gridLayout_88->addLayout(gridLayout_89, 0, 0, 1, 1);

        horizontalSpacer_341 = new QSpacerItem(8, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_88->addItem(horizontalSpacer_341, 0, 1, 1, 1);

        tabWidget_2->addTab(tab_15, QString());

        gridLayout_13->addWidget(tabWidget_2, 0, 0, 1, 1);

        MainWindowapp2->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindowapp2);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1200, 22));
        MainWindowapp2->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindowapp2);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindowapp2->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindowapp2);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindowapp2->setStatusBar(statusBar);
        QWidget::setTabOrder(checkBox1, textField_name1);
        QWidget::setTabOrder(textField_name1, spinBox_start1);
        QWidget::setTabOrder(spinBox_start1, spinBox_duration1);
        QWidget::setTabOrder(spinBox_duration1, checkBox2);
        QWidget::setTabOrder(checkBox2, textField_name2);
        QWidget::setTabOrder(textField_name2, spinBox_start2);
        QWidget::setTabOrder(spinBox_start2, spinBox_duration2);
        QWidget::setTabOrder(spinBox_duration2, checkBox3);
        QWidget::setTabOrder(checkBox3, textField_name3);
        QWidget::setTabOrder(textField_name3, spinBox_start3);
        QWidget::setTabOrder(spinBox_start3, spinBox_duration3);
        QWidget::setTabOrder(spinBox_duration3, checkBox4);
        QWidget::setTabOrder(checkBox4, textField_name4);
        QWidget::setTabOrder(textField_name4, spinBox_start4);
        QWidget::setTabOrder(spinBox_start4, spinBox_duration4);
        QWidget::setTabOrder(spinBox_duration4, checkBox5);
        QWidget::setTabOrder(checkBox5, textField_name5);
        QWidget::setTabOrder(textField_name5, spinBox_start5);
        QWidget::setTabOrder(spinBox_start5, spinBox_duration5);
        QWidget::setTabOrder(spinBox_duration5, checkBox6);
        QWidget::setTabOrder(checkBox6, textField_name6);
        QWidget::setTabOrder(textField_name6, spinBox_start6);
        QWidget::setTabOrder(spinBox_start6, spinBox_duration6);
        QWidget::setTabOrder(spinBox_duration6, checkBox7);
        QWidget::setTabOrder(checkBox7, textField_name7);
        QWidget::setTabOrder(textField_name7, spinBox_start7);
        QWidget::setTabOrder(spinBox_start7, spinBox_duration7);
        QWidget::setTabOrder(spinBox_duration7, checkBox8);
        QWidget::setTabOrder(checkBox8, textField_name8);
        QWidget::setTabOrder(textField_name8, spinBox_start8);
        QWidget::setTabOrder(spinBox_start8, spinBox_duration8);
        QWidget::setTabOrder(spinBox_duration8, checkBox9);
        QWidget::setTabOrder(checkBox9, textField_name9);
        QWidget::setTabOrder(textField_name9, spinBox_start9);
        QWidget::setTabOrder(spinBox_start9, spinBox_duration9);
        QWidget::setTabOrder(spinBox_duration9, checkBox10);
        QWidget::setTabOrder(checkBox10, textField_name10);
        QWidget::setTabOrder(textField_name10, spinBox_start10);
        QWidget::setTabOrder(spinBox_start10, spinBox_duration10);
        QWidget::setTabOrder(spinBox_duration10, checkBox11);
        QWidget::setTabOrder(checkBox11, textField_name11);
        QWidget::setTabOrder(textField_name11, spinBox_start11);
        QWidget::setTabOrder(spinBox_start11, spinBox_duration11);
        QWidget::setTabOrder(spinBox_duration11, checkBox12);
        QWidget::setTabOrder(checkBox12, textField_name12);
        QWidget::setTabOrder(textField_name12, spinBox_start12);
        QWidget::setTabOrder(spinBox_start12, spinBox_duration12);
        QWidget::setTabOrder(spinBox_duration12, checkBox13);
        QWidget::setTabOrder(checkBox13, textField_name13);
        QWidget::setTabOrder(textField_name13, spinBox_start13);
        QWidget::setTabOrder(spinBox_start13, spinBox_duration13);
        QWidget::setTabOrder(spinBox_duration13, checkBox14);
        QWidget::setTabOrder(checkBox14, textField_name14);
        QWidget::setTabOrder(textField_name14, spinBox_start14);
        QWidget::setTabOrder(spinBox_start14, spinBox_duration14);
        QWidget::setTabOrder(spinBox_duration14, checkBox15);
        QWidget::setTabOrder(checkBox15, textField_name15);
        QWidget::setTabOrder(textField_name15, spinBox_start15);
        QWidget::setTabOrder(spinBox_start15, spinBox_duration15);
        QWidget::setTabOrder(spinBox_duration15, checkBox16);
        QWidget::setTabOrder(checkBox16, textField_name16);
        QWidget::setTabOrder(textField_name16, spinBox_start16);
        QWidget::setTabOrder(spinBox_start16, spinBox_duration16);
        QWidget::setTabOrder(spinBox_duration16, checkBox17);
        QWidget::setTabOrder(checkBox17, textField_name17);
        QWidget::setTabOrder(textField_name17, spinBox_start17);
        QWidget::setTabOrder(spinBox_start17, spinBox_duration17);
        QWidget::setTabOrder(spinBox_duration17, checkBox18);
        QWidget::setTabOrder(checkBox18, textField_name18);
        QWidget::setTabOrder(textField_name18, spinBox_start18);
        QWidget::setTabOrder(spinBox_start18, spinBox_duration18);
        QWidget::setTabOrder(spinBox_duration18, checkBox19);
        QWidget::setTabOrder(checkBox19, textField_name19);
        QWidget::setTabOrder(textField_name19, spinBox_start19);
        QWidget::setTabOrder(spinBox_start19, spinBox_duration19);
        QWidget::setTabOrder(spinBox_duration19, checkBox20);
        QWidget::setTabOrder(checkBox20, textField_name20);
        QWidget::setTabOrder(textField_name20, spinBox_start20);
        QWidget::setTabOrder(spinBox_start20, spinBox_duration20);
        QWidget::setTabOrder(spinBox_duration20, checkBox21);
        QWidget::setTabOrder(checkBox21, textField_name21);
        QWidget::setTabOrder(textField_name21, spinBox_start21);
        QWidget::setTabOrder(spinBox_start21, spinBox_duration21);
        QWidget::setTabOrder(spinBox_duration21, checkBox22);
        QWidget::setTabOrder(checkBox22, textField_name22);
        QWidget::setTabOrder(textField_name22, spinBox_start22);
        QWidget::setTabOrder(spinBox_start22, spinBox_duration22);
        QWidget::setTabOrder(spinBox_duration22, checkBox23);
        QWidget::setTabOrder(checkBox23, textField_name23);
        QWidget::setTabOrder(textField_name23, spinBox_start23);
        QWidget::setTabOrder(spinBox_start23, spinBox_duration23);
        QWidget::setTabOrder(spinBox_duration23, checkBox24);
        QWidget::setTabOrder(checkBox24, textField_name24);
        QWidget::setTabOrder(textField_name24, spinBox_start24);
        QWidget::setTabOrder(spinBox_start24, spinBox_duration24);
        QWidget::setTabOrder(spinBox_duration24, textFIeld_name1_38);
        QWidget::setTabOrder(textFIeld_name1_38, scrollArea_14);
        QWidget::setTabOrder(scrollArea_14, textFIeld_name1_39);
        QWidget::setTabOrder(textFIeld_name1_39, checkBox_312);
        QWidget::setTabOrder(checkBox_312, checkBox_314);
        QWidget::setTabOrder(checkBox_314, tabWidget_2);
        QWidget::setTabOrder(tabWidget_2, textFIeld_name1_46);
        QWidget::setTabOrder(textFIeld_name1_46, textFIeld_name1_48);
        QWidget::setTabOrder(textFIeld_name1_48, checkBox_311);
        QWidget::setTabOrder(checkBox_311, textFIeld_name1_42);
        QWidget::setTabOrder(textFIeld_name1_42, textFIeld_name1_47);
        QWidget::setTabOrder(textFIeld_name1_47, textFIeld_name1_40);
        QWidget::setTabOrder(textFIeld_name1_40, checkBox_313);
        QWidget::setTabOrder(checkBox_313, textFIeld_name1_45);
        QWidget::setTabOrder(textFIeld_name1_45, textFIeld_name1_41);
        QWidget::setTabOrder(textFIeld_name1_41, textFIeld_name1_43);
        QWidget::setTabOrder(textFIeld_name1_43, textFIeld_name1_44);
        QWidget::setTabOrder(textFIeld_name1_44, checkBox_315);
        QWidget::setTabOrder(checkBox_315, scrollArea_node1);
        QWidget::setTabOrder(scrollArea_node1, armButton);
        QWidget::setTabOrder(armButton, startButton);
        QWidget::setTabOrder(startButton, stopResetButton);
        QWidget::setTabOrder(stopResetButton, writeConfigButton);
        QWidget::setTabOrder(writeConfigButton, readConfigButton);
        QWidget::setTabOrder(readConfigButton, previewButton);
        QWidget::setTabOrder(previewButton, spinBox_SlowMo);
        QWidget::setTabOrder(spinBox_SlowMo, spinBox_cycleCounter);
        QWidget::setTabOrder(spinBox_cycleCounter, spinBox_Pause);
        QWidget::setTabOrder(spinBox_Pause, spinBox_start5_91);
        QWidget::setTabOrder(spinBox_start5_91, spinBox_start5_92);
        QWidget::setTabOrder(spinBox_start5_92, pushButton_27);
        QWidget::setTabOrder(pushButton_27, pushButton_28);
        QWidget::setTabOrder(pushButton_28, pushButton_29);
        QWidget::setTabOrder(pushButton_29, pushButton_30);
        QWidget::setTabOrder(pushButton_30, scrollArea_16);
        QWidget::setTabOrder(scrollArea_16, textFIeld_name5_79);
        QWidget::setTabOrder(textFIeld_name5_79, spinBox_start2_13);
        QWidget::setTabOrder(spinBox_start2_13, checkBox_121);
        QWidget::setTabOrder(checkBox_121, checkBox_122);
        QWidget::setTabOrder(checkBox_122, checkBox_123);
        QWidget::setTabOrder(checkBox_123, spinBox_start1_7);
        QWidget::setTabOrder(spinBox_start1_7, checkBox_124);
        QWidget::setTabOrder(checkBox_124, textFIeld_name1_7);
        QWidget::setTabOrder(textFIeld_name1_7, spinBox_duration2_13);
        QWidget::setTabOrder(spinBox_duration2_13, spinBox_duration4_13);
        QWidget::setTabOrder(spinBox_duration4_13, textFIeld_name4_13);
        QWidget::setTabOrder(textFIeld_name4_13, spinBox_duration5_79);
        QWidget::setTabOrder(spinBox_duration5_79, checkBox_125);
        QWidget::setTabOrder(checkBox_125, textFIeld_name2_13);
        QWidget::setTabOrder(textFIeld_name2_13, spinBox_duration5_80);
        QWidget::setTabOrder(spinBox_duration5_80, textFIeld_name5_80);
        QWidget::setTabOrder(textFIeld_name5_80, spinBox_start5_93);
        QWidget::setTabOrder(spinBox_start5_93, spinBox_start3_13);
        QWidget::setTabOrder(spinBox_start3_13, spinBox_duration4_14);
        QWidget::setTabOrder(spinBox_duration4_14, checkBox_126);
        QWidget::setTabOrder(checkBox_126, spinBox_start5_94);
        QWidget::setTabOrder(spinBox_start5_94, spinBox_duration5_81);
        QWidget::setTabOrder(spinBox_duration5_81, textFIeld_name5_81);
        QWidget::setTabOrder(textFIeld_name5_81, spinBox_start5_95);
        QWidget::setTabOrder(spinBox_start5_95, textFIeld_name4_14);
        QWidget::setTabOrder(textFIeld_name4_14, spinBox_start3_14);
        QWidget::setTabOrder(spinBox_start3_14, spinBox_start5_96);
        QWidget::setTabOrder(spinBox_start5_96, spinBox_duration5_82);
        QWidget::setTabOrder(spinBox_duration5_82, textFIeld_name5_82);
        QWidget::setTabOrder(textFIeld_name5_82, checkBox_127);
        QWidget::setTabOrder(checkBox_127, spinBox_start5_97);
        QWidget::setTabOrder(spinBox_start5_97, textFIeld_name2_14);
        QWidget::setTabOrder(textFIeld_name2_14, textFIeld_name5_83);
        QWidget::setTabOrder(textFIeld_name5_83, checkBox_128);
        QWidget::setTabOrder(checkBox_128, textFIeld_name3_13);
        QWidget::setTabOrder(textFIeld_name3_13, spinBox_duration5_83);
        QWidget::setTabOrder(spinBox_duration5_83, textFIeld_name3_14);
        QWidget::setTabOrder(textFIeld_name3_14, spinBox_duration5_84);
        QWidget::setTabOrder(spinBox_duration5_84, checkBox_129);
        QWidget::setTabOrder(checkBox_129, textFIeld_name5_84);
        QWidget::setTabOrder(textFIeld_name5_84, textFIeld_name5_85);
        QWidget::setTabOrder(textFIeld_name5_85, spinBox_duration5_85);
        QWidget::setTabOrder(spinBox_duration5_85, textFIeld_name5_86);
        QWidget::setTabOrder(textFIeld_name5_86, spinBox_start2_14);
        QWidget::setTabOrder(spinBox_start2_14, spinBox_duration5_86);
        QWidget::setTabOrder(spinBox_duration5_86, spinBox_duration5_87);
        QWidget::setTabOrder(spinBox_duration5_87, spinBox_duration5_88);
        QWidget::setTabOrder(spinBox_duration5_88, spinBox_start5_98);
        QWidget::setTabOrder(spinBox_start5_98, textFIeld_name5_87);
        QWidget::setTabOrder(textFIeld_name5_87, spinBox_start5_99);
        QWidget::setTabOrder(spinBox_start5_99, checkBox_130);
        QWidget::setTabOrder(checkBox_130, spinBox_duration3_13);
        QWidget::setTabOrder(spinBox_duration3_13, checkBox_131);
        QWidget::setTabOrder(checkBox_131, spinBox_start4_13);
        QWidget::setTabOrder(spinBox_start4_13, textFIeld_name5_88);
        QWidget::setTabOrder(textFIeld_name5_88, spinBox_start5_100);
        QWidget::setTabOrder(spinBox_start5_100, spinBox_duration5_89);
        QWidget::setTabOrder(spinBox_duration5_89, spinBox_duration1_7);
        QWidget::setTabOrder(spinBox_duration1_7, spinBox_duration5_90);
        QWidget::setTabOrder(spinBox_duration5_90, spinBox_start5_101);
        QWidget::setTabOrder(spinBox_start5_101, spinBox_start4_14);
        QWidget::setTabOrder(spinBox_start4_14, spinBox_duration2_14);
        QWidget::setTabOrder(spinBox_duration2_14, checkBox_132);
        QWidget::setTabOrder(checkBox_132, spinBox_duration3_14);
        QWidget::setTabOrder(spinBox_duration3_14, checkBox_133);
        QWidget::setTabOrder(checkBox_133, checkBox_134);
        QWidget::setTabOrder(checkBox_134, checkBox_135);
        QWidget::setTabOrder(checkBox_135, textFIeld_name5_89);
        QWidget::setTabOrder(textFIeld_name5_89, checkBox_136);
        QWidget::setTabOrder(checkBox_136, spinBox_start5_102);
        QWidget::setTabOrder(spinBox_start5_102, textFIeld_name5_90);
        QWidget::setTabOrder(textFIeld_name5_90, spinBox_duration5_91);
        QWidget::setTabOrder(spinBox_duration5_91, checkBox_137);
        QWidget::setTabOrder(checkBox_137, checkBox_138);
        QWidget::setTabOrder(checkBox_138, checkBox_139);
        QWidget::setTabOrder(checkBox_139, spinBox_start5_103);
        QWidget::setTabOrder(spinBox_start5_103, checkBox_140);
        QWidget::setTabOrder(checkBox_140, spinBox_start5_104);
        QWidget::setTabOrder(spinBox_start5_104, spinBox_start5_105);
        QWidget::setTabOrder(spinBox_start5_105, textFIeld_name5_91);
        QWidget::setTabOrder(textFIeld_name5_91, spinBox_start5_241);
        QWidget::setTabOrder(spinBox_start5_241, pushButton_67);
        QWidget::setTabOrder(pushButton_67, pushButton_68);
        QWidget::setTabOrder(pushButton_68, spinBox_start5_242);
        QWidget::setTabOrder(spinBox_start5_242, pushButton_69);
        QWidget::setTabOrder(pushButton_69, pushButton_70);
        QWidget::setTabOrder(pushButton_70, scrollArea_17);
        QWidget::setTabOrder(scrollArea_17, textFIeld_name5_209);
        QWidget::setTabOrder(textFIeld_name5_209, spinBox_start2_33);
        QWidget::setTabOrder(spinBox_start2_33, checkBox_336);
        QWidget::setTabOrder(checkBox_336, checkBox_337);
        QWidget::setTabOrder(checkBox_337, checkBox_338);
        QWidget::setTabOrder(checkBox_338, spinBox_start1_17);
        QWidget::setTabOrder(spinBox_start1_17, checkBox_339);
        QWidget::setTabOrder(checkBox_339, textFIeld_name1_50);
        QWidget::setTabOrder(textFIeld_name1_50, spinBox_duration2_33);
        QWidget::setTabOrder(spinBox_duration2_33, spinBox_duration4_33);
        QWidget::setTabOrder(spinBox_duration4_33, textFIeld_name4_33);
        QWidget::setTabOrder(textFIeld_name4_33, spinBox_duration5_209);
        QWidget::setTabOrder(spinBox_duration5_209, checkBox_340);
        QWidget::setTabOrder(checkBox_340, textFIeld_name2_33);
        QWidget::setTabOrder(textFIeld_name2_33, spinBox_duration5_210);
        QWidget::setTabOrder(spinBox_duration5_210, textFIeld_name5_210);
        QWidget::setTabOrder(textFIeld_name5_210, spinBox_start5_243);
        QWidget::setTabOrder(spinBox_start5_243, spinBox_start3_33);
        QWidget::setTabOrder(spinBox_start3_33, spinBox_duration4_34);
        QWidget::setTabOrder(spinBox_duration4_34, checkBox_341);
        QWidget::setTabOrder(checkBox_341, spinBox_start5_244);
        QWidget::setTabOrder(spinBox_start5_244, spinBox_duration5_211);
        QWidget::setTabOrder(spinBox_duration5_211, textFIeld_name5_211);
        QWidget::setTabOrder(textFIeld_name5_211, spinBox_start5_245);
        QWidget::setTabOrder(spinBox_start5_245, textFIeld_name4_34);
        QWidget::setTabOrder(textFIeld_name4_34, spinBox_start3_34);
        QWidget::setTabOrder(spinBox_start3_34, spinBox_start5_246);
        QWidget::setTabOrder(spinBox_start5_246, spinBox_duration5_212);
        QWidget::setTabOrder(spinBox_duration5_212, textFIeld_name5_212);
        QWidget::setTabOrder(textFIeld_name5_212, checkBox_342);
        QWidget::setTabOrder(checkBox_342, spinBox_start5_247);
        QWidget::setTabOrder(spinBox_start5_247, textFIeld_name2_34);
        QWidget::setTabOrder(textFIeld_name2_34, textFIeld_name5_213);
        QWidget::setTabOrder(textFIeld_name5_213, checkBox_343);
        QWidget::setTabOrder(checkBox_343, textFIeld_name3_33);
        QWidget::setTabOrder(textFIeld_name3_33, spinBox_duration5_213);
        QWidget::setTabOrder(spinBox_duration5_213, textFIeld_name3_34);
        QWidget::setTabOrder(textFIeld_name3_34, spinBox_duration5_214);
        QWidget::setTabOrder(spinBox_duration5_214, checkBox_344);
        QWidget::setTabOrder(checkBox_344, textFIeld_name5_214);
        QWidget::setTabOrder(textFIeld_name5_214, textFIeld_name5_215);
        QWidget::setTabOrder(textFIeld_name5_215, spinBox_duration5_215);
        QWidget::setTabOrder(spinBox_duration5_215, textFIeld_name5_216);
        QWidget::setTabOrder(textFIeld_name5_216, spinBox_start2_34);
        QWidget::setTabOrder(spinBox_start2_34, spinBox_duration5_216);
        QWidget::setTabOrder(spinBox_duration5_216, spinBox_duration5_217);
        QWidget::setTabOrder(spinBox_duration5_217, spinBox_duration5_218);
        QWidget::setTabOrder(spinBox_duration5_218, spinBox_start5_248);
        QWidget::setTabOrder(spinBox_start5_248, textFIeld_name5_217);
        QWidget::setTabOrder(textFIeld_name5_217, spinBox_start5_249);
        QWidget::setTabOrder(spinBox_start5_249, checkBox_345);
        QWidget::setTabOrder(checkBox_345, spinBox_duration3_33);
        QWidget::setTabOrder(spinBox_duration3_33, checkBox_346);
        QWidget::setTabOrder(checkBox_346, spinBox_start4_33);
        QWidget::setTabOrder(spinBox_start4_33, textFIeld_name5_218);
        QWidget::setTabOrder(textFIeld_name5_218, spinBox_start5_250);
        QWidget::setTabOrder(spinBox_start5_250, spinBox_duration5_219);
        QWidget::setTabOrder(spinBox_duration5_219, spinBox_duration1_17);
        QWidget::setTabOrder(spinBox_duration1_17, spinBox_duration5_220);
        QWidget::setTabOrder(spinBox_duration5_220, spinBox_start5_251);
        QWidget::setTabOrder(spinBox_start5_251, spinBox_start4_34);
        QWidget::setTabOrder(spinBox_start4_34, spinBox_duration2_34);
        QWidget::setTabOrder(spinBox_duration2_34, checkBox_347);
        QWidget::setTabOrder(checkBox_347, spinBox_duration3_34);
        QWidget::setTabOrder(spinBox_duration3_34, checkBox_348);
        QWidget::setTabOrder(checkBox_348, checkBox_349);
        QWidget::setTabOrder(checkBox_349, checkBox_350);
        QWidget::setTabOrder(checkBox_350, textFIeld_name5_219);
        QWidget::setTabOrder(textFIeld_name5_219, checkBox_351);
        QWidget::setTabOrder(checkBox_351, spinBox_start5_252);
        QWidget::setTabOrder(spinBox_start5_252, textFIeld_name5_220);
        QWidget::setTabOrder(textFIeld_name5_220, spinBox_duration5_221);
        QWidget::setTabOrder(spinBox_duration5_221, checkBox_352);
        QWidget::setTabOrder(checkBox_352, checkBox_353);
        QWidget::setTabOrder(checkBox_353, checkBox_354);
        QWidget::setTabOrder(checkBox_354, spinBox_start5_253);
        QWidget::setTabOrder(spinBox_start5_253, checkBox_355);
        QWidget::setTabOrder(checkBox_355, spinBox_start5_254);
        QWidget::setTabOrder(spinBox_start5_254, spinBox_start5_255);
        QWidget::setTabOrder(spinBox_start5_255, textFIeld_name5_221);
        QWidget::setTabOrder(textFIeld_name5_221, spinBox_start5_256);
        QWidget::setTabOrder(spinBox_start5_256, spinBox_start5_257);
        QWidget::setTabOrder(spinBox_start5_257, pushButton_71);
        QWidget::setTabOrder(pushButton_71, pushButton_72);
        QWidget::setTabOrder(pushButton_72, pushButton_73);
        QWidget::setTabOrder(pushButton_73, pushButton_74);
        QWidget::setTabOrder(pushButton_74, scrollArea_18);
        QWidget::setTabOrder(scrollArea_18, textFIeld_name5_222);
        QWidget::setTabOrder(textFIeld_name5_222, spinBox_start2_35);
        QWidget::setTabOrder(spinBox_start2_35, checkBox_356);
        QWidget::setTabOrder(checkBox_356, checkBox_357);
        QWidget::setTabOrder(checkBox_357, checkBox_358);
        QWidget::setTabOrder(checkBox_358, spinBox_start1_18);
        QWidget::setTabOrder(spinBox_start1_18, checkBox_359);
        QWidget::setTabOrder(checkBox_359, textFIeld_name1_51);
        QWidget::setTabOrder(textFIeld_name1_51, spinBox_duration2_35);
        QWidget::setTabOrder(spinBox_duration2_35, spinBox_duration4_35);
        QWidget::setTabOrder(spinBox_duration4_35, textFIeld_name4_35);
        QWidget::setTabOrder(textFIeld_name4_35, spinBox_duration5_222);
        QWidget::setTabOrder(spinBox_duration5_222, checkBox_360);
        QWidget::setTabOrder(checkBox_360, textFIeld_name2_35);
        QWidget::setTabOrder(textFIeld_name2_35, spinBox_duration5_223);
        QWidget::setTabOrder(spinBox_duration5_223, textFIeld_name5_223);
        QWidget::setTabOrder(textFIeld_name5_223, spinBox_start5_258);
        QWidget::setTabOrder(spinBox_start5_258, spinBox_start3_35);
        QWidget::setTabOrder(spinBox_start3_35, spinBox_duration4_36);
        QWidget::setTabOrder(spinBox_duration4_36, checkBox_361);
        QWidget::setTabOrder(checkBox_361, spinBox_start5_259);
        QWidget::setTabOrder(spinBox_start5_259, spinBox_duration5_224);
        QWidget::setTabOrder(spinBox_duration5_224, textFIeld_name5_224);
        QWidget::setTabOrder(textFIeld_name5_224, spinBox_start5_260);
        QWidget::setTabOrder(spinBox_start5_260, textFIeld_name4_36);
        QWidget::setTabOrder(textFIeld_name4_36, spinBox_start3_36);
        QWidget::setTabOrder(spinBox_start3_36, spinBox_start5_261);
        QWidget::setTabOrder(spinBox_start5_261, spinBox_duration5_225);
        QWidget::setTabOrder(spinBox_duration5_225, textFIeld_name5_225);
        QWidget::setTabOrder(textFIeld_name5_225, checkBox_362);
        QWidget::setTabOrder(checkBox_362, spinBox_start5_262);
        QWidget::setTabOrder(spinBox_start5_262, textFIeld_name2_36);
        QWidget::setTabOrder(textFIeld_name2_36, textFIeld_name5_226);
        QWidget::setTabOrder(textFIeld_name5_226, checkBox_363);
        QWidget::setTabOrder(checkBox_363, textFIeld_name3_35);
        QWidget::setTabOrder(textFIeld_name3_35, spinBox_duration5_226);
        QWidget::setTabOrder(spinBox_duration5_226, textFIeld_name3_36);
        QWidget::setTabOrder(textFIeld_name3_36, spinBox_duration5_227);
        QWidget::setTabOrder(spinBox_duration5_227, checkBox_364);
        QWidget::setTabOrder(checkBox_364, textFIeld_name5_227);
        QWidget::setTabOrder(textFIeld_name5_227, textFIeld_name5_228);
        QWidget::setTabOrder(textFIeld_name5_228, spinBox_duration5_228);
        QWidget::setTabOrder(spinBox_duration5_228, textFIeld_name5_229);
        QWidget::setTabOrder(textFIeld_name5_229, spinBox_start2_36);
        QWidget::setTabOrder(spinBox_start2_36, spinBox_duration5_229);
        QWidget::setTabOrder(spinBox_duration5_229, spinBox_duration5_230);
        QWidget::setTabOrder(spinBox_duration5_230, spinBox_duration5_231);
        QWidget::setTabOrder(spinBox_duration5_231, spinBox_start5_263);
        QWidget::setTabOrder(spinBox_start5_263, textFIeld_name5_230);
        QWidget::setTabOrder(textFIeld_name5_230, spinBox_start5_264);
        QWidget::setTabOrder(spinBox_start5_264, checkBox_365);
        QWidget::setTabOrder(checkBox_365, spinBox_duration3_35);
        QWidget::setTabOrder(spinBox_duration3_35, checkBox_366);
        QWidget::setTabOrder(checkBox_366, spinBox_start4_35);
        QWidget::setTabOrder(spinBox_start4_35, textFIeld_name5_231);
        QWidget::setTabOrder(textFIeld_name5_231, spinBox_start5_265);
        QWidget::setTabOrder(spinBox_start5_265, spinBox_duration5_232);
        QWidget::setTabOrder(spinBox_duration5_232, spinBox_duration1_18);
        QWidget::setTabOrder(spinBox_duration1_18, spinBox_duration5_233);
        QWidget::setTabOrder(spinBox_duration5_233, spinBox_start5_266);
        QWidget::setTabOrder(spinBox_start5_266, spinBox_start4_36);
        QWidget::setTabOrder(spinBox_start4_36, spinBox_duration2_36);
        QWidget::setTabOrder(spinBox_duration2_36, checkBox_367);
        QWidget::setTabOrder(checkBox_367, spinBox_duration3_36);
        QWidget::setTabOrder(spinBox_duration3_36, checkBox_368);
        QWidget::setTabOrder(checkBox_368, checkBox_369);
        QWidget::setTabOrder(checkBox_369, checkBox_370);
        QWidget::setTabOrder(checkBox_370, textFIeld_name5_232);
        QWidget::setTabOrder(textFIeld_name5_232, checkBox_371);
        QWidget::setTabOrder(checkBox_371, spinBox_start5_267);
        QWidget::setTabOrder(spinBox_start5_267, textFIeld_name5_233);
        QWidget::setTabOrder(textFIeld_name5_233, spinBox_duration5_234);
        QWidget::setTabOrder(spinBox_duration5_234, checkBox_372);
        QWidget::setTabOrder(checkBox_372, checkBox_373);
        QWidget::setTabOrder(checkBox_373, checkBox_374);
        QWidget::setTabOrder(checkBox_374, spinBox_start5_268);
        QWidget::setTabOrder(spinBox_start5_268, checkBox_375);
        QWidget::setTabOrder(checkBox_375, spinBox_start5_269);
        QWidget::setTabOrder(spinBox_start5_269, spinBox_start5_270);
        QWidget::setTabOrder(spinBox_start5_270, textFIeld_name5_234);
        QWidget::setTabOrder(textFIeld_name5_234, spinBox_start5_271);
        QWidget::setTabOrder(spinBox_start5_271, spinBox_start5_272);
        QWidget::setTabOrder(spinBox_start5_272, pushButton_75);
        QWidget::setTabOrder(pushButton_75, pushButton_76);
        QWidget::setTabOrder(pushButton_76, pushButton_77);
        QWidget::setTabOrder(pushButton_77, pushButton_78);
        QWidget::setTabOrder(pushButton_78, scrollArea_19);
        QWidget::setTabOrder(scrollArea_19, textFIeld_name5_235);
        QWidget::setTabOrder(textFIeld_name5_235, spinBox_start2_37);
        QWidget::setTabOrder(spinBox_start2_37, checkBox_376);
        QWidget::setTabOrder(checkBox_376, checkBox_377);
        QWidget::setTabOrder(checkBox_377, checkBox_378);
        QWidget::setTabOrder(checkBox_378, spinBox_start1_19);
        QWidget::setTabOrder(spinBox_start1_19, checkBox_379);
        QWidget::setTabOrder(checkBox_379, textFIeld_name1_52);
        QWidget::setTabOrder(textFIeld_name1_52, spinBox_duration2_37);
        QWidget::setTabOrder(spinBox_duration2_37, spinBox_duration4_37);
        QWidget::setTabOrder(spinBox_duration4_37, textFIeld_name4_37);
        QWidget::setTabOrder(textFIeld_name4_37, spinBox_duration5_235);
        QWidget::setTabOrder(spinBox_duration5_235, checkBox_380);
        QWidget::setTabOrder(checkBox_380, textFIeld_name2_37);
        QWidget::setTabOrder(textFIeld_name2_37, spinBox_duration5_236);
        QWidget::setTabOrder(spinBox_duration5_236, textFIeld_name5_236);
        QWidget::setTabOrder(textFIeld_name5_236, spinBox_start5_273);
        QWidget::setTabOrder(spinBox_start5_273, spinBox_start3_37);
        QWidget::setTabOrder(spinBox_start3_37, spinBox_duration4_38);
        QWidget::setTabOrder(spinBox_duration4_38, checkBox_381);
        QWidget::setTabOrder(checkBox_381, spinBox_start5_274);
        QWidget::setTabOrder(spinBox_start5_274, spinBox_duration5_237);
        QWidget::setTabOrder(spinBox_duration5_237, textFIeld_name5_237);
        QWidget::setTabOrder(textFIeld_name5_237, spinBox_start5_275);
        QWidget::setTabOrder(spinBox_start5_275, textFIeld_name4_38);
        QWidget::setTabOrder(textFIeld_name4_38, spinBox_start3_38);
        QWidget::setTabOrder(spinBox_start3_38, spinBox_start5_276);
        QWidget::setTabOrder(spinBox_start5_276, spinBox_duration5_238);
        QWidget::setTabOrder(spinBox_duration5_238, textFIeld_name5_238);
        QWidget::setTabOrder(textFIeld_name5_238, checkBox_382);
        QWidget::setTabOrder(checkBox_382, spinBox_start5_277);
        QWidget::setTabOrder(spinBox_start5_277, textFIeld_name2_38);
        QWidget::setTabOrder(textFIeld_name2_38, textFIeld_name5_239);
        QWidget::setTabOrder(textFIeld_name5_239, checkBox_383);
        QWidget::setTabOrder(checkBox_383, textFIeld_name3_37);
        QWidget::setTabOrder(textFIeld_name3_37, spinBox_duration5_239);
        QWidget::setTabOrder(spinBox_duration5_239, textFIeld_name3_38);
        QWidget::setTabOrder(textFIeld_name3_38, spinBox_duration5_240);
        QWidget::setTabOrder(spinBox_duration5_240, checkBox_384);
        QWidget::setTabOrder(checkBox_384, textFIeld_name5_240);
        QWidget::setTabOrder(textFIeld_name5_240, textFIeld_name5_241);
        QWidget::setTabOrder(textFIeld_name5_241, spinBox_duration5_241);
        QWidget::setTabOrder(spinBox_duration5_241, textFIeld_name5_242);
        QWidget::setTabOrder(textFIeld_name5_242, spinBox_start2_38);
        QWidget::setTabOrder(spinBox_start2_38, spinBox_duration5_242);
        QWidget::setTabOrder(spinBox_duration5_242, spinBox_duration5_243);
        QWidget::setTabOrder(spinBox_duration5_243, spinBox_duration5_244);
        QWidget::setTabOrder(spinBox_duration5_244, spinBox_start5_278);
        QWidget::setTabOrder(spinBox_start5_278, textFIeld_name5_243);
        QWidget::setTabOrder(textFIeld_name5_243, spinBox_start5_279);
        QWidget::setTabOrder(spinBox_start5_279, checkBox_385);
        QWidget::setTabOrder(checkBox_385, spinBox_duration3_37);
        QWidget::setTabOrder(spinBox_duration3_37, checkBox_386);
        QWidget::setTabOrder(checkBox_386, spinBox_start4_37);
        QWidget::setTabOrder(spinBox_start4_37, textFIeld_name5_244);
        QWidget::setTabOrder(textFIeld_name5_244, spinBox_start5_280);
        QWidget::setTabOrder(spinBox_start5_280, spinBox_duration5_245);
        QWidget::setTabOrder(spinBox_duration5_245, spinBox_duration1_19);
        QWidget::setTabOrder(spinBox_duration1_19, spinBox_duration5_246);
        QWidget::setTabOrder(spinBox_duration5_246, spinBox_start5_281);
        QWidget::setTabOrder(spinBox_start5_281, spinBox_start4_38);
        QWidget::setTabOrder(spinBox_start4_38, spinBox_duration2_38);
        QWidget::setTabOrder(spinBox_duration2_38, checkBox_387);
        QWidget::setTabOrder(checkBox_387, spinBox_duration3_38);
        QWidget::setTabOrder(spinBox_duration3_38, checkBox_388);
        QWidget::setTabOrder(checkBox_388, checkBox_389);
        QWidget::setTabOrder(checkBox_389, checkBox_390);
        QWidget::setTabOrder(checkBox_390, textFIeld_name5_245);
        QWidget::setTabOrder(textFIeld_name5_245, checkBox_391);
        QWidget::setTabOrder(checkBox_391, spinBox_start5_282);
        QWidget::setTabOrder(spinBox_start5_282, textFIeld_name5_246);
        QWidget::setTabOrder(textFIeld_name5_246, spinBox_duration5_247);
        QWidget::setTabOrder(spinBox_duration5_247, checkBox_392);
        QWidget::setTabOrder(checkBox_392, checkBox_393);
        QWidget::setTabOrder(checkBox_393, checkBox_394);
        QWidget::setTabOrder(checkBox_394, spinBox_start5_283);
        QWidget::setTabOrder(spinBox_start5_283, checkBox_395);
        QWidget::setTabOrder(checkBox_395, spinBox_start5_284);
        QWidget::setTabOrder(spinBox_start5_284, spinBox_start5_285);
        QWidget::setTabOrder(spinBox_start5_285, textFIeld_name5_247);
        QWidget::setTabOrder(textFIeld_name5_247, saveButton);
        QWidget::setTabOrder(saveButton, openButton);

        retranslateUi(MainWindowapp2);

        tabWidget_2->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindowapp2);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindowapp2)
    {
        MainWindowapp2->setWindowTitle(QApplication::translate("MainWindowapp2", "MainWindowapp2", nullptr));
        label_ID_19->setText(QApplication::translate("MainWindowapp2", "Node", nullptr));
        label_start_19->setText(QApplication::translate("MainWindowapp2", "Start", nullptr));
        label_msStart_20->setText(QApplication::translate("MainWindowapp2", "(ms)", nullptr));
        label_duration_23->setText(QApplication::translate("MainWindowapp2", "Duration", nullptr));
        label_msDuration_36->setText(QApplication::translate("MainWindowapp2", "(ms)", nullptr));
        label_duration_24->setText(QApplication::translate("MainWindowapp2", "Total Duration", nullptr));
        textFIeld_name1_38->setText(QApplication::translate("MainWindowapp2", "3200", nullptr));
        label_msDuration_37->setText(QApplication::translate("MainWindowapp2", "(ms)", nullptr));
        label_duration_25->setText(QApplication::translate("MainWindowapp2", "Total Progress", nullptr));
        label_63->setText(QApplication::translate("MainWindowapp2", "(1/1)", nullptr));
        checkBox_312->setText(QString());
        textFIeld_name1_48->setText(QApplication::translate("MainWindowapp2", "0", nullptr));
        textFIeld_name1_42->setText(QApplication::translate("MainWindowapp2", "0", nullptr));
        label_ID3_33->setText(QApplication::translate("MainWindowapp2", "3", nullptr));
        label_ID4_230->setText(QApplication::translate("MainWindowapp2", "4", nullptr));
        label_ID2_33->setText(QApplication::translate("MainWindowapp2", "2", nullptr));
        circle_inactive3_116->setText(QString());
        circle_inactive3_119->setText(QString());
        circle_inactive3_117->setText(QString());
        label_ID4_231->setText(QApplication::translate("MainWindowapp2", "5", nullptr));
        label_ID1_18->setText(QApplication::translate("MainWindowapp2", "1", nullptr));
        textFIeld_name1_40->setText(QApplication::translate("MainWindowapp2", "3200", nullptr));
        textFIeld_name1_39->setText(QApplication::translate("MainWindowapp2", "200", nullptr));
        textFIeld_name1_46->setText(QApplication::translate("MainWindowapp2", "0", nullptr));
        checkBox_311->setText(QString());
        circle_inactive3_120->setText(QString());
        textFIeld_name1_43->setText(QApplication::translate("MainWindowapp2", "0", nullptr));
        checkBox_315->setText(QString());
        checkBox_313->setText(QString());
        circle_inactive3_118->setText(QString());
        checkBox_314->setText(QString());
        textFIeld_name1_41->setText(QApplication::translate("MainWindowapp2", "0", nullptr));
        textFIeld_name1_45->setText(QApplication::translate("MainWindowapp2", "0", nullptr));
        textFIeld_name1_44->setText(QApplication::translate("MainWindowapp2", "0", nullptr));
        textFIeld_name1_47->setText(QApplication::translate("MainWindowapp2", "0", nullptr));
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_11), QApplication::translate("MainWindowapp2", "Overview", nullptr));
        label_ID_node1->setText(QApplication::translate("MainWindowapp2", "ID", nullptr));
        label_name_node1->setText(QApplication::translate("MainWindowapp2", "Name    ", nullptr));
        label_start_node1->setText(QApplication::translate("MainWindowapp2", "  Start", nullptr));
        label_msStart_node1->setText(QApplication::translate("MainWindowapp2", "(ms)", nullptr));
        label_duration_node1->setText(QApplication::translate("MainWindowapp2", "Duration", nullptr));
        label_msDuration_node1->setText(QApplication::translate("MainWindowapp2", "(ms)", nullptr));
        checkBox10->setText(QString());
        checkBox22->setText(QString());
        textField_name14->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox18->setText(QString());
        label_4->setText(QApplication::translate("MainWindowapp2", "4", nullptr));
        checkBox12->setText(QString());
        label_19->setText(QApplication::translate("MainWindowapp2", "19", nullptr));
        textField_name5->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox1->setText(QString());
        label_15->setText(QApplication::translate("MainWindowapp2", "15", nullptr));
        textField_name16->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox14->setText(QString());
        circleInactive13->setText(QString());
        checkBox11->setText(QString());
        label_14->setText(QApplication::translate("MainWindowapp2", "14", nullptr));
        checkBox5->setText(QString());
        checkBox4->setText(QString());
        label_16->setText(QApplication::translate("MainWindowapp2", "16", nullptr));
        circleInactive3->setText(QString());
        label_6->setText(QApplication::translate("MainWindowapp2", "6", nullptr));
        circleInactive1->setText(QString());
        label_7->setText(QApplication::translate("MainWindowapp2", "7", nullptr));
        textField_name8->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textField_name2->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textField_name24->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textField_name7->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_18->setText(QApplication::translate("MainWindowapp2", "18", nullptr));
        circleInactive17->setText(QString());
        textField_name18->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox8->setText(QString());
        checkBox21->setText(QString());
        checkBox2->setText(QString());
        label_21->setText(QApplication::translate("MainWindowapp2", "21", nullptr));
        textField_name21->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_5->setText(QApplication::translate("MainWindowapp2", "5", nullptr));
        label_23->setText(QApplication::translate("MainWindowapp2", "23", nullptr));
        checkBox20->setText(QString());
        circleInactive12->setText(QString());
        circleInactive15->setText(QString());
        circleInactive14->setText(QString());
        label_9->setText(QApplication::translate("MainWindowapp2", "9", nullptr));
        checkBox16->setText(QString());
        label_20->setText(QApplication::translate("MainWindowapp2", "20", nullptr));
        textField_name22->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        circleInactive21->setText(QString());
        textField_name13->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_24->setText(QApplication::translate("MainWindowapp2", "24", nullptr));
        checkBox19->setText(QString());
        label_8->setText(QApplication::translate("MainWindowapp2", "8", nullptr));
        circleInactive23->setText(QString());
        circleInactive7->setText(QString());
        textField_name19->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        circleInactive16->setText(QString());
        checkBox17->setText(QString());
        label_11->setText(QApplication::translate("MainWindowapp2", "11", nullptr));
        checkBox3->setText(QString());
        circleInactive22->setText(QString());
        textField_name23->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textField_name20->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        circleInactive10->setText(QString());
        label_10->setText(QApplication::translate("MainWindowapp2", "10", nullptr));
        circleInactive8->setText(QString());
        circleInactive2->setText(QString());
        textField_name12->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textField_name3->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textField_name1->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox15->setText(QString());
        checkBox24->setText(QString());
        textField_name15->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        circleInactive11->setText(QString());
        label_3->setText(QApplication::translate("MainWindowapp2", "3", nullptr));
        checkBox7->setText(QString());
        circleInactive9->setText(QString());
        checkBox13->setText(QString());
        label_12->setText(QApplication::translate("MainWindowapp2", "12", nullptr));
        label_13->setText(QApplication::translate("MainWindowapp2", "13", nullptr));
        label_2->setText(QApplication::translate("MainWindowapp2", "2", nullptr));
        circleInactive6->setText(QString());
        textField_name10->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        circleInactive20->setText(QString());
        circleInactive4->setText(QString());
        circleInactive19->setText(QString());
        circleInactive5->setText(QString());
        checkBox9->setText(QString());
        textField_name17->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox6->setText(QString());
        textField_name9->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textField_name6->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_1->setText(QApplication::translate("MainWindowapp2", "1", nullptr));
        textField_name11->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textField_name4->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        circleInactive18->setText(QString());
        label_17->setText(QApplication::translate("MainWindowapp2", "17", nullptr));
        label_22->setText(QApplication::translate("MainWindowapp2", "22", nullptr));
        checkBox23->setText(QString());
        circleInactive24->setText(QString());
        pushButton_Help->setText(QString());
        groupBox_preview->setTitle(QApplication::translate("MainWindowapp2", "Preview", nullptr));
        label_80->setText(QApplication::translate("MainWindowapp2", "SlowMo/Times", nullptr));
        previewButton->setText(QApplication::translate("MainWindowapp2", "Preview", nullptr));
        groupBox_2->setTitle(QApplication::translate("MainWindowapp2", "Node", nullptr));
        writeConfigButton->setText(QApplication::translate("MainWindowapp2", "WriteConfig", nullptr));
        readConfigButton->setText(QApplication::translate("MainWindowapp2", "ReadConfig", nullptr));
        startButton->setText(QApplication::translate("MainWindowapp2", "Start/Pause", nullptr));
        stopResetButton->setText(QApplication::translate("MainWindowapp2", "Stop/Reset", nullptr));
        openButton->setText(QApplication::translate("MainWindowapp2", " Open", nullptr));
        saveButton->setText(QApplication::translate("MainWindowapp2", " Save", nullptr));
        logoPlaceholder->setText(QApplication::translate("MainWindowapp2", "Logo placeholder", nullptr));
        label_67->setText(QApplication::translate("MainWindowapp2", "Cycle Counter", nullptr));
        label_66->setText(QApplication::translate("MainWindowapp2", "Pause (ms)", nullptr));
        label_81->setText(QApplication::translate("MainWindowapp2", "Cycle progress", nullptr));
        progressBar_node1->setFormat(QApplication::translate("MainWindowapp2", "%v ms", nullptr));
        label_cycleCounter->setText(QApplication::translate("MainWindowapp2", "(3/5)", nullptr));
        label_CycleDuration->setText(QApplication::translate("MainWindowapp2", "Cycle duration: 0 ms", nullptr));
        label_TotalDuration->setText(QApplication::translate("MainWindowapp2", "Total duration: 0 ms", nullptr));
        tabWidget_2->setTabText(tabWidget_2->indexOf(Node_1), QApplication::translate("MainWindowapp2", "1_Node", nullptr));
        label_ID_8->setText(QApplication::translate("MainWindowapp2", "ID", nullptr));
        label_name_9->setText(QApplication::translate("MainWindowapp2", "Name    ", nullptr));
        label_start_8->setText(QApplication::translate("MainWindowapp2", "  Start", nullptr));
        label_msStart_9->setText(QApplication::translate("MainWindowapp2", "(ms)", nullptr));
        label_duration_8->setText(QApplication::translate("MainWindowapp2", "Duration", nullptr));
        label_msDuration_14->setText(QApplication::translate("MainWindowapp2", "(ms)", nullptr));
        label_25->setText(QApplication::translate("MainWindowapp2", "(0/0)", nullptr));
        pushButton_27->setText(QApplication::translate("MainWindowapp2", "Stop/Reset", nullptr));
        pushButton_28->setText(QApplication::translate("MainWindowapp2", "WriteConfig", nullptr));
        label_26->setText(QApplication::translate("MainWindowapp2", "Minimum: 0 ms", nullptr));
        label_27->setText(QApplication::translate("MainWindowapp2", "Cycle Duration", nullptr));
        pushButton_29->setText(QApplication::translate("MainWindowapp2", "Start", nullptr));
        label_msDuration_15->setText(QApplication::translate("MainWindowapp2", "(ms)", nullptr));
        pushButton_30->setText(QApplication::translate("MainWindowapp2", "ReadConfig", nullptr));
        label_28->setText(QApplication::translate("MainWindowapp2", "Cycle Counter", nullptr));
        textFIeld_name5_79->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_91->setText(QApplication::translate("MainWindowapp2", "6", nullptr));
        circle_inactive3_43->setText(QString());
        checkBox_121->setText(QString());
        checkBox_122->setText(QString());
        checkBox_123->setText(QString());
        checkBox_124->setText(QString());
        circle_inactive3_44->setText(QString());
        textFIeld_name1_7->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name4_13->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox_125->setText(QString());
        textFIeld_name2_13->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name5_80->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_92->setText(QApplication::translate("MainWindowapp2", "4", nullptr));
        label_ID4_93->setText(QApplication::translate("MainWindowapp2", "10", nullptr));
        checkBox_126->setText(QString());
        textFIeld_name5_81->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_94->setText(QApplication::translate("MainWindowapp2", "15", nullptr));
        textFIeld_name4_14->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name5_82->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID2_13->setText(QApplication::translate("MainWindowapp2", "2", nullptr));
        checkBox_127->setText(QString());
        textFIeld_name2_14->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name5_83->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox_128->setText(QString());
        label_ID4_95->setText(QApplication::translate("MainWindowapp2", "7", nullptr));
        circle_inactive3_45->setText(QString());
        textFIeld_name3_13->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name3_14->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox_129->setText(QString());
        textFIeld_name5_84->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name5_85->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_96->setText(QApplication::translate("MainWindowapp2", "20", nullptr));
        label_ID4_97->setText(QApplication::translate("MainWindowapp2", "5", nullptr));
        textFIeld_name5_86->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_98->setText(QApplication::translate("MainWindowapp2", "11", nullptr));
        textFIeld_name5_87->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox_130->setText(QString());
        label_ID4_99->setText(QApplication::translate("MainWindowapp2", "13", nullptr));
        label_ID4_100->setText(QApplication::translate("MainWindowapp2", "17", nullptr));
        label_ID2_14->setText(QApplication::translate("MainWindowapp2", "8", nullptr));
        label_ID4_101->setText(QApplication::translate("MainWindowapp2", "16", nullptr));
        checkBox_131->setText(QString());
        circle_inactive3_46->setText(QString());
        label_ID4_102->setText(QApplication::translate("MainWindowapp2", "14", nullptr));
        label_ID4_103->setText(QApplication::translate("MainWindowapp2", "12", nullptr));
        textFIeld_name5_88->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID3_13->setText(QApplication::translate("MainWindowapp2", "3", nullptr));
        circle_inactive1_79->setText(QString());
        checkBox_132->setText(QString());
        checkBox_133->setText(QString());
        checkBox_134->setText(QString());
        checkBox_135->setText(QString());
        textFIeld_name5_89->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox_136->setText(QString());
        label_ID3_14->setText(QApplication::translate("MainWindowapp2", "9", nullptr));
        label_ID1_7->setText(QApplication::translate("MainWindowapp2", "1", nullptr));
        textFIeld_name5_90->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_104->setText(QApplication::translate("MainWindowapp2", "19", nullptr));
        checkBox_137->setText(QString());
        checkBox_138->setText(QString());
        checkBox_139->setText(QString());
        checkBox_140->setText(QString());
        label_ID4_105->setText(QApplication::translate("MainWindowapp2", "18", nullptr));
        circle_inactive3_47->setText(QString());
        circle_inactive1_80->setText(QString());
        textFIeld_name5_91->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        circle_inactive3_48->setText(QString());
        circle_inactive3_49->setText(QString());
        circle_inactive1_81->setText(QString());
        circle_inactive1_82->setText(QString());
        circle_inactive1_83->setText(QString());
        circle_inactive1_84->setText(QString());
        circle_inactive1_85->setText(QString());
        circle_inactive1_86->setText(QString());
        circle_inactive1_87->setText(QString());
        circle_inactive1_88->setText(QString());
        circle_inactive1_89->setText(QString());
        circle_inactive1_90->setText(QString());
        circle_inactive1_91->setText(QString());
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_12), QApplication::translate("MainWindowapp2", "2_Node", nullptr));
        label_ID_21->setText(QApplication::translate("MainWindowapp2", "ID", nullptr));
        label_name_19->setText(QApplication::translate("MainWindowapp2", "Name    ", nullptr));
        label_start_21->setText(QApplication::translate("MainWindowapp2", "  Start", nullptr));
        label_msStart_22->setText(QApplication::translate("MainWindowapp2", "(ms)", nullptr));
        label_duration_27->setText(QApplication::translate("MainWindowapp2", "Duration", nullptr));
        label_msDuration_40->setText(QApplication::translate("MainWindowapp2", "(ms)", nullptr));
        label_68->setText(QApplication::translate("MainWindowapp2", "(0/0)", nullptr));
        label_69->setText(QApplication::translate("MainWindowapp2", "Cycle Counter", nullptr));
        label_70->setText(QApplication::translate("MainWindowapp2", "Cycle Duration", nullptr));
        label_71->setText(QApplication::translate("MainWindowapp2", "Minimum: 0 ms", nullptr));
        pushButton_67->setText(QApplication::translate("MainWindowapp2", "WriteConfig", nullptr));
        pushButton_68->setText(QApplication::translate("MainWindowapp2", "Stop/Reset", nullptr));
        pushButton_69->setText(QApplication::translate("MainWindowapp2", "Start", nullptr));
        pushButton_70->setText(QApplication::translate("MainWindowapp2", "ReadConfig", nullptr));
        label_msDuration_41->setText(QApplication::translate("MainWindowapp2", "(ms)", nullptr));
        textFIeld_name5_209->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_247->setText(QApplication::translate("MainWindowapp2", "6", nullptr));
        circle_inactive3_128->setText(QString());
        checkBox_336->setText(QString());
        checkBox_337->setText(QString());
        checkBox_338->setText(QString());
        checkBox_339->setText(QString());
        circle_inactive3_129->setText(QString());
        textFIeld_name1_50->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name4_33->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox_340->setText(QString());
        textFIeld_name2_33->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name5_210->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_248->setText(QApplication::translate("MainWindowapp2", "4", nullptr));
        label_ID4_249->setText(QApplication::translate("MainWindowapp2", "10", nullptr));
        checkBox_341->setText(QString());
        textFIeld_name5_211->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_250->setText(QApplication::translate("MainWindowapp2", "15", nullptr));
        textFIeld_name4_34->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name5_212->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID2_36->setText(QApplication::translate("MainWindowapp2", "2", nullptr));
        checkBox_342->setText(QString());
        textFIeld_name2_34->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name5_213->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox_343->setText(QString());
        label_ID4_251->setText(QApplication::translate("MainWindowapp2", "7", nullptr));
        circle_inactive3_130->setText(QString());
        textFIeld_name3_33->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name3_34->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox_344->setText(QString());
        textFIeld_name5_214->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name5_215->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_252->setText(QApplication::translate("MainWindowapp2", "20", nullptr));
        label_ID4_253->setText(QApplication::translate("MainWindowapp2", "5", nullptr));
        textFIeld_name5_216->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_254->setText(QApplication::translate("MainWindowapp2", "11", nullptr));
        textFIeld_name5_217->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox_345->setText(QString());
        label_ID4_255->setText(QApplication::translate("MainWindowapp2", "13", nullptr));
        label_ID4_256->setText(QApplication::translate("MainWindowapp2", "17", nullptr));
        label_ID2_37->setText(QApplication::translate("MainWindowapp2", "8", nullptr));
        label_ID4_257->setText(QApplication::translate("MainWindowapp2", "16", nullptr));
        checkBox_346->setText(QString());
        circle_inactive3_131->setText(QString());
        label_ID4_258->setText(QApplication::translate("MainWindowapp2", "14", nullptr));
        label_ID4_259->setText(QApplication::translate("MainWindowapp2", "12", nullptr));
        textFIeld_name5_218->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID3_36->setText(QApplication::translate("MainWindowapp2", "3", nullptr));
        circle_inactive1_209->setText(QString());
        checkBox_347->setText(QString());
        checkBox_348->setText(QString());
        checkBox_349->setText(QString());
        checkBox_350->setText(QString());
        textFIeld_name5_219->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox_351->setText(QString());
        label_ID3_37->setText(QApplication::translate("MainWindowapp2", "9", nullptr));
        label_ID1_20->setText(QApplication::translate("MainWindowapp2", "1", nullptr));
        textFIeld_name5_220->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_260->setText(QApplication::translate("MainWindowapp2", "19", nullptr));
        checkBox_352->setText(QString());
        checkBox_353->setText(QString());
        checkBox_354->setText(QString());
        checkBox_355->setText(QString());
        label_ID4_261->setText(QApplication::translate("MainWindowapp2", "18", nullptr));
        circle_inactive3_132->setText(QString());
        circle_inactive1_210->setText(QString());
        textFIeld_name5_221->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        circle_inactive3_133->setText(QString());
        circle_inactive3_134->setText(QString());
        circle_inactive1_211->setText(QString());
        circle_inactive1_212->setText(QString());
        circle_inactive1_213->setText(QString());
        circle_inactive1_214->setText(QString());
        circle_inactive1_215->setText(QString());
        circle_inactive1_216->setText(QString());
        circle_inactive1_217->setText(QString());
        circle_inactive1_218->setText(QString());
        circle_inactive1_219->setText(QString());
        circle_inactive1_220->setText(QString());
        circle_inactive1_221->setText(QString());
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_13), QApplication::translate("MainWindowapp2", "3_Node", nullptr));
        label_ID_22->setText(QApplication::translate("MainWindowapp2", "ID", nullptr));
        label_name_20->setText(QApplication::translate("MainWindowapp2", "Name    ", nullptr));
        label_start_22->setText(QApplication::translate("MainWindowapp2", "  Start", nullptr));
        label_msStart_23->setText(QApplication::translate("MainWindowapp2", "(ms)", nullptr));
        label_duration_28->setText(QApplication::translate("MainWindowapp2", "Duration", nullptr));
        label_msDuration_42->setText(QApplication::translate("MainWindowapp2", "(ms)", nullptr));
        label_72->setText(QApplication::translate("MainWindowapp2", "(0/0)", nullptr));
        pushButton_71->setText(QApplication::translate("MainWindowapp2", "Stop/Reset", nullptr));
        pushButton_72->setText(QApplication::translate("MainWindowapp2", "WriteConfig", nullptr));
        label_73->setText(QApplication::translate("MainWindowapp2", "Minimum: 0 ms", nullptr));
        label_74->setText(QApplication::translate("MainWindowapp2", "Cycle Duration", nullptr));
        pushButton_73->setText(QApplication::translate("MainWindowapp2", "Start", nullptr));
        label_msDuration_43->setText(QApplication::translate("MainWindowapp2", "(ms)", nullptr));
        pushButton_74->setText(QApplication::translate("MainWindowapp2", "ReadConfig", nullptr));
        label_75->setText(QApplication::translate("MainWindowapp2", "Cycle Counter", nullptr));
        textFIeld_name5_222->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_262->setText(QApplication::translate("MainWindowapp2", "6", nullptr));
        circle_inactive3_135->setText(QString());
        checkBox_356->setText(QString());
        checkBox_357->setText(QString());
        checkBox_358->setText(QString());
        checkBox_359->setText(QString());
        circle_inactive3_136->setText(QString());
        textFIeld_name1_51->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name4_35->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox_360->setText(QString());
        textFIeld_name2_35->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name5_223->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_263->setText(QApplication::translate("MainWindowapp2", "4", nullptr));
        label_ID4_264->setText(QApplication::translate("MainWindowapp2", "10", nullptr));
        checkBox_361->setText(QString());
        textFIeld_name5_224->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_265->setText(QApplication::translate("MainWindowapp2", "15", nullptr));
        textFIeld_name4_36->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name5_225->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID2_38->setText(QApplication::translate("MainWindowapp2", "2", nullptr));
        checkBox_362->setText(QString());
        textFIeld_name2_36->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name5_226->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox_363->setText(QString());
        label_ID4_266->setText(QApplication::translate("MainWindowapp2", "7", nullptr));
        circle_inactive3_137->setText(QString());
        textFIeld_name3_35->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name3_36->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox_364->setText(QString());
        textFIeld_name5_227->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name5_228->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_267->setText(QApplication::translate("MainWindowapp2", "20", nullptr));
        label_ID4_268->setText(QApplication::translate("MainWindowapp2", "5", nullptr));
        textFIeld_name5_229->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_269->setText(QApplication::translate("MainWindowapp2", "11", nullptr));
        textFIeld_name5_230->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox_365->setText(QString());
        label_ID4_270->setText(QApplication::translate("MainWindowapp2", "13", nullptr));
        label_ID4_271->setText(QApplication::translate("MainWindowapp2", "17", nullptr));
        label_ID2_39->setText(QApplication::translate("MainWindowapp2", "8", nullptr));
        label_ID4_272->setText(QApplication::translate("MainWindowapp2", "16", nullptr));
        checkBox_366->setText(QString());
        circle_inactive3_138->setText(QString());
        label_ID4_273->setText(QApplication::translate("MainWindowapp2", "14", nullptr));
        label_ID4_274->setText(QApplication::translate("MainWindowapp2", "12", nullptr));
        textFIeld_name5_231->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID3_38->setText(QApplication::translate("MainWindowapp2", "3", nullptr));
        circle_inactive1_222->setText(QString());
        checkBox_367->setText(QString());
        checkBox_368->setText(QString());
        checkBox_369->setText(QString());
        checkBox_370->setText(QString());
        textFIeld_name5_232->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox_371->setText(QString());
        label_ID3_39->setText(QApplication::translate("MainWindowapp2", "9", nullptr));
        label_ID1_21->setText(QApplication::translate("MainWindowapp2", "1", nullptr));
        textFIeld_name5_233->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_275->setText(QApplication::translate("MainWindowapp2", "19", nullptr));
        checkBox_372->setText(QString());
        checkBox_373->setText(QString());
        checkBox_374->setText(QString());
        checkBox_375->setText(QString());
        label_ID4_276->setText(QApplication::translate("MainWindowapp2", "18", nullptr));
        circle_inactive3_139->setText(QString());
        circle_inactive1_223->setText(QString());
        textFIeld_name5_234->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        circle_inactive3_140->setText(QString());
        circle_inactive3_141->setText(QString());
        circle_inactive1_224->setText(QString());
        circle_inactive1_225->setText(QString());
        circle_inactive1_226->setText(QString());
        circle_inactive1_227->setText(QString());
        circle_inactive1_228->setText(QString());
        circle_inactive1_229->setText(QString());
        circle_inactive1_230->setText(QString());
        circle_inactive1_231->setText(QString());
        circle_inactive1_232->setText(QString());
        circle_inactive1_233->setText(QString());
        circle_inactive1_234->setText(QString());
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_14), QApplication::translate("MainWindowapp2", "4_Node", nullptr));
        label_ID_23->setText(QApplication::translate("MainWindowapp2", "ID", nullptr));
        label_name_21->setText(QApplication::translate("MainWindowapp2", "Name    ", nullptr));
        label_start_23->setText(QApplication::translate("MainWindowapp2", "  Start", nullptr));
        label_msStart_24->setText(QApplication::translate("MainWindowapp2", "(ms)", nullptr));
        label_duration_29->setText(QApplication::translate("MainWindowapp2", "Duration", nullptr));
        label_msDuration_44->setText(QApplication::translate("MainWindowapp2", "(ms)", nullptr));
        label_76->setText(QApplication::translate("MainWindowapp2", "(0/0)", nullptr));
        pushButton_75->setText(QApplication::translate("MainWindowapp2", "Stop/Reset", nullptr));
        pushButton_76->setText(QApplication::translate("MainWindowapp2", "WriteConfig", nullptr));
        label_77->setText(QApplication::translate("MainWindowapp2", "Minimum: 0 ms", nullptr));
        label_78->setText(QApplication::translate("MainWindowapp2", "Cycle Duration", nullptr));
        pushButton_77->setText(QApplication::translate("MainWindowapp2", "Start", nullptr));
        label_msDuration_45->setText(QApplication::translate("MainWindowapp2", "(ms)", nullptr));
        pushButton_78->setText(QApplication::translate("MainWindowapp2", "ReadConfig", nullptr));
        label_79->setText(QApplication::translate("MainWindowapp2", "Cycle Counter", nullptr));
        textFIeld_name5_235->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_277->setText(QApplication::translate("MainWindowapp2", "6", nullptr));
        circle_inactive3_142->setText(QString());
        checkBox_376->setText(QString());
        checkBox_377->setText(QString());
        checkBox_378->setText(QString());
        checkBox_379->setText(QString());
        circle_inactive3_143->setText(QString());
        textFIeld_name1_52->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name4_37->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox_380->setText(QString());
        textFIeld_name2_37->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name5_236->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_278->setText(QApplication::translate("MainWindowapp2", "4", nullptr));
        label_ID4_279->setText(QApplication::translate("MainWindowapp2", "10", nullptr));
        checkBox_381->setText(QString());
        textFIeld_name5_237->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_280->setText(QApplication::translate("MainWindowapp2", "15", nullptr));
        textFIeld_name4_38->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name5_238->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID2_40->setText(QApplication::translate("MainWindowapp2", "2", nullptr));
        checkBox_382->setText(QString());
        textFIeld_name2_38->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name5_239->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox_383->setText(QString());
        label_ID4_281->setText(QApplication::translate("MainWindowapp2", "7", nullptr));
        circle_inactive3_144->setText(QString());
        textFIeld_name3_37->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name3_38->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox_384->setText(QString());
        textFIeld_name5_240->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        textFIeld_name5_241->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_282->setText(QApplication::translate("MainWindowapp2", "20", nullptr));
        label_ID4_283->setText(QApplication::translate("MainWindowapp2", "5", nullptr));
        textFIeld_name5_242->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_284->setText(QApplication::translate("MainWindowapp2", "11", nullptr));
        textFIeld_name5_243->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox_385->setText(QString());
        label_ID4_285->setText(QApplication::translate("MainWindowapp2", "13", nullptr));
        label_ID4_286->setText(QApplication::translate("MainWindowapp2", "17", nullptr));
        label_ID2_41->setText(QApplication::translate("MainWindowapp2", "8", nullptr));
        label_ID4_287->setText(QApplication::translate("MainWindowapp2", "16", nullptr));
        checkBox_386->setText(QString());
        circle_inactive3_145->setText(QString());
        label_ID4_288->setText(QApplication::translate("MainWindowapp2", "14", nullptr));
        label_ID4_289->setText(QApplication::translate("MainWindowapp2", "12", nullptr));
        textFIeld_name5_244->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID3_40->setText(QApplication::translate("MainWindowapp2", "3", nullptr));
        circle_inactive1_235->setText(QString());
        checkBox_387->setText(QString());
        checkBox_388->setText(QString());
        checkBox_389->setText(QString());
        checkBox_390->setText(QString());
        textFIeld_name5_245->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        checkBox_391->setText(QString());
        label_ID3_41->setText(QApplication::translate("MainWindowapp2", "9", nullptr));
        label_ID1_22->setText(QApplication::translate("MainWindowapp2", "1", nullptr));
        textFIeld_name5_246->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        label_ID4_290->setText(QApplication::translate("MainWindowapp2", "19", nullptr));
        checkBox_392->setText(QString());
        checkBox_393->setText(QString());
        checkBox_394->setText(QString());
        checkBox_395->setText(QString());
        label_ID4_291->setText(QApplication::translate("MainWindowapp2", "18", nullptr));
        circle_inactive3_146->setText(QString());
        circle_inactive1_236->setText(QString());
        textFIeld_name5_247->setText(QApplication::translate("MainWindowapp2", "Relay", nullptr));
        circle_inactive3_147->setText(QString());
        circle_inactive3_148->setText(QString());
        circle_inactive1_237->setText(QString());
        circle_inactive1_238->setText(QString());
        circle_inactive1_239->setText(QString());
        circle_inactive1_240->setText(QString());
        circle_inactive1_241->setText(QString());
        circle_inactive1_242->setText(QString());
        circle_inactive1_243->setText(QString());
        circle_inactive1_244->setText(QString());
        circle_inactive1_245->setText(QString());
        circle_inactive1_246->setText(QString());
        circle_inactive1_247->setText(QString());
        tabWidget_2->setTabText(tabWidget_2->indexOf(tab_15), QApplication::translate("MainWindowapp2", "5_Node", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindowapp2: public Ui_MainWindowapp2 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOWAPP2_H
