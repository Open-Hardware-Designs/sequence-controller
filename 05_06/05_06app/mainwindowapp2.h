#ifndef MAINWINDOWAPP2_H
#define MAINWINDOWAPP2_H

#include <QMainWindow>

namespace Ui {
class MainWindowapp2;
}

class MainWindowapp2 : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindowapp2(QWidget *parent = 0);
    ~MainWindowapp2();

private:
    Ui::MainWindowapp2 *ui;
};

#endif // MAINWINDOWAPP2_H
