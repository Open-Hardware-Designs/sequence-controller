from PyQt5 import QtCore, QtGui, QtWidgets
# from PyQt5.QtGui import QPixmap
# from PyQt5.QtWidgets import QLabel

import time
#import csv
import json

import struct
import serial
import serial.tools.list_ports

# import os

class PreviewWorker(QtCore.QThread):

	#add some signals
	circle_1_cleared = QtCore.pyqtSignal()
	circle_2_cleared = QtCore.pyqtSignal()
	circle_3_cleared = QtCore.pyqtSignal()
	circle_4_cleared = QtCore.pyqtSignal()
	circle_5_cleared = QtCore.pyqtSignal()
	circle_6_cleared = QtCore.pyqtSignal()
	circle_7_cleared = QtCore.pyqtSignal()
	circle_8_cleared = QtCore.pyqtSignal()
	circle_9_cleared = QtCore.pyqtSignal()
	circle_10_cleared = QtCore.pyqtSignal()
	circle_11_cleared = QtCore.pyqtSignal()
	circle_12_cleared = QtCore.pyqtSignal()
	circle_13_cleared = QtCore.pyqtSignal()
	circle_14_cleared = QtCore.pyqtSignal()
	circle_15_cleared = QtCore.pyqtSignal()
	circle_16_cleared = QtCore.pyqtSignal()
	circle_17_cleared = QtCore.pyqtSignal()
	circle_18_cleared = QtCore.pyqtSignal()
	circle_19_cleared = QtCore.pyqtSignal()
	circle_20_cleared = QtCore.pyqtSignal()
	circle_21_cleared = QtCore.pyqtSignal()
	circle_22_cleared = QtCore.pyqtSignal()
	circle_23_cleared = QtCore.pyqtSignal()
	circle_24_cleared = QtCore.pyqtSignal()

	circle_1_latched = QtCore.pyqtSignal()
	circle_2_latched = QtCore.pyqtSignal()
	circle_3_latched = QtCore.pyqtSignal()
	circle_4_latched = QtCore.pyqtSignal()
	circle_5_latched = QtCore.pyqtSignal()
	circle_6_latched = QtCore.pyqtSignal()
	circle_7_latched = QtCore.pyqtSignal()
	circle_8_latched = QtCore.pyqtSignal()
	circle_9_latched = QtCore.pyqtSignal()
	circle_10_latched = QtCore.pyqtSignal()
	circle_11_latched = QtCore.pyqtSignal()
	circle_12_latched = QtCore.pyqtSignal()
	circle_13_latched = QtCore.pyqtSignal()
	circle_14_latched = QtCore.pyqtSignal()
	circle_15_latched = QtCore.pyqtSignal()
	circle_16_latched = QtCore.pyqtSignal()
	circle_17_latched = QtCore.pyqtSignal()
	circle_18_latched = QtCore.pyqtSignal()
	circle_19_latched = QtCore.pyqtSignal()
	circle_20_latched = QtCore.pyqtSignal()
	circle_21_latched = QtCore.pyqtSignal()
	circle_22_latched = QtCore.pyqtSignal()
	circle_23_latched = QtCore.pyqtSignal()
	circle_24_latched = QtCore.pyqtSignal()

	circle_1_finished = QtCore.pyqtSignal()
	circle_2_finished = QtCore.pyqtSignal()
	circle_3_finished = QtCore.pyqtSignal()
	circle_4_finished = QtCore.pyqtSignal()
	circle_5_finished = QtCore.pyqtSignal()
	circle_6_finished = QtCore.pyqtSignal()
	circle_7_finished = QtCore.pyqtSignal()
	circle_8_finished = QtCore.pyqtSignal()
	circle_9_finished = QtCore.pyqtSignal()
	circle_10_finished = QtCore.pyqtSignal()
	circle_11_finished = QtCore.pyqtSignal()
	circle_12_finished = QtCore.pyqtSignal()
	circle_13_finished = QtCore.pyqtSignal()
	circle_14_finished = QtCore.pyqtSignal()
	circle_15_finished = QtCore.pyqtSignal()
	circle_16_finished = QtCore.pyqtSignal()
	circle_17_finished = QtCore.pyqtSignal()
	circle_18_finished = QtCore.pyqtSignal()
	circle_19_finished = QtCore.pyqtSignal()
	circle_20_finished = QtCore.pyqtSignal()
	circle_21_finished = QtCore.pyqtSignal()
	circle_22_finished = QtCore.pyqtSignal()
	circle_23_finished = QtCore.pyqtSignal()
	circle_24_finished = QtCore.pyqtSignal()

	cycleCounter_current = QtCore.pyqtSignal(int)

	progressBar_current = QtCore.pyqtSignal(int)
	progressBar_maximum = QtCore.pyqtSignal(int)
	progressBar_reset = QtCore.pyqtSignal()
	disable_action_buttons_sig = QtCore.pyqtSignal()
	enable_action_buttons_sig = QtCore.pyqtSignal()


	def __init__(self, great_dictionary):
		QtCore.QThread.__init__(self)
		self.great_dictionary = great_dictionary

		self.max_duration = 0
		self.max_durations_list = []
		self.start_list = []
		self.duration_list = []
		self.checkbox_list = []

		self.checkbox_list.append(self.great_dictionary["Relay_1"]["enabled"])
		self.checkbox_list.append(self.great_dictionary["Relay_2"]["enabled"])
		self.checkbox_list.append(self.great_dictionary["Relay_3"]["enabled"])
		self.checkbox_list.append(self.great_dictionary["Relay_4"]["enabled"])
		self.checkbox_list.append(self.great_dictionary["Relay_5"]["enabled"])
		self.checkbox_list.append(self.great_dictionary["Relay_6"]["enabled"])
		self.checkbox_list.append(self.great_dictionary["Relay_7"]["enabled"])
		self.checkbox_list.append(self.great_dictionary["Relay_8"]["enabled"])
		self.checkbox_list.append(self.great_dictionary["Relay_9"]["enabled"])
		self.checkbox_list.append(self.great_dictionary["Relay_10"]["enabled"])
		self.checkbox_list.append(self.great_dictionary["Relay_11"]["enabled"])
		self.checkbox_list.append(self.great_dictionary["Relay_12"]["enabled"])
		self.checkbox_list.append(self.great_dictionary["Relay_13"]["enabled"])
		self.checkbox_list.append(self.great_dictionary["Relay_14"]["enabled"])
		self.checkbox_list.append(self.great_dictionary["Relay_15"]["enabled"])
		self.checkbox_list.append(self.great_dictionary["Relay_16"]["enabled"])
		self.checkbox_list.append(self.great_dictionary["Relay_17"]["enabled"])
		self.checkbox_list.append(self.great_dictionary["Relay_18"]["enabled"])
		self.checkbox_list.append(self.great_dictionary["Relay_19"]["enabled"])
		self.checkbox_list.append(self.great_dictionary["Relay_20"]["enabled"])
		self.checkbox_list.append(self.great_dictionary["Relay_21"]["enabled"])
		self.checkbox_list.append(self.great_dictionary["Relay_22"]["enabled"])
		self.checkbox_list.append(self.great_dictionary["Relay_23"]["enabled"])
		self.checkbox_list.append(self.great_dictionary["Relay_24"]["enabled"])

		self.duration_list.append(self.great_dictionary["Relay_1"]["duration"])
		self.duration_list.append(self.great_dictionary["Relay_2"]["duration"])
		self.duration_list.append(self.great_dictionary["Relay_3"]["duration"])
		self.duration_list.append(self.great_dictionary["Relay_4"]["duration"])
		self.duration_list.append(self.great_dictionary["Relay_5"]["duration"])
		self.duration_list.append(self.great_dictionary["Relay_6"]["duration"])
		self.duration_list.append(self.great_dictionary["Relay_7"]["duration"])
		self.duration_list.append(self.great_dictionary["Relay_8"]["duration"])
		self.duration_list.append(self.great_dictionary["Relay_9"]["duration"])
		self.duration_list.append(self.great_dictionary["Relay_10"]["duration"])
		self.duration_list.append(self.great_dictionary["Relay_11"]["duration"])
		self.duration_list.append(self.great_dictionary["Relay_12"]["duration"])
		self.duration_list.append(self.great_dictionary["Relay_13"]["duration"])
		self.duration_list.append(self.great_dictionary["Relay_14"]["duration"])
		self.duration_list.append(self.great_dictionary["Relay_15"]["duration"])
		self.duration_list.append(self.great_dictionary["Relay_16"]["duration"])
		self.duration_list.append(self.great_dictionary["Relay_17"]["duration"])
		self.duration_list.append(self.great_dictionary["Relay_18"]["duration"])
		self.duration_list.append(self.great_dictionary["Relay_19"]["duration"])
		self.duration_list.append(self.great_dictionary["Relay_20"]["duration"])
		self.duration_list.append(self.great_dictionary["Relay_21"]["duration"])
		self.duration_list.append(self.great_dictionary["Relay_22"]["duration"])
		self.duration_list.append(self.great_dictionary["Relay_23"]["duration"])
		self.duration_list.append(self.great_dictionary["Relay_24"]["duration"])

		self.start_list.append(self.great_dictionary["Relay_1"]["start"])
		self.start_list.append(self.great_dictionary["Relay_2"]["start"])
		self.start_list.append(self.great_dictionary["Relay_3"]["start"])
		self.start_list.append(self.great_dictionary["Relay_4"]["start"])
		self.start_list.append(self.great_dictionary["Relay_5"]["start"])
		self.start_list.append(self.great_dictionary["Relay_6"]["start"])
		self.start_list.append(self.great_dictionary["Relay_7"]["start"])
		self.start_list.append(self.great_dictionary["Relay_8"]["start"])
		self.start_list.append(self.great_dictionary["Relay_9"]["start"])
		self.start_list.append(self.great_dictionary["Relay_10"]["start"])
		self.start_list.append(self.great_dictionary["Relay_11"]["start"])
		self.start_list.append(self.great_dictionary["Relay_12"]["start"])
		self.start_list.append(self.great_dictionary["Relay_13"]["start"])
		self.start_list.append(self.great_dictionary["Relay_14"]["start"])
		self.start_list.append(self.great_dictionary["Relay_15"]["start"])
		self.start_list.append(self.great_dictionary["Relay_16"]["start"])
		self.start_list.append(self.great_dictionary["Relay_17"]["start"])
		self.start_list.append(self.great_dictionary["Relay_18"]["start"])
		self.start_list.append(self.great_dictionary["Relay_19"]["start"])
		self.start_list.append(self.great_dictionary["Relay_20"]["start"])
		self.start_list.append(self.great_dictionary["Relay_21"]["start"])
		self.start_list.append(self.great_dictionary["Relay_22"]["start"])
		self.start_list.append(self.great_dictionary["Relay_23"]["start"])
		self.start_list.append(self.great_dictionary["Relay_24"]["start"])

		self.max_durations_list.append(self.great_dictionary["Relay_1"]["duration"] + self.great_dictionary["Relay_1"]["start"])
		self.max_durations_list.append(self.great_dictionary["Relay_2"]["duration"] + self.great_dictionary["Relay_2"]["start"])
		self.max_durations_list.append(self.great_dictionary["Relay_3"]["duration"] + self.great_dictionary["Relay_3"]["start"])
		self.max_durations_list.append(self.great_dictionary["Relay_4"]["duration"] + self.great_dictionary["Relay_4"]["start"])
		self.max_durations_list.append(self.great_dictionary["Relay_5"]["duration"] + self.great_dictionary["Relay_5"]["start"])
		self.max_durations_list.append(self.great_dictionary["Relay_6"]["duration"] + self.great_dictionary["Relay_6"]["start"])
		self.max_durations_list.append(self.great_dictionary["Relay_7"]["duration"] + self.great_dictionary["Relay_7"]["start"])
		self.max_durations_list.append(self.great_dictionary["Relay_8"]["duration"] + self.great_dictionary["Relay_8"]["start"])
		self.max_durations_list.append(self.great_dictionary["Relay_9"]["duration"] + self.great_dictionary["Relay_9"]["start"])
		self.max_durations_list.append(self.great_dictionary["Relay_10"]["duration"] + self.great_dictionary["Relay_10"]["start"])
		self.max_durations_list.append(self.great_dictionary["Relay_11"]["duration"] + self.great_dictionary["Relay_11"]["start"])
		self.max_durations_list.append(self.great_dictionary["Relay_12"]["duration"] + self.great_dictionary["Relay_12"]["start"])
		self.max_durations_list.append(self.great_dictionary["Relay_13"]["duration"] + self.great_dictionary["Relay_13"]["start"])
		self.max_durations_list.append(self.great_dictionary["Relay_14"]["duration"] + self.great_dictionary["Relay_14"]["start"])
		self.max_durations_list.append(self.great_dictionary["Relay_15"]["duration"] + self.great_dictionary["Relay_15"]["start"])
		self.max_durations_list.append(self.great_dictionary["Relay_16"]["duration"] + self.great_dictionary["Relay_16"]["start"])
		self.max_durations_list.append(self.great_dictionary["Relay_17"]["duration"] + self.great_dictionary["Relay_17"]["start"])
		self.max_durations_list.append(self.great_dictionary["Relay_18"]["duration"] + self.great_dictionary["Relay_18"]["start"])
		self.max_durations_list.append(self.great_dictionary["Relay_19"]["duration"] + self.great_dictionary["Relay_19"]["start"])
		self.max_durations_list.append(self.great_dictionary["Relay_20"]["duration"] + self.great_dictionary["Relay_20"]["start"])
		self.max_durations_list.append(self.great_dictionary["Relay_21"]["duration"] + self.great_dictionary["Relay_21"]["start"])
		self.max_durations_list.append(self.great_dictionary["Relay_22"]["duration"] + self.great_dictionary["Relay_22"]["start"])
		self.max_durations_list.append(self.great_dictionary["Relay_23"]["duration"] + self.great_dictionary["Relay_23"]["start"])
		self.max_durations_list.append(self.great_dictionary["Relay_24"]["duration"] + self.great_dictionary["Relay_24"]["start"])


	def stop(self):
		self._isRunning = False

	def run(self):
		# add self running app here
		if 1 in self.checkbox_list:
			self._isRunning = True
			self.disable_action_buttons_sig.emit()
			self.max_duration_list_enabled = []
			#while self._isRunning is True:
			for x in range(self.great_dictionary["cycle_counter"]):
				if self._isRunning is False:
					break
				self.cycleCounter_current.emit(x+1)

				for i in range(0,24):
					if self.checkbox_list[i] == True:
						exec("self.circle_{}_cleared.emit()".format(i+1)) #Clear circle icon to prepare for preview
						self.max_duration_list_enabled.append(self.max_durations_list[i]) #Add only enabled relays for the max duration counting

				self.max_duration = max(self.max_duration_list_enabled)
				print("max duration: {}".format(self.max_duration))
				if self.max_duration == 0:
					break

				start_time = int(round(time.time() * 1000)) #Get the current time for the reference
				self.progressBar_reset.emit()
				self.progressBar_maximum.emit((self.max_duration + self.great_dictionary["pause"]) * self.great_dictionary["slow_motion"])
				# Add progress Bar Reset
				# Add progress Bar Maximum value set

				while True and self._isRunning:
					now = int(round(time.time() * 1000))
					self.progressBar_current.emit(now - start_time)
					for i in range(0,24):
						if self.checkbox_list[i] == True:
							if now >= start_time + (self.start_list[i] * self.great_dictionary["slow_motion"]) and now < start_time + (self.start_list[i] + self.duration_list[i]) * self.great_dictionary["slow_motion"]:
								exec("self.circle_{}_latched.emit()".format(i+1))
								#print("latched {}".format(i+1))
							if now >= start_time + (self.start_list[i] + self.duration_list[i]) * self.great_dictionary["slow_motion"]:
								#print("finished {}".format(i+1))
								exec("self.circle_{}_finished.emit()".format(i+1))


					if now >= start_time + ((self.max_duration + self.great_dictionary["pause"]) * self.great_dictionary["slow_motion"]):
						print("Break reached")
						self.progressBar_current.emit((self.great_dictionary["pause"] + self.max_duration) * self.great_dictionary["slow_motion"])

						# add enable preview button
						break
					time.sleep(0.001)
					QtWidgets.QApplication.processEvents()
		self.enable_action_buttons_sig.emit()
