

from PyQt5 import QtCore, QtGui, QtWidgets


import time
#import csv
import json

import struct
import serial
import serial.tools.list_ports

import os


class SerialWorker(QtCore.QThread):
	# add some signals here
	serial_port_connected_sig = QtCore.pyqtSignal(str)
	serial_port_disconnected_sig = QtCore.pyqtSignal()

	message_delivered = QtCore.pyqtSignal()

	read_node_configuration = QtCore.pyqtSignal(dict)
	#write_node_configuration = QtCore.pyqtSignal(dict)

	uart_port_name = "FT232R USB UART"
	xbee_port_name = "FT231X USB UART"
	port_path = False
	device_name = None
	baud_rate = 9600
	ser = None

	device_name_old = "NULL"





	def __init__(self):
		QtCore.QThread.__init__(self)

	def stop(self):
		self._isRunning = False

	def update_port_path(self):
		self.ports = list(serial.tools.list_ports.comports())
		self.port_path = self.list_serial_ports()

	def isDeviceNameChanged(self):
		if self.port_path is not False:
			if self.device_name is not self.device_name_old:
				self.reset_serial_connection()





	# Returns False if no port found, returns Xbee or Serial if found. Xbee has priority
	def list_serial_ports(self):
		port_p = False
		for p in self.ports:
			if self.xbee_port_name in p[1]:
				self.device_name = "XBee"
				return p[0]
			elif self.uart_port_name in p[1]:
				port_p = p[0]
				self.device_name = "Serial"
		return port_p

	def relay_control_start_manual(self):
		pass

	def relay_control_start(self):
		#self.relay_control_stop()
		start_str = "7E00020101" # Removed AA for the checksum
		start_bytes = bytes.fromhex(start_str)
		self.serial_write(start_bytes)
		print("Start")

	def relay_control_stop(self):
		stop_str = "7E00020102"
		stop_bytes = bytes.fromhex(stop_str)
		self.serial_write(stop_bytes)
		print("Stop")

	def serial_write(self, message):
		if self.ser != None:
			if self.ser.isOpen():
				self.ser.write(message)

	def serial_connect(self):
		try:
			self.ser = serial.Serial(self.port_path, self.baud_rate)
			print("Serial Open with {} - {}".format(self.device_name, self.port_path))
			self.serial_port_connected_sig.emit(self.device_name)
			self.device_name_old = self.device_name
		except OSError as e:
			print("Serial Connect - OSError - {}".format(e))

	def serial_disconnect(self):
		try:
			if self.ser.isOpen():
				self.ser.close()
				self.ser = None
		except AttributeError as e:
			print("Serial Disconnect - AttributeError - {}".format(e))


	def reset_serial_connection(self):
		print("Serial Reset")
		if self.ser == None:
			self.serial_connect()
		else:
			self.serial_disconnect()
			self.serial_connect()

	def relay_write_config_2(self, great_dictionary):
		self.great_dictionary = great_dictionary
		self.relay_control_stop()
		START_BYTE = b'\x7E'
		CHECKSUM = b'\xAA'
		RELAY_CONFIG = b'\x02'
		packet_length = "00FB" ## Update packet length
		packet_length_b = bytes.fromhex(packet_length)
		RELAY_1 = b'\x01'
		RELAY_2 = b'\x02'
		RELAY_3 = b'\x03'
		RELAY_4 = b'\x04'
		RELAY_5 = b'\x05'
		RELAY_6 = b'\x06'
		RELAY_7 = b'\x07'
		RELAY_8 = b'\x08'
		RELAY_9 = b'\x09'
		RELAY_10 = b'\x0A'
		RELAY_11 = b'\x0B'
		RELAY_12 = b'\x0C'
		RELAY_13 = b'\x0D'
		RELAY_14 = b'\x0E'
		RELAY_15 = b'\x0F'
		RELAY_16 = b'\x10'
		RELAY_17 = b'\x11'
		RELAY_18 = b'\x12'
		RELAY_19 = b'\x13'
		RELAY_20 = b'\x14'
		RELAY_21 = b'\x15'
		RELAY_22 = b'\x16'
		RELAY_23 = b'\x17'
		RELAY_24 = b'\x18'

		SEQUENCE_COUNTER = b'\x00'
		DURATION_ID = b'\xFC'

		checkBox1_b = struct.pack("?",int(self.checkBox1.isChecked()))
		checkBox2_b = struct.pack("?",int(self.checkBox2.isChecked()))
		checkBox3_b = struct.pack("?",int(self.checkBox3.isChecked()))
		checkBox4_b = struct.pack("?",int(self.checkBox4.isChecked()))
		checkBox5_b = struct.pack("?",int(self.checkBox5.isChecked()))
		checkBox6_b = struct.pack("?",int(self.checkBox6.isChecked()))
		checkBox7_b = struct.pack("?",int(self.checkBox7.isChecked()))
		checkBox8_b = struct.pack("?",int(self.checkBox8.isChecked()))
		checkBox9_b = struct.pack("?",int(self.checkBox9.isChecked()))
		checkBox10_b = struct.pack("?",int(self.checkBox10.isChecked()))
		checkBox11_b = struct.pack("?",int(self.checkBox11.isChecked()))
		checkBox12_b = struct.pack("?",int(self.checkBox12.isChecked()))
		checkBox13_b = struct.pack("?",int(self.checkBox13.isChecked()))
		checkBox14_b = struct.pack("?",int(self.checkBox14.isChecked()))
		checkBox15_b = struct.pack("?",int(self.checkBox15.isChecked()))
		checkBox16_b = struct.pack("?",int(self.checkBox16.isChecked()))
		checkBox17_b = struct.pack("?",int(self.checkBox17.isChecked()))
		checkBox18_b = struct.pack("?",int(self.checkBox18.isChecked()))
		checkBox19_b = struct.pack("?",int(self.checkBox19.isChecked()))
		checkBox20_b = struct.pack("?",int(self.checkBox20.isChecked()))
		checkBox21_b = struct.pack("?",int(self.checkBox21.isChecked()))
		checkBox22_b = struct.pack("?",int(self.checkBox22.isChecked()))
		checkBox23_b = struct.pack("?",int(self.checkBox23.isChecked()))
		checkBox24_b = struct.pack("?",int(self.checkBox24.isChecked()))

		spinBox_start1_b = struct.pack(">L",int(self.spinBox_start1.value()))
		spinBox_start2_b = struct.pack(">L",int(self.spinBox_start2.value()))
		spinBox_start3_b = struct.pack(">L",int(self.spinBox_start3.value()))
		spinBox_start4_b = struct.pack(">L",int(self.spinBox_start4.value()))
		spinBox_start5_b = struct.pack(">L",int(self.spinBox_start5.value()))
		spinBox_start6_b = struct.pack(">L",int(self.spinBox_start6.value()))
		spinBox_start7_b = struct.pack(">L",int(self.spinBox_start7.value()))
		spinBox_start8_b = struct.pack(">L",int(self.spinBox_start8.value()))
		spinBox_start9_b = struct.pack(">L",int(self.spinBox_start9.value()))
		spinBox_start10_b = struct.pack(">L",int(self.spinBox_start10.value()))
		spinBox_start11_b = struct.pack(">L",int(self.spinBox_start11.value()))
		spinBox_start12_b = struct.pack(">L",int(self.spinBox_start12.value()))
		spinBox_start13_b = struct.pack(">L",int(self.spinBox_start13.value()))
		spinBox_start14_b = struct.pack(">L",int(self.spinBox_start14.value()))
		spinBox_start15_b = struct.pack(">L",int(self.spinBox_start15.value()))
		spinBox_start16_b = struct.pack(">L",int(self.spinBox_start16.value()))
		spinBox_start17_b = struct.pack(">L",int(self.spinBox_start17.value()))
		spinBox_start18_b = struct.pack(">L",int(self.spinBox_start18.value()))
		spinBox_start19_b = struct.pack(">L",int(self.spinBox_start19.value()))
		spinBox_start20_b = struct.pack(">L",int(self.spinBox_start20.value()))
		spinBox_start21_b = struct.pack(">L",int(self.spinBox_start21.value()))
		spinBox_start22_b = struct.pack(">L",int(self.spinBox_start22.value()))
		spinBox_start23_b = struct.pack(">L",int(self.spinBox_start23.value()))
		spinBox_start24_b = struct.pack(">L",int(self.spinBox_start24.value()))

		spinBox_duration1_b = struct.pack(">L",int(self.spinBox_duration1.value()))
		spinBox_duration2_b = struct.pack(">L",int(self.spinBox_duration2.value()))
		spinBox_duration3_b = struct.pack(">L",int(self.spinBox_duration3.value()))
		spinBox_duration4_b = struct.pack(">L",int(self.spinBox_duration4.value()))
		spinBox_duration5_b = struct.pack(">L",int(self.spinBox_duration5.value()))
		spinBox_duration6_b = struct.pack(">L",int(self.spinBox_duration6.value()))
		spinBox_duration7_b = struct.pack(">L",int(self.spinBox_duration7.value()))
		spinBox_duration8_b = struct.pack(">L",int(self.spinBox_duration8.value()))
		spinBox_duration9_b = struct.pack(">L",int(self.spinBox_duration9.value()))
		spinBox_duration10_b = struct.pack(">L",int(self.spinBox_duration10.value()))
		spinBox_duration11_b = struct.pack(">L",int(self.spinBox_duration11.value()))
		spinBox_duration12_b = struct.pack(">L",int(self.spinBox_duration12.value()))
		spinBox_duration13_b = struct.pack(">L",int(self.spinBox_duration13.value()))
		spinBox_duration14_b = struct.pack(">L",int(self.spinBox_duration14.value()))
		spinBox_duration15_b = struct.pack(">L",int(self.spinBox_duration15.value()))
		spinBox_duration16_b = struct.pack(">L",int(self.spinBox_duration16.value()))
		spinBox_duration17_b = struct.pack(">L",int(self.spinBox_duration17.value()))
		spinBox_duration18_b = struct.pack(">L",int(self.spinBox_duration18.value()))
		spinBox_duration19_b = struct.pack(">L",int(self.spinBox_duration19.value()))
		spinBox_duration20_b = struct.pack(">L",int(self.spinBox_duration20.value()))
		spinBox_duration21_b = struct.pack(">L",int(self.spinBox_duration21.value()))
		spinBox_duration22_b = struct.pack(">L",int(self.spinBox_duration22.value()))
		spinBox_duration23_b = struct.pack(">L",int(self.spinBox_duration23.value()))
		spinBox_duration24_b = struct.pack(">L",int(self.spinBox_duration24.value()))

		cycleCounter_b = struct.pack(">L",int(self.spinBox_cycleCounter.value()))

		self.find_max_duration()
		cycleDuration_b = struct.pack(">L",int(self.spinBox_Pause.value() + self.max_duration))

		config_bytes = START_BYTE
		config_bytes += packet_length_b
		config_bytes += RELAY_CONFIG

		config_bytes += RELAY_1
		config_bytes += spinBox_start1_b
		config_bytes += spinBox_duration1_b
		config_bytes += checkBox1_b

		config_bytes += RELAY_2
		config_bytes += spinBox_start2_b
		config_bytes += spinBox_duration2_b
		config_bytes += checkBox2_b

		config_bytes += RELAY_3
		config_bytes += spinBox_start3_b
		config_bytes += spinBox_duration3_b
		config_bytes += checkBox3_b

		config_bytes += RELAY_4
		config_bytes += spinBox_start4_b
		config_bytes += spinBox_duration4_b
		config_bytes += checkBox4_b

		config_bytes += RELAY_5
		config_bytes += spinBox_start5_b
		config_bytes += spinBox_duration5_b
		config_bytes += checkBox5_b

		config_bytes += RELAY_6
		config_bytes += spinBox_start6_b
		config_bytes += spinBox_duration6_b
		config_bytes += checkBox6_b

		config_bytes += RELAY_7
		config_bytes += spinBox_start7_b
		config_bytes += spinBox_duration7_b
		config_bytes += checkBox7_b

		config_bytes += RELAY_8
		config_bytes += spinBox_start8_b
		config_bytes += spinBox_duration8_b
		config_bytes += checkBox8_b

		config_bytes += RELAY_9
		config_bytes += spinBox_start9_b
		config_bytes += spinBox_duration9_b
		config_bytes += checkBox9_b

		config_bytes += RELAY_10
		config_bytes += spinBox_start10_b
		config_bytes += spinBox_duration10_b
		config_bytes += checkBox10_b

		config_bytes += RELAY_11
		config_bytes += spinBox_start11_b
		config_bytes += spinBox_duration11_b
		config_bytes += checkBox11_b

		config_bytes += RELAY_12
		config_bytes += spinBox_start12_b
		config_bytes += spinBox_duration12_b
		config_bytes += checkBox12_b

		config_bytes += RELAY_13
		config_bytes += spinBox_start13_b
		config_bytes += spinBox_duration13_b
		config_bytes += checkBox13_b

		config_bytes += RELAY_14
		config_bytes += spinBox_start14_b
		config_bytes += spinBox_duration14_b
		config_bytes += checkBox14_b

		config_bytes += RELAY_15
		config_bytes += spinBox_start15_b
		config_bytes += spinBox_duration15_b
		config_bytes += checkBox15_b

		config_bytes += RELAY_16
		config_bytes += spinBox_start16_b
		config_bytes += spinBox_duration16_b
		config_bytes += checkBox16_b

		config_bytes += RELAY_17
		config_bytes += spinBox_start17_b
		config_bytes += spinBox_duration17_b
		config_bytes += checkBox17_b

		config_bytes += RELAY_18
		config_bytes += spinBox_start18_b
		config_bytes += spinBox_duration18_b
		config_bytes += checkBox18_b

		config_bytes += RELAY_19
		config_bytes += spinBox_start19_b
		config_bytes += spinBox_duration19_b
		config_bytes += checkBox19_b

		config_bytes += RELAY_20
		config_bytes += spinBox_start20_b
		config_bytes += spinBox_duration20_b
		config_bytes += checkBox20_b

		config_bytes += RELAY_21
		config_bytes += spinBox_start21_b
		config_bytes += spinBox_duration21_b
		config_bytes += checkBox21_b

		config_bytes += RELAY_22
		config_bytes += spinBox_start22_b
		config_bytes += spinBox_duration22_b
		config_bytes += checkBox22_b

		config_bytes += RELAY_23
		config_bytes += spinBox_start23_b
		config_bytes += spinBox_duration23_b
		config_bytes += checkBox23_b

		config_bytes += RELAY_24
		config_bytes += spinBox_start24_b
		config_bytes += spinBox_duration24_b
		config_bytes += checkBox24_b

		config_bytes += SEQUENCE_COUNTER
		config_bytes += cycleCounter_b

		config_bytes += DURATION_ID
		config_bytes += cycleDuration_b

		#config_bytes += CHECKSUM

		print("Config Len: {}".format(len(config_bytes)))
		#print(type(config_bytes))
		#print(config_bytes.decode("utf-8"))
		#print(config_bytes.hex())
		print(checkBox1_b)
		print(type(checkBox1_b))
		print("Writing configuration")
		print(config_bytes)

		# self.serial_port.write(config_bytes)

	def run(self):
		self._isRunning = True
		while self._isRunning is True:
			self.update_port_path()
			if self.port_path is not False:
				self.isDeviceNameChanged()
			else:
				print("Emit that connection is DOWN")
				self.serial_port_disconnected_sig.emit()
				self.serial_disconnect()
				self.device_name = None
				self.device_name_old = None
			time.sleep(.5)
			QtWidgets.QApplication.processEvents()
