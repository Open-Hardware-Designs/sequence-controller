#!/home/soso/Projects/sequence-controller/env/bin/python3 python
# -*- coding: utf-8 -*-

import UI_mainwindowapp2
import UI_popupwindow
import sys

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QLabel

import time
#import csv
import json

import struct
import serial
import serial.tools.list_ports

import os

from preview_thread import PreviewWorker
from preview_manual_thread import ManualPreviewWorker
from serial_thread import SerialWorker


class ExampleApp(QtWidgets.QMainWindow, UI_mainwindowapp2.Ui_MainWindowapp2, UI_popupwindow.Ui_popup_window):
	previewdemo_terminate_sig = QtCore.pyqtSignal()
	serial_relay_control_start_sig = QtCore.pyqtSignal()

	app_name = "Sequence Controller"


	def __init__(self):
		# Explaining super is out of the scope of this article
		# So please google it if you're not familar with it
		# Simple reason why we use it here is that it allows us to
		# access variables, methods etc in the design.py file
		super(self.__class__, self).__init__()
		self.setupUi(self)  # This is defined in design.py file automatically
							# It sets up layout and widgets that are defined

		self.checkAllBox.stateChanged.connect(self.check_all_boxes)
		self.check8Box.stateChanged.connect(self.check_8_boxes)

		self.checkBox1.stateChanged.connect(self.state_changedGlobal)
		self.checkBox2.stateChanged.connect(self.state_changedGlobal)
		self.checkBox3.stateChanged.connect(self.state_changedGlobal)
		self.checkBox4.stateChanged.connect(self.state_changedGlobal)
		self.checkBox5.stateChanged.connect(self.state_changedGlobal)
		self.checkBox6.stateChanged.connect(self.state_changedGlobal)
		self.checkBox7.stateChanged.connect(self.state_changedGlobal)
		self.checkBox8.stateChanged.connect(self.state_changedGlobal)
		self.checkBox9.stateChanged.connect(self.state_changedGlobal)
		self.checkBox10.stateChanged.connect(self.state_changedGlobal)
		self.checkBox11.stateChanged.connect(self.state_changedGlobal)
		self.checkBox12.stateChanged.connect(self.state_changedGlobal)
		self.checkBox13.stateChanged.connect(self.state_changedGlobal)
		self.checkBox14.stateChanged.connect(self.state_changedGlobal)
		self.checkBox15.stateChanged.connect(self.state_changedGlobal)
		self.checkBox16.stateChanged.connect(self.state_changedGlobal)
		self.checkBox17.stateChanged.connect(self.state_changedGlobal)
		self.checkBox18.stateChanged.connect(self.state_changedGlobal)
		self.checkBox19.stateChanged.connect(self.state_changedGlobal)
		self.checkBox20.stateChanged.connect(self.state_changedGlobal)
		self.checkBox21.stateChanged.connect(self.state_changedGlobal)
		self.checkBox22.stateChanged.connect(self.state_changedGlobal)
		self.checkBox23.stateChanged.connect(self.state_changedGlobal)
		self.checkBox24.stateChanged.connect(self.state_changedGlobal)

		self.pushButton_activate1.pressed.connect(self.manual_activate)
		self.pushButton_activate2.pressed.connect(self.manual_activate)
		self.pushButton_activate3.pressed.connect(self.manual_activate)
		self.pushButton_activate4.pressed.connect(self.manual_activate)
		self.pushButton_activate5.pressed.connect(self.manual_activate)
		self.pushButton_activate6.pressed.connect(self.manual_activate)
		self.pushButton_activate7.pressed.connect(self.manual_activate)
		self.pushButton_activate8.pressed.connect(self.manual_activate)
		self.pushButton_activate9.pressed.connect(self.manual_activate)
		self.pushButton_activate10.pressed.connect(self.manual_activate)
		self.pushButton_activate11.pressed.connect(self.manual_activate)
		self.pushButton_activate12.pressed.connect(self.manual_activate)
		self.pushButton_activate13.pressed.connect(self.manual_activate)
		self.pushButton_activate14.pressed.connect(self.manual_activate)
		self.pushButton_activate15.pressed.connect(self.manual_activate)
		self.pushButton_activate16.pressed.connect(self.manual_activate)
		self.pushButton_activate17.pressed.connect(self.manual_activate)
		self.pushButton_activate18.pressed.connect(self.manual_activate)
		self.pushButton_activate19.pressed.connect(self.manual_activate)
		self.pushButton_activate20.pressed.connect(self.manual_activate)
		self.pushButton_activate21.pressed.connect(self.manual_activate)
		self.pushButton_activate22.pressed.connect(self.manual_activate)
		self.pushButton_activate23.pressed.connect(self.manual_activate)
		self.pushButton_activate24.pressed.connect(self.manual_activate)

		self.armButton.clicked.connect(self.arm_stateChanged)

		#self.previewButton.clicked.connect(self.preview_demo_timer)
		self.previewButton.pressed.connect(self.thread_demo_1)
		self.previewButton.released.connect(self.thread_demo_1_released)
		#self.resetStartButton.clicked.connect(self.reset_start_values)
		##self.resetDurationButton.clicked.connect(self.reset_duration_values)
		#self.previewButton.clicked.connect(self.status_bar_demo)

		self.spinBoxStartAll.valueChanged.connect(self.write_start_values)
		self.spinBoxdurationAll.valueChanged.connect(self.write_duration_values)
		self.spinBox_orderAll.valueChanged.connect(self.write_order_values)

		self.openButton.clicked.connect(self.file_open)
		self.saveButton.clicked.connect(self.file_save)

		self.pushButton_Help.clicked.connect(self.open_popup)

		self.startButton.pressed.connect(self.start_pressed)
		self.stopResetButton.pressed.connect(self.relay_control_stop)
		self.writeConfigButton.pressed.connect(self.relay_write_config_2)

		self.spinBox_start1.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_start2.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_start3.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_start4.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_start5.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_start6.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_start7.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_start8.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_start9.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_start10.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_start11.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_start12.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_start13.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_start14.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_start15.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_start16.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_start17.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_start18.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_start19.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_start20.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_start21.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_start22.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_start23.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_start24.valueChanged.connect(self.update_cycle_duration)

		self.spinBox_duration1.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_duration2.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_duration3.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_duration4.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_duration5.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_duration6.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_duration7.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_duration8.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_duration9.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_duration10.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_duration11.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_duration12.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_duration13.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_duration14.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_duration15.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_duration16.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_duration17.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_duration18.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_duration19.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_duration20.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_duration21.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_duration22.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_duration23.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_duration24.valueChanged.connect(self.update_cycle_duration)

		self.spinBox_Pause.valueChanged.connect(self.update_cycle_duration)
		self.spinBox_cycleCounter.valueChanged.connect(self.update_cycle_duration)


		self.great_dictionary = {}
		self.manual_preview = {}


		self.preview_timer_counter = 0
		self.image_base_path = "../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/"
		self.circle_disabled = "FeedbackCircles-02.png"
		self.circle_enabled = "FeedbackCircles-01.png"
		self.circle_latched = "FeedbackCircles-06.png"
		self.circle_finished = "FeedbackCircles-03.png"

		self.max_duration = 0
		self.max_durations_list = []
		self.start_list = []
		self.duration_list = []

		self.checkbox_list = []

		self.put_to_dict()

		self.serial_device_status = False

		self.serial_backend = SerialWorker()
		self.serial_backend.serial_port_connected_sig.connect(self.device_is_connected)
		self.serial_backend.serial_port_disconnected_sig.connect(self.device_is_disconnected)
		self.serial_backend.start()
		self.previewButton_lastState = True

		#self.previewButton.setShortcut("b")
		self.startButton.setShortcut("b")

		self.radioButton_manual.clicked.connect(self.change_sequence_mode)
		self.radioButton_automatic.clicked.connect(self.change_sequence_mode)

		self.sequence_started = False


	def device_is_connected(self, device_type):
		#print("lool")
		self.statusBar.showMessage("{} is connected".format(device_type))
		self.serial_device_status = True
		self.update_node_buttons()

	def device_is_disconnected(self):
		self.statusBar.showMessage("No device")
		self.serial_device_status = False
		self.update_node_buttons()

	def update_node_buttons(self):
		if self.serial_device_status:
			self.writeConfigButton.setDisabled(False)
			self.readConfigButton.setDisabled(False)
			self.stopResetButton.setDisabled(False)
			if self.armButton.isChecked():
				self.startButton.setDisabled(False)
		else:
			self.writeConfigButton.setDisabled(True)
			self.readConfigButton.setDisabled(True)
			self.stopResetButton.setDisabled(True)
			self.startButton.setDisabled(True)
			self.armButton.setChecked(False)

	# For debugging
	def print_working_directory(self):
		self.previewButton.setText("dir {}".format(os.getcwd()))

	def relay_write_config_2(self):
		self.sequence_started = False
		#self.relay_control_stop()

		#self.serial_backend.relay_write_config_2(self.great_dictionary)

		START_BYTE = b'\x7E'
		CHECKSUM = b'\xAA'
		RELAY_CONFIG = b'\x02'
		packet_length = "00FB" ## Update packet length
		packet_length_b = bytes.fromhex(packet_length)
		RELAY_1 = b'\x01'
		RELAY_2 = b'\x02'
		RELAY_3 = b'\x03'
		RELAY_4 = b'\x04'
		RELAY_5 = b'\x05'
		RELAY_6 = b'\x06'
		RELAY_7 = b'\x07'
		RELAY_8 = b'\x08'
		RELAY_9 = b'\x09'
		RELAY_10 = b'\x0A'
		RELAY_11 = b'\x0B'
		RELAY_12 = b'\x0C'
		RELAY_13 = b'\x0D'
		RELAY_14 = b'\x0E'
		RELAY_15 = b'\x0F'
		RELAY_16 = b'\x10'
		RELAY_17 = b'\x11'
		RELAY_18 = b'\x12'
		RELAY_19 = b'\x13'
		RELAY_20 = b'\x14'
		RELAY_21 = b'\x15'
		RELAY_22 = b'\x16'
		RELAY_23 = b'\x17'
		RELAY_24 = b'\x18'

		SEQUENCE_COUNTER = b'\x00'
		DURATION_ID = b'\xFC'

		checkBox1_b = struct.pack("?",int(self.checkBox1.isChecked()))
		checkBox2_b = struct.pack("?",int(self.checkBox2.isChecked()))
		checkBox3_b = struct.pack("?",int(self.checkBox3.isChecked()))
		checkBox4_b = struct.pack("?",int(self.checkBox4.isChecked()))
		checkBox5_b = struct.pack("?",int(self.checkBox5.isChecked()))
		checkBox6_b = struct.pack("?",int(self.checkBox6.isChecked()))
		checkBox7_b = struct.pack("?",int(self.checkBox7.isChecked()))
		checkBox8_b = struct.pack("?",int(self.checkBox8.isChecked()))
		checkBox9_b = struct.pack("?",int(self.checkBox9.isChecked()))
		checkBox10_b = struct.pack("?",int(self.checkBox10.isChecked()))
		checkBox11_b = struct.pack("?",int(self.checkBox11.isChecked()))
		checkBox12_b = struct.pack("?",int(self.checkBox12.isChecked()))
		checkBox13_b = struct.pack("?",int(self.checkBox13.isChecked()))
		checkBox14_b = struct.pack("?",int(self.checkBox14.isChecked()))
		checkBox15_b = struct.pack("?",int(self.checkBox15.isChecked()))
		checkBox16_b = struct.pack("?",int(self.checkBox16.isChecked()))
		checkBox17_b = struct.pack("?",int(self.checkBox17.isChecked()))
		checkBox18_b = struct.pack("?",int(self.checkBox18.isChecked()))
		checkBox19_b = struct.pack("?",int(self.checkBox19.isChecked()))
		checkBox20_b = struct.pack("?",int(self.checkBox20.isChecked()))
		checkBox21_b = struct.pack("?",int(self.checkBox21.isChecked()))
		checkBox22_b = struct.pack("?",int(self.checkBox22.isChecked()))
		checkBox23_b = struct.pack("?",int(self.checkBox23.isChecked()))
		checkBox24_b = struct.pack("?",int(self.checkBox24.isChecked()))

		spinBox_start1_b = struct.pack(">L",int(self.spinBox_start1.value()))
		spinBox_start2_b = struct.pack(">L",int(self.spinBox_start2.value()))
		spinBox_start3_b = struct.pack(">L",int(self.spinBox_start3.value()))
		spinBox_start4_b = struct.pack(">L",int(self.spinBox_start4.value()))
		spinBox_start5_b = struct.pack(">L",int(self.spinBox_start5.value()))
		spinBox_start6_b = struct.pack(">L",int(self.spinBox_start6.value()))
		spinBox_start7_b = struct.pack(">L",int(self.spinBox_start7.value()))
		spinBox_start8_b = struct.pack(">L",int(self.spinBox_start8.value()))
		spinBox_start9_b = struct.pack(">L",int(self.spinBox_start9.value()))
		spinBox_start10_b = struct.pack(">L",int(self.spinBox_start10.value()))
		spinBox_start11_b = struct.pack(">L",int(self.spinBox_start11.value()))
		spinBox_start12_b = struct.pack(">L",int(self.spinBox_start12.value()))
		spinBox_start13_b = struct.pack(">L",int(self.spinBox_start13.value()))
		spinBox_start14_b = struct.pack(">L",int(self.spinBox_start14.value()))
		spinBox_start15_b = struct.pack(">L",int(self.spinBox_start15.value()))
		spinBox_start16_b = struct.pack(">L",int(self.spinBox_start16.value()))
		spinBox_start17_b = struct.pack(">L",int(self.spinBox_start17.value()))
		spinBox_start18_b = struct.pack(">L",int(self.spinBox_start18.value()))
		spinBox_start19_b = struct.pack(">L",int(self.spinBox_start19.value()))
		spinBox_start20_b = struct.pack(">L",int(self.spinBox_start20.value()))
		spinBox_start21_b = struct.pack(">L",int(self.spinBox_start21.value()))
		spinBox_start22_b = struct.pack(">L",int(self.spinBox_start22.value()))
		spinBox_start23_b = struct.pack(">L",int(self.spinBox_start23.value()))
		spinBox_start24_b = struct.pack(">L",int(self.spinBox_start24.value()))

		spinBox_duration1_b = struct.pack(">L",int(self.spinBox_duration1.value()))
		spinBox_duration2_b = struct.pack(">L",int(self.spinBox_duration2.value()))
		spinBox_duration3_b = struct.pack(">L",int(self.spinBox_duration3.value()))
		spinBox_duration4_b = struct.pack(">L",int(self.spinBox_duration4.value()))
		spinBox_duration5_b = struct.pack(">L",int(self.spinBox_duration5.value()))
		spinBox_duration6_b = struct.pack(">L",int(self.spinBox_duration6.value()))
		spinBox_duration7_b = struct.pack(">L",int(self.spinBox_duration7.value()))
		spinBox_duration8_b = struct.pack(">L",int(self.spinBox_duration8.value()))
		spinBox_duration9_b = struct.pack(">L",int(self.spinBox_duration9.value()))
		spinBox_duration10_b = struct.pack(">L",int(self.spinBox_duration10.value()))
		spinBox_duration11_b = struct.pack(">L",int(self.spinBox_duration11.value()))
		spinBox_duration12_b = struct.pack(">L",int(self.spinBox_duration12.value()))
		spinBox_duration13_b = struct.pack(">L",int(self.spinBox_duration13.value()))
		spinBox_duration14_b = struct.pack(">L",int(self.spinBox_duration14.value()))
		spinBox_duration15_b = struct.pack(">L",int(self.spinBox_duration15.value()))
		spinBox_duration16_b = struct.pack(">L",int(self.spinBox_duration16.value()))
		spinBox_duration17_b = struct.pack(">L",int(self.spinBox_duration17.value()))
		spinBox_duration18_b = struct.pack(">L",int(self.spinBox_duration18.value()))
		spinBox_duration19_b = struct.pack(">L",int(self.spinBox_duration19.value()))
		spinBox_duration20_b = struct.pack(">L",int(self.spinBox_duration20.value()))
		spinBox_duration21_b = struct.pack(">L",int(self.spinBox_duration21.value()))
		spinBox_duration22_b = struct.pack(">L",int(self.spinBox_duration22.value()))
		spinBox_duration23_b = struct.pack(">L",int(self.spinBox_duration23.value()))
		spinBox_duration24_b = struct.pack(">L",int(self.spinBox_duration24.value()))

		cycleCounter_b = struct.pack(">L",int(self.spinBox_cycleCounter.value()))

		self.find_max_duration()
		cycleDuration_b = struct.pack(">L",int(self.max_duration))
		#cycleDuration_b = struct.pack(">L",int(self.spinBox_Pause.value() + self.max_duration))
		#print("PAUSE {}".format(self.spinBox_Pause.value()))

		config_bytes = START_BYTE
		config_bytes += packet_length_b
		config_bytes += RELAY_CONFIG

		config_bytes += RELAY_1
		config_bytes += spinBox_start1_b
		config_bytes += spinBox_duration1_b
		config_bytes += checkBox1_b

		config_bytes += RELAY_2
		config_bytes += spinBox_start2_b
		config_bytes += spinBox_duration2_b
		config_bytes += checkBox2_b

		config_bytes += RELAY_3
		config_bytes += spinBox_start3_b
		config_bytes += spinBox_duration3_b
		config_bytes += checkBox3_b

		config_bytes += RELAY_4
		config_bytes += spinBox_start4_b
		config_bytes += spinBox_duration4_b
		config_bytes += checkBox4_b

		config_bytes += RELAY_5
		config_bytes += spinBox_start5_b
		config_bytes += spinBox_duration5_b
		config_bytes += checkBox5_b

		config_bytes += RELAY_6
		config_bytes += spinBox_start6_b
		config_bytes += spinBox_duration6_b
		config_bytes += checkBox6_b

		config_bytes += RELAY_7
		config_bytes += spinBox_start7_b
		config_bytes += spinBox_duration7_b
		config_bytes += checkBox7_b

		config_bytes += RELAY_8
		config_bytes += spinBox_start8_b
		config_bytes += spinBox_duration8_b
		config_bytes += checkBox8_b

		config_bytes += RELAY_9
		config_bytes += spinBox_start9_b
		config_bytes += spinBox_duration9_b
		config_bytes += checkBox9_b

		config_bytes += RELAY_10
		config_bytes += spinBox_start10_b
		config_bytes += spinBox_duration10_b
		config_bytes += checkBox10_b

		config_bytes += RELAY_11
		config_bytes += spinBox_start11_b
		config_bytes += spinBox_duration11_b
		config_bytes += checkBox11_b

		config_bytes += RELAY_12
		config_bytes += spinBox_start12_b
		config_bytes += spinBox_duration12_b
		config_bytes += checkBox12_b

		config_bytes += RELAY_13
		config_bytes += spinBox_start13_b
		config_bytes += spinBox_duration13_b
		config_bytes += checkBox13_b

		config_bytes += RELAY_14
		config_bytes += spinBox_start14_b
		config_bytes += spinBox_duration14_b
		config_bytes += checkBox14_b

		config_bytes += RELAY_15
		config_bytes += spinBox_start15_b
		config_bytes += spinBox_duration15_b
		config_bytes += checkBox15_b

		config_bytes += RELAY_16
		config_bytes += spinBox_start16_b
		config_bytes += spinBox_duration16_b
		config_bytes += checkBox16_b

		config_bytes += RELAY_17
		config_bytes += spinBox_start17_b
		config_bytes += spinBox_duration17_b
		config_bytes += checkBox17_b

		config_bytes += RELAY_18
		config_bytes += spinBox_start18_b
		config_bytes += spinBox_duration18_b
		config_bytes += checkBox18_b

		config_bytes += RELAY_19
		config_bytes += spinBox_start19_b
		config_bytes += spinBox_duration19_b
		config_bytes += checkBox19_b

		config_bytes += RELAY_20
		config_bytes += spinBox_start20_b
		config_bytes += spinBox_duration20_b
		config_bytes += checkBox20_b

		config_bytes += RELAY_21
		config_bytes += spinBox_start21_b
		config_bytes += spinBox_duration21_b
		config_bytes += checkBox21_b

		config_bytes += RELAY_22
		config_bytes += spinBox_start22_b
		config_bytes += spinBox_duration22_b
		config_bytes += checkBox22_b

		config_bytes += RELAY_23
		config_bytes += spinBox_start23_b
		config_bytes += spinBox_duration23_b
		config_bytes += checkBox23_b

		config_bytes += RELAY_24
		config_bytes += spinBox_start24_b
		config_bytes += spinBox_duration24_b
		config_bytes += checkBox24_b

		config_bytes += SEQUENCE_COUNTER
		config_bytes += cycleCounter_b

		config_bytes += DURATION_ID
		config_bytes += cycleDuration_b

		#config_bytes += CHECKSUM

		print("Config Len: {}".format(len(config_bytes)))
		#print(type(config_bytes))
		#print(config_bytes.decode("utf-8"))
		#print(config_bytes.hex())
		print(checkBox1_b)
		print(type(checkBox1_b))
		print("Writing configuration")
		print(config_bytes)
		#print(hex(config_bytes)
		print(" ".join("{:02x}".format(x) for x in config_bytes))
		self.serial_backend.serial_write(config_bytes)

		# self.serial_port.write(config_bytes)

	def manual_activate(self):
		# Get self object
		self.put_to_dict()

		senderButton = self.sender()

		print(str(senderButton.objectName())[19:])
		sender_number = str(senderButton.objectName())[19:]
		START_BYTE = b'\x7E'
		CHECKSUM = b'\xAA'
		RELAY_MANUAL_ACTIVATE = b'\x03'

		serial_packet = START_BYTE
		serial_packet += struct.pack(">H", 6) # -1 because RELAY_MANUAL_ACTIVATE needs to be excluded
		serial_packet += RELAY_MANUAL_ACTIVATE
		serial_packet += struct.pack(">B",int(sender_number))
		key = "Relay_"
		key += str(sender_number)

		serial_packet += struct.pack('>L',int(self.great_dictionary[key]["duration"]))
		#serial_packet += CHECKSUM
		#print(serial_packet)
		print("")
		#print(serial_packet.encode("hex"))
		self.serial_backend.serial_write(serial_packet)

		self.manual_preview[key] = ManualPreviewWorker(int(self.great_dictionary[key]["duration"]), int(str(key)[6:]))
		self.manual_preview[key].relay_finished.connect(self.circle_finish_manual)
		self.manual_preview[key].start()
		exec("self.circleInactive{}.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))".format(int(str(key)[6:])))


		#pass



	def start_pressed(self):
		if self.radioButton_automatic.isChecked() is True:
			print("Automatic start")
			self.relay_control_start()
		elif self.radioButton_manual.isChecked() is True:
			print("Manual start")

			#print(self.statusBar.currentMessage())
			self.put_to_dict()
			#print(self.great_dictionary.keys())

			if self.sequence_started is False:
				self.sequence_started = True
				self.start_sequence_counter = 0
				#self.current_message = self.statusBar.currentMessage()

			if self.sequence_started is True:
				self.start_sequence_counter += 1
				if self.start_sequence_counter < 25:
					START_BYTE = b'\x7E'
					CHECKSUM = b'\xAA'
					RELAY_MANUAL_ACTIVATE = b'\x03'

					#manual_start_command = START_BYTE
					#manual_start_command += packet_length_b #Still to addWidget
					manual_start_command = RELAY_MANUAL_ACTIVATE


					highest_order_number = 1
					for key in self.great_dictionary:
						#print(key)
						#print(self.great_dictionary[key])
						#print(self.great_dictionary[key]["enabled"])
						if "Relay" in key:
							if self.great_dictionary[key]["enabled"] is True and self.great_dictionary[key]["duration"] is not 0:
								if highest_order_number < int(self.great_dictionary[key]["order"]):
									highest_order_number = int(self.great_dictionary[key]["order"])
								#print(self.great_dictionary[key]["order"])
								if self.great_dictionary[key]["order"] == self.start_sequence_counter:
									self.manual_preview[key] = ManualPreviewWorker(int(self.great_dictionary[key]["duration"]), int(str(key)[6:]))
									self.manual_preview[key].relay_finished.connect(self.circle_finish_manual)
									self.manual_preview[key].start()
									exec("self.circleInactive{}.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))".format(int(str(key)[6:])))
									print(str(key)[6:])

									manual_start_command += struct.pack(">B",int(str(key)[6:]))
									manual_start_command += struct.pack('>L',int(self.great_dictionary[key]["duration"]))
					#print(highest_order_number)
					if self.start_sequence_counter < highest_order_number + 1:
						serial_packet = START_BYTE
						serial_packet += struct.pack(">H", (len(manual_start_command))) # -1 because RELAY_MANUAL_ACTIVATE needs to be excluded
						print("Command Len: " + str(len(manual_start_command) - 1))
						serial_packet += manual_start_command
						#serial_packet += CHECKSUM
						print(serial_packet)
						print(" ".join("{:02x}".format(x) for x in serial_packet))
						self.serial_backend.serial_write(serial_packet)
						sequence_counter_text = "\t - \tSequence counter: "
						if self.start_sequence_counter == 24:
							self.label_manual.setText("Manual sequence -\n Next Relay 1")
						else:
							self.label_manual.setText("Manual sequence -\n Next Relay {}".format(self.start_sequence_counter + 1))
						#self.statusBar.showMessage(self.current_message + sequence_counter_text + str(self.start_sequence_counter))
				# for i in range(1,25):
				# 	relay_data = {}
				# 	exec("relay_data = self.great_dictionary['Relay_{}']".format(i))
				# 	print(relay_data)


			#self.great_dictionary
			# Scan for check boxes
			# Scan for Sequence numbers
			# Check if sequnce has been started
			## if no Start a sequence




	def relay_control_start(self):
		#self.relay_control_stop()
		self.spinBox_SlowMo.setValue(1)
		self.serial_backend.relay_control_start()
		self.previewButton_lastState = True
		self.thread_demo_1()
		self.writeConfigButton.setDisabled(True)


	def relay_control_stop(self):
		self.sequence_started = False
		self.label_manual.setText("Manual sequence -\n Next Relay 1")
		for relay in range(1,25):
			exec("self.circleInactive{}.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))".format(relay))
		#self.statusBar.showMessage(self.current_message)
		self.serial_backend.relay_control_stop()
		try:
			self.previewdemo.stop()
		except:
			pass#self.previewdemo_terminate_sig.emit()
		self.enable_action_buttons()
		self.previewButton.setText("Preview")


	def thread_demo_1_released(self):
		self.previewButton_lastState = True
		self.previewdemo.stop()
		#self.previewdemo_terminate_sig.emit()
		self.enable_action_buttons()
		self.previewButton.setText("Preview")

	def thread_demo_1(self):

		#self.previewButton_lastState = self.previewButton.isChecked()
		try:
			if self.previewdemo.isRunning():
				self.previewButton_lastState = False
				self.previewButton.setChecked(False)
		except:
			pass
		#checked = self.previewButton.isChecked()
		checked = self.previewButton_lastState
		#self.previewButton_lastState = self.previewButton.isChecked()
		if checked:
			self.previewButton_lastState = False
			#if self.previewdemo.isRunning:
			#	print("Running thread")
			self.put_to_dict()
			self.previewdemo = PreviewWorker(self.great_dictionary)
			self.previewdemo.circle_1_cleared.connect(self.circle_1_enable)
			self.previewdemo.circle_2_cleared.connect(self.circle_2_enable)
			self.previewdemo.circle_3_cleared.connect(self.circle_3_enable)
			self.previewdemo.circle_4_cleared.connect(self.circle_4_enable)
			self.previewdemo.circle_5_cleared.connect(self.circle_5_enable)
			self.previewdemo.circle_6_cleared.connect(self.circle_6_enable)
			self.previewdemo.circle_7_cleared.connect(self.circle_7_enable)
			self.previewdemo.circle_8_cleared.connect(self.circle_8_enable)
			self.previewdemo.circle_9_cleared.connect(self.circle_9_enable)
			self.previewdemo.circle_10_cleared.connect(self.circle_10_enable)
			self.previewdemo.circle_11_cleared.connect(self.circle_11_enable)
			self.previewdemo.circle_12_cleared.connect(self.circle_12_enable)
			self.previewdemo.circle_13_cleared.connect(self.circle_13_enable)
			self.previewdemo.circle_14_cleared.connect(self.circle_14_enable)
			self.previewdemo.circle_15_cleared.connect(self.circle_15_enable)
			self.previewdemo.circle_16_cleared.connect(self.circle_16_enable)
			self.previewdemo.circle_17_cleared.connect(self.circle_17_enable)
			self.previewdemo.circle_18_cleared.connect(self.circle_18_enable)
			self.previewdemo.circle_19_cleared.connect(self.circle_19_enable)
			self.previewdemo.circle_20_cleared.connect(self.circle_20_enable)
			self.previewdemo.circle_21_cleared.connect(self.circle_21_enable)
			self.previewdemo.circle_22_cleared.connect(self.circle_22_enable)
			self.previewdemo.circle_23_cleared.connect(self.circle_23_enable)
			self.previewdemo.circle_24_cleared.connect(self.circle_24_enable)

			self.previewdemo.circle_1_latched.connect(self.circle_1_latch)
			self.previewdemo.circle_2_latched.connect(self.circle_2_latch)
			self.previewdemo.circle_3_latched.connect(self.circle_3_latch)
			self.previewdemo.circle_4_latched.connect(self.circle_4_latch)
			self.previewdemo.circle_5_latched.connect(self.circle_5_latch)
			self.previewdemo.circle_6_latched.connect(self.circle_6_latch)
			self.previewdemo.circle_7_latched.connect(self.circle_7_latch)
			self.previewdemo.circle_8_latched.connect(self.circle_8_latch)
			self.previewdemo.circle_9_latched.connect(self.circle_9_latch)
			self.previewdemo.circle_10_latched.connect(self.circle_10_latch)
			self.previewdemo.circle_11_latched.connect(self.circle_11_latch)
			self.previewdemo.circle_12_latched.connect(self.circle_12_latch)
			self.previewdemo.circle_13_latched.connect(self.circle_13_latch)
			self.previewdemo.circle_14_latched.connect(self.circle_14_latch)
			self.previewdemo.circle_15_latched.connect(self.circle_15_latch)
			self.previewdemo.circle_16_latched.connect(self.circle_16_latch)
			self.previewdemo.circle_17_latched.connect(self.circle_17_latch)
			self.previewdemo.circle_18_latched.connect(self.circle_18_latch)
			self.previewdemo.circle_19_latched.connect(self.circle_19_latch)
			self.previewdemo.circle_20_latched.connect(self.circle_20_latch)
			self.previewdemo.circle_21_latched.connect(self.circle_21_latch)
			self.previewdemo.circle_22_latched.connect(self.circle_22_latch)
			self.previewdemo.circle_23_latched.connect(self.circle_23_latch)
			self.previewdemo.circle_24_latched.connect(self.circle_24_latch)

			self.previewdemo.circle_1_finished.connect(self.circle_1_finish)
			self.previewdemo.circle_2_finished.connect(self.circle_2_finish)
			self.previewdemo.circle_3_finished.connect(self.circle_3_finish)
			self.previewdemo.circle_4_finished.connect(self.circle_4_finish)
			self.previewdemo.circle_5_finished.connect(self.circle_5_finish)
			self.previewdemo.circle_6_finished.connect(self.circle_6_finish)
			self.previewdemo.circle_7_finished.connect(self.circle_7_finish)
			self.previewdemo.circle_8_finished.connect(self.circle_8_finish)
			self.previewdemo.circle_9_finished.connect(self.circle_9_finish)
			self.previewdemo.circle_10_finished.connect(self.circle_10_finish)
			self.previewdemo.circle_11_finished.connect(self.circle_11_finish)
			self.previewdemo.circle_12_finished.connect(self.circle_12_finish)
			self.previewdemo.circle_13_finished.connect(self.circle_13_finish)
			self.previewdemo.circle_14_finished.connect(self.circle_14_finish)
			self.previewdemo.circle_15_finished.connect(self.circle_15_finish)
			self.previewdemo.circle_16_finished.connect(self.circle_16_finish)
			self.previewdemo.circle_17_finished.connect(self.circle_17_finish)
			self.previewdemo.circle_18_finished.connect(self.circle_18_finish)
			self.previewdemo.circle_19_finished.connect(self.circle_19_finish)
			self.previewdemo.circle_20_finished.connect(self.circle_20_finish)
			self.previewdemo.circle_21_finished.connect(self.circle_21_finish)
			self.previewdemo.circle_22_finished.connect(self.circle_22_finish)
			self.previewdemo.circle_23_finished.connect(self.circle_23_finish)
			self.previewdemo.circle_24_finished.connect(self.circle_24_finish)

			self.previewdemo.cycleCounter_current.connect(self.update_current_cycle)

			self.previewdemo.progressBar_reset.connect(self.reset_progress_bar)
			self.previewdemo.progressBar_maximum.connect(self.setMaximum_progress_bar)
			self.previewdemo.progressBar_current.connect(self.update_progress_bar)
			self.previewdemo.disable_action_buttons_sig.connect(self.disable_action_buttons)
			self.previewdemo.enable_action_buttons_sig.connect(self.enable_action_buttons)

			self.previewdemo.start()
			self.previewButton.setText("Stop")
		else:
			self.previewButton_lastState = True
			self.previewdemo.stop()
			#self.previewdemo_terminate_sig.emit()
			self.enable_action_buttons()
			self.previewButton.setText("Preview")

	def reset_progress_bar(self):
		self.progressBar_node1.reset()

	def update_progress_bar(self, value):
		self.progressBar_node1.setValue(value)

	def setMaximum_progress_bar(self, maximum):
		self.progressBar_node1.setMaximum(maximum)


	def update_current_cycle(self, cycle):
		self.label_cycleCounter.setText("{}/{}".format(cycle, self.spinBox_cycleCounter.value()))

	def disable_action_buttons(self):
		self.spinBox_SlowMo.setDisabled(True)
		self.spinBox_cycleCounter.setDisabled(True)
		self.spinBox_Pause.setDisabled(True)
		self.previewButton.setChecked(True)
		self.startButton.setDisabled(True)
		#self.stopResetButton.setDisabled(True)
		self.previewButton

	# After completing preview cycle re-enable action buttons and spin boxes
	def enable_action_buttons(self):
		self.spinBox_SlowMo.setDisabled(False)
		self.spinBox_cycleCounter.setDisabled(False)
		self.spinBox_Pause.setDisabled(False)
		self.previewButton.setChecked(False) # add to unckech or smthing
		self.previewButton.setText("Preview")
		if self.serial_device_status:
			self.stopResetButton.setDisabled(False)
			self.writeConfigButton.setDisabled(False)
		if self.armButton.isChecked():
			self.startButton.setDisabled(False)



	def find_max_duration(self):
		"""Find maximum cycle duration"""
		# Update Lists with new values
		self.update_item_lists()
		self.max_duration_list_enabled = []
		for i in range(0,24):
			if self.checkbox_list[i] == True:
				self.max_duration_list_enabled.append(self.max_durations_list[i])
		#max_duration = max(max_duration_list_enabled)
		try:
			self.max_duration = max(self.max_duration_list_enabled) + self.spinBox_Pause.value()
		except ValueError:
			self.max_duration = 0
		#print("Max duration {}".format(self.max_duration))


	def update_cycle_duration(self):
		self.find_max_duration()
		self.label_CycleDuration.setText("Cycle duration: {}ms".format(self.max_duration))
		self.label_TotalDuration.setText("Total duration: {}ms".format(self.max_duration * self.spinBox_cycleCounter.value()))
		self.label_cycleCounter.setText("1/{}".format(self.spinBox_cycleCounter.value()))

	def open_popup(self):
		self.window = QtWidgets.QMainWindow()
		self.ui = UI_popupwindow.Ui_popup_window()
		self.ui.setupUi(self.window)
		self.window.show()
		self.window.move(250, 100)

	def preview_timer_test(self):
		self.preview_timer_counter = self.preview_timer_counter + 1
		self.label_65.setText("Preview_timer {}".format(self.preview_timer_counter))



	# Circle 1
	def circle_1_enable(self):
		self.circleInactive1.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_2_enable(self):
		self.circleInactive2.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)
	def circle_3_enable(self):
		self.circleInactive3.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_4_enable(self):
		self.circleInactive4.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)
	def circle_5_enable(self):
		self.circleInactive5.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_6_enable(self):
		self.circleInactive6.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)
	def circle_7_enable(self):
		self.circleInactive7.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_8_enable(self):
		self.circleInactive8.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)
	def circle_9_enable(self):
		self.circleInactive9.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_10_enable(self):
		self.circleInactive10.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)
	def circle_11_enable(self):
		self.circleInactive11.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_12_enable(self):
		self.circleInactive12.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)
	def circle_13_enable(self):
		self.circleInactive13.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_14_enable(self):
		self.circleInactive14.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)
	def circle_15_enable(self):
		self.circleInactive15.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_16_enable(self):
		self.circleInactive16.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)
	def circle_17_enable(self):
		self.circleInactive17.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_18_enable(self):
		self.circleInactive18.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)
	def circle_19_enable(self):
		self.circleInactive19.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_20_enable(self):
		self.circleInactive20.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)
	def circle_21_enable(self):
		self.circleInactive21.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_22_enable(self):
		self.circleInactive22.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)
	def circle_23_enable(self):
		self.circleInactive23.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_24_enable(self):
		self.circleInactive24.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)
	# Circle 1
	def circle_1_latch(self):
		self.circleInactive1.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_2_latch(self):
		self.circleInactive2.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)
	def circle_3_latch(self):
		self.circleInactive3.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_4_latch(self):
		self.circleInactive4.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)
	def circle_5_latch(self):
		self.circleInactive5.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_6_latch(self):
		self.circleInactive6.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)
	def circle_7_latch(self):
		self.circleInactive7.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_8_latch(self):
		self.circleInactive8.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)
	def circle_9_latch(self):
		self.circleInactive9.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_10_latch(self):
		self.circleInactive10.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)
	def circle_11_latch(self):
		self.circleInactive11.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_12_latch(self):
		self.circleInactive12.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)
	def circle_13_latch(self):
		self.circleInactive13.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_14_latch(self):
		self.circleInactive14.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)
	def circle_15_latch(self):
		self.circleInactive15.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_16_latch(self):
		self.circleInactive16.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)
	def circle_17_latch(self):
		self.circleInactive17.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_18_latch(self):
		self.circleInactive18.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)
	def circle_19_latch(self):
		self.circleInactive19.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_20_latch(self):
		self.circleInactive20.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)
	def circle_21_latch(self):
		self.circleInactive21.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_22_latch(self):
		self.circleInactive22.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)
	def circle_23_latch(self):
		self.circleInactive23.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer1.singleShot(self.spinBox_duration1.value(), self.circle_1_finished)
	def circle_24_latch(self):
		self.circleInactive24.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_latched))
		#self.preview_timer2.singleShot(self.spinBox_duration2.value(), self.circle_2_finished)

	def circle_1_finish(self):
		self.circleInactive1.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))
	def circle_2_finish(self):
		self.circleInactive2.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))
	def circle_3_finish(self):
		self.circleInactive3.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))
	def circle_4_finish(self):
		self.circleInactive4.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))
	def circle_5_finish(self):
		self.circleInactive5.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))
	def circle_6_finish(self):
		self.circleInactive6.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))
	def circle_7_finish(self):
		self.circleInactive7.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))
	def circle_8_finish(self):
		self.circleInactive8.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))
	def circle_9_finish(self):
		self.circleInactive9.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))
	def circle_10_finish(self):
		self.circleInactive10.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))
	def circle_11_finish(self):
		self.circleInactive11.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))
	def circle_12_finish(self):
		self.circleInactive12.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))
	def circle_13_finish(self):
		self.circleInactive13.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))
	def circle_14_finish(self):
		self.circleInactive14.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))
	def circle_15_finish(self):
		self.circleInactive15.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))
	def circle_16_finish(self):
		self.circleInactive16.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))
	def circle_17_finish(self):
		self.circleInactive17.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))
	def circle_18_finish(self):
		self.circleInactive18.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))
	def circle_19_finish(self):
		self.circleInactive19.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))
	def circle_20_finish(self):
		self.circleInactive20.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))
	def circle_21_finish(self):
		self.circleInactive21.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))
	def circle_22_finish(self):
		self.circleInactive22.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))
	def circle_23_finish(self):
		self.circleInactive23.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))
	def circle_24_finish(self):
		self.circleInactive24.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))

	def circle_finish_manual(self, relay_number):
		exec("self.circleInactive{}.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_finished))".format(relay_number))


	def put_to_dict(self):

		for i in range(1,25):
			exec('self.great_dictionary["Relay_%s"] = {}' % str(i))
			exec('self.great_dictionary["Relay_{}"]["name"] = self.textField_name{}.text()'.format(i, i))
			exec('self.great_dictionary["Relay_{}"]["start"] = self.spinBox_start{}.value()'.format(i, i))
			exec('self.great_dictionary["Relay_{}"]["duration"] = self.spinBox_duration{}.value()'.format(i, i))
			exec('self.great_dictionary["Relay_{}"]["enabled"] = self.checkBox{}.isChecked()'.format(i, i))
			exec('self.great_dictionary["Relay_{}"]["order"] = self.spinBox_order{}.value()'.format(i, i))

		self.great_dictionary["slow_motion"] = self.spinBox_SlowMo.value()
		self.great_dictionary["cycle_counter"] = self.spinBox_cycleCounter.value()
		self.great_dictionary["pause"] = self.spinBox_Pause.value()
		self.great_dictionary["Automatic_mode"] = self.radioButton_automatic.isChecked()
		self.great_dictionary["Manual_mode"] = self.radioButton_manual.isChecked()


	def retrieve_from_dict(self):
		for i in range(1,25):
			exec('self.textField_name{}.setText(self.great_dictionary["Relay_{}"]["name"])'.format(i, i))
			exec('self.spinBox_start{}.setValue(self.great_dictionary["Relay_{}"]["start"])'.format(i, i))
			exec('self.spinBox_duration{}.setValue(self.great_dictionary["Relay_{}"]["duration"])'.format(i, i))
			exec('self.checkBox{}.setChecked(self.great_dictionary["Relay_{}"]["enabled"])'.format(i, i))
			exec('self.spinBox_order{}.setValue(self.great_dictionary["Relay_{}"]["order"])'.format(i, i))

		self.spinBox_SlowMo.setValue(self.great_dictionary["slow_motion"])
		self.spinBox_cycleCounter.setValue(self.great_dictionary["cycle_counter"])
		self.spinBox_Pause.setValue(self.great_dictionary["pause"])
		self.radioButton_automatic.setChecked(self.great_dictionary["Automatic_mode"])
		self.radioButton_manual.setChecked(self.great_dictionary["Manual_mode"])

		self.change_sequence_mode()

	def file_open(self):
		name = QtWidgets.QFileDialog.getOpenFileName(self, 'Open File', "../../Sequence-storage")
		#print(name)
		try:
			with open(name[0]) as f:
				self.great_dictionary = json.load(f)
			self.retrieve_from_dict()
			self.setWindowTitle("{} - ".format(os.path.basename(name[0])) + self.app_name)

		except:
			print("No file selected (Open)")

	def file_save(self):
		#name = QtWidgets.QFileDialog.getSaveFileName(self, 'Save File')
		self.put_to_dict()
		print(self.great_dictionary)
		name = QtWidgets.QFileDialog.getSaveFileName(self, 'Save File', "../../Sequence-storage")
		try:
			with open(name[0], 'w') as f:
				json.dump(self.great_dictionary, f)
				self.setWindowTitle("{} - ".format(os.path.basename(name[0])) + self.app_name)
		except:
			print("No file selected (Save)")

	def update_item_lists(self):

		#self.max_duration = 0
		#reset the lists
		self.max_durations_list = []
		self.start_list = []
		self.duration_list = []
		self.checkbox_list = []

		self.checkbox_list.append(int(self.checkBox1.isChecked()))
		self.checkbox_list.append(int(self.checkBox2.isChecked()))
		self.checkbox_list.append(int(self.checkBox3.isChecked()))
		self.checkbox_list.append(int(self.checkBox4.isChecked()))
		self.checkbox_list.append(int(self.checkBox5.isChecked()))
		self.checkbox_list.append(int(self.checkBox6.isChecked()))
		self.checkbox_list.append(int(self.checkBox7.isChecked()))
		self.checkbox_list.append(int(self.checkBox8.isChecked()))
		self.checkbox_list.append(int(self.checkBox9.isChecked()))
		self.checkbox_list.append(int(self.checkBox10.isChecked()))
		self.checkbox_list.append(int(self.checkBox11.isChecked()))
		self.checkbox_list.append(int(self.checkBox12.isChecked()))
		self.checkbox_list.append(int(self.checkBox13.isChecked()))
		self.checkbox_list.append(int(self.checkBox14.isChecked()))
		self.checkbox_list.append(int(self.checkBox15.isChecked()))
		self.checkbox_list.append(int(self.checkBox16.isChecked()))
		self.checkbox_list.append(int(self.checkBox17.isChecked()))
		self.checkbox_list.append(int(self.checkBox18.isChecked()))
		self.checkbox_list.append(int(self.checkBox19.isChecked()))
		self.checkbox_list.append(int(self.checkBox20.isChecked()))
		self.checkbox_list.append(int(self.checkBox21.isChecked()))
		self.checkbox_list.append(int(self.checkBox22.isChecked()))
		self.checkbox_list.append(int(self.checkBox23.isChecked()))
		self.checkbox_list.append(int(self.checkBox24.isChecked()))

		self.duration_list.append(int(self.spinBox_duration1.value()))
		self.duration_list.append(int(self.spinBox_duration2.value()))
		self.duration_list.append(int(self.spinBox_duration3.value()))
		self.duration_list.append(int(self.spinBox_duration4.value()))
		self.duration_list.append(int(self.spinBox_duration5.value()))
		self.duration_list.append(int(self.spinBox_duration6.value()))
		self.duration_list.append(int(self.spinBox_duration7.value()))
		self.duration_list.append(int(self.spinBox_duration8.value()))
		self.duration_list.append(int(self.spinBox_duration9.value()))
		self.duration_list.append(int(self.spinBox_duration10.value()))
		self.duration_list.append(int(self.spinBox_duration11.value()))
		self.duration_list.append(int(self.spinBox_duration12.value()))
		self.duration_list.append(int(self.spinBox_duration13.value()))
		self.duration_list.append(int(self.spinBox_duration14.value()))
		self.duration_list.append(int(self.spinBox_duration15.value()))
		self.duration_list.append(int(self.spinBox_duration16.value()))
		self.duration_list.append(int(self.spinBox_duration17.value()))
		self.duration_list.append(int(self.spinBox_duration18.value()))
		self.duration_list.append(int(self.spinBox_duration19.value()))
		self.duration_list.append(int(self.spinBox_duration20.value()))
		self.duration_list.append(int(self.spinBox_duration21.value()))
		self.duration_list.append(int(self.spinBox_duration22.value()))
		self.duration_list.append(int(self.spinBox_duration23.value()))
		self.duration_list.append(int(self.spinBox_duration24.value()))

		self.start_list.append(int(self.spinBox_start1.value()))
		self.start_list.append(int(self.spinBox_start2.value()))
		self.start_list.append(int(self.spinBox_start3.value()))
		self.start_list.append(int(self.spinBox_start4.value()))
		self.start_list.append(int(self.spinBox_start5.value()))
		self.start_list.append(int(self.spinBox_start6.value()))
		self.start_list.append(int(self.spinBox_start7.value()))
		self.start_list.append(int(self.spinBox_start8.value()))
		self.start_list.append(int(self.spinBox_start9.value()))
		self.start_list.append(int(self.spinBox_start10.value()))
		self.start_list.append(int(self.spinBox_start11.value()))
		self.start_list.append(int(self.spinBox_start12.value()))
		self.start_list.append(int(self.spinBox_start13.value()))
		self.start_list.append(int(self.spinBox_start14.value()))
		self.start_list.append(int(self.spinBox_start15.value()))
		self.start_list.append(int(self.spinBox_start16.value()))
		self.start_list.append(int(self.spinBox_start17.value()))
		self.start_list.append(int(self.spinBox_start18.value()))
		self.start_list.append(int(self.spinBox_start19.value()))
		self.start_list.append(int(self.spinBox_start20.value()))
		self.start_list.append(int(self.spinBox_start21.value()))
		self.start_list.append(int(self.spinBox_start22.value()))
		self.start_list.append(int(self.spinBox_start23.value()))
		self.start_list.append(int(self.spinBox_start24.value()))

		self.max_durations_list.append(int(self.spinBox_start1.value()) + int(self.spinBox_duration1.value()))
		self.max_durations_list.append(int(self.spinBox_start2.value()) + int(self.spinBox_duration2.value()))
		self.max_durations_list.append(int(self.spinBox_start3.value()) + int(self.spinBox_duration3.value()))
		self.max_durations_list.append(int(self.spinBox_start4.value()) + int(self.spinBox_duration4.value()))
		self.max_durations_list.append(int(self.spinBox_start5.value()) + int(self.spinBox_duration5.value()))
		self.max_durations_list.append(int(self.spinBox_start6.value()) + int(self.spinBox_duration6.value()))
		self.max_durations_list.append(int(self.spinBox_start7.value()) + int(self.spinBox_duration7.value()))
		self.max_durations_list.append(int(self.spinBox_start8.value()) + int(self.spinBox_duration8.value()))
		self.max_durations_list.append(int(self.spinBox_start9.value()) + int(self.spinBox_duration9.value()))
		self.max_durations_list.append(int(self.spinBox_start10.value()) + int(self.spinBox_duration10.value()))
		self.max_durations_list.append(int(self.spinBox_start11.value()) + int(self.spinBox_duration11.value()))
		self.max_durations_list.append(int(self.spinBox_start12.value()) + int(self.spinBox_duration12.value()))
		self.max_durations_list.append(int(self.spinBox_start13.value()) + int(self.spinBox_duration13.value()))
		self.max_durations_list.append(int(self.spinBox_start14.value()) + int(self.spinBox_duration14.value()))
		self.max_durations_list.append(int(self.spinBox_start15.value()) + int(self.spinBox_duration15.value()))
		self.max_durations_list.append(int(self.spinBox_start16.value()) + int(self.spinBox_duration16.value()))
		self.max_durations_list.append(int(self.spinBox_start17.value()) + int(self.spinBox_duration17.value()))
		self.max_durations_list.append(int(self.spinBox_start18.value()) + int(self.spinBox_duration18.value()))
		self.max_durations_list.append(int(self.spinBox_start19.value()) + int(self.spinBox_duration19.value()))
		self.max_durations_list.append(int(self.spinBox_start20.value()) + int(self.spinBox_duration20.value()))
		self.max_durations_list.append(int(self.spinBox_start21.value()) + int(self.spinBox_duration21.value()))
		self.max_durations_list.append(int(self.spinBox_start22.value()) + int(self.spinBox_duration22.value()))
		self.max_durations_list.append(int(self.spinBox_start23.value()) + int(self.spinBox_duration23.value()))
		self.max_durations_list.append(int(self.spinBox_start24.value()) + int(self.spinBox_duration24.value()))


	def disarm_messagebox(self):
		choice = QtWidgets.QMessageBox.question(self, 'Arm the system', "Do you really want to arm the system?", QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
		if choice == QtWidgets.QMessageBox.Yes:
			pass
		#arm the system
		else:
			pass

	def disable_activate_buttons(self):
		for i in range(1,25):
			exec("self.pushButton_activate{}.setDisabled(True)".format(i))

	def enable_activate_buttons(self):
		for i in range(1,25):
			exec("self.pushButton_activate{}.setDisabled(False)".format(i))

	def arm_stateChanged(self, checked):
		if self.serial_device_status:
			if checked:
				if self.serial_device_status:
					choice = QtWidgets.QMessageBox.question(self, 'Arm the system', "Do you really want to arm the system?", QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
					if choice == QtWidgets.QMessageBox.Yes:
						try:
							if self.previewdemo.isRunning() is False:
								self.startButton.setDisabled(False)
								self.enable_activate_buttons()
						except AttributeError:
							self.startButton.setDisabled(False)
							self.enable_activate_buttons()
					else:
						self.armButton.setChecked(False)
			else:
				self.startButton.setDisabled(True)
				self.disable_activate_buttons()
				self.relay_control_stop()
		else:
			self.armButton.setChecked(False)
			# Add send stop command and disarm command




		#self.disarmedButton.clicked.connect(self.disarm_button)

	def disarm_button(self):
		self.spinBox_start12.setDisabled(False)

	def write_order_values(self):
		order_counter = 1
		order_value = 1
		while order_counter < 25:
			for i in range(0, self.spinBox_orderAll.value()):
				if order_counter < 25:
					exec("self.spinBox_order{}.setValue({})".format(order_counter, order_value))
				order_counter += 1
			order_value += 1


	def write_start_values(self):
		for i in range(1,25):
			exec("self.spinBox_start{}.setValue({})".format(i, (i - 1) * self.spinBoxStartAll.value()))

	def write_duration_values(self):
		for i in range(1, 25):
			exec("self.spinBox_duration{}.setValue({})".format(i, self.spinBoxdurationAll.value()))

	def reset_relay_values(self):
		for i in range(1,25):
			exec("self.spinBox_start{}.setValue(0)".format(i))
			exec("self.spinBox_duration{}.setValue(0)".format(i))
	def reset_duration_values(self):
		for i in range(1,25):
			#exec("self.spinBox_start{}.setValue(0)".format(i))
			exec("self.spinBox_duration{}.setValue(0)".format(i))
	def reset_start_values(self):
		for i in range(1,25):
			exec("self.spinBox_start{}.setValue(0)".format(i))
			#exec("self.spinBox_duration{}.setValue(0)".format(i))


	def check_all_boxes(self, state):
		if state == QtCore.Qt.Checked:
			for i in range(1,25):
				exec("self.checkBox{}.setChecked(True)".format(i))
		else:
			for i in range(1,25):
				exec("self.checkBox{}.setChecked(False)".format(i))

	def check_8_boxes(self, state):
		if state == QtCore.Qt.Checked:
			for i in range(1,9):
				exec("self.checkBox{}.setChecked(True)".format(i))
		else:
			for i in range(1,9):
				exec("self.checkBox{}.setChecked(False)".format(i))

	def change_sequence_mode(self):
		if self.radioButton_automatic.isChecked():
			self.spinBox_orderAll.setDisabled(True)
			self.spinBoxStartAll.setDisabled(False)
			self.previewButton.setDisabled(False)
			checkbox_list_temp = []
			for i in range(1,25):
				exec("self.spinBox_order{}.setDisabled(True)".format(i))
				exec("checkbox_list_temp.append(self.checkBox{}.isChecked())".format(i))
				if checkbox_list_temp[i-1] is True:
					exec("self.spinBox_start{}.setDisabled(False)".format(i))
		elif self.radioButton_manual.isChecked():
			self.spinBox_orderAll.setDisabled(False)
			self.spinBoxStartAll.setDisabled(True)
			self.previewButton.setDisabled(True)
			checkbox_list_temp = []
			for i in range(1,25):
				exec("self.spinBox_start{}.setDisabled(True)".format(i))
				exec("checkbox_list_temp.append(self.checkBox{}.isChecked())".format(i))
				if checkbox_list_temp[i-1] is True:
					exec("self.spinBox_order{}.setDisabled(False)".format(i))

	def state_changedGlobal(self, state):
		image_base_path = "../../../ui-elements/01_sequence-controller/style_guide/icons/feedback_dots/"
		circle_disabled = "FeedbackCircles-01.png"
		circle_enabled = "FeedbackCircles-02.png"
		senderBox = self.sender()
		#print(str(senderBox.objectName())[8:])
		if state == QtCore.Qt.Checked:
			exec("self.textField_name%s.setDisabled(False)" % str(senderBox.objectName())[8:] )
			exec("self.spinBox_duration%s.setDisabled(False)" % str(senderBox.objectName())[8:] )
			exec("self.circleInactive%s.setPixmap(QtGui.QPixmap(image_base_path + circle_disabled))" % str(senderBox.objectName())[8:] )
			if self.radioButton_automatic.isChecked():
				exec("self.spinBox_start%s.setDisabled(False)" % str(senderBox.objectName())[8:] )
				exec("self.spinBox_order%s.setDisabled(True)" % str(senderBox.objectName())[8:] )
			elif self.radioButton_manual.isChecked():
				exec("self.spinBox_start%s.setDisabled(True)" % str(senderBox.objectName())[8:] )
				exec("self.spinBox_order%s.setDisabled(False)" % str(senderBox.objectName())[8:] )
		else:
			exec("self.textField_name%s.setDisabled(True)" % str(senderBox.objectName())[8:] )
			exec("self.spinBox_duration%s.setDisabled(True)" % str(senderBox.objectName())[8:] )
			exec("self.circleInactive%s.setPixmap(QtGui.QPixmap(image_base_path + circle_enabled))" % str(senderBox.objectName())[8:] )
			exec("self.spinBox_start%s.setDisabled(True)" % str(senderBox.objectName())[8:] )
			exec("self.spinBox_order%s.setDisabled(True)" % str(senderBox.objectName())[8:] )

			#vars()['textField_name' +
			#self.textField_name2.setText(str(senderBox.objectName()))
		self.update_cycle_duration()


	def state_changed(self, state):
		if state == QtCore.Qt.Checked:
			self.circleInactive12.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_disabled))
			self.textField_name12.setDisabled(False)
			self.spinBox_start12.setDisabled(False)
			self.spinBox_start12.setDisabled(False)
		else:
			self.circleInactive12.setPixmap(QtGui.QPixmap(self.image_base_path + self.circle_enabled))
			self.textField_name12.setDisabled(True)
			self.spinBox_start12.setDisabled(True)
			self.spinBox_duration12.setDisabled(True)


def main():
	app = QtWidgets.QApplication(sys.argv)  # A new instance of QApplication
	form = ExampleApp()                 # We set the form to be our (design)
	form.show()                         # Show the form
	app.exec_()                         # and execute the app


if __name__ == '__main__':              # if we're running file directly and not importing it
	main()                              # run the main function
