from PyQt5 import QtCore, QtGui, QtWidgets

import time

class ManualPreviewWorker(QtCore.QThread):
    relay_finished = QtCore.pyqtSignal(int)

    def __init__(self, time_delta, relay_number):
        QtCore.QThread.__init__(self)

        self.time_delta = time_delta
        self.relay_number = relay_number

    def run(self):
        print(self.time_delta)
        print(self.relay_number)

        start_time = int(round(time.time() * 1000)) #Get the current time for the reference
        while True:
            now = int(round(time.time() * 1000))
            if now >= start_time + self.time_delta:
                self.relay_finished.emit(self.relay_number)
                break
            time.sleep(0.001)
