/********************************************************************************
** Form generated from reading UI file 'mainwindowapp.ui'
**
** Created by: Qt User Interface Compiler version 5.11.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOWAPP_H
#define UI_MAINWINDOWAPP_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindowApp
{
public:
    QAction *actionSave;
    QAction *actionOpen;
    QWidget *centralWidget;
    QGridLayout *gridLayout_6;
    QTabWidget *tabWidget;
    QWidget *tab_2;
    QGridLayout *gridLayout_47;
    QGridLayout *gridLayout_4;
    QHBoxLayout *horizontalLayout_23;
    QSpacerItem *horizontalSpacer_179;
    QLabel *label_ID_13;
    QSpacerItem *horizontalSpacer_181;
    QLabel *label_start_13;
    QLabel *label_msStart_14;
    QSpacerItem *horizontalSpacer_5;
    QLabel *label_duration_13;
    QLabel *label_msDuration_24;
    QSpacerItem *horizontalSpacer_185;
    QSpacerItem *horizontalSpacer_184;
    QSpacerItem *horizontalSpacer_3;
    QGridLayout *gridLayout_44;
    QSpacerItem *horizontalSpacer_7;
    QLabel *label_duration_14;
    QHBoxLayout *horizontalLayout_24;
    QLineEdit *textFIeld_name1_22;
    QSpacerItem *verticalSpacer_51;
    QSpacerItem *horizontalSpacer_6;
    QSpacerItem *verticalSpacer_54;
    QLabel *label_msDuration_26;
    QSpacerItem *verticalSpacer_52;
    QSpacerItem *verticalSpacer_53;
    QLabel *label_duration_15;
    QSpacerItem *verticalSpacer;
    QSpacerItem *horizontalSpacer_8;
    QLabel *label_45;
    QProgressBar *progressBar_18;
    QFrame *line_12;
    QScrollArea *scrollArea_8;
    QWidget *scrollAreaWidgetContents_12;
    QGridLayout *gridLayout_45;
    QGridLayout *gridLayout_46;
    QLabel *label_ID4_167;
    QSpacerItem *horizontalSpacer_192;
    QCheckBox *checkBox_235;
    QSpacerItem *horizontalSpacer_193;
    QLineEdit *textFIeld_name1_12;
    QSpacerItem *horizontalSpacer_190;
    QLabel *circle_inactive3_79;
    QLabel *label_ID3_23;
    QLabel *circle_inactive3_83;
    QSpacerItem *horizontalSpacer_188;
    QLabel *label_ID2_23;
    QCheckBox *checkBox_224;
    QSpacerItem *horizontalSpacer_2;
    QProgressBar *progressBar_16;
    QLineEdit *textFIeld_name1_13;
    QProgressBar *progressBar_13;
    QLabel *label_ID1_12;
    QSpacerItem *horizontalSpacer_189;
    QCheckBox *checkBox_231;
    QCheckBox *checkBox_221;
    QLabel *circle_inactive3_82;
    QCheckBox *checkBox_232;
    QLineEdit *textFIeld_name1_17;
    QLineEdit *textFIeld_name1_21;
    QSpacerItem *horizontalSpacer_191;
    QLabel *circle_inactive3_81;
    QLabel *label_ID4_172;
    QLineEdit *textFIeld_name1_18;
    QSpacerItem *horizontalSpacer_4;
    QProgressBar *progressBar_15;
    QLabel *circle_inactive3_84;
    QLineEdit *textFIeld_name1_19;
    QLineEdit *textFIeld_name1_16;
    QLineEdit *textFIeld_name1_14;
    QLineEdit *textFIeld_name1_20;
    QProgressBar *progressBar_17;
    QProgressBar *progressBar_14;
    QLineEdit *textFIeld_name1_15;
    QWidget *Node_1;
    QGridLayout *gridLayout_3;
    QGridLayout *gridLayout_2;
    QHBoxLayout *horizontalLayout_21;
    QSpacerItem *horizontalSpacer_162;
    QSpacerItem *horizontalSpacer_163;
    QLabel *label_ID_12;
    QSpacerItem *horizontalSpacer_164;
    QLabel *label_name_13;
    QSpacerItem *horizontalSpacer_165;
    QSpacerItem *horizontalSpacer_166;
    QLabel *label_start_12;
    QLabel *label_msStart_13;
    QSpacerItem *horizontalSpacer_167;
    QLabel *label_duration_12;
    QLabel *label_msDuration_22;
    QSpacerItem *horizontalSpacer_168;
    QGridLayout *gridLayout_41;
    QHBoxLayout *horizontalLayout_22;
    QProgressBar *progressBar_11;
    QLabel *label_41;
    QSpinBox *spinBox_start5_151;
    QSpinBox *spinBox_start5_152;
    QPushButton *pushButton_43;
    QSpacerItem *horizontalSpacer_169;
    QSpacerItem *verticalSpacer_46;
    QPushButton *pushButton_44;
    QLabel *label_42;
    QLabel *label_43;
    QFrame *line_11;
    QSpacerItem *horizontalSpacer_170;
    QSpacerItem *verticalSpacer_47;
    QPushButton *pushButton_45;
    QLabel *label_msDuration_23;
    QSpacerItem *verticalSpacer_48;
    QPushButton *pushButton_46;
    QSpacerItem *verticalSpacer_49;
    QLabel *label_44;
    QSpacerItem *verticalSpacer_50;
    QScrollArea *scrollArea_7;
    QWidget *scrollAreaWidgetContents_11;
    QGridLayout *gridLayout_42;
    QGridLayout *gridLayout_43;
    QLineEdit *textFIeld_name5_131;
    QSpinBox *spinBox_start2_21;
    QLabel *label_ID4_151;
    QLabel *circle_inactive3_71;
    QSpacerItem *horizontalSpacer_171;
    QCheckBox *checkBox_201;
    QCheckBox *checkBox_202;
    QCheckBox *checkBox_203;
    QSpinBox *spinBox_start1_11;
    QCheckBox *checkBox_204;
    QLabel *circle_inactive3_72;
    QLineEdit *textFIeld_name1_11;
    QSpinBox *spinBox_duration2_21;
    QSpinBox *spinBox_duration4_21;
    QLineEdit *textFIeld_name4_21;
    QSpinBox *spinBox_duration5_131;
    QCheckBox *checkBox_205;
    QLineEdit *textFIeld_name2_21;
    QSpinBox *spinBox_duration5_132;
    QLineEdit *textFIeld_name5_132;
    QSpinBox *spinBox_start5_153;
    QLabel *label_ID4_152;
    QSpinBox *spinBox_start3_21;
    QLabel *label_ID4_153;
    QSpinBox *spinBox_duration4_22;
    QCheckBox *checkBox_206;
    QSpinBox *spinBox_start5_154;
    QSpinBox *spinBox_duration5_133;
    QLineEdit *textFIeld_name5_133;
    QLabel *label_ID4_154;
    QSpinBox *spinBox_start5_155;
    QLineEdit *textFIeld_name4_22;
    QSpinBox *spinBox_start3_22;
    QSpinBox *spinBox_start5_156;
    QSpacerItem *horizontalSpacer_172;
    QSpacerItem *horizontalSpacer_173;
    QSpinBox *spinBox_duration5_134;
    QLineEdit *textFIeld_name5_134;
    QLabel *label_ID2_21;
    QCheckBox *checkBox_207;
    QSpinBox *spinBox_start5_157;
    QLineEdit *textFIeld_name2_22;
    QLineEdit *textFIeld_name5_135;
    QCheckBox *checkBox_208;
    QLabel *label_ID4_155;
    QLabel *circle_inactive3_73;
    QLineEdit *textFIeld_name3_21;
    QSpinBox *spinBox_duration5_135;
    QLineEdit *textFIeld_name3_22;
    QSpinBox *spinBox_duration5_136;
    QCheckBox *checkBox_209;
    QLineEdit *textFIeld_name5_136;
    QLineEdit *textFIeld_name5_137;
    QSpinBox *spinBox_duration5_137;
    QLabel *label_ID4_156;
    QSpacerItem *horizontalSpacer_174;
    QLabel *label_ID4_157;
    QLineEdit *textFIeld_name5_138;
    QSpinBox *spinBox_start2_22;
    QSpinBox *spinBox_duration5_138;
    QLabel *label_ID4_158;
    QSpinBox *spinBox_duration5_139;
    QSpinBox *spinBox_duration5_140;
    QSpinBox *spinBox_start5_158;
    QLineEdit *textFIeld_name5_139;
    QSpinBox *spinBox_start5_159;
    QCheckBox *checkBox_210;
    QLabel *label_ID4_159;
    QLabel *label_ID4_160;
    QSpinBox *spinBox_duration3_21;
    QLabel *label_ID2_22;
    QLabel *label_ID4_161;
    QCheckBox *checkBox_211;
    QLabel *circle_inactive3_74;
    QLabel *label_ID4_162;
    QLabel *label_ID4_163;
    QSpinBox *spinBox_start4_21;
    QLineEdit *textFIeld_name5_140;
    QSpinBox *spinBox_start5_160;
    QSpinBox *spinBox_duration5_141;
    QLabel *label_ID3_21;
    QSpinBox *spinBox_duration1_11;
    QSpinBox *spinBox_duration5_142;
    QSpinBox *spinBox_start5_161;
    QSpinBox *spinBox_start4_22;
    QSpinBox *spinBox_duration2_22;
    QLabel *circle_inactive1_131;
    QCheckBox *checkBox_212;
    QSpinBox *spinBox_duration3_22;
    QCheckBox *checkBox_213;
    QCheckBox *checkBox_214;
    QCheckBox *checkBox_215;
    QLineEdit *textFIeld_name5_141;
    QCheckBox *checkBox_216;
    QSpinBox *spinBox_start5_162;
    QLabel *label_ID3_22;
    QLabel *label_ID1_11;
    QLineEdit *textFIeld_name5_142;
    QSpinBox *spinBox_duration5_143;
    QSpacerItem *horizontalSpacer_175;
    QLabel *label_ID4_164;
    QCheckBox *checkBox_217;
    QCheckBox *checkBox_218;
    QCheckBox *checkBox_219;
    QSpinBox *spinBox_start5_163;
    QCheckBox *checkBox_220;
    QSpinBox *spinBox_start5_164;
    QLabel *label_ID4_165;
    QLabel *circle_inactive3_75;
    QSpinBox *spinBox_start5_165;
    QLabel *circle_inactive1_132;
    QLineEdit *textFIeld_name5_143;
    QLabel *circle_inactive3_76;
    QSpacerItem *horizontalSpacer_176;
    QLabel *circle_inactive3_77;
    QLabel *circle_inactive1_133;
    QLabel *circle_inactive1_134;
    QLabel *circle_inactive1_135;
    QLabel *circle_inactive1_136;
    QLabel *circle_inactive1_137;
    QLabel *circle_inactive1_138;
    QLabel *circle_inactive1_139;
    QLabel *circle_inactive1_140;
    QLabel *circle_inactive1_141;
    QLabel *circle_inactive1_142;
    QLabel *circle_inactive1_143;
    QSpacerItem *horizontalSpacer_177;
    QSpacerItem *horizontalSpacer;
    QWidget *tab_3;
    QGridLayout *gridLayout_21;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_9;
    QSpacerItem *horizontalSpacer_55;
    QSpacerItem *horizontalSpacer_56;
    QLabel *label_ID_6;
    QSpacerItem *horizontalSpacer_65;
    QLabel *label_name_7;
    QSpacerItem *horizontalSpacer_66;
    QSpacerItem *horizontalSpacer_67;
    QLabel *label_start_6;
    QLabel *label_msStart_7;
    QSpacerItem *horizontalSpacer_68;
    QLabel *label_duration_6;
    QLabel *label_msDuration_10;
    QSpacerItem *horizontalSpacer_69;
    QGridLayout *gridLayout_17;
    QHBoxLayout *horizontalLayout_10;
    QProgressBar *progressBar_5;
    QLabel *label_18;
    QSpinBox *spinBox_start5_75;
    QSpinBox *spinBox_start5_74;
    QPushButton *pushButton_21;
    QSpacerItem *horizontalSpacer_77;
    QSpacerItem *verticalSpacer_21;
    QPushButton *pushButton_19;
    QLabel *label_17;
    QLabel *label_19;
    QFrame *line_5;
    QSpacerItem *horizontalSpacer_76;
    QSpacerItem *verticalSpacer_6;
    QPushButton *pushButton_20;
    QLabel *label_msDuration_11;
    QSpacerItem *verticalSpacer_19;
    QPushButton *pushButton_22;
    QSpacerItem *verticalSpacer_20;
    QLabel *label_20;
    QSpacerItem *verticalSpacer_35;
    QScrollArea *scrollArea_2;
    QWidget *scrollAreaWidgetContents_5;
    QGridLayout *gridLayout_15;
    QGridLayout *gridLayout_16;
    QLineEdit *textFIeld_name5_53;
    QSpinBox *spinBox_start2_9;
    QLabel *label_ID4_61;
    QLabel *circle_inactive3_29;
    QSpacerItem *horizontalSpacer_70;
    QCheckBox *checkBox_81;
    QCheckBox *checkBox_82;
    QCheckBox *checkBox_83;
    QSpinBox *spinBox_start1_5;
    QCheckBox *checkBox_84;
    QLabel *circle_inactive3_30;
    QLineEdit *textFIeld_name1_5;
    QSpinBox *spinBox_duration2_9;
    QSpinBox *spinBox_duration4_9;
    QLineEdit *textFIeld_name4_9;
    QSpinBox *spinBox_duration5_53;
    QCheckBox *checkBox_85;
    QLineEdit *textFIeld_name2_9;
    QSpinBox *spinBox_duration5_54;
    QLineEdit *textFIeld_name5_54;
    QSpinBox *spinBox_start5_61;
    QLabel *label_ID4_62;
    QSpinBox *spinBox_start3_9;
    QLabel *label_ID4_63;
    QSpinBox *spinBox_duration4_10;
    QCheckBox *checkBox_86;
    QSpinBox *spinBox_start5_62;
    QSpinBox *spinBox_duration5_55;
    QLineEdit *textFIeld_name5_55;
    QLabel *label_ID4_64;
    QSpinBox *spinBox_start5_63;
    QLineEdit *textFIeld_name4_10;
    QSpinBox *spinBox_start3_10;
    QSpinBox *spinBox_start5_64;
    QSpacerItem *horizontalSpacer_71;
    QSpacerItem *horizontalSpacer_72;
    QSpinBox *spinBox_duration5_56;
    QLineEdit *textFIeld_name5_56;
    QLabel *label_ID2_9;
    QCheckBox *checkBox_87;
    QSpinBox *spinBox_start5_65;
    QLineEdit *textFIeld_name2_10;
    QLineEdit *textFIeld_name5_57;
    QCheckBox *checkBox_88;
    QLabel *label_ID4_65;
    QLabel *circle_inactive3_31;
    QLineEdit *textFIeld_name3_9;
    QSpinBox *spinBox_duration5_57;
    QLineEdit *textFIeld_name3_10;
    QSpinBox *spinBox_duration5_58;
    QCheckBox *checkBox_89;
    QLineEdit *textFIeld_name5_58;
    QLineEdit *textFIeld_name5_59;
    QSpinBox *spinBox_duration5_59;
    QLabel *label_ID4_66;
    QSpacerItem *horizontalSpacer_73;
    QLabel *label_ID4_67;
    QLineEdit *textFIeld_name5_60;
    QSpinBox *spinBox_start2_10;
    QSpinBox *spinBox_duration5_60;
    QLabel *label_ID4_68;
    QSpinBox *spinBox_duration5_61;
    QSpinBox *spinBox_duration5_62;
    QSpinBox *spinBox_start5_66;
    QLineEdit *textFIeld_name5_61;
    QSpinBox *spinBox_start5_67;
    QCheckBox *checkBox_90;
    QLabel *label_ID4_69;
    QLabel *label_ID4_70;
    QSpinBox *spinBox_duration3_9;
    QLabel *label_ID2_10;
    QLabel *label_ID4_71;
    QCheckBox *checkBox_91;
    QLabel *circle_inactive3_32;
    QLabel *label_ID4_72;
    QLabel *label_ID4_73;
    QSpinBox *spinBox_start4_9;
    QLineEdit *textFIeld_name5_62;
    QSpinBox *spinBox_start5_68;
    QSpinBox *spinBox_duration5_63;
    QLabel *label_ID3_9;
    QSpinBox *spinBox_duration1_5;
    QSpinBox *spinBox_duration5_64;
    QSpinBox *spinBox_start5_69;
    QSpinBox *spinBox_start4_10;
    QSpinBox *spinBox_duration2_10;
    QLabel *circle_inactive1_53;
    QCheckBox *checkBox_92;
    QSpinBox *spinBox_duration3_10;
    QCheckBox *checkBox_93;
    QCheckBox *checkBox_94;
    QCheckBox *checkBox_95;
    QLineEdit *textFIeld_name5_63;
    QCheckBox *checkBox_96;
    QSpinBox *spinBox_start5_70;
    QLabel *label_ID3_10;
    QLabel *label_ID1_5;
    QLineEdit *textFIeld_name5_64;
    QSpinBox *spinBox_duration5_65;
    QSpacerItem *horizontalSpacer_74;
    QLabel *label_ID4_74;
    QCheckBox *checkBox_97;
    QCheckBox *checkBox_98;
    QCheckBox *checkBox_99;
    QSpinBox *spinBox_start5_71;
    QCheckBox *checkBox_100;
    QSpinBox *spinBox_start5_72;
    QLabel *label_ID4_75;
    QLabel *circle_inactive3_33;
    QSpinBox *spinBox_start5_73;
    QLabel *circle_inactive1_54;
    QLineEdit *textFIeld_name5_65;
    QLabel *circle_inactive3_34;
    QSpacerItem *horizontalSpacer_75;
    QLabel *circle_inactive3_35;
    QLabel *circle_inactive1_55;
    QLabel *circle_inactive1_56;
    QLabel *circle_inactive1_57;
    QLabel *circle_inactive1_58;
    QLabel *circle_inactive1_59;
    QLabel *circle_inactive1_60;
    QLabel *circle_inactive1_61;
    QLabel *circle_inactive1_62;
    QLabel *circle_inactive1_63;
    QLabel *circle_inactive1_64;
    QLabel *circle_inactive1_65;
    QSpacerItem *horizontalSpacer_79;
    QSpacerItem *horizontalSpacer_78;
    QWidget *tab_4;
    QGridLayout *gridLayout_30;
    QGridLayout *gridLayout_26;
    QHBoxLayout *horizontalLayout_15;
    QSpacerItem *horizontalSpacer_111;
    QSpacerItem *horizontalSpacer_112;
    QLabel *label_ID_9;
    QSpacerItem *horizontalSpacer_113;
    QLabel *label_name_10;
    QSpacerItem *horizontalSpacer_114;
    QSpacerItem *horizontalSpacer_115;
    QLabel *label_start_9;
    QLabel *label_msStart_10;
    QSpacerItem *horizontalSpacer_116;
    QLabel *label_duration_9;
    QLabel *label_msDuration_16;
    QSpacerItem *horizontalSpacer_117;
    QGridLayout *gridLayout_27;
    QSpacerItem *horizontalSpacer_119;
    QSpacerItem *verticalSpacer_32;
    QHBoxLayout *horizontalLayout_16;
    QProgressBar *progressBar_8;
    QLabel *label_30;
    QLabel *label_32;
    QLabel *label_31;
    QSpacerItem *verticalSpacer_31;
    QSpinBox *spinBox_start5_107;
    QSpacerItem *verticalSpacer_33;
    QLabel *label_29;
    QPushButton *pushButton_31;
    QPushButton *pushButton_33;
    QSpacerItem *horizontalSpacer_118;
    QSpinBox *spinBox_start5_106;
    QPushButton *pushButton_32;
    QSpacerItem *verticalSpacer_30;
    QFrame *line_8;
    QPushButton *pushButton_34;
    QLabel *label_msDuration_17;
    QSpacerItem *verticalSpacer_34;
    QScrollArea *scrollArea_4;
    QWidget *scrollAreaWidgetContents_8;
    QGridLayout *gridLayout_28;
    QGridLayout *gridLayout_29;
    QLineEdit *textFIeld_name5_92;
    QSpinBox *spinBox_start2_15;
    QLabel *label_ID4_106;
    QLabel *circle_inactive3_50;
    QSpacerItem *horizontalSpacer_120;
    QCheckBox *checkBox_141;
    QCheckBox *checkBox_142;
    QCheckBox *checkBox_143;
    QSpinBox *spinBox_start1_8;
    QCheckBox *checkBox_144;
    QLabel *circle_inactive3_51;
    QLineEdit *textFIeld_name1_8;
    QSpinBox *spinBox_duration2_15;
    QSpinBox *spinBox_duration4_15;
    QLineEdit *textFIeld_name4_15;
    QSpinBox *spinBox_duration5_92;
    QCheckBox *checkBox_145;
    QLineEdit *textFIeld_name2_15;
    QSpinBox *spinBox_duration5_93;
    QLineEdit *textFIeld_name5_93;
    QSpinBox *spinBox_start5_108;
    QLabel *label_ID4_107;
    QSpinBox *spinBox_start3_15;
    QLabel *label_ID4_108;
    QSpinBox *spinBox_duration4_16;
    QCheckBox *checkBox_146;
    QSpinBox *spinBox_start5_109;
    QSpinBox *spinBox_duration5_94;
    QLineEdit *textFIeld_name5_94;
    QLabel *label_ID4_109;
    QSpinBox *spinBox_start5_110;
    QLineEdit *textFIeld_name4_16;
    QSpinBox *spinBox_start3_16;
    QSpinBox *spinBox_start5_111;
    QSpacerItem *horizontalSpacer_121;
    QSpacerItem *horizontalSpacer_122;
    QSpinBox *spinBox_duration5_95;
    QLineEdit *textFIeld_name5_95;
    QLabel *label_ID2_15;
    QCheckBox *checkBox_147;
    QSpinBox *spinBox_start5_112;
    QLineEdit *textFIeld_name2_16;
    QLineEdit *textFIeld_name5_96;
    QCheckBox *checkBox_148;
    QLabel *label_ID4_110;
    QLabel *circle_inactive3_52;
    QLineEdit *textFIeld_name3_15;
    QSpinBox *spinBox_duration5_96;
    QLineEdit *textFIeld_name3_16;
    QSpinBox *spinBox_duration5_97;
    QCheckBox *checkBox_149;
    QLineEdit *textFIeld_name5_97;
    QLineEdit *textFIeld_name5_98;
    QSpinBox *spinBox_duration5_98;
    QLabel *label_ID4_111;
    QSpacerItem *horizontalSpacer_123;
    QLabel *label_ID4_112;
    QLineEdit *textFIeld_name5_99;
    QSpinBox *spinBox_start2_16;
    QSpinBox *spinBox_duration5_99;
    QLabel *label_ID4_113;
    QSpinBox *spinBox_duration5_100;
    QSpinBox *spinBox_duration5_101;
    QSpinBox *spinBox_start5_113;
    QLineEdit *textFIeld_name5_100;
    QSpinBox *spinBox_start5_114;
    QCheckBox *checkBox_150;
    QLabel *label_ID4_114;
    QLabel *label_ID4_115;
    QSpinBox *spinBox_duration3_15;
    QLabel *label_ID2_16;
    QLabel *label_ID4_116;
    QCheckBox *checkBox_151;
    QLabel *circle_inactive3_53;
    QLabel *label_ID4_117;
    QLabel *label_ID4_118;
    QSpinBox *spinBox_start4_15;
    QLineEdit *textFIeld_name5_101;
    QSpinBox *spinBox_start5_115;
    QSpinBox *spinBox_duration5_102;
    QLabel *label_ID3_15;
    QSpinBox *spinBox_duration1_8;
    QSpinBox *spinBox_duration5_103;
    QSpinBox *spinBox_start5_116;
    QSpinBox *spinBox_start4_16;
    QSpinBox *spinBox_duration2_16;
    QLabel *circle_inactive1_92;
    QCheckBox *checkBox_152;
    QSpinBox *spinBox_duration3_16;
    QCheckBox *checkBox_153;
    QCheckBox *checkBox_154;
    QCheckBox *checkBox_155;
    QLineEdit *textFIeld_name5_102;
    QCheckBox *checkBox_156;
    QSpinBox *spinBox_start5_117;
    QLabel *label_ID3_16;
    QLabel *label_ID1_8;
    QLineEdit *textFIeld_name5_103;
    QSpinBox *spinBox_duration5_104;
    QSpacerItem *horizontalSpacer_124;
    QLabel *label_ID4_119;
    QCheckBox *checkBox_157;
    QCheckBox *checkBox_158;
    QCheckBox *checkBox_159;
    QSpinBox *spinBox_start5_118;
    QCheckBox *checkBox_160;
    QSpinBox *spinBox_start5_119;
    QLabel *label_ID4_120;
    QLabel *circle_inactive3_54;
    QSpinBox *spinBox_start5_120;
    QLabel *circle_inactive1_93;
    QLineEdit *textFIeld_name5_104;
    QLabel *circle_inactive3_55;
    QSpacerItem *horizontalSpacer_125;
    QLabel *circle_inactive3_56;
    QLabel *circle_inactive1_94;
    QLabel *circle_inactive1_95;
    QLabel *circle_inactive1_96;
    QLabel *circle_inactive1_97;
    QLabel *circle_inactive1_98;
    QLabel *circle_inactive1_99;
    QLabel *circle_inactive1_100;
    QLabel *circle_inactive1_101;
    QLabel *circle_inactive1_102;
    QLabel *circle_inactive1_103;
    QLabel *circle_inactive1_104;
    QSpacerItem *horizontalSpacer_126;
    QSpacerItem *horizontalSpacer_127;
    QWidget *tab_5;
    QGridLayout *gridLayout_35;
    QGridLayout *gridLayout_31;
    QHBoxLayout *horizontalLayout_17;
    QSpacerItem *horizontalSpacer_128;
    QSpacerItem *horizontalSpacer_129;
    QLabel *label_ID_10;
    QSpacerItem *horizontalSpacer_130;
    QLabel *label_name_11;
    QSpacerItem *horizontalSpacer_131;
    QSpacerItem *horizontalSpacer_132;
    QLabel *label_start_10;
    QLabel *label_msStart_11;
    QSpacerItem *horizontalSpacer_133;
    QLabel *label_duration_10;
    QLabel *label_msDuration_18;
    QSpacerItem *horizontalSpacer_134;
    QGridLayout *gridLayout_32;
    QHBoxLayout *horizontalLayout_18;
    QProgressBar *progressBar_9;
    QLabel *label_33;
    QSpinBox *spinBox_start5_121;
    QSpinBox *spinBox_start5_122;
    QPushButton *pushButton_35;
    QSpacerItem *horizontalSpacer_135;
    QSpacerItem *verticalSpacer_36;
    QPushButton *pushButton_36;
    QLabel *label_34;
    QLabel *label_35;
    QFrame *line_9;
    QSpacerItem *horizontalSpacer_136;
    QSpacerItem *verticalSpacer_37;
    QPushButton *pushButton_37;
    QLabel *label_msDuration_19;
    QSpacerItem *verticalSpacer_38;
    QPushButton *pushButton_38;
    QSpacerItem *verticalSpacer_39;
    QLabel *label_36;
    QSpacerItem *verticalSpacer_40;
    QScrollArea *scrollArea_5;
    QWidget *scrollAreaWidgetContents_9;
    QGridLayout *gridLayout_33;
    QGridLayout *gridLayout_34;
    QLineEdit *textFIeld_name5_105;
    QSpinBox *spinBox_start2_17;
    QLabel *label_ID4_121;
    QLabel *circle_inactive3_57;
    QSpacerItem *horizontalSpacer_137;
    QCheckBox *checkBox_161;
    QCheckBox *checkBox_162;
    QCheckBox *checkBox_163;
    QSpinBox *spinBox_start1_9;
    QCheckBox *checkBox_164;
    QLabel *circle_inactive3_58;
    QLineEdit *textFIeld_name1_9;
    QSpinBox *spinBox_duration2_17;
    QSpinBox *spinBox_duration4_17;
    QLineEdit *textFIeld_name4_17;
    QSpinBox *spinBox_duration5_105;
    QCheckBox *checkBox_165;
    QLineEdit *textFIeld_name2_17;
    QSpinBox *spinBox_duration5_106;
    QLineEdit *textFIeld_name5_106;
    QSpinBox *spinBox_start5_123;
    QLabel *label_ID4_122;
    QSpinBox *spinBox_start3_17;
    QLabel *label_ID4_123;
    QSpinBox *spinBox_duration4_18;
    QCheckBox *checkBox_166;
    QSpinBox *spinBox_start5_124;
    QSpinBox *spinBox_duration5_107;
    QLineEdit *textFIeld_name5_107;
    QLabel *label_ID4_124;
    QSpinBox *spinBox_start5_125;
    QLineEdit *textFIeld_name4_18;
    QSpinBox *spinBox_start3_18;
    QSpinBox *spinBox_start5_126;
    QSpacerItem *horizontalSpacer_138;
    QSpacerItem *horizontalSpacer_139;
    QSpinBox *spinBox_duration5_108;
    QLineEdit *textFIeld_name5_108;
    QLabel *label_ID2_17;
    QCheckBox *checkBox_167;
    QSpinBox *spinBox_start5_127;
    QLineEdit *textFIeld_name2_18;
    QLineEdit *textFIeld_name5_109;
    QCheckBox *checkBox_168;
    QLabel *label_ID4_125;
    QLabel *circle_inactive3_59;
    QLineEdit *textFIeld_name3_17;
    QSpinBox *spinBox_duration5_109;
    QLineEdit *textFIeld_name3_18;
    QSpinBox *spinBox_duration5_110;
    QCheckBox *checkBox_169;
    QLineEdit *textFIeld_name5_110;
    QLineEdit *textFIeld_name5_111;
    QSpinBox *spinBox_duration5_111;
    QLabel *label_ID4_126;
    QSpacerItem *horizontalSpacer_140;
    QLabel *label_ID4_127;
    QLineEdit *textFIeld_name5_112;
    QSpinBox *spinBox_start2_18;
    QSpinBox *spinBox_duration5_112;
    QLabel *label_ID4_128;
    QSpinBox *spinBox_duration5_113;
    QSpinBox *spinBox_duration5_114;
    QSpinBox *spinBox_start5_128;
    QLineEdit *textFIeld_name5_113;
    QSpinBox *spinBox_start5_129;
    QCheckBox *checkBox_170;
    QLabel *label_ID4_129;
    QLabel *label_ID4_130;
    QSpinBox *spinBox_duration3_17;
    QLabel *label_ID2_18;
    QLabel *label_ID4_131;
    QCheckBox *checkBox_171;
    QLabel *circle_inactive3_60;
    QLabel *label_ID4_132;
    QLabel *label_ID4_133;
    QSpinBox *spinBox_start4_17;
    QLineEdit *textFIeld_name5_114;
    QSpinBox *spinBox_start5_130;
    QSpinBox *spinBox_duration5_115;
    QLabel *label_ID3_17;
    QSpinBox *spinBox_duration1_9;
    QSpinBox *spinBox_duration5_116;
    QSpinBox *spinBox_start5_131;
    QSpinBox *spinBox_start4_18;
    QSpinBox *spinBox_duration2_18;
    QLabel *circle_inactive1_105;
    QCheckBox *checkBox_172;
    QSpinBox *spinBox_duration3_18;
    QCheckBox *checkBox_173;
    QCheckBox *checkBox_174;
    QCheckBox *checkBox_175;
    QLineEdit *textFIeld_name5_115;
    QCheckBox *checkBox_176;
    QSpinBox *spinBox_start5_132;
    QLabel *label_ID3_18;
    QLabel *label_ID1_9;
    QLineEdit *textFIeld_name5_116;
    QSpinBox *spinBox_duration5_117;
    QSpacerItem *horizontalSpacer_141;
    QLabel *label_ID4_134;
    QCheckBox *checkBox_177;
    QCheckBox *checkBox_178;
    QCheckBox *checkBox_179;
    QSpinBox *spinBox_start5_133;
    QCheckBox *checkBox_180;
    QSpinBox *spinBox_start5_134;
    QLabel *label_ID4_135;
    QLabel *circle_inactive3_61;
    QSpinBox *spinBox_start5_135;
    QLabel *circle_inactive1_106;
    QLineEdit *textFIeld_name5_117;
    QLabel *circle_inactive3_62;
    QSpacerItem *horizontalSpacer_142;
    QLabel *circle_inactive3_63;
    QLabel *circle_inactive1_107;
    QLabel *circle_inactive1_108;
    QLabel *circle_inactive1_109;
    QLabel *circle_inactive1_110;
    QLabel *circle_inactive1_111;
    QLabel *circle_inactive1_112;
    QLabel *circle_inactive1_113;
    QLabel *circle_inactive1_114;
    QLabel *circle_inactive1_115;
    QLabel *circle_inactive1_116;
    QLabel *circle_inactive1_117;
    QSpacerItem *horizontalSpacer_143;
    QSpacerItem *horizontalSpacer_144;
    QWidget *tab;
    QGridLayout *gridLayout_40;
    QGridLayout *gridLayout_36;
    QHBoxLayout *horizontalLayout_19;
    QSpacerItem *horizontalSpacer_145;
    QSpacerItem *horizontalSpacer_146;
    QLabel *label_ID_11;
    QSpacerItem *horizontalSpacer_147;
    QLabel *label_name_12;
    QSpacerItem *horizontalSpacer_148;
    QSpacerItem *horizontalSpacer_149;
    QLabel *label_start_11;
    QLabel *label_msStart_12;
    QSpacerItem *horizontalSpacer_150;
    QLabel *label_duration_11;
    QLabel *label_msDuration_20;
    QSpacerItem *horizontalSpacer_151;
    QGridLayout *gridLayout_37;
    QHBoxLayout *horizontalLayout_20;
    QProgressBar *progressBar_10;
    QLabel *label_37;
    QSpinBox *spinBox_start5_136;
    QSpinBox *spinBox_start5_137;
    QPushButton *pushButton_39;
    QSpacerItem *horizontalSpacer_152;
    QSpacerItem *verticalSpacer_41;
    QPushButton *pushButton_40;
    QLabel *label_38;
    QLabel *label_39;
    QFrame *line_10;
    QSpacerItem *horizontalSpacer_153;
    QSpacerItem *verticalSpacer_42;
    QPushButton *pushButton_41;
    QLabel *label_msDuration_21;
    QSpacerItem *verticalSpacer_43;
    QPushButton *pushButton_42;
    QSpacerItem *verticalSpacer_44;
    QLabel *label_40;
    QSpacerItem *verticalSpacer_45;
    QScrollArea *scrollArea_6;
    QWidget *scrollAreaWidgetContents_10;
    QGridLayout *gridLayout_38;
    QGridLayout *gridLayout_39;
    QLineEdit *textFIeld_name5_118;
    QSpinBox *spinBox_start2_19;
    QLabel *label_ID4_136;
    QLabel *circle_inactive3_64;
    QSpacerItem *horizontalSpacer_154;
    QCheckBox *checkBox_181;
    QCheckBox *checkBox_182;
    QCheckBox *checkBox_183;
    QSpinBox *spinBox_start1_10;
    QCheckBox *checkBox_184;
    QLabel *circle_inactive3_65;
    QLineEdit *textFIeld_name1_10;
    QSpinBox *spinBox_duration2_19;
    QSpinBox *spinBox_duration4_19;
    QLineEdit *textFIeld_name4_19;
    QSpinBox *spinBox_duration5_118;
    QCheckBox *checkBox_185;
    QLineEdit *textFIeld_name2_19;
    QSpinBox *spinBox_duration5_119;
    QLineEdit *textFIeld_name5_119;
    QSpinBox *spinBox_start5_138;
    QLabel *label_ID4_137;
    QSpinBox *spinBox_start3_19;
    QLabel *label_ID4_138;
    QSpinBox *spinBox_duration4_20;
    QCheckBox *checkBox_186;
    QSpinBox *spinBox_start5_139;
    QSpinBox *spinBox_duration5_120;
    QLineEdit *textFIeld_name5_120;
    QLabel *label_ID4_139;
    QSpinBox *spinBox_start5_140;
    QLineEdit *textFIeld_name4_20;
    QSpinBox *spinBox_start3_20;
    QSpinBox *spinBox_start5_141;
    QSpacerItem *horizontalSpacer_155;
    QSpacerItem *horizontalSpacer_156;
    QSpinBox *spinBox_duration5_121;
    QLineEdit *textFIeld_name5_121;
    QLabel *label_ID2_19;
    QCheckBox *checkBox_187;
    QSpinBox *spinBox_start5_142;
    QLineEdit *textFIeld_name2_20;
    QLineEdit *textFIeld_name5_122;
    QCheckBox *checkBox_188;
    QLabel *label_ID4_140;
    QLabel *circle_inactive3_66;
    QLineEdit *textFIeld_name3_19;
    QSpinBox *spinBox_duration5_122;
    QLineEdit *textFIeld_name3_20;
    QSpinBox *spinBox_duration5_123;
    QCheckBox *checkBox_189;
    QLineEdit *textFIeld_name5_123;
    QLineEdit *textFIeld_name5_124;
    QSpinBox *spinBox_duration5_124;
    QLabel *label_ID4_141;
    QSpacerItem *horizontalSpacer_157;
    QLabel *label_ID4_142;
    QLineEdit *textFIeld_name5_125;
    QSpinBox *spinBox_start2_20;
    QSpinBox *spinBox_duration5_125;
    QLabel *label_ID4_143;
    QSpinBox *spinBox_duration5_126;
    QSpinBox *spinBox_duration5_127;
    QSpinBox *spinBox_start5_143;
    QLineEdit *textFIeld_name5_126;
    QSpinBox *spinBox_start5_144;
    QCheckBox *checkBox_190;
    QLabel *label_ID4_144;
    QLabel *label_ID4_145;
    QSpinBox *spinBox_duration3_19;
    QLabel *label_ID2_20;
    QLabel *label_ID4_146;
    QCheckBox *checkBox_191;
    QLabel *circle_inactive3_67;
    QLabel *label_ID4_147;
    QLabel *label_ID4_148;
    QSpinBox *spinBox_start4_19;
    QLineEdit *textFIeld_name5_127;
    QSpinBox *spinBox_start5_145;
    QSpinBox *spinBox_duration5_128;
    QLabel *label_ID3_19;
    QSpinBox *spinBox_duration1_10;
    QSpinBox *spinBox_duration5_129;
    QSpinBox *spinBox_start5_146;
    QSpinBox *spinBox_start4_20;
    QSpinBox *spinBox_duration2_20;
    QLabel *circle_inactive1_118;
    QCheckBox *checkBox_192;
    QSpinBox *spinBox_duration3_20;
    QCheckBox *checkBox_193;
    QCheckBox *checkBox_194;
    QCheckBox *checkBox_195;
    QLineEdit *textFIeld_name5_128;
    QCheckBox *checkBox_196;
    QSpinBox *spinBox_start5_147;
    QLabel *label_ID3_20;
    QLabel *label_ID1_10;
    QLineEdit *textFIeld_name5_129;
    QSpinBox *spinBox_duration5_130;
    QSpacerItem *horizontalSpacer_158;
    QLabel *label_ID4_149;
    QCheckBox *checkBox_197;
    QCheckBox *checkBox_198;
    QCheckBox *checkBox_199;
    QSpinBox *spinBox_start5_148;
    QCheckBox *checkBox_200;
    QSpinBox *spinBox_start5_149;
    QLabel *label_ID4_150;
    QLabel *circle_inactive3_68;
    QSpinBox *spinBox_start5_150;
    QLabel *circle_inactive1_119;
    QLineEdit *textFIeld_name5_130;
    QLabel *circle_inactive3_69;
    QSpacerItem *horizontalSpacer_159;
    QLabel *circle_inactive3_70;
    QLabel *circle_inactive1_120;
    QLabel *circle_inactive1_121;
    QLabel *circle_inactive1_122;
    QLabel *circle_inactive1_123;
    QLabel *circle_inactive1_124;
    QLabel *circle_inactive1_125;
    QLabel *circle_inactive1_126;
    QLabel *circle_inactive1_127;
    QLabel *circle_inactive1_128;
    QLabel *circle_inactive1_129;
    QLabel *circle_inactive1_130;
    QSpacerItem *horizontalSpacer_160;
    QSpacerItem *horizontalSpacer_161;
    QMenuBar *menuBar;
    QMenu *menuOpen;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindowApp)
    {
        if (MainWindowApp->objectName().isEmpty())
            MainWindowApp->setObjectName(QStringLiteral("MainWindowApp"));
        MainWindowApp->resize(1061, 697);
        actionSave = new QAction(MainWindowApp);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionOpen = new QAction(MainWindowApp);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        centralWidget = new QWidget(MainWindowApp);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout_6 = new QGridLayout(centralWidget);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setMinimumSize(QSize(150, 300));
        QPalette palette;
        QBrush brush(QColor(25, 46, 71, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(226, 234, 249, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        QBrush brush2(QColor(167, 224, 255, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Light, brush2);
        QBrush brush3(QColor(136, 189, 218, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        QBrush brush4(QColor(53, 77, 91, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush4);
        QBrush brush5(QColor(70, 103, 121, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush5);
        QBrush brush6(QColor(0, 0, 0, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Text, brush6);
        QBrush brush7(QColor(255, 255, 255, 255));
        brush7.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush7);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush6);
        palette.setBrush(QPalette::Active, QPalette::Base, brush7);
        QBrush brush8(QColor(106, 155, 182, 255));
        brush8.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Window, brush8);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush6);
        QBrush brush9(QColor(180, 205, 218, 255));
        brush9.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush9);
        QBrush brush10(QColor(255, 255, 220, 255));
        brush10.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush10);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush8);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush7);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush7);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush8);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush9);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush10);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush6);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush8);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush7);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush8);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush8);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush6);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush8);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush10);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush6);
        tabWidget->setPalette(palette);
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        gridLayout_47 = new QGridLayout(tab_2);
        gridLayout_47->setSpacing(6);
        gridLayout_47->setContentsMargins(11, 11, 11, 11);
        gridLayout_47->setObjectName(QStringLiteral("gridLayout_47"));
        gridLayout_4 = new QGridLayout();
        gridLayout_4->setSpacing(6);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        horizontalLayout_23 = new QHBoxLayout();
        horizontalLayout_23->setSpacing(6);
        horizontalLayout_23->setObjectName(QStringLiteral("horizontalLayout_23"));
        horizontalSpacer_179 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_23->addItem(horizontalSpacer_179);

        label_ID_13 = new QLabel(tab_2);
        label_ID_13->setObjectName(QStringLiteral("label_ID_13"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Ignored);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_ID_13->sizePolicy().hasHeightForWidth());
        label_ID_13->setSizePolicy(sizePolicy);
        label_ID_13->setMaximumSize(QSize(16777215, 16777215));
        QFont font;
        font.setFamily(QStringLiteral("Titillium Web"));
        font.setPointSize(20);
        font.setBold(true);
        font.setWeight(75);
        label_ID_13->setFont(font);
        label_ID_13->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_23->addWidget(label_ID_13);

        horizontalSpacer_181 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_23->addItem(horizontalSpacer_181);

        label_start_13 = new QLabel(tab_2);
        label_start_13->setObjectName(QStringLiteral("label_start_13"));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label_start_13->sizePolicy().hasHeightForWidth());
        label_start_13->setSizePolicy(sizePolicy1);
        label_start_13->setFont(font);
        label_start_13->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_23->addWidget(label_start_13);

        label_msStart_14 = new QLabel(tab_2);
        label_msStart_14->setObjectName(QStringLiteral("label_msStart_14"));
        QFont font1;
        font1.setFamily(QStringLiteral("Titillium Web"));
        label_msStart_14->setFont(font1);
        label_msStart_14->setAlignment(Qt::AlignCenter);

        horizontalLayout_23->addWidget(label_msStart_14);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_23->addItem(horizontalSpacer_5);

        label_duration_13 = new QLabel(tab_2);
        label_duration_13->setObjectName(QStringLiteral("label_duration_13"));
        label_duration_13->setFont(font);
        label_duration_13->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_23->addWidget(label_duration_13);

        label_msDuration_24 = new QLabel(tab_2);
        label_msDuration_24->setObjectName(QStringLiteral("label_msDuration_24"));
        label_msDuration_24->setFont(font1);
        label_msDuration_24->setAlignment(Qt::AlignCenter);

        horizontalLayout_23->addWidget(label_msDuration_24);

        horizontalSpacer_185 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_23->addItem(horizontalSpacer_185);

        horizontalSpacer_184 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_23->addItem(horizontalSpacer_184);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_23->addItem(horizontalSpacer_3);


        gridLayout_4->addLayout(horizontalLayout_23, 0, 0, 1, 1);

        gridLayout_44 = new QGridLayout();
        gridLayout_44->setSpacing(6);
        gridLayout_44->setObjectName(QStringLiteral("gridLayout_44"));
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_44->addItem(horizontalSpacer_7, 1, 0, 1, 1);

        label_duration_14 = new QLabel(tab_2);
        label_duration_14->setObjectName(QStringLiteral("label_duration_14"));
        label_duration_14->setFont(font);
        label_duration_14->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_44->addWidget(label_duration_14, 1, 2, 1, 1);

        horizontalLayout_24 = new QHBoxLayout();
        horizontalLayout_24->setSpacing(6);
        horizontalLayout_24->setObjectName(QStringLiteral("horizontalLayout_24"));

        gridLayout_44->addLayout(horizontalLayout_24, 3, 0, 1, 7);

        textFIeld_name1_22 = new QLineEdit(tab_2);
        textFIeld_name1_22->setObjectName(QStringLiteral("textFIeld_name1_22"));
        textFIeld_name1_22->setEnabled(false);
        QFont font2;
        font2.setFamily(QStringLiteral("Titillium Web"));
        font2.setPointSize(13);
        textFIeld_name1_22->setFont(font2);

        gridLayout_44->addWidget(textFIeld_name1_22, 2, 2, 1, 2);

        verticalSpacer_51 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_44->addItem(verticalSpacer_51, 6, 1, 1, 1);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_44->addItem(horizontalSpacer_6, 6, 5, 1, 1);

        verticalSpacer_54 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_44->addItem(verticalSpacer_54, 8, 1, 1, 1);

        label_msDuration_26 = new QLabel(tab_2);
        label_msDuration_26->setObjectName(QStringLiteral("label_msDuration_26"));
        label_msDuration_26->setFont(font1);
        label_msDuration_26->setAlignment(Qt::AlignCenter);

        gridLayout_44->addWidget(label_msDuration_26, 1, 3, 1, 1);

        verticalSpacer_52 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_44->addItem(verticalSpacer_52, 1, 1, 1, 1);

        verticalSpacer_53 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_44->addItem(verticalSpacer_53, 4, 1, 1, 1);

        label_duration_15 = new QLabel(tab_2);
        label_duration_15->setObjectName(QStringLiteral("label_duration_15"));
        label_duration_15->setFont(font);
        label_duration_15->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout_44->addWidget(label_duration_15, 6, 2, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_44->addItem(verticalSpacer, 0, 2, 1, 1);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_44->addItem(horizontalSpacer_8, 6, 4, 1, 1);

        label_45 = new QLabel(tab_2);
        label_45->setObjectName(QStringLiteral("label_45"));
        QFont font3;
        font3.setFamily(QStringLiteral("Titillium Web"));
        font3.setItalic(false);
        label_45->setFont(font3);

        gridLayout_44->addWidget(label_45, 7, 4, 1, 1);

        progressBar_18 = new QProgressBar(tab_2);
        progressBar_18->setObjectName(QStringLiteral("progressBar_18"));
        progressBar_18->setValue(24);

        gridLayout_44->addWidget(progressBar_18, 7, 2, 1, 2);

        line_12 = new QFrame(tab_2);
        line_12->setObjectName(QStringLiteral("line_12"));
        line_12->setFrameShape(QFrame::HLine);
        line_12->setFrameShadow(QFrame::Sunken);

        gridLayout_44->addWidget(line_12, 5, 0, 1, 7);


        gridLayout_4->addLayout(gridLayout_44, 0, 2, 2, 1);

        scrollArea_8 = new QScrollArea(tab_2);
        scrollArea_8->setObjectName(QStringLiteral("scrollArea_8"));
        scrollArea_8->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        scrollArea_8->setWidgetResizable(true);
        scrollAreaWidgetContents_12 = new QWidget();
        scrollAreaWidgetContents_12->setObjectName(QStringLiteral("scrollAreaWidgetContents_12"));
        scrollAreaWidgetContents_12->setGeometry(QRect(0, 0, 594, 516));
        gridLayout_45 = new QGridLayout(scrollAreaWidgetContents_12);
        gridLayout_45->setSpacing(6);
        gridLayout_45->setContentsMargins(11, 11, 11, 11);
        gridLayout_45->setObjectName(QStringLiteral("gridLayout_45"));
        gridLayout_46 = new QGridLayout();
        gridLayout_46->setSpacing(1);
        gridLayout_46->setObjectName(QStringLiteral("gridLayout_46"));
        label_ID4_167 = new QLabel(scrollAreaWidgetContents_12);
        label_ID4_167->setObjectName(QStringLiteral("label_ID4_167"));
        QFont font4;
        font4.setFamily(QStringLiteral("Titillium Web"));
        font4.setPointSize(15);
        font4.setBold(true);
        font4.setWeight(75);
        label_ID4_167->setFont(font4);
        label_ID4_167->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_46->addWidget(label_ID4_167, 3, 2, 1, 1);

        horizontalSpacer_192 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_46->addItem(horizontalSpacer_192, 5, 3, 1, 1);

        checkBox_235 = new QCheckBox(scrollAreaWidgetContents_12);
        checkBox_235->setObjectName(QStringLiteral("checkBox_235"));
        checkBox_235->setEnabled(true);
        checkBox_235->setBaseSize(QSize(0, 0));
        QFont font5;
        font5.setFamily(QStringLiteral("Titillium Web"));
        font5.setPointSize(14);
        font5.setBold(true);
        font5.setWeight(75);
        checkBox_235->setFont(font5);
        checkBox_235->setTabletTracking(false);
        checkBox_235->setAutoFillBackground(false);
        checkBox_235->setChecked(false);
        checkBox_235->setTristate(false);

        gridLayout_46->addWidget(checkBox_235, 2, 1, 1, 1);

        horizontalSpacer_193 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_46->addItem(horizontalSpacer_193, 0, 7, 1, 1);

        textFIeld_name1_12 = new QLineEdit(scrollAreaWidgetContents_12);
        textFIeld_name1_12->setObjectName(QStringLiteral("textFIeld_name1_12"));
        textFIeld_name1_12->setEnabled(false);
        textFIeld_name1_12->setFont(font2);

        gridLayout_46->addWidget(textFIeld_name1_12, 0, 5, 1, 2);

        horizontalSpacer_190 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_46->addItem(horizontalSpacer_190, 5, 4, 1, 1);

        circle_inactive3_79 = new QLabel(scrollAreaWidgetContents_12);
        circle_inactive3_79->setObjectName(QStringLiteral("circle_inactive3_79"));
        circle_inactive3_79->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_79->setAlignment(Qt::AlignCenter);

        gridLayout_46->addWidget(circle_inactive3_79, 3, 3, 1, 1);

        label_ID3_23 = new QLabel(scrollAreaWidgetContents_12);
        label_ID3_23->setObjectName(QStringLiteral("label_ID3_23"));
        label_ID3_23->setFont(font4);
        label_ID3_23->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_46->addWidget(label_ID3_23, 2, 2, 1, 1);

        circle_inactive3_83 = new QLabel(scrollAreaWidgetContents_12);
        circle_inactive3_83->setObjectName(QStringLiteral("circle_inactive3_83"));
        circle_inactive3_83->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-03.png")));
        circle_inactive3_83->setAlignment(Qt::AlignCenter);

        gridLayout_46->addWidget(circle_inactive3_83, 0, 3, 1, 1);

        horizontalSpacer_188 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_46->addItem(horizontalSpacer_188, 5, 0, 1, 1);

        label_ID2_23 = new QLabel(scrollAreaWidgetContents_12);
        label_ID2_23->setObjectName(QStringLiteral("label_ID2_23"));
        label_ID2_23->setFont(font4);
        label_ID2_23->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_46->addWidget(label_ID2_23, 1, 2, 1, 1);

        checkBox_224 = new QCheckBox(scrollAreaWidgetContents_12);
        checkBox_224->setObjectName(QStringLiteral("checkBox_224"));
        checkBox_224->setEnabled(true);
        checkBox_224->setBaseSize(QSize(0, 0));
        checkBox_224->setFont(font5);
        checkBox_224->setTabletTracking(false);
        checkBox_224->setAutoFillBackground(false);
        checkBox_224->setChecked(false);
        checkBox_224->setTristate(false);

        gridLayout_46->addWidget(checkBox_224, 4, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_46->addItem(horizontalSpacer_2, 5, 2, 1, 1);

        progressBar_16 = new QProgressBar(scrollAreaWidgetContents_12);
        progressBar_16->setObjectName(QStringLiteral("progressBar_16"));
        progressBar_16->setValue(0);

        gridLayout_46->addWidget(progressBar_16, 3, 12, 1, 1);

        textFIeld_name1_13 = new QLineEdit(scrollAreaWidgetContents_12);
        textFIeld_name1_13->setObjectName(QStringLiteral("textFIeld_name1_13"));
        textFIeld_name1_13->setEnabled(false);
        textFIeld_name1_13->setFont(font2);

        gridLayout_46->addWidget(textFIeld_name1_13, 0, 8, 1, 2);

        progressBar_13 = new QProgressBar(scrollAreaWidgetContents_12);
        progressBar_13->setObjectName(QStringLiteral("progressBar_13"));
        progressBar_13->setValue(100);

        gridLayout_46->addWidget(progressBar_13, 0, 12, 1, 1);

        label_ID1_12 = new QLabel(scrollAreaWidgetContents_12);
        label_ID1_12->setObjectName(QStringLiteral("label_ID1_12"));
        label_ID1_12->setFont(font4);
        label_ID1_12->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_46->addWidget(label_ID1_12, 0, 2, 1, 1);

        horizontalSpacer_189 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_46->addItem(horizontalSpacer_189, 0, 11, 1, 1);

        checkBox_231 = new QCheckBox(scrollAreaWidgetContents_12);
        checkBox_231->setObjectName(QStringLiteral("checkBox_231"));
        checkBox_231->setEnabled(true);
        checkBox_231->setBaseSize(QSize(0, 0));
        checkBox_231->setFont(font5);
        checkBox_231->setTabletTracking(false);
        checkBox_231->setAutoFillBackground(false);
        checkBox_231->setChecked(true);
        checkBox_231->setTristate(false);

        gridLayout_46->addWidget(checkBox_231, 0, 1, 1, 1);

        checkBox_221 = new QCheckBox(scrollAreaWidgetContents_12);
        checkBox_221->setObjectName(QStringLiteral("checkBox_221"));
        checkBox_221->setEnabled(true);
        checkBox_221->setBaseSize(QSize(0, 0));
        checkBox_221->setFont(font5);
        checkBox_221->setTabletTracking(false);
        checkBox_221->setAutoFillBackground(false);
        checkBox_221->setChecked(false);
        checkBox_221->setTristate(false);

        gridLayout_46->addWidget(checkBox_221, 3, 1, 1, 1);

        circle_inactive3_82 = new QLabel(scrollAreaWidgetContents_12);
        circle_inactive3_82->setObjectName(QStringLiteral("circle_inactive3_82"));
        circle_inactive3_82->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_82->setAlignment(Qt::AlignCenter);

        gridLayout_46->addWidget(circle_inactive3_82, 4, 3, 1, 1);

        checkBox_232 = new QCheckBox(scrollAreaWidgetContents_12);
        checkBox_232->setObjectName(QStringLiteral("checkBox_232"));
        checkBox_232->setEnabled(true);
        checkBox_232->setBaseSize(QSize(0, 0));
        checkBox_232->setFont(font5);
        checkBox_232->setTabletTracking(false);
        checkBox_232->setAutoFillBackground(false);
        checkBox_232->setChecked(false);
        checkBox_232->setTristate(false);

        gridLayout_46->addWidget(checkBox_232, 1, 1, 1, 1);

        textFIeld_name1_17 = new QLineEdit(scrollAreaWidgetContents_12);
        textFIeld_name1_17->setObjectName(QStringLiteral("textFIeld_name1_17"));
        textFIeld_name1_17->setEnabled(false);
        textFIeld_name1_17->setFont(font2);

        gridLayout_46->addWidget(textFIeld_name1_17, 4, 5, 1, 2);

        textFIeld_name1_21 = new QLineEdit(scrollAreaWidgetContents_12);
        textFIeld_name1_21->setObjectName(QStringLiteral("textFIeld_name1_21"));
        textFIeld_name1_21->setEnabled(false);
        textFIeld_name1_21->setFont(font2);

        gridLayout_46->addWidget(textFIeld_name1_21, 4, 8, 1, 2);

        horizontalSpacer_191 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_46->addItem(horizontalSpacer_191, 5, 7, 1, 1);

        circle_inactive3_81 = new QLabel(scrollAreaWidgetContents_12);
        circle_inactive3_81->setObjectName(QStringLiteral("circle_inactive3_81"));
        circle_inactive3_81->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_81->setAlignment(Qt::AlignCenter);

        gridLayout_46->addWidget(circle_inactive3_81, 2, 3, 1, 1);

        label_ID4_172 = new QLabel(scrollAreaWidgetContents_12);
        label_ID4_172->setObjectName(QStringLiteral("label_ID4_172"));
        label_ID4_172->setFont(font4);
        label_ID4_172->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_46->addWidget(label_ID4_172, 4, 2, 1, 1);

        textFIeld_name1_18 = new QLineEdit(scrollAreaWidgetContents_12);
        textFIeld_name1_18->setObjectName(QStringLiteral("textFIeld_name1_18"));
        textFIeld_name1_18->setEnabled(false);
        textFIeld_name1_18->setFont(font2);

        gridLayout_46->addWidget(textFIeld_name1_18, 1, 8, 1, 2);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_46->addItem(horizontalSpacer_4, 0, 4, 1, 1);

        progressBar_15 = new QProgressBar(scrollAreaWidgetContents_12);
        progressBar_15->setObjectName(QStringLiteral("progressBar_15"));
        progressBar_15->setValue(0);

        gridLayout_46->addWidget(progressBar_15, 2, 12, 1, 1);

        circle_inactive3_84 = new QLabel(scrollAreaWidgetContents_12);
        circle_inactive3_84->setObjectName(QStringLiteral("circle_inactive3_84"));
        circle_inactive3_84->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_84->setAlignment(Qt::AlignCenter);

        gridLayout_46->addWidget(circle_inactive3_84, 1, 3, 1, 1);

        textFIeld_name1_19 = new QLineEdit(scrollAreaWidgetContents_12);
        textFIeld_name1_19->setObjectName(QStringLiteral("textFIeld_name1_19"));
        textFIeld_name1_19->setEnabled(false);
        textFIeld_name1_19->setFont(font2);

        gridLayout_46->addWidget(textFIeld_name1_19, 2, 8, 1, 2);

        textFIeld_name1_16 = new QLineEdit(scrollAreaWidgetContents_12);
        textFIeld_name1_16->setObjectName(QStringLiteral("textFIeld_name1_16"));
        textFIeld_name1_16->setEnabled(false);
        textFIeld_name1_16->setFont(font2);

        gridLayout_46->addWidget(textFIeld_name1_16, 3, 5, 1, 2);

        textFIeld_name1_14 = new QLineEdit(scrollAreaWidgetContents_12);
        textFIeld_name1_14->setObjectName(QStringLiteral("textFIeld_name1_14"));
        textFIeld_name1_14->setEnabled(false);
        textFIeld_name1_14->setFont(font2);

        gridLayout_46->addWidget(textFIeld_name1_14, 1, 5, 1, 2);

        textFIeld_name1_20 = new QLineEdit(scrollAreaWidgetContents_12);
        textFIeld_name1_20->setObjectName(QStringLiteral("textFIeld_name1_20"));
        textFIeld_name1_20->setEnabled(false);
        textFIeld_name1_20->setFont(font2);

        gridLayout_46->addWidget(textFIeld_name1_20, 3, 8, 1, 2);

        progressBar_17 = new QProgressBar(scrollAreaWidgetContents_12);
        progressBar_17->setObjectName(QStringLiteral("progressBar_17"));
        progressBar_17->setValue(0);

        gridLayout_46->addWidget(progressBar_17, 4, 12, 1, 1);

        progressBar_14 = new QProgressBar(scrollAreaWidgetContents_12);
        progressBar_14->setObjectName(QStringLiteral("progressBar_14"));
        progressBar_14->setValue(0);

        gridLayout_46->addWidget(progressBar_14, 1, 12, 1, 1);

        textFIeld_name1_15 = new QLineEdit(scrollAreaWidgetContents_12);
        textFIeld_name1_15->setObjectName(QStringLiteral("textFIeld_name1_15"));
        textFIeld_name1_15->setEnabled(false);
        textFIeld_name1_15->setFont(font2);

        gridLayout_46->addWidget(textFIeld_name1_15, 2, 5, 1, 2);


        gridLayout_45->addLayout(gridLayout_46, 1, 0, 1, 1);

        scrollArea_8->setWidget(scrollAreaWidgetContents_12);

        gridLayout_4->addWidget(scrollArea_8, 1, 0, 1, 2);


        gridLayout_47->addLayout(gridLayout_4, 0, 0, 1, 1);

        tabWidget->addTab(tab_2, QString());
        Node_1 = new QWidget();
        Node_1->setObjectName(QStringLiteral("Node_1"));
        gridLayout_3 = new QGridLayout(Node_1);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        horizontalLayout_21 = new QHBoxLayout();
        horizontalLayout_21->setSpacing(6);
        horizontalLayout_21->setObjectName(QStringLiteral("horizontalLayout_21"));
        horizontalSpacer_162 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_21->addItem(horizontalSpacer_162);

        horizontalSpacer_163 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_21->addItem(horizontalSpacer_163);

        label_ID_12 = new QLabel(Node_1);
        label_ID_12->setObjectName(QStringLiteral("label_ID_12"));
        sizePolicy.setHeightForWidth(label_ID_12->sizePolicy().hasHeightForWidth());
        label_ID_12->setSizePolicy(sizePolicy);
        label_ID_12->setMaximumSize(QSize(16777215, 16777215));
        label_ID_12->setFont(font);
        label_ID_12->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_21->addWidget(label_ID_12);

        horizontalSpacer_164 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_21->addItem(horizontalSpacer_164);

        label_name_13 = new QLabel(Node_1);
        label_name_13->setObjectName(QStringLiteral("label_name_13"));
        label_name_13->setFont(font);
        label_name_13->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_21->addWidget(label_name_13);

        horizontalSpacer_165 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_21->addItem(horizontalSpacer_165);

        horizontalSpacer_166 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_21->addItem(horizontalSpacer_166);

        label_start_12 = new QLabel(Node_1);
        label_start_12->setObjectName(QStringLiteral("label_start_12"));
        sizePolicy1.setHeightForWidth(label_start_12->sizePolicy().hasHeightForWidth());
        label_start_12->setSizePolicy(sizePolicy1);
        label_start_12->setFont(font);
        label_start_12->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_21->addWidget(label_start_12);

        label_msStart_13 = new QLabel(Node_1);
        label_msStart_13->setObjectName(QStringLiteral("label_msStart_13"));
        label_msStart_13->setFont(font1);
        label_msStart_13->setAlignment(Qt::AlignCenter);

        horizontalLayout_21->addWidget(label_msStart_13);

        horizontalSpacer_167 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_21->addItem(horizontalSpacer_167);

        label_duration_12 = new QLabel(Node_1);
        label_duration_12->setObjectName(QStringLiteral("label_duration_12"));
        label_duration_12->setFont(font);
        label_duration_12->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_21->addWidget(label_duration_12);

        label_msDuration_22 = new QLabel(Node_1);
        label_msDuration_22->setObjectName(QStringLiteral("label_msDuration_22"));
        label_msDuration_22->setFont(font1);
        label_msDuration_22->setAlignment(Qt::AlignCenter);

        horizontalLayout_21->addWidget(label_msDuration_22);

        horizontalSpacer_168 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_21->addItem(horizontalSpacer_168);


        gridLayout_2->addLayout(horizontalLayout_21, 0, 0, 1, 1);

        gridLayout_41 = new QGridLayout();
        gridLayout_41->setSpacing(6);
        gridLayout_41->setObjectName(QStringLiteral("gridLayout_41"));
        horizontalLayout_22 = new QHBoxLayout();
        horizontalLayout_22->setSpacing(6);
        horizontalLayout_22->setObjectName(QStringLiteral("horizontalLayout_22"));
        progressBar_11 = new QProgressBar(Node_1);
        progressBar_11->setObjectName(QStringLiteral("progressBar_11"));
        progressBar_11->setValue(48);

        horizontalLayout_22->addWidget(progressBar_11);

        label_41 = new QLabel(Node_1);
        label_41->setObjectName(QStringLiteral("label_41"));
        label_41->setFont(font3);

        horizontalLayout_22->addWidget(label_41);


        gridLayout_41->addLayout(horizontalLayout_22, 4, 0, 1, 5);

        spinBox_start5_151 = new QSpinBox(Node_1);
        spinBox_start5_151->setObjectName(QStringLiteral("spinBox_start5_151"));
        QFont font6;
        font6.setFamily(QStringLiteral("Titillium Web"));
        font6.setPointSize(12);
        spinBox_start5_151->setFont(font6);
        spinBox_start5_151->setMaximum(10000000);
        spinBox_start5_151->setValue(32000);

        gridLayout_41->addWidget(spinBox_start5_151, 2, 3, 1, 2);

        spinBox_start5_152 = new QSpinBox(Node_1);
        spinBox_start5_152->setObjectName(QStringLiteral("spinBox_start5_152"));
        spinBox_start5_152->setFont(font6);
        spinBox_start5_152->setMaximum(10000000);
        spinBox_start5_152->setValue(12);

        gridLayout_41->addWidget(spinBox_start5_152, 2, 0, 1, 2);

        pushButton_43 = new QPushButton(Node_1);
        pushButton_43->setObjectName(QStringLiteral("pushButton_43"));
        pushButton_43->setMinimumSize(QSize(0, 80));
        pushButton_43->setFont(font2);

        gridLayout_41->addWidget(pushButton_43, 10, 3, 1, 2);

        horizontalSpacer_169 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_41->addItem(horizontalSpacer_169, 8, 2, 1, 1);

        verticalSpacer_46 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_41->addItem(verticalSpacer_46, 7, 0, 1, 1);

        pushButton_44 = new QPushButton(Node_1);
        pushButton_44->setObjectName(QStringLiteral("pushButton_44"));
        pushButton_44->setMinimumSize(QSize(0, 80));
        pushButton_44->setFont(font2);

        gridLayout_41->addWidget(pushButton_44, 8, 0, 1, 2);

        label_42 = new QLabel(Node_1);
        label_42->setObjectName(QStringLiteral("label_42"));
        QFont font7;
        font7.setFamily(QStringLiteral("Titillium Web"));
        font7.setPointSize(9);
        font7.setItalic(true);
        label_42->setFont(font7);
        label_42->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_41->addWidget(label_42, 3, 0, 1, 5);

        label_43 = new QLabel(Node_1);
        label_43->setObjectName(QStringLiteral("label_43"));
        QFont font8;
        font8.setFamily(QStringLiteral("Titillium Web"));
        font8.setItalic(true);
        label_43->setFont(font8);

        gridLayout_41->addWidget(label_43, 1, 3, 1, 1);

        line_11 = new QFrame(Node_1);
        line_11->setObjectName(QStringLiteral("line_11"));
        line_11->setFrameShape(QFrame::HLine);
        line_11->setFrameShadow(QFrame::Sunken);

        gridLayout_41->addWidget(line_11, 6, 0, 1, 5);

        horizontalSpacer_170 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_41->addItem(horizontalSpacer_170, 0, 1, 1, 1);

        verticalSpacer_47 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_41->addItem(verticalSpacer_47, 0, 0, 1, 1);

        pushButton_45 = new QPushButton(Node_1);
        pushButton_45->setObjectName(QStringLiteral("pushButton_45"));
        pushButton_45->setMinimumSize(QSize(0, 80));
        pushButton_45->setFont(font2);

        gridLayout_41->addWidget(pushButton_45, 10, 0, 1, 2);

        label_msDuration_23 = new QLabel(Node_1);
        label_msDuration_23->setObjectName(QStringLiteral("label_msDuration_23"));
        label_msDuration_23->setFont(font1);
        label_msDuration_23->setAlignment(Qt::AlignCenter);

        gridLayout_41->addWidget(label_msDuration_23, 1, 4, 1, 1);

        verticalSpacer_48 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_41->addItem(verticalSpacer_48, 5, 0, 1, 1);

        pushButton_46 = new QPushButton(Node_1);
        pushButton_46->setObjectName(QStringLiteral("pushButton_46"));
        pushButton_46->setMinimumSize(QSize(0, 80));
        pushButton_46->setFont(font2);

        gridLayout_41->addWidget(pushButton_46, 8, 3, 1, 2);

        verticalSpacer_49 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_41->addItem(verticalSpacer_49, 9, 0, 1, 1);

        label_44 = new QLabel(Node_1);
        label_44->setObjectName(QStringLiteral("label_44"));
        label_44->setFont(font8);

        gridLayout_41->addWidget(label_44, 1, 0, 1, 1);

        verticalSpacer_50 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_41->addItem(verticalSpacer_50, 11, 0, 1, 1);


        gridLayout_2->addLayout(gridLayout_41, 0, 2, 2, 1);

        scrollArea_7 = new QScrollArea(Node_1);
        scrollArea_7->setObjectName(QStringLiteral("scrollArea_7"));
        scrollArea_7->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        scrollArea_7->setWidgetResizable(true);
        scrollAreaWidgetContents_11 = new QWidget();
        scrollAreaWidgetContents_11->setObjectName(QStringLiteral("scrollAreaWidgetContents_11"));
        scrollAreaWidgetContents_11->setGeometry(QRect(0, -51, 654, 740));
        gridLayout_42 = new QGridLayout(scrollAreaWidgetContents_11);
        gridLayout_42->setSpacing(6);
        gridLayout_42->setContentsMargins(11, 11, 11, 11);
        gridLayout_42->setObjectName(QStringLiteral("gridLayout_42"));
        gridLayout_43 = new QGridLayout();
        gridLayout_43->setSpacing(1);
        gridLayout_43->setObjectName(QStringLiteral("gridLayout_43"));
        textFIeld_name5_131 = new QLineEdit(scrollAreaWidgetContents_11);
        textFIeld_name5_131->setObjectName(QStringLiteral("textFIeld_name5_131"));
        textFIeld_name5_131->setFont(font2);

        gridLayout_43->addWidget(textFIeld_name5_131, 12, 4, 1, 1);

        spinBox_start2_21 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_start2_21->setObjectName(QStringLiteral("spinBox_start2_21"));
        spinBox_start2_21->setFont(font6);
        spinBox_start2_21->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_start2_21, 7, 6, 1, 2);

        label_ID4_151 = new QLabel(scrollAreaWidgetContents_11);
        label_ID4_151->setObjectName(QStringLiteral("label_ID4_151"));
        label_ID4_151->setFont(font4);
        label_ID4_151->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_43->addWidget(label_ID4_151, 5, 2, 1, 1);

        circle_inactive3_71 = new QLabel(scrollAreaWidgetContents_11);
        circle_inactive3_71->setObjectName(QStringLiteral("circle_inactive3_71"));
        circle_inactive3_71->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-03.png")));
        circle_inactive3_71->setAlignment(Qt::AlignCenter);

        gridLayout_43->addWidget(circle_inactive3_71, 5, 3, 1, 1);

        horizontalSpacer_171 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_43->addItem(horizontalSpacer_171, 6, 0, 1, 1);

        checkBox_201 = new QCheckBox(scrollAreaWidgetContents_11);
        checkBox_201->setObjectName(QStringLiteral("checkBox_201"));
        checkBox_201->setEnabled(true);
        checkBox_201->setBaseSize(QSize(0, 0));
        checkBox_201->setFont(font5);
        checkBox_201->setTabletTracking(false);
        checkBox_201->setAutoFillBackground(false);
        checkBox_201->setChecked(true);
        checkBox_201->setTristate(false);

        gridLayout_43->addWidget(checkBox_201, 3, 1, 1, 1);

        checkBox_202 = new QCheckBox(scrollAreaWidgetContents_11);
        checkBox_202->setObjectName(QStringLiteral("checkBox_202"));
        checkBox_202->setEnabled(true);
        checkBox_202->setBaseSize(QSize(0, 0));
        checkBox_202->setFont(font5);
        checkBox_202->setTabletTracking(false);
        checkBox_202->setAutoFillBackground(false);
        checkBox_202->setChecked(true);
        checkBox_202->setTristate(false);

        gridLayout_43->addWidget(checkBox_202, 5, 1, 1, 1);

        checkBox_203 = new QCheckBox(scrollAreaWidgetContents_11);
        checkBox_203->setObjectName(QStringLiteral("checkBox_203"));
        checkBox_203->setEnabled(true);
        checkBox_203->setBaseSize(QSize(0, 0));
        checkBox_203->setFont(font5);
        checkBox_203->setTabletTracking(false);
        checkBox_203->setAutoFillBackground(false);
        checkBox_203->setChecked(true);
        checkBox_203->setTristate(false);

        gridLayout_43->addWidget(checkBox_203, 8, 1, 1, 1);

        spinBox_start1_11 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_start1_11->setObjectName(QStringLiteral("spinBox_start1_11"));
        spinBox_start1_11->setFont(font6);
        spinBox_start1_11->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_start1_11, 0, 6, 1, 2);

        checkBox_204 = new QCheckBox(scrollAreaWidgetContents_11);
        checkBox_204->setObjectName(QStringLiteral("checkBox_204"));
        checkBox_204->setEnabled(true);
        checkBox_204->setBaseSize(QSize(0, 0));
        checkBox_204->setFont(font5);
        checkBox_204->setTabletTracking(false);
        checkBox_204->setAutoFillBackground(false);
        checkBox_204->setChecked(true);
        checkBox_204->setTristate(false);

        gridLayout_43->addWidget(checkBox_204, 4, 1, 1, 1);

        circle_inactive3_72 = new QLabel(scrollAreaWidgetContents_11);
        circle_inactive3_72->setObjectName(QStringLiteral("circle_inactive3_72"));
        circle_inactive3_72->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-03.png")));
        circle_inactive3_72->setAlignment(Qt::AlignCenter);

        gridLayout_43->addWidget(circle_inactive3_72, 3, 3, 1, 1);

        textFIeld_name1_11 = new QLineEdit(scrollAreaWidgetContents_11);
        textFIeld_name1_11->setObjectName(QStringLiteral("textFIeld_name1_11"));
        textFIeld_name1_11->setFont(font2);

        gridLayout_43->addWidget(textFIeld_name1_11, 0, 4, 1, 1);

        spinBox_duration2_21 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_duration2_21->setObjectName(QStringLiteral("spinBox_duration2_21"));
        spinBox_duration2_21->setFont(font6);
        spinBox_duration2_21->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_duration2_21, 7, 9, 1, 2);

        spinBox_duration4_21 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_duration4_21->setObjectName(QStringLiteral("spinBox_duration4_21"));
        spinBox_duration4_21->setFont(font6);
        spinBox_duration4_21->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_duration4_21, 3, 9, 1, 2);

        textFIeld_name4_21 = new QLineEdit(scrollAreaWidgetContents_11);
        textFIeld_name4_21->setObjectName(QStringLiteral("textFIeld_name4_21"));
        textFIeld_name4_21->setFont(font2);

        gridLayout_43->addWidget(textFIeld_name4_21, 3, 4, 1, 1);

        spinBox_duration5_131 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_duration5_131->setObjectName(QStringLiteral("spinBox_duration5_131"));
        spinBox_duration5_131->setFont(font6);
        spinBox_duration5_131->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_duration5_131, 6, 9, 1, 2);

        checkBox_205 = new QCheckBox(scrollAreaWidgetContents_11);
        checkBox_205->setObjectName(QStringLiteral("checkBox_205"));
        checkBox_205->setEnabled(true);
        checkBox_205->setBaseSize(QSize(0, 0));
        checkBox_205->setFont(font5);
        checkBox_205->setTabletTracking(false);
        checkBox_205->setAutoFillBackground(false);
        checkBox_205->setChecked(true);
        checkBox_205->setTristate(false);

        gridLayout_43->addWidget(checkBox_205, 14, 1, 1, 1);

        textFIeld_name2_21 = new QLineEdit(scrollAreaWidgetContents_11);
        textFIeld_name2_21->setObjectName(QStringLiteral("textFIeld_name2_21"));
        textFIeld_name2_21->setFont(font2);

        gridLayout_43->addWidget(textFIeld_name2_21, 7, 4, 1, 1);

        spinBox_duration5_132 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_duration5_132->setObjectName(QStringLiteral("spinBox_duration5_132"));
        spinBox_duration5_132->setFont(font6);
        spinBox_duration5_132->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_duration5_132, 10, 9, 1, 2);

        textFIeld_name5_132 = new QLineEdit(scrollAreaWidgetContents_11);
        textFIeld_name5_132->setObjectName(QStringLiteral("textFIeld_name5_132"));
        textFIeld_name5_132->setFont(font2);

        gridLayout_43->addWidget(textFIeld_name5_132, 4, 4, 1, 1);

        spinBox_start5_153 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_start5_153->setObjectName(QStringLiteral("spinBox_start5_153"));
        spinBox_start5_153->setFont(font6);
        spinBox_start5_153->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_start5_153, 4, 6, 1, 2);

        label_ID4_152 = new QLabel(scrollAreaWidgetContents_11);
        label_ID4_152->setObjectName(QStringLiteral("label_ID4_152"));
        label_ID4_152->setFont(font4);
        label_ID4_152->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_43->addWidget(label_ID4_152, 3, 2, 1, 1);

        spinBox_start3_21 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_start3_21->setObjectName(QStringLiteral("spinBox_start3_21"));
        spinBox_start3_21->setFont(font6);
        spinBox_start3_21->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_start3_21, 8, 6, 1, 2);

        label_ID4_153 = new QLabel(scrollAreaWidgetContents_11);
        label_ID4_153->setObjectName(QStringLiteral("label_ID4_153"));
        label_ID4_153->setFont(font4);
        label_ID4_153->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_43->addWidget(label_ID4_153, 9, 2, 1, 1);

        spinBox_duration4_22 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_duration4_22->setObjectName(QStringLiteral("spinBox_duration4_22"));
        spinBox_duration4_22->setFont(font6);
        spinBox_duration4_22->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_duration4_22, 9, 9, 1, 2);

        checkBox_206 = new QCheckBox(scrollAreaWidgetContents_11);
        checkBox_206->setObjectName(QStringLiteral("checkBox_206"));
        checkBox_206->setEnabled(true);
        checkBox_206->setBaseSize(QSize(0, 0));
        checkBox_206->setFont(font5);
        checkBox_206->setTabletTracking(false);
        checkBox_206->setAutoFillBackground(false);
        checkBox_206->setChecked(true);
        checkBox_206->setTristate(false);

        gridLayout_43->addWidget(checkBox_206, 13, 1, 1, 1);

        spinBox_start5_154 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_start5_154->setObjectName(QStringLiteral("spinBox_start5_154"));
        spinBox_start5_154->setFont(font6);
        spinBox_start5_154->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_start5_154, 15, 6, 1, 2);

        spinBox_duration5_133 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_duration5_133->setObjectName(QStringLiteral("spinBox_duration5_133"));
        spinBox_duration5_133->setFont(font6);
        spinBox_duration5_133->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_duration5_133, 15, 9, 1, 2);

        textFIeld_name5_133 = new QLineEdit(scrollAreaWidgetContents_11);
        textFIeld_name5_133->setObjectName(QStringLiteral("textFIeld_name5_133"));
        textFIeld_name5_133->setFont(font2);

        gridLayout_43->addWidget(textFIeld_name5_133, 10, 4, 1, 1);

        label_ID4_154 = new QLabel(scrollAreaWidgetContents_11);
        label_ID4_154->setObjectName(QStringLiteral("label_ID4_154"));
        label_ID4_154->setFont(font4);
        label_ID4_154->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_43->addWidget(label_ID4_154, 14, 2, 1, 1);

        spinBox_start5_155 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_start5_155->setObjectName(QStringLiteral("spinBox_start5_155"));
        spinBox_start5_155->setFont(font6);
        spinBox_start5_155->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_start5_155, 14, 6, 1, 2);

        textFIeld_name4_22 = new QLineEdit(scrollAreaWidgetContents_11);
        textFIeld_name4_22->setObjectName(QStringLiteral("textFIeld_name4_22"));
        textFIeld_name4_22->setFont(font2);

        gridLayout_43->addWidget(textFIeld_name4_22, 9, 4, 1, 1);

        spinBox_start3_22 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_start3_22->setObjectName(QStringLiteral("spinBox_start3_22"));
        spinBox_start3_22->setFont(font6);
        spinBox_start3_22->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_start3_22, 2, 6, 1, 2);

        spinBox_start5_156 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_start5_156->setObjectName(QStringLiteral("spinBox_start5_156"));
        spinBox_start5_156->setFont(font6);
        spinBox_start5_156->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_start5_156, 6, 6, 1, 2);

        horizontalSpacer_172 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_43->addItem(horizontalSpacer_172, 0, 12, 1, 1);

        horizontalSpacer_173 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_43->addItem(horizontalSpacer_173, 10, 5, 1, 1);

        spinBox_duration5_134 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_duration5_134->setObjectName(QStringLiteral("spinBox_duration5_134"));
        spinBox_duration5_134->setFont(font6);
        spinBox_duration5_134->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_duration5_134, 11, 9, 1, 2);

        textFIeld_name5_134 = new QLineEdit(scrollAreaWidgetContents_11);
        textFIeld_name5_134->setObjectName(QStringLiteral("textFIeld_name5_134"));
        textFIeld_name5_134->setFont(font2);

        gridLayout_43->addWidget(textFIeld_name5_134, 5, 4, 1, 1);

        label_ID2_21 = new QLabel(scrollAreaWidgetContents_11);
        label_ID2_21->setObjectName(QStringLiteral("label_ID2_21"));
        label_ID2_21->setFont(font4);
        label_ID2_21->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_43->addWidget(label_ID2_21, 1, 2, 1, 1);

        checkBox_207 = new QCheckBox(scrollAreaWidgetContents_11);
        checkBox_207->setObjectName(QStringLiteral("checkBox_207"));
        checkBox_207->setEnabled(true);
        checkBox_207->setBaseSize(QSize(0, 0));
        checkBox_207->setFont(font5);
        checkBox_207->setTabletTracking(false);
        checkBox_207->setAutoFillBackground(false);
        checkBox_207->setChecked(true);
        checkBox_207->setTristate(false);

        gridLayout_43->addWidget(checkBox_207, 6, 1, 1, 1);

        spinBox_start5_157 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_start5_157->setObjectName(QStringLiteral("spinBox_start5_157"));
        spinBox_start5_157->setFont(font6);
        spinBox_start5_157->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_start5_157, 17, 6, 1, 2);

        textFIeld_name2_22 = new QLineEdit(scrollAreaWidgetContents_11);
        textFIeld_name2_22->setObjectName(QStringLiteral("textFIeld_name2_22"));
        textFIeld_name2_22->setFont(font2);

        gridLayout_43->addWidget(textFIeld_name2_22, 1, 4, 1, 1);

        textFIeld_name5_135 = new QLineEdit(scrollAreaWidgetContents_11);
        textFIeld_name5_135->setObjectName(QStringLiteral("textFIeld_name5_135"));
        textFIeld_name5_135->setFont(font2);

        gridLayout_43->addWidget(textFIeld_name5_135, 6, 4, 1, 1);

        checkBox_208 = new QCheckBox(scrollAreaWidgetContents_11);
        checkBox_208->setObjectName(QStringLiteral("checkBox_208"));
        checkBox_208->setEnabled(true);
        checkBox_208->setBaseSize(QSize(0, 0));
        checkBox_208->setFont(font5);
        checkBox_208->setTabletTracking(false);
        checkBox_208->setAutoFillBackground(false);
        checkBox_208->setChecked(true);
        checkBox_208->setTristate(false);

        gridLayout_43->addWidget(checkBox_208, 7, 1, 1, 1);

        label_ID4_155 = new QLabel(scrollAreaWidgetContents_11);
        label_ID4_155->setObjectName(QStringLiteral("label_ID4_155"));
        label_ID4_155->setFont(font4);
        label_ID4_155->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_43->addWidget(label_ID4_155, 6, 2, 1, 1);

        circle_inactive3_73 = new QLabel(scrollAreaWidgetContents_11);
        circle_inactive3_73->setObjectName(QStringLiteral("circle_inactive3_73"));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        circle_inactive3_73->setPalette(palette1);
        circle_inactive3_73->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-02.png")));
        circle_inactive3_73->setAlignment(Qt::AlignCenter);

        gridLayout_43->addWidget(circle_inactive3_73, 8, 3, 1, 1);

        textFIeld_name3_21 = new QLineEdit(scrollAreaWidgetContents_11);
        textFIeld_name3_21->setObjectName(QStringLiteral("textFIeld_name3_21"));
        textFIeld_name3_21->setFont(font2);

        gridLayout_43->addWidget(textFIeld_name3_21, 2, 4, 1, 1);

        spinBox_duration5_135 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_duration5_135->setObjectName(QStringLiteral("spinBox_duration5_135"));
        spinBox_duration5_135->setFont(font6);
        spinBox_duration5_135->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_duration5_135, 5, 9, 1, 2);

        textFIeld_name3_22 = new QLineEdit(scrollAreaWidgetContents_11);
        textFIeld_name3_22->setObjectName(QStringLiteral("textFIeld_name3_22"));
        textFIeld_name3_22->setFont(font2);

        gridLayout_43->addWidget(textFIeld_name3_22, 8, 4, 1, 1);

        spinBox_duration5_136 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_duration5_136->setObjectName(QStringLiteral("spinBox_duration5_136"));
        spinBox_duration5_136->setFont(font6);
        spinBox_duration5_136->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_duration5_136, 14, 9, 1, 2);

        checkBox_209 = new QCheckBox(scrollAreaWidgetContents_11);
        checkBox_209->setObjectName(QStringLiteral("checkBox_209"));
        checkBox_209->setEnabled(true);
        checkBox_209->setBaseSize(QSize(0, 0));
        checkBox_209->setFont(font5);
        checkBox_209->setTabletTracking(false);
        checkBox_209->setAutoFillBackground(false);
        checkBox_209->setChecked(true);
        checkBox_209->setTristate(false);

        gridLayout_43->addWidget(checkBox_209, 12, 1, 1, 1);

        textFIeld_name5_136 = new QLineEdit(scrollAreaWidgetContents_11);
        textFIeld_name5_136->setObjectName(QStringLiteral("textFIeld_name5_136"));
        textFIeld_name5_136->setFont(font2);

        gridLayout_43->addWidget(textFIeld_name5_136, 18, 4, 1, 1);

        textFIeld_name5_137 = new QLineEdit(scrollAreaWidgetContents_11);
        textFIeld_name5_137->setObjectName(QStringLiteral("textFIeld_name5_137"));
        textFIeld_name5_137->setFont(font2);

        gridLayout_43->addWidget(textFIeld_name5_137, 20, 4, 1, 1);

        spinBox_duration5_137 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_duration5_137->setObjectName(QStringLiteral("spinBox_duration5_137"));
        spinBox_duration5_137->setFont(font6);
        spinBox_duration5_137->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_duration5_137, 4, 9, 1, 2);

        label_ID4_156 = new QLabel(scrollAreaWidgetContents_11);
        label_ID4_156->setObjectName(QStringLiteral("label_ID4_156"));
        label_ID4_156->setFont(font4);
        label_ID4_156->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_43->addWidget(label_ID4_156, 20, 2, 1, 1);

        horizontalSpacer_174 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_43->addItem(horizontalSpacer_174, 10, 8, 1, 1);

        label_ID4_157 = new QLabel(scrollAreaWidgetContents_11);
        label_ID4_157->setObjectName(QStringLiteral("label_ID4_157"));
        label_ID4_157->setFont(font4);
        label_ID4_157->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_43->addWidget(label_ID4_157, 4, 2, 1, 1);

        textFIeld_name5_138 = new QLineEdit(scrollAreaWidgetContents_11);
        textFIeld_name5_138->setObjectName(QStringLiteral("textFIeld_name5_138"));
        textFIeld_name5_138->setFont(font2);

        gridLayout_43->addWidget(textFIeld_name5_138, 13, 4, 1, 1);

        spinBox_start2_22 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_start2_22->setObjectName(QStringLiteral("spinBox_start2_22"));
        spinBox_start2_22->setFont(font6);
        spinBox_start2_22->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_start2_22, 1, 6, 1, 2);

        spinBox_duration5_138 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_duration5_138->setObjectName(QStringLiteral("spinBox_duration5_138"));
        spinBox_duration5_138->setFont(font6);
        spinBox_duration5_138->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_duration5_138, 17, 9, 1, 2);

        label_ID4_158 = new QLabel(scrollAreaWidgetContents_11);
        label_ID4_158->setObjectName(QStringLiteral("label_ID4_158"));
        label_ID4_158->setFont(font4);
        label_ID4_158->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_43->addWidget(label_ID4_158, 10, 2, 1, 1);

        spinBox_duration5_139 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_duration5_139->setObjectName(QStringLiteral("spinBox_duration5_139"));
        spinBox_duration5_139->setFont(font6);
        spinBox_duration5_139->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_duration5_139, 12, 9, 1, 2);

        spinBox_duration5_140 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_duration5_140->setObjectName(QStringLiteral("spinBox_duration5_140"));
        spinBox_duration5_140->setFont(font6);
        spinBox_duration5_140->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_duration5_140, 20, 9, 1, 2);

        spinBox_start5_158 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_start5_158->setObjectName(QStringLiteral("spinBox_start5_158"));
        spinBox_start5_158->setFont(font6);
        spinBox_start5_158->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_start5_158, 20, 6, 1, 2);

        textFIeld_name5_139 = new QLineEdit(scrollAreaWidgetContents_11);
        textFIeld_name5_139->setObjectName(QStringLiteral("textFIeld_name5_139"));
        textFIeld_name5_139->setFont(font2);

        gridLayout_43->addWidget(textFIeld_name5_139, 11, 4, 1, 1);

        spinBox_start5_159 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_start5_159->setObjectName(QStringLiteral("spinBox_start5_159"));
        spinBox_start5_159->setFont(font6);
        spinBox_start5_159->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_start5_159, 13, 6, 1, 2);

        checkBox_210 = new QCheckBox(scrollAreaWidgetContents_11);
        checkBox_210->setObjectName(QStringLiteral("checkBox_210"));
        checkBox_210->setEnabled(true);
        checkBox_210->setBaseSize(QSize(0, 0));
        checkBox_210->setFont(font5);
        checkBox_210->setTabletTracking(false);
        checkBox_210->setAutoFillBackground(false);
        checkBox_210->setChecked(true);
        checkBox_210->setTristate(false);

        gridLayout_43->addWidget(checkBox_210, 15, 1, 1, 1);

        label_ID4_159 = new QLabel(scrollAreaWidgetContents_11);
        label_ID4_159->setObjectName(QStringLiteral("label_ID4_159"));
        label_ID4_159->setFont(font4);
        label_ID4_159->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_43->addWidget(label_ID4_159, 12, 2, 1, 1);

        label_ID4_160 = new QLabel(scrollAreaWidgetContents_11);
        label_ID4_160->setObjectName(QStringLiteral("label_ID4_160"));
        label_ID4_160->setFont(font4);
        label_ID4_160->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_43->addWidget(label_ID4_160, 16, 2, 1, 1);

        spinBox_duration3_21 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_duration3_21->setObjectName(QStringLiteral("spinBox_duration3_21"));
        spinBox_duration3_21->setFont(font6);
        spinBox_duration3_21->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_duration3_21, 8, 9, 1, 2);

        label_ID2_22 = new QLabel(scrollAreaWidgetContents_11);
        label_ID2_22->setObjectName(QStringLiteral("label_ID2_22"));
        label_ID2_22->setFont(font4);
        label_ID2_22->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_43->addWidget(label_ID2_22, 7, 2, 1, 1);

        label_ID4_161 = new QLabel(scrollAreaWidgetContents_11);
        label_ID4_161->setObjectName(QStringLiteral("label_ID4_161"));
        label_ID4_161->setFont(font4);
        label_ID4_161->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_43->addWidget(label_ID4_161, 15, 2, 1, 1);

        checkBox_211 = new QCheckBox(scrollAreaWidgetContents_11);
        checkBox_211->setObjectName(QStringLiteral("checkBox_211"));
        checkBox_211->setEnabled(true);
        checkBox_211->setBaseSize(QSize(0, 0));
        checkBox_211->setFont(font5);
        checkBox_211->setTabletTracking(false);
        checkBox_211->setAutoFillBackground(false);
        checkBox_211->setChecked(false);
        checkBox_211->setTristate(false);

        gridLayout_43->addWidget(checkBox_211, 0, 1, 1, 1);

        circle_inactive3_74 = new QLabel(scrollAreaWidgetContents_11);
        circle_inactive3_74->setObjectName(QStringLiteral("circle_inactive3_74"));
        circle_inactive3_74->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-03.png")));
        circle_inactive3_74->setAlignment(Qt::AlignCenter);

        gridLayout_43->addWidget(circle_inactive3_74, 2, 3, 1, 1);

        label_ID4_162 = new QLabel(scrollAreaWidgetContents_11);
        label_ID4_162->setObjectName(QStringLiteral("label_ID4_162"));
        label_ID4_162->setFont(font4);
        label_ID4_162->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_43->addWidget(label_ID4_162, 13, 2, 1, 1);

        label_ID4_163 = new QLabel(scrollAreaWidgetContents_11);
        label_ID4_163->setObjectName(QStringLiteral("label_ID4_163"));
        label_ID4_163->setFont(font4);
        label_ID4_163->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_43->addWidget(label_ID4_163, 11, 2, 1, 1);

        spinBox_start4_21 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_start4_21->setObjectName(QStringLiteral("spinBox_start4_21"));
        spinBox_start4_21->setFont(font6);
        spinBox_start4_21->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_start4_21, 3, 6, 1, 2);

        textFIeld_name5_140 = new QLineEdit(scrollAreaWidgetContents_11);
        textFIeld_name5_140->setObjectName(QStringLiteral("textFIeld_name5_140"));
        textFIeld_name5_140->setFont(font2);

        gridLayout_43->addWidget(textFIeld_name5_140, 16, 4, 1, 1);

        spinBox_start5_160 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_start5_160->setObjectName(QStringLiteral("spinBox_start5_160"));
        spinBox_start5_160->setFont(font6);
        spinBox_start5_160->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_start5_160, 12, 6, 1, 2);

        spinBox_duration5_141 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_duration5_141->setObjectName(QStringLiteral("spinBox_duration5_141"));
        spinBox_duration5_141->setFont(font6);
        spinBox_duration5_141->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_duration5_141, 18, 9, 1, 2);

        label_ID3_21 = new QLabel(scrollAreaWidgetContents_11);
        label_ID3_21->setObjectName(QStringLiteral("label_ID3_21"));
        label_ID3_21->setFont(font4);
        label_ID3_21->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_43->addWidget(label_ID3_21, 2, 2, 1, 1);

        spinBox_duration1_11 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_duration1_11->setObjectName(QStringLiteral("spinBox_duration1_11"));
        spinBox_duration1_11->setFont(font6);
        spinBox_duration1_11->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_duration1_11, 0, 9, 1, 2);

        spinBox_duration5_142 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_duration5_142->setObjectName(QStringLiteral("spinBox_duration5_142"));
        spinBox_duration5_142->setFont(font6);
        spinBox_duration5_142->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_duration5_142, 16, 9, 1, 2);

        spinBox_start5_161 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_start5_161->setObjectName(QStringLiteral("spinBox_start5_161"));
        spinBox_start5_161->setFont(font6);
        spinBox_start5_161->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_start5_161, 10, 6, 1, 2);

        spinBox_start4_22 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_start4_22->setObjectName(QStringLiteral("spinBox_start4_22"));
        spinBox_start4_22->setFont(font6);
        spinBox_start4_22->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_start4_22, 9, 6, 1, 2);

        spinBox_duration2_22 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_duration2_22->setObjectName(QStringLiteral("spinBox_duration2_22"));
        spinBox_duration2_22->setFont(font6);
        spinBox_duration2_22->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_duration2_22, 1, 9, 1, 2);

        circle_inactive1_131 = new QLabel(scrollAreaWidgetContents_11);
        circle_inactive1_131->setObjectName(QStringLiteral("circle_inactive1_131"));
        circle_inactive1_131->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_131->setAlignment(Qt::AlignCenter);

        gridLayout_43->addWidget(circle_inactive1_131, 0, 3, 1, 1);

        checkBox_212 = new QCheckBox(scrollAreaWidgetContents_11);
        checkBox_212->setObjectName(QStringLiteral("checkBox_212"));
        checkBox_212->setEnabled(true);
        checkBox_212->setBaseSize(QSize(0, 0));
        checkBox_212->setFont(font5);
        checkBox_212->setTabletTracking(false);
        checkBox_212->setAutoFillBackground(false);
        checkBox_212->setChecked(false);
        checkBox_212->setTristate(false);

        gridLayout_43->addWidget(checkBox_212, 1, 1, 1, 1);

        spinBox_duration3_22 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_duration3_22->setObjectName(QStringLiteral("spinBox_duration3_22"));
        spinBox_duration3_22->setFont(font6);
        spinBox_duration3_22->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_duration3_22, 2, 9, 1, 2);

        checkBox_213 = new QCheckBox(scrollAreaWidgetContents_11);
        checkBox_213->setObjectName(QStringLiteral("checkBox_213"));
        checkBox_213->setEnabled(true);
        checkBox_213->setBaseSize(QSize(0, 0));
        checkBox_213->setFont(font5);
        checkBox_213->setTabletTracking(false);
        checkBox_213->setAutoFillBackground(false);
        checkBox_213->setChecked(true);
        checkBox_213->setTristate(false);

        gridLayout_43->addWidget(checkBox_213, 11, 1, 1, 1);

        checkBox_214 = new QCheckBox(scrollAreaWidgetContents_11);
        checkBox_214->setObjectName(QStringLiteral("checkBox_214"));
        checkBox_214->setEnabled(true);
        checkBox_214->setBaseSize(QSize(0, 0));
        checkBox_214->setFont(font5);
        checkBox_214->setTabletTracking(false);
        checkBox_214->setAutoFillBackground(false);
        checkBox_214->setChecked(true);
        checkBox_214->setTristate(false);

        gridLayout_43->addWidget(checkBox_214, 9, 1, 1, 1);

        checkBox_215 = new QCheckBox(scrollAreaWidgetContents_11);
        checkBox_215->setObjectName(QStringLiteral("checkBox_215"));
        checkBox_215->setEnabled(true);
        checkBox_215->setBaseSize(QSize(0, 0));
        checkBox_215->setFont(font5);
        checkBox_215->setTabletTracking(false);
        checkBox_215->setAutoFillBackground(false);
        checkBox_215->setChecked(true);
        checkBox_215->setTristate(false);

        gridLayout_43->addWidget(checkBox_215, 2, 1, 1, 1);

        textFIeld_name5_141 = new QLineEdit(scrollAreaWidgetContents_11);
        textFIeld_name5_141->setObjectName(QStringLiteral("textFIeld_name5_141"));
        textFIeld_name5_141->setFont(font2);

        gridLayout_43->addWidget(textFIeld_name5_141, 15, 4, 1, 1);

        checkBox_216 = new QCheckBox(scrollAreaWidgetContents_11);
        checkBox_216->setObjectName(QStringLiteral("checkBox_216"));
        checkBox_216->setEnabled(true);
        checkBox_216->setBaseSize(QSize(0, 0));
        checkBox_216->setFont(font5);
        checkBox_216->setTabletTracking(false);
        checkBox_216->setAutoFillBackground(false);
        checkBox_216->setChecked(true);
        checkBox_216->setTristate(false);

        gridLayout_43->addWidget(checkBox_216, 10, 1, 1, 1);

        spinBox_start5_162 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_start5_162->setObjectName(QStringLiteral("spinBox_start5_162"));
        spinBox_start5_162->setFont(font6);
        spinBox_start5_162->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_start5_162, 5, 6, 1, 2);

        label_ID3_22 = new QLabel(scrollAreaWidgetContents_11);
        label_ID3_22->setObjectName(QStringLiteral("label_ID3_22"));
        label_ID3_22->setFont(font4);
        label_ID3_22->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_43->addWidget(label_ID3_22, 8, 2, 1, 1);

        label_ID1_11 = new QLabel(scrollAreaWidgetContents_11);
        label_ID1_11->setObjectName(QStringLiteral("label_ID1_11"));
        label_ID1_11->setFont(font4);
        label_ID1_11->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_43->addWidget(label_ID1_11, 0, 2, 1, 1);

        textFIeld_name5_142 = new QLineEdit(scrollAreaWidgetContents_11);
        textFIeld_name5_142->setObjectName(QStringLiteral("textFIeld_name5_142"));
        textFIeld_name5_142->setFont(font2);

        gridLayout_43->addWidget(textFIeld_name5_142, 14, 4, 1, 1);

        spinBox_duration5_143 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_duration5_143->setObjectName(QStringLiteral("spinBox_duration5_143"));
        spinBox_duration5_143->setFont(font6);
        spinBox_duration5_143->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_duration5_143, 13, 9, 1, 2);

        horizontalSpacer_175 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_43->addItem(horizontalSpacer_175, 21, 3, 1, 1);

        label_ID4_164 = new QLabel(scrollAreaWidgetContents_11);
        label_ID4_164->setObjectName(QStringLiteral("label_ID4_164"));
        label_ID4_164->setFont(font4);
        label_ID4_164->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_43->addWidget(label_ID4_164, 18, 2, 1, 1);

        checkBox_217 = new QCheckBox(scrollAreaWidgetContents_11);
        checkBox_217->setObjectName(QStringLiteral("checkBox_217"));
        checkBox_217->setEnabled(true);
        checkBox_217->setBaseSize(QSize(0, 0));
        checkBox_217->setFont(font5);
        checkBox_217->setTabletTracking(false);
        checkBox_217->setAutoFillBackground(false);
        checkBox_217->setChecked(true);
        checkBox_217->setTristate(false);

        gridLayout_43->addWidget(checkBox_217, 18, 1, 1, 1);

        checkBox_218 = new QCheckBox(scrollAreaWidgetContents_11);
        checkBox_218->setObjectName(QStringLiteral("checkBox_218"));
        checkBox_218->setEnabled(true);
        checkBox_218->setBaseSize(QSize(0, 0));
        checkBox_218->setFont(font5);
        checkBox_218->setTabletTracking(false);
        checkBox_218->setAutoFillBackground(false);
        checkBox_218->setChecked(true);
        checkBox_218->setTristate(false);

        gridLayout_43->addWidget(checkBox_218, 20, 1, 1, 1);

        checkBox_219 = new QCheckBox(scrollAreaWidgetContents_11);
        checkBox_219->setObjectName(QStringLiteral("checkBox_219"));
        checkBox_219->setEnabled(true);
        checkBox_219->setBaseSize(QSize(0, 0));
        checkBox_219->setFont(font5);
        checkBox_219->setTabletTracking(false);
        checkBox_219->setAutoFillBackground(false);
        checkBox_219->setChecked(true);
        checkBox_219->setTristate(false);

        gridLayout_43->addWidget(checkBox_219, 17, 1, 1, 1);

        spinBox_start5_163 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_start5_163->setObjectName(QStringLiteral("spinBox_start5_163"));
        spinBox_start5_163->setFont(font6);
        spinBox_start5_163->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_start5_163, 11, 6, 1, 2);

        checkBox_220 = new QCheckBox(scrollAreaWidgetContents_11);
        checkBox_220->setObjectName(QStringLiteral("checkBox_220"));
        checkBox_220->setEnabled(true);
        checkBox_220->setBaseSize(QSize(0, 0));
        checkBox_220->setFont(font5);
        checkBox_220->setTabletTracking(false);
        checkBox_220->setAutoFillBackground(false);
        checkBox_220->setChecked(true);
        checkBox_220->setTristate(false);

        gridLayout_43->addWidget(checkBox_220, 16, 1, 1, 1);

        spinBox_start5_164 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_start5_164->setObjectName(QStringLiteral("spinBox_start5_164"));
        spinBox_start5_164->setFont(font6);
        spinBox_start5_164->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_start5_164, 16, 6, 1, 2);

        label_ID4_165 = new QLabel(scrollAreaWidgetContents_11);
        label_ID4_165->setObjectName(QStringLiteral("label_ID4_165"));
        label_ID4_165->setFont(font4);
        label_ID4_165->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_43->addWidget(label_ID4_165, 17, 2, 1, 1);

        circle_inactive3_75 = new QLabel(scrollAreaWidgetContents_11);
        circle_inactive3_75->setObjectName(QStringLiteral("circle_inactive3_75"));
        circle_inactive3_75->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-03.png")));
        circle_inactive3_75->setAlignment(Qt::AlignCenter);

        gridLayout_43->addWidget(circle_inactive3_75, 4, 3, 1, 1);

        spinBox_start5_165 = new QSpinBox(scrollAreaWidgetContents_11);
        spinBox_start5_165->setObjectName(QStringLiteral("spinBox_start5_165"));
        spinBox_start5_165->setFont(font6);
        spinBox_start5_165->setMaximum(10000000);

        gridLayout_43->addWidget(spinBox_start5_165, 18, 6, 1, 2);

        circle_inactive1_132 = new QLabel(scrollAreaWidgetContents_11);
        circle_inactive1_132->setObjectName(QStringLiteral("circle_inactive1_132"));
        circle_inactive1_132->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_132->setAlignment(Qt::AlignCenter);

        gridLayout_43->addWidget(circle_inactive1_132, 1, 3, 1, 1);

        textFIeld_name5_143 = new QLineEdit(scrollAreaWidgetContents_11);
        textFIeld_name5_143->setObjectName(QStringLiteral("textFIeld_name5_143"));
        textFIeld_name5_143->setFont(font2);

        gridLayout_43->addWidget(textFIeld_name5_143, 17, 4, 1, 1);

        circle_inactive3_76 = new QLabel(scrollAreaWidgetContents_11);
        circle_inactive3_76->setObjectName(QStringLiteral("circle_inactive3_76"));
        circle_inactive3_76->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-03.png")));
        circle_inactive3_76->setAlignment(Qt::AlignCenter);

        gridLayout_43->addWidget(circle_inactive3_76, 6, 3, 1, 1);

        horizontalSpacer_176 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_43->addItem(horizontalSpacer_176, 0, 8, 1, 1);

        circle_inactive3_77 = new QLabel(scrollAreaWidgetContents_11);
        circle_inactive3_77->setObjectName(QStringLiteral("circle_inactive3_77"));
        circle_inactive3_77->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-03.png")));
        circle_inactive3_77->setAlignment(Qt::AlignCenter);

        gridLayout_43->addWidget(circle_inactive3_77, 7, 3, 1, 1);

        circle_inactive1_133 = new QLabel(scrollAreaWidgetContents_11);
        circle_inactive1_133->setObjectName(QStringLiteral("circle_inactive1_133"));
        circle_inactive1_133->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-04.png")));
        circle_inactive1_133->setAlignment(Qt::AlignCenter);

        gridLayout_43->addWidget(circle_inactive1_133, 9, 3, 1, 1);

        circle_inactive1_134 = new QLabel(scrollAreaWidgetContents_11);
        circle_inactive1_134->setObjectName(QStringLiteral("circle_inactive1_134"));
        circle_inactive1_134->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-04.png")));
        circle_inactive1_134->setAlignment(Qt::AlignCenter);

        gridLayout_43->addWidget(circle_inactive1_134, 10, 3, 1, 1);

        circle_inactive1_135 = new QLabel(scrollAreaWidgetContents_11);
        circle_inactive1_135->setObjectName(QStringLiteral("circle_inactive1_135"));
        circle_inactive1_135->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-04.png")));
        circle_inactive1_135->setAlignment(Qt::AlignCenter);

        gridLayout_43->addWidget(circle_inactive1_135, 11, 3, 1, 1);

        circle_inactive1_136 = new QLabel(scrollAreaWidgetContents_11);
        circle_inactive1_136->setObjectName(QStringLiteral("circle_inactive1_136"));
        circle_inactive1_136->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-04.png")));
        circle_inactive1_136->setAlignment(Qt::AlignCenter);

        gridLayout_43->addWidget(circle_inactive1_136, 12, 3, 1, 1);

        circle_inactive1_137 = new QLabel(scrollAreaWidgetContents_11);
        circle_inactive1_137->setObjectName(QStringLiteral("circle_inactive1_137"));
        circle_inactive1_137->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-04.png")));
        circle_inactive1_137->setAlignment(Qt::AlignCenter);

        gridLayout_43->addWidget(circle_inactive1_137, 13, 3, 1, 1);

        circle_inactive1_138 = new QLabel(scrollAreaWidgetContents_11);
        circle_inactive1_138->setObjectName(QStringLiteral("circle_inactive1_138"));
        circle_inactive1_138->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-04.png")));
        circle_inactive1_138->setAlignment(Qt::AlignCenter);

        gridLayout_43->addWidget(circle_inactive1_138, 14, 3, 1, 1);

        circle_inactive1_139 = new QLabel(scrollAreaWidgetContents_11);
        circle_inactive1_139->setObjectName(QStringLiteral("circle_inactive1_139"));
        circle_inactive1_139->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-04.png")));
        circle_inactive1_139->setAlignment(Qt::AlignCenter);

        gridLayout_43->addWidget(circle_inactive1_139, 15, 3, 1, 1);

        circle_inactive1_140 = new QLabel(scrollAreaWidgetContents_11);
        circle_inactive1_140->setObjectName(QStringLiteral("circle_inactive1_140"));
        circle_inactive1_140->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-04.png")));
        circle_inactive1_140->setAlignment(Qt::AlignCenter);

        gridLayout_43->addWidget(circle_inactive1_140, 16, 3, 1, 1);

        circle_inactive1_141 = new QLabel(scrollAreaWidgetContents_11);
        circle_inactive1_141->setObjectName(QStringLiteral("circle_inactive1_141"));
        circle_inactive1_141->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-04.png")));
        circle_inactive1_141->setAlignment(Qt::AlignCenter);

        gridLayout_43->addWidget(circle_inactive1_141, 17, 3, 1, 1);

        circle_inactive1_142 = new QLabel(scrollAreaWidgetContents_11);
        circle_inactive1_142->setObjectName(QStringLiteral("circle_inactive1_142"));
        circle_inactive1_142->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-04.png")));
        circle_inactive1_142->setAlignment(Qt::AlignCenter);

        gridLayout_43->addWidget(circle_inactive1_142, 18, 3, 1, 1);

        circle_inactive1_143 = new QLabel(scrollAreaWidgetContents_11);
        circle_inactive1_143->setObjectName(QStringLiteral("circle_inactive1_143"));
        circle_inactive1_143->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-04.png")));
        circle_inactive1_143->setAlignment(Qt::AlignCenter);

        gridLayout_43->addWidget(circle_inactive1_143, 20, 3, 1, 1);


        gridLayout_42->addLayout(gridLayout_43, 1, 0, 1, 1);

        scrollArea_7->setWidget(scrollAreaWidgetContents_11);

        gridLayout_2->addWidget(scrollArea_7, 1, 0, 1, 1);

        horizontalSpacer_177 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_177, 1, 1, 1, 1);


        gridLayout_3->addLayout(gridLayout_2, 0, 0, 1, 1);

        horizontalSpacer = new QSpacerItem(8, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer, 0, 1, 1, 1);

        tabWidget->addTab(Node_1, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        gridLayout_21 = new QGridLayout(tab_3);
        gridLayout_21->setSpacing(6);
        gridLayout_21->setContentsMargins(11, 11, 11, 11);
        gridLayout_21->setObjectName(QStringLiteral("gridLayout_21"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        horizontalSpacer_55 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_55);

        horizontalSpacer_56 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_56);

        label_ID_6 = new QLabel(tab_3);
        label_ID_6->setObjectName(QStringLiteral("label_ID_6"));
        sizePolicy.setHeightForWidth(label_ID_6->sizePolicy().hasHeightForWidth());
        label_ID_6->setSizePolicy(sizePolicy);
        label_ID_6->setMaximumSize(QSize(16777215, 16777215));
        label_ID_6->setFont(font);
        label_ID_6->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_9->addWidget(label_ID_6);

        horizontalSpacer_65 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_65);

        label_name_7 = new QLabel(tab_3);
        label_name_7->setObjectName(QStringLiteral("label_name_7"));
        label_name_7->setFont(font);
        label_name_7->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_9->addWidget(label_name_7);

        horizontalSpacer_66 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_66);

        horizontalSpacer_67 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_67);

        label_start_6 = new QLabel(tab_3);
        label_start_6->setObjectName(QStringLiteral("label_start_6"));
        sizePolicy1.setHeightForWidth(label_start_6->sizePolicy().hasHeightForWidth());
        label_start_6->setSizePolicy(sizePolicy1);
        label_start_6->setFont(font);
        label_start_6->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_9->addWidget(label_start_6);

        label_msStart_7 = new QLabel(tab_3);
        label_msStart_7->setObjectName(QStringLiteral("label_msStart_7"));
        label_msStart_7->setFont(font1);
        label_msStart_7->setAlignment(Qt::AlignCenter);

        horizontalLayout_9->addWidget(label_msStart_7);

        horizontalSpacer_68 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_68);

        label_duration_6 = new QLabel(tab_3);
        label_duration_6->setObjectName(QStringLiteral("label_duration_6"));
        label_duration_6->setFont(font);
        label_duration_6->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_9->addWidget(label_duration_6);

        label_msDuration_10 = new QLabel(tab_3);
        label_msDuration_10->setObjectName(QStringLiteral("label_msDuration_10"));
        label_msDuration_10->setFont(font1);
        label_msDuration_10->setAlignment(Qt::AlignCenter);

        horizontalLayout_9->addWidget(label_msDuration_10);

        horizontalSpacer_69 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_69);


        gridLayout->addLayout(horizontalLayout_9, 0, 0, 1, 1);

        gridLayout_17 = new QGridLayout();
        gridLayout_17->setSpacing(6);
        gridLayout_17->setObjectName(QStringLiteral("gridLayout_17"));
        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        progressBar_5 = new QProgressBar(tab_3);
        progressBar_5->setObjectName(QStringLiteral("progressBar_5"));
        progressBar_5->setValue(0);

        horizontalLayout_10->addWidget(progressBar_5);

        label_18 = new QLabel(tab_3);
        label_18->setObjectName(QStringLiteral("label_18"));
        label_18->setFont(font3);

        horizontalLayout_10->addWidget(label_18);


        gridLayout_17->addLayout(horizontalLayout_10, 4, 0, 1, 5);

        spinBox_start5_75 = new QSpinBox(tab_3);
        spinBox_start5_75->setObjectName(QStringLiteral("spinBox_start5_75"));
        spinBox_start5_75->setFont(font6);
        spinBox_start5_75->setMaximum(10000000);
        spinBox_start5_75->setValue(0);

        gridLayout_17->addWidget(spinBox_start5_75, 2, 3, 1, 2);

        spinBox_start5_74 = new QSpinBox(tab_3);
        spinBox_start5_74->setObjectName(QStringLiteral("spinBox_start5_74"));
        spinBox_start5_74->setFont(font6);
        spinBox_start5_74->setMaximum(10000000);
        spinBox_start5_74->setValue(0);

        gridLayout_17->addWidget(spinBox_start5_74, 2, 0, 1, 2);

        pushButton_21 = new QPushButton(tab_3);
        pushButton_21->setObjectName(QStringLiteral("pushButton_21"));
        pushButton_21->setMinimumSize(QSize(0, 80));
        pushButton_21->setFont(font2);

        gridLayout_17->addWidget(pushButton_21, 10, 3, 1, 2);

        horizontalSpacer_77 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_17->addItem(horizontalSpacer_77, 8, 2, 1, 1);

        verticalSpacer_21 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_17->addItem(verticalSpacer_21, 7, 0, 1, 1);

        pushButton_19 = new QPushButton(tab_3);
        pushButton_19->setObjectName(QStringLiteral("pushButton_19"));
        pushButton_19->setMinimumSize(QSize(0, 80));
        pushButton_19->setFont(font2);

        gridLayout_17->addWidget(pushButton_19, 8, 0, 1, 2);

        label_17 = new QLabel(tab_3);
        label_17->setObjectName(QStringLiteral("label_17"));
        label_17->setFont(font7);
        label_17->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_17->addWidget(label_17, 3, 0, 1, 5);

        label_19 = new QLabel(tab_3);
        label_19->setObjectName(QStringLiteral("label_19"));
        label_19->setFont(font8);

        gridLayout_17->addWidget(label_19, 1, 3, 1, 1);

        line_5 = new QFrame(tab_3);
        line_5->setObjectName(QStringLiteral("line_5"));
        line_5->setFrameShape(QFrame::HLine);
        line_5->setFrameShadow(QFrame::Sunken);

        gridLayout_17->addWidget(line_5, 6, 0, 1, 5);

        horizontalSpacer_76 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_17->addItem(horizontalSpacer_76, 0, 1, 1, 1);

        verticalSpacer_6 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_17->addItem(verticalSpacer_6, 0, 0, 1, 1);

        pushButton_20 = new QPushButton(tab_3);
        pushButton_20->setObjectName(QStringLiteral("pushButton_20"));
        pushButton_20->setMinimumSize(QSize(0, 80));
        pushButton_20->setFont(font2);

        gridLayout_17->addWidget(pushButton_20, 10, 0, 1, 2);

        label_msDuration_11 = new QLabel(tab_3);
        label_msDuration_11->setObjectName(QStringLiteral("label_msDuration_11"));
        label_msDuration_11->setFont(font1);
        label_msDuration_11->setAlignment(Qt::AlignCenter);

        gridLayout_17->addWidget(label_msDuration_11, 1, 4, 1, 1);

        verticalSpacer_19 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_17->addItem(verticalSpacer_19, 5, 0, 1, 1);

        pushButton_22 = new QPushButton(tab_3);
        pushButton_22->setObjectName(QStringLiteral("pushButton_22"));
        pushButton_22->setMinimumSize(QSize(0, 80));
        pushButton_22->setFont(font2);

        gridLayout_17->addWidget(pushButton_22, 8, 3, 1, 2);

        verticalSpacer_20 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_17->addItem(verticalSpacer_20, 9, 0, 1, 1);

        label_20 = new QLabel(tab_3);
        label_20->setObjectName(QStringLiteral("label_20"));
        label_20->setFont(font8);

        gridLayout_17->addWidget(label_20, 1, 0, 1, 1);

        verticalSpacer_35 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_17->addItem(verticalSpacer_35, 11, 0, 1, 1);


        gridLayout->addLayout(gridLayout_17, 0, 2, 2, 1);

        scrollArea_2 = new QScrollArea(tab_3);
        scrollArea_2->setObjectName(QStringLiteral("scrollArea_2"));
        scrollArea_2->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        scrollArea_2->setWidgetResizable(true);
        scrollAreaWidgetContents_5 = new QWidget();
        scrollAreaWidgetContents_5->setObjectName(QStringLiteral("scrollAreaWidgetContents_5"));
        scrollAreaWidgetContents_5->setGeometry(QRect(0, -224, 654, 740));
        gridLayout_15 = new QGridLayout(scrollAreaWidgetContents_5);
        gridLayout_15->setSpacing(6);
        gridLayout_15->setContentsMargins(11, 11, 11, 11);
        gridLayout_15->setObjectName(QStringLiteral("gridLayout_15"));
        gridLayout_16 = new QGridLayout();
        gridLayout_16->setSpacing(1);
        gridLayout_16->setObjectName(QStringLiteral("gridLayout_16"));
        textFIeld_name5_53 = new QLineEdit(scrollAreaWidgetContents_5);
        textFIeld_name5_53->setObjectName(QStringLiteral("textFIeld_name5_53"));
        textFIeld_name5_53->setFont(font2);

        gridLayout_16->addWidget(textFIeld_name5_53, 12, 4, 1, 1);

        spinBox_start2_9 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_start2_9->setObjectName(QStringLiteral("spinBox_start2_9"));
        spinBox_start2_9->setFont(font6);
        spinBox_start2_9->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_start2_9, 7, 6, 1, 2);

        label_ID4_61 = new QLabel(scrollAreaWidgetContents_5);
        label_ID4_61->setObjectName(QStringLiteral("label_ID4_61"));
        label_ID4_61->setFont(font4);
        label_ID4_61->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_16->addWidget(label_ID4_61, 5, 2, 1, 1);

        circle_inactive3_29 = new QLabel(scrollAreaWidgetContents_5);
        circle_inactive3_29->setObjectName(QStringLiteral("circle_inactive3_29"));
        circle_inactive3_29->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_29->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(circle_inactive3_29, 5, 3, 1, 1);

        horizontalSpacer_70 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_16->addItem(horizontalSpacer_70, 6, 0, 1, 1);

        checkBox_81 = new QCheckBox(scrollAreaWidgetContents_5);
        checkBox_81->setObjectName(QStringLiteral("checkBox_81"));
        checkBox_81->setEnabled(true);
        checkBox_81->setBaseSize(QSize(0, 0));
        checkBox_81->setFont(font5);
        checkBox_81->setTabletTracking(false);
        checkBox_81->setAutoFillBackground(false);
        checkBox_81->setChecked(false);
        checkBox_81->setTristate(false);

        gridLayout_16->addWidget(checkBox_81, 3, 1, 1, 1);

        checkBox_82 = new QCheckBox(scrollAreaWidgetContents_5);
        checkBox_82->setObjectName(QStringLiteral("checkBox_82"));
        checkBox_82->setEnabled(true);
        checkBox_82->setBaseSize(QSize(0, 0));
        checkBox_82->setFont(font5);
        checkBox_82->setTabletTracking(false);
        checkBox_82->setAutoFillBackground(false);
        checkBox_82->setChecked(false);
        checkBox_82->setTristate(false);

        gridLayout_16->addWidget(checkBox_82, 5, 1, 1, 1);

        checkBox_83 = new QCheckBox(scrollAreaWidgetContents_5);
        checkBox_83->setObjectName(QStringLiteral("checkBox_83"));
        checkBox_83->setEnabled(true);
        checkBox_83->setBaseSize(QSize(0, 0));
        checkBox_83->setFont(font5);
        checkBox_83->setTabletTracking(false);
        checkBox_83->setAutoFillBackground(false);
        checkBox_83->setChecked(false);
        checkBox_83->setTristate(false);

        gridLayout_16->addWidget(checkBox_83, 8, 1, 1, 1);

        spinBox_start1_5 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_start1_5->setObjectName(QStringLiteral("spinBox_start1_5"));
        spinBox_start1_5->setFont(font6);
        spinBox_start1_5->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_start1_5, 0, 6, 1, 2);

        checkBox_84 = new QCheckBox(scrollAreaWidgetContents_5);
        checkBox_84->setObjectName(QStringLiteral("checkBox_84"));
        checkBox_84->setEnabled(true);
        checkBox_84->setBaseSize(QSize(0, 0));
        checkBox_84->setFont(font5);
        checkBox_84->setTabletTracking(false);
        checkBox_84->setAutoFillBackground(false);
        checkBox_84->setChecked(false);
        checkBox_84->setTristate(false);

        gridLayout_16->addWidget(checkBox_84, 4, 1, 1, 1);

        circle_inactive3_30 = new QLabel(scrollAreaWidgetContents_5);
        circle_inactive3_30->setObjectName(QStringLiteral("circle_inactive3_30"));
        circle_inactive3_30->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_30->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(circle_inactive3_30, 3, 3, 1, 1);

        textFIeld_name1_5 = new QLineEdit(scrollAreaWidgetContents_5);
        textFIeld_name1_5->setObjectName(QStringLiteral("textFIeld_name1_5"));
        textFIeld_name1_5->setFont(font2);

        gridLayout_16->addWidget(textFIeld_name1_5, 0, 4, 1, 1);

        spinBox_duration2_9 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_duration2_9->setObjectName(QStringLiteral("spinBox_duration2_9"));
        spinBox_duration2_9->setFont(font6);
        spinBox_duration2_9->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_duration2_9, 7, 9, 1, 2);

        spinBox_duration4_9 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_duration4_9->setObjectName(QStringLiteral("spinBox_duration4_9"));
        spinBox_duration4_9->setFont(font6);
        spinBox_duration4_9->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_duration4_9, 3, 9, 1, 2);

        textFIeld_name4_9 = new QLineEdit(scrollAreaWidgetContents_5);
        textFIeld_name4_9->setObjectName(QStringLiteral("textFIeld_name4_9"));
        textFIeld_name4_9->setFont(font2);

        gridLayout_16->addWidget(textFIeld_name4_9, 3, 4, 1, 1);

        spinBox_duration5_53 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_duration5_53->setObjectName(QStringLiteral("spinBox_duration5_53"));
        spinBox_duration5_53->setFont(font6);
        spinBox_duration5_53->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_duration5_53, 6, 9, 1, 2);

        checkBox_85 = new QCheckBox(scrollAreaWidgetContents_5);
        checkBox_85->setObjectName(QStringLiteral("checkBox_85"));
        checkBox_85->setEnabled(true);
        checkBox_85->setBaseSize(QSize(0, 0));
        checkBox_85->setFont(font5);
        checkBox_85->setTabletTracking(false);
        checkBox_85->setAutoFillBackground(false);
        checkBox_85->setChecked(false);
        checkBox_85->setTristate(false);

        gridLayout_16->addWidget(checkBox_85, 14, 1, 1, 1);

        textFIeld_name2_9 = new QLineEdit(scrollAreaWidgetContents_5);
        textFIeld_name2_9->setObjectName(QStringLiteral("textFIeld_name2_9"));
        textFIeld_name2_9->setFont(font2);

        gridLayout_16->addWidget(textFIeld_name2_9, 7, 4, 1, 1);

        spinBox_duration5_54 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_duration5_54->setObjectName(QStringLiteral("spinBox_duration5_54"));
        spinBox_duration5_54->setFont(font6);
        spinBox_duration5_54->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_duration5_54, 10, 9, 1, 2);

        textFIeld_name5_54 = new QLineEdit(scrollAreaWidgetContents_5);
        textFIeld_name5_54->setObjectName(QStringLiteral("textFIeld_name5_54"));
        textFIeld_name5_54->setFont(font2);

        gridLayout_16->addWidget(textFIeld_name5_54, 4, 4, 1, 1);

        spinBox_start5_61 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_start5_61->setObjectName(QStringLiteral("spinBox_start5_61"));
        spinBox_start5_61->setFont(font6);
        spinBox_start5_61->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_start5_61, 4, 6, 1, 2);

        label_ID4_62 = new QLabel(scrollAreaWidgetContents_5);
        label_ID4_62->setObjectName(QStringLiteral("label_ID4_62"));
        label_ID4_62->setFont(font4);
        label_ID4_62->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_16->addWidget(label_ID4_62, 3, 2, 1, 1);

        spinBox_start3_9 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_start3_9->setObjectName(QStringLiteral("spinBox_start3_9"));
        spinBox_start3_9->setFont(font6);
        spinBox_start3_9->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_start3_9, 8, 6, 1, 2);

        label_ID4_63 = new QLabel(scrollAreaWidgetContents_5);
        label_ID4_63->setObjectName(QStringLiteral("label_ID4_63"));
        label_ID4_63->setFont(font4);
        label_ID4_63->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_16->addWidget(label_ID4_63, 9, 2, 1, 1);

        spinBox_duration4_10 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_duration4_10->setObjectName(QStringLiteral("spinBox_duration4_10"));
        spinBox_duration4_10->setFont(font6);
        spinBox_duration4_10->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_duration4_10, 9, 9, 1, 2);

        checkBox_86 = new QCheckBox(scrollAreaWidgetContents_5);
        checkBox_86->setObjectName(QStringLiteral("checkBox_86"));
        checkBox_86->setEnabled(true);
        checkBox_86->setBaseSize(QSize(0, 0));
        checkBox_86->setFont(font5);
        checkBox_86->setTabletTracking(false);
        checkBox_86->setAutoFillBackground(false);
        checkBox_86->setChecked(false);
        checkBox_86->setTristate(false);

        gridLayout_16->addWidget(checkBox_86, 13, 1, 1, 1);

        spinBox_start5_62 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_start5_62->setObjectName(QStringLiteral("spinBox_start5_62"));
        spinBox_start5_62->setFont(font6);
        spinBox_start5_62->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_start5_62, 15, 6, 1, 2);

        spinBox_duration5_55 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_duration5_55->setObjectName(QStringLiteral("spinBox_duration5_55"));
        spinBox_duration5_55->setFont(font6);
        spinBox_duration5_55->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_duration5_55, 15, 9, 1, 2);

        textFIeld_name5_55 = new QLineEdit(scrollAreaWidgetContents_5);
        textFIeld_name5_55->setObjectName(QStringLiteral("textFIeld_name5_55"));
        textFIeld_name5_55->setFont(font2);

        gridLayout_16->addWidget(textFIeld_name5_55, 10, 4, 1, 1);

        label_ID4_64 = new QLabel(scrollAreaWidgetContents_5);
        label_ID4_64->setObjectName(QStringLiteral("label_ID4_64"));
        label_ID4_64->setFont(font4);
        label_ID4_64->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_16->addWidget(label_ID4_64, 14, 2, 1, 1);

        spinBox_start5_63 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_start5_63->setObjectName(QStringLiteral("spinBox_start5_63"));
        spinBox_start5_63->setFont(font6);
        spinBox_start5_63->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_start5_63, 14, 6, 1, 2);

        textFIeld_name4_10 = new QLineEdit(scrollAreaWidgetContents_5);
        textFIeld_name4_10->setObjectName(QStringLiteral("textFIeld_name4_10"));
        textFIeld_name4_10->setFont(font2);

        gridLayout_16->addWidget(textFIeld_name4_10, 9, 4, 1, 1);

        spinBox_start3_10 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_start3_10->setObjectName(QStringLiteral("spinBox_start3_10"));
        spinBox_start3_10->setFont(font6);
        spinBox_start3_10->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_start3_10, 2, 6, 1, 2);

        spinBox_start5_64 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_start5_64->setObjectName(QStringLiteral("spinBox_start5_64"));
        spinBox_start5_64->setFont(font6);
        spinBox_start5_64->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_start5_64, 6, 6, 1, 2);

        horizontalSpacer_71 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_16->addItem(horizontalSpacer_71, 0, 12, 1, 1);

        horizontalSpacer_72 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_16->addItem(horizontalSpacer_72, 10, 5, 1, 1);

        spinBox_duration5_56 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_duration5_56->setObjectName(QStringLiteral("spinBox_duration5_56"));
        spinBox_duration5_56->setFont(font6);
        spinBox_duration5_56->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_duration5_56, 11, 9, 1, 2);

        textFIeld_name5_56 = new QLineEdit(scrollAreaWidgetContents_5);
        textFIeld_name5_56->setObjectName(QStringLiteral("textFIeld_name5_56"));
        textFIeld_name5_56->setFont(font2);

        gridLayout_16->addWidget(textFIeld_name5_56, 5, 4, 1, 1);

        label_ID2_9 = new QLabel(scrollAreaWidgetContents_5);
        label_ID2_9->setObjectName(QStringLiteral("label_ID2_9"));
        label_ID2_9->setFont(font4);
        label_ID2_9->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_16->addWidget(label_ID2_9, 1, 2, 1, 1);

        checkBox_87 = new QCheckBox(scrollAreaWidgetContents_5);
        checkBox_87->setObjectName(QStringLiteral("checkBox_87"));
        checkBox_87->setEnabled(true);
        checkBox_87->setBaseSize(QSize(0, 0));
        checkBox_87->setFont(font5);
        checkBox_87->setTabletTracking(false);
        checkBox_87->setAutoFillBackground(false);
        checkBox_87->setChecked(false);
        checkBox_87->setTristate(false);

        gridLayout_16->addWidget(checkBox_87, 6, 1, 1, 1);

        spinBox_start5_65 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_start5_65->setObjectName(QStringLiteral("spinBox_start5_65"));
        spinBox_start5_65->setFont(font6);
        spinBox_start5_65->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_start5_65, 17, 6, 1, 2);

        textFIeld_name2_10 = new QLineEdit(scrollAreaWidgetContents_5);
        textFIeld_name2_10->setObjectName(QStringLiteral("textFIeld_name2_10"));
        textFIeld_name2_10->setFont(font2);

        gridLayout_16->addWidget(textFIeld_name2_10, 1, 4, 1, 1);

        textFIeld_name5_57 = new QLineEdit(scrollAreaWidgetContents_5);
        textFIeld_name5_57->setObjectName(QStringLiteral("textFIeld_name5_57"));
        textFIeld_name5_57->setFont(font2);

        gridLayout_16->addWidget(textFIeld_name5_57, 6, 4, 1, 1);

        checkBox_88 = new QCheckBox(scrollAreaWidgetContents_5);
        checkBox_88->setObjectName(QStringLiteral("checkBox_88"));
        checkBox_88->setEnabled(true);
        checkBox_88->setBaseSize(QSize(0, 0));
        checkBox_88->setFont(font5);
        checkBox_88->setTabletTracking(false);
        checkBox_88->setAutoFillBackground(false);
        checkBox_88->setChecked(false);
        checkBox_88->setTristate(false);

        gridLayout_16->addWidget(checkBox_88, 7, 1, 1, 1);

        label_ID4_65 = new QLabel(scrollAreaWidgetContents_5);
        label_ID4_65->setObjectName(QStringLiteral("label_ID4_65"));
        label_ID4_65->setFont(font4);
        label_ID4_65->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_16->addWidget(label_ID4_65, 6, 2, 1, 1);

        circle_inactive3_31 = new QLabel(scrollAreaWidgetContents_5);
        circle_inactive3_31->setObjectName(QStringLiteral("circle_inactive3_31"));
        QPalette palette2;
        palette2.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette2.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette2.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        circle_inactive3_31->setPalette(palette2);
        circle_inactive3_31->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_31->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(circle_inactive3_31, 8, 3, 1, 1);

        textFIeld_name3_9 = new QLineEdit(scrollAreaWidgetContents_5);
        textFIeld_name3_9->setObjectName(QStringLiteral("textFIeld_name3_9"));
        textFIeld_name3_9->setFont(font2);

        gridLayout_16->addWidget(textFIeld_name3_9, 2, 4, 1, 1);

        spinBox_duration5_57 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_duration5_57->setObjectName(QStringLiteral("spinBox_duration5_57"));
        spinBox_duration5_57->setFont(font6);
        spinBox_duration5_57->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_duration5_57, 5, 9, 1, 2);

        textFIeld_name3_10 = new QLineEdit(scrollAreaWidgetContents_5);
        textFIeld_name3_10->setObjectName(QStringLiteral("textFIeld_name3_10"));
        textFIeld_name3_10->setFont(font2);

        gridLayout_16->addWidget(textFIeld_name3_10, 8, 4, 1, 1);

        spinBox_duration5_58 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_duration5_58->setObjectName(QStringLiteral("spinBox_duration5_58"));
        spinBox_duration5_58->setFont(font6);
        spinBox_duration5_58->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_duration5_58, 14, 9, 1, 2);

        checkBox_89 = new QCheckBox(scrollAreaWidgetContents_5);
        checkBox_89->setObjectName(QStringLiteral("checkBox_89"));
        checkBox_89->setEnabled(true);
        checkBox_89->setBaseSize(QSize(0, 0));
        checkBox_89->setFont(font5);
        checkBox_89->setTabletTracking(false);
        checkBox_89->setAutoFillBackground(false);
        checkBox_89->setChecked(false);
        checkBox_89->setTristate(false);

        gridLayout_16->addWidget(checkBox_89, 12, 1, 1, 1);

        textFIeld_name5_58 = new QLineEdit(scrollAreaWidgetContents_5);
        textFIeld_name5_58->setObjectName(QStringLiteral("textFIeld_name5_58"));
        textFIeld_name5_58->setFont(font2);

        gridLayout_16->addWidget(textFIeld_name5_58, 18, 4, 1, 1);

        textFIeld_name5_59 = new QLineEdit(scrollAreaWidgetContents_5);
        textFIeld_name5_59->setObjectName(QStringLiteral("textFIeld_name5_59"));
        textFIeld_name5_59->setFont(font2);

        gridLayout_16->addWidget(textFIeld_name5_59, 20, 4, 1, 1);

        spinBox_duration5_59 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_duration5_59->setObjectName(QStringLiteral("spinBox_duration5_59"));
        spinBox_duration5_59->setFont(font6);
        spinBox_duration5_59->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_duration5_59, 4, 9, 1, 2);

        label_ID4_66 = new QLabel(scrollAreaWidgetContents_5);
        label_ID4_66->setObjectName(QStringLiteral("label_ID4_66"));
        label_ID4_66->setFont(font4);
        label_ID4_66->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_16->addWidget(label_ID4_66, 20, 2, 1, 1);

        horizontalSpacer_73 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_16->addItem(horizontalSpacer_73, 10, 8, 1, 1);

        label_ID4_67 = new QLabel(scrollAreaWidgetContents_5);
        label_ID4_67->setObjectName(QStringLiteral("label_ID4_67"));
        label_ID4_67->setFont(font4);
        label_ID4_67->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_16->addWidget(label_ID4_67, 4, 2, 1, 1);

        textFIeld_name5_60 = new QLineEdit(scrollAreaWidgetContents_5);
        textFIeld_name5_60->setObjectName(QStringLiteral("textFIeld_name5_60"));
        textFIeld_name5_60->setFont(font2);

        gridLayout_16->addWidget(textFIeld_name5_60, 13, 4, 1, 1);

        spinBox_start2_10 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_start2_10->setObjectName(QStringLiteral("spinBox_start2_10"));
        spinBox_start2_10->setFont(font6);
        spinBox_start2_10->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_start2_10, 1, 6, 1, 2);

        spinBox_duration5_60 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_duration5_60->setObjectName(QStringLiteral("spinBox_duration5_60"));
        spinBox_duration5_60->setFont(font6);
        spinBox_duration5_60->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_duration5_60, 17, 9, 1, 2);

        label_ID4_68 = new QLabel(scrollAreaWidgetContents_5);
        label_ID4_68->setObjectName(QStringLiteral("label_ID4_68"));
        label_ID4_68->setFont(font4);
        label_ID4_68->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_16->addWidget(label_ID4_68, 10, 2, 1, 1);

        spinBox_duration5_61 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_duration5_61->setObjectName(QStringLiteral("spinBox_duration5_61"));
        spinBox_duration5_61->setFont(font6);
        spinBox_duration5_61->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_duration5_61, 12, 9, 1, 2);

        spinBox_duration5_62 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_duration5_62->setObjectName(QStringLiteral("spinBox_duration5_62"));
        spinBox_duration5_62->setFont(font6);
        spinBox_duration5_62->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_duration5_62, 20, 9, 1, 2);

        spinBox_start5_66 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_start5_66->setObjectName(QStringLiteral("spinBox_start5_66"));
        spinBox_start5_66->setFont(font6);
        spinBox_start5_66->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_start5_66, 20, 6, 1, 2);

        textFIeld_name5_61 = new QLineEdit(scrollAreaWidgetContents_5);
        textFIeld_name5_61->setObjectName(QStringLiteral("textFIeld_name5_61"));
        textFIeld_name5_61->setFont(font2);

        gridLayout_16->addWidget(textFIeld_name5_61, 11, 4, 1, 1);

        spinBox_start5_67 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_start5_67->setObjectName(QStringLiteral("spinBox_start5_67"));
        spinBox_start5_67->setFont(font6);
        spinBox_start5_67->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_start5_67, 13, 6, 1, 2);

        checkBox_90 = new QCheckBox(scrollAreaWidgetContents_5);
        checkBox_90->setObjectName(QStringLiteral("checkBox_90"));
        checkBox_90->setEnabled(true);
        checkBox_90->setBaseSize(QSize(0, 0));
        checkBox_90->setFont(font5);
        checkBox_90->setTabletTracking(false);
        checkBox_90->setAutoFillBackground(false);
        checkBox_90->setChecked(false);
        checkBox_90->setTristate(false);

        gridLayout_16->addWidget(checkBox_90, 15, 1, 1, 1);

        label_ID4_69 = new QLabel(scrollAreaWidgetContents_5);
        label_ID4_69->setObjectName(QStringLiteral("label_ID4_69"));
        label_ID4_69->setFont(font4);
        label_ID4_69->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_16->addWidget(label_ID4_69, 12, 2, 1, 1);

        label_ID4_70 = new QLabel(scrollAreaWidgetContents_5);
        label_ID4_70->setObjectName(QStringLiteral("label_ID4_70"));
        label_ID4_70->setFont(font4);
        label_ID4_70->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_16->addWidget(label_ID4_70, 16, 2, 1, 1);

        spinBox_duration3_9 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_duration3_9->setObjectName(QStringLiteral("spinBox_duration3_9"));
        spinBox_duration3_9->setFont(font6);
        spinBox_duration3_9->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_duration3_9, 8, 9, 1, 2);

        label_ID2_10 = new QLabel(scrollAreaWidgetContents_5);
        label_ID2_10->setObjectName(QStringLiteral("label_ID2_10"));
        label_ID2_10->setFont(font4);
        label_ID2_10->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_16->addWidget(label_ID2_10, 7, 2, 1, 1);

        label_ID4_71 = new QLabel(scrollAreaWidgetContents_5);
        label_ID4_71->setObjectName(QStringLiteral("label_ID4_71"));
        label_ID4_71->setFont(font4);
        label_ID4_71->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_16->addWidget(label_ID4_71, 15, 2, 1, 1);

        checkBox_91 = new QCheckBox(scrollAreaWidgetContents_5);
        checkBox_91->setObjectName(QStringLiteral("checkBox_91"));
        checkBox_91->setEnabled(true);
        checkBox_91->setBaseSize(QSize(0, 0));
        checkBox_91->setFont(font5);
        checkBox_91->setTabletTracking(false);
        checkBox_91->setAutoFillBackground(false);
        checkBox_91->setChecked(false);
        checkBox_91->setTristate(false);

        gridLayout_16->addWidget(checkBox_91, 0, 1, 1, 1);

        circle_inactive3_32 = new QLabel(scrollAreaWidgetContents_5);
        circle_inactive3_32->setObjectName(QStringLiteral("circle_inactive3_32"));
        circle_inactive3_32->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_32->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(circle_inactive3_32, 2, 3, 1, 1);

        label_ID4_72 = new QLabel(scrollAreaWidgetContents_5);
        label_ID4_72->setObjectName(QStringLiteral("label_ID4_72"));
        label_ID4_72->setFont(font4);
        label_ID4_72->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_16->addWidget(label_ID4_72, 13, 2, 1, 1);

        label_ID4_73 = new QLabel(scrollAreaWidgetContents_5);
        label_ID4_73->setObjectName(QStringLiteral("label_ID4_73"));
        label_ID4_73->setFont(font4);
        label_ID4_73->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_16->addWidget(label_ID4_73, 11, 2, 1, 1);

        spinBox_start4_9 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_start4_9->setObjectName(QStringLiteral("spinBox_start4_9"));
        spinBox_start4_9->setFont(font6);
        spinBox_start4_9->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_start4_9, 3, 6, 1, 2);

        textFIeld_name5_62 = new QLineEdit(scrollAreaWidgetContents_5);
        textFIeld_name5_62->setObjectName(QStringLiteral("textFIeld_name5_62"));
        textFIeld_name5_62->setFont(font2);

        gridLayout_16->addWidget(textFIeld_name5_62, 16, 4, 1, 1);

        spinBox_start5_68 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_start5_68->setObjectName(QStringLiteral("spinBox_start5_68"));
        spinBox_start5_68->setFont(font6);
        spinBox_start5_68->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_start5_68, 12, 6, 1, 2);

        spinBox_duration5_63 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_duration5_63->setObjectName(QStringLiteral("spinBox_duration5_63"));
        spinBox_duration5_63->setFont(font6);
        spinBox_duration5_63->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_duration5_63, 18, 9, 1, 2);

        label_ID3_9 = new QLabel(scrollAreaWidgetContents_5);
        label_ID3_9->setObjectName(QStringLiteral("label_ID3_9"));
        label_ID3_9->setFont(font4);
        label_ID3_9->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_16->addWidget(label_ID3_9, 2, 2, 1, 1);

        spinBox_duration1_5 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_duration1_5->setObjectName(QStringLiteral("spinBox_duration1_5"));
        spinBox_duration1_5->setFont(font6);
        spinBox_duration1_5->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_duration1_5, 0, 9, 1, 2);

        spinBox_duration5_64 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_duration5_64->setObjectName(QStringLiteral("spinBox_duration5_64"));
        spinBox_duration5_64->setFont(font6);
        spinBox_duration5_64->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_duration5_64, 16, 9, 1, 2);

        spinBox_start5_69 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_start5_69->setObjectName(QStringLiteral("spinBox_start5_69"));
        spinBox_start5_69->setFont(font6);
        spinBox_start5_69->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_start5_69, 10, 6, 1, 2);

        spinBox_start4_10 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_start4_10->setObjectName(QStringLiteral("spinBox_start4_10"));
        spinBox_start4_10->setFont(font6);
        spinBox_start4_10->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_start4_10, 9, 6, 1, 2);

        spinBox_duration2_10 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_duration2_10->setObjectName(QStringLiteral("spinBox_duration2_10"));
        spinBox_duration2_10->setFont(font6);
        spinBox_duration2_10->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_duration2_10, 1, 9, 1, 2);

        circle_inactive1_53 = new QLabel(scrollAreaWidgetContents_5);
        circle_inactive1_53->setObjectName(QStringLiteral("circle_inactive1_53"));
        circle_inactive1_53->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_53->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(circle_inactive1_53, 0, 3, 1, 1);

        checkBox_92 = new QCheckBox(scrollAreaWidgetContents_5);
        checkBox_92->setObjectName(QStringLiteral("checkBox_92"));
        checkBox_92->setEnabled(true);
        checkBox_92->setBaseSize(QSize(0, 0));
        checkBox_92->setFont(font5);
        checkBox_92->setTabletTracking(false);
        checkBox_92->setAutoFillBackground(false);
        checkBox_92->setChecked(false);
        checkBox_92->setTristate(false);

        gridLayout_16->addWidget(checkBox_92, 1, 1, 1, 1);

        spinBox_duration3_10 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_duration3_10->setObjectName(QStringLiteral("spinBox_duration3_10"));
        spinBox_duration3_10->setFont(font6);
        spinBox_duration3_10->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_duration3_10, 2, 9, 1, 2);

        checkBox_93 = new QCheckBox(scrollAreaWidgetContents_5);
        checkBox_93->setObjectName(QStringLiteral("checkBox_93"));
        checkBox_93->setEnabled(true);
        checkBox_93->setBaseSize(QSize(0, 0));
        checkBox_93->setFont(font5);
        checkBox_93->setTabletTracking(false);
        checkBox_93->setAutoFillBackground(false);
        checkBox_93->setChecked(false);
        checkBox_93->setTristate(false);

        gridLayout_16->addWidget(checkBox_93, 11, 1, 1, 1);

        checkBox_94 = new QCheckBox(scrollAreaWidgetContents_5);
        checkBox_94->setObjectName(QStringLiteral("checkBox_94"));
        checkBox_94->setEnabled(true);
        checkBox_94->setBaseSize(QSize(0, 0));
        checkBox_94->setFont(font5);
        checkBox_94->setTabletTracking(false);
        checkBox_94->setAutoFillBackground(false);
        checkBox_94->setChecked(false);
        checkBox_94->setTristate(false);

        gridLayout_16->addWidget(checkBox_94, 9, 1, 1, 1);

        checkBox_95 = new QCheckBox(scrollAreaWidgetContents_5);
        checkBox_95->setObjectName(QStringLiteral("checkBox_95"));
        checkBox_95->setEnabled(true);
        checkBox_95->setBaseSize(QSize(0, 0));
        checkBox_95->setFont(font5);
        checkBox_95->setTabletTracking(false);
        checkBox_95->setAutoFillBackground(false);
        checkBox_95->setChecked(false);
        checkBox_95->setTristate(false);

        gridLayout_16->addWidget(checkBox_95, 2, 1, 1, 1);

        textFIeld_name5_63 = new QLineEdit(scrollAreaWidgetContents_5);
        textFIeld_name5_63->setObjectName(QStringLiteral("textFIeld_name5_63"));
        textFIeld_name5_63->setFont(font2);

        gridLayout_16->addWidget(textFIeld_name5_63, 15, 4, 1, 1);

        checkBox_96 = new QCheckBox(scrollAreaWidgetContents_5);
        checkBox_96->setObjectName(QStringLiteral("checkBox_96"));
        checkBox_96->setEnabled(true);
        checkBox_96->setBaseSize(QSize(0, 0));
        checkBox_96->setFont(font5);
        checkBox_96->setTabletTracking(false);
        checkBox_96->setAutoFillBackground(false);
        checkBox_96->setChecked(false);
        checkBox_96->setTristate(false);

        gridLayout_16->addWidget(checkBox_96, 10, 1, 1, 1);

        spinBox_start5_70 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_start5_70->setObjectName(QStringLiteral("spinBox_start5_70"));
        spinBox_start5_70->setFont(font6);
        spinBox_start5_70->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_start5_70, 5, 6, 1, 2);

        label_ID3_10 = new QLabel(scrollAreaWidgetContents_5);
        label_ID3_10->setObjectName(QStringLiteral("label_ID3_10"));
        label_ID3_10->setFont(font4);
        label_ID3_10->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_16->addWidget(label_ID3_10, 8, 2, 1, 1);

        label_ID1_5 = new QLabel(scrollAreaWidgetContents_5);
        label_ID1_5->setObjectName(QStringLiteral("label_ID1_5"));
        label_ID1_5->setFont(font4);
        label_ID1_5->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_16->addWidget(label_ID1_5, 0, 2, 1, 1);

        textFIeld_name5_64 = new QLineEdit(scrollAreaWidgetContents_5);
        textFIeld_name5_64->setObjectName(QStringLiteral("textFIeld_name5_64"));
        textFIeld_name5_64->setFont(font2);

        gridLayout_16->addWidget(textFIeld_name5_64, 14, 4, 1, 1);

        spinBox_duration5_65 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_duration5_65->setObjectName(QStringLiteral("spinBox_duration5_65"));
        spinBox_duration5_65->setFont(font6);
        spinBox_duration5_65->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_duration5_65, 13, 9, 1, 2);

        horizontalSpacer_74 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_16->addItem(horizontalSpacer_74, 21, 3, 1, 1);

        label_ID4_74 = new QLabel(scrollAreaWidgetContents_5);
        label_ID4_74->setObjectName(QStringLiteral("label_ID4_74"));
        label_ID4_74->setFont(font4);
        label_ID4_74->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_16->addWidget(label_ID4_74, 18, 2, 1, 1);

        checkBox_97 = new QCheckBox(scrollAreaWidgetContents_5);
        checkBox_97->setObjectName(QStringLiteral("checkBox_97"));
        checkBox_97->setEnabled(true);
        checkBox_97->setBaseSize(QSize(0, 0));
        checkBox_97->setFont(font5);
        checkBox_97->setTabletTracking(false);
        checkBox_97->setAutoFillBackground(false);
        checkBox_97->setChecked(false);
        checkBox_97->setTristate(false);

        gridLayout_16->addWidget(checkBox_97, 18, 1, 1, 1);

        checkBox_98 = new QCheckBox(scrollAreaWidgetContents_5);
        checkBox_98->setObjectName(QStringLiteral("checkBox_98"));
        checkBox_98->setEnabled(true);
        checkBox_98->setBaseSize(QSize(0, 0));
        checkBox_98->setFont(font5);
        checkBox_98->setTabletTracking(false);
        checkBox_98->setAutoFillBackground(false);
        checkBox_98->setChecked(false);
        checkBox_98->setTristate(false);

        gridLayout_16->addWidget(checkBox_98, 20, 1, 1, 1);

        checkBox_99 = new QCheckBox(scrollAreaWidgetContents_5);
        checkBox_99->setObjectName(QStringLiteral("checkBox_99"));
        checkBox_99->setEnabled(true);
        checkBox_99->setBaseSize(QSize(0, 0));
        checkBox_99->setFont(font5);
        checkBox_99->setTabletTracking(false);
        checkBox_99->setAutoFillBackground(false);
        checkBox_99->setChecked(false);
        checkBox_99->setTristate(false);

        gridLayout_16->addWidget(checkBox_99, 17, 1, 1, 1);

        spinBox_start5_71 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_start5_71->setObjectName(QStringLiteral("spinBox_start5_71"));
        spinBox_start5_71->setFont(font6);
        spinBox_start5_71->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_start5_71, 11, 6, 1, 2);

        checkBox_100 = new QCheckBox(scrollAreaWidgetContents_5);
        checkBox_100->setObjectName(QStringLiteral("checkBox_100"));
        checkBox_100->setEnabled(true);
        checkBox_100->setBaseSize(QSize(0, 0));
        checkBox_100->setFont(font5);
        checkBox_100->setTabletTracking(false);
        checkBox_100->setAutoFillBackground(false);
        checkBox_100->setChecked(false);
        checkBox_100->setTristate(false);

        gridLayout_16->addWidget(checkBox_100, 16, 1, 1, 1);

        spinBox_start5_72 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_start5_72->setObjectName(QStringLiteral("spinBox_start5_72"));
        spinBox_start5_72->setFont(font6);
        spinBox_start5_72->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_start5_72, 16, 6, 1, 2);

        label_ID4_75 = new QLabel(scrollAreaWidgetContents_5);
        label_ID4_75->setObjectName(QStringLiteral("label_ID4_75"));
        label_ID4_75->setFont(font4);
        label_ID4_75->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_16->addWidget(label_ID4_75, 17, 2, 1, 1);

        circle_inactive3_33 = new QLabel(scrollAreaWidgetContents_5);
        circle_inactive3_33->setObjectName(QStringLiteral("circle_inactive3_33"));
        circle_inactive3_33->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_33->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(circle_inactive3_33, 4, 3, 1, 1);

        spinBox_start5_73 = new QSpinBox(scrollAreaWidgetContents_5);
        spinBox_start5_73->setObjectName(QStringLiteral("spinBox_start5_73"));
        spinBox_start5_73->setFont(font6);
        spinBox_start5_73->setMaximum(10000000);

        gridLayout_16->addWidget(spinBox_start5_73, 18, 6, 1, 2);

        circle_inactive1_54 = new QLabel(scrollAreaWidgetContents_5);
        circle_inactive1_54->setObjectName(QStringLiteral("circle_inactive1_54"));
        circle_inactive1_54->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_54->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(circle_inactive1_54, 1, 3, 1, 1);

        textFIeld_name5_65 = new QLineEdit(scrollAreaWidgetContents_5);
        textFIeld_name5_65->setObjectName(QStringLiteral("textFIeld_name5_65"));
        textFIeld_name5_65->setFont(font2);

        gridLayout_16->addWidget(textFIeld_name5_65, 17, 4, 1, 1);

        circle_inactive3_34 = new QLabel(scrollAreaWidgetContents_5);
        circle_inactive3_34->setObjectName(QStringLiteral("circle_inactive3_34"));
        circle_inactive3_34->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_34->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(circle_inactive3_34, 6, 3, 1, 1);

        horizontalSpacer_75 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_16->addItem(horizontalSpacer_75, 0, 8, 1, 1);

        circle_inactive3_35 = new QLabel(scrollAreaWidgetContents_5);
        circle_inactive3_35->setObjectName(QStringLiteral("circle_inactive3_35"));
        circle_inactive3_35->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_35->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(circle_inactive3_35, 7, 3, 1, 1);

        circle_inactive1_55 = new QLabel(scrollAreaWidgetContents_5);
        circle_inactive1_55->setObjectName(QStringLiteral("circle_inactive1_55"));
        circle_inactive1_55->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_55->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(circle_inactive1_55, 9, 3, 1, 1);

        circle_inactive1_56 = new QLabel(scrollAreaWidgetContents_5);
        circle_inactive1_56->setObjectName(QStringLiteral("circle_inactive1_56"));
        circle_inactive1_56->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_56->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(circle_inactive1_56, 10, 3, 1, 1);

        circle_inactive1_57 = new QLabel(scrollAreaWidgetContents_5);
        circle_inactive1_57->setObjectName(QStringLiteral("circle_inactive1_57"));
        circle_inactive1_57->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_57->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(circle_inactive1_57, 11, 3, 1, 1);

        circle_inactive1_58 = new QLabel(scrollAreaWidgetContents_5);
        circle_inactive1_58->setObjectName(QStringLiteral("circle_inactive1_58"));
        circle_inactive1_58->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_58->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(circle_inactive1_58, 12, 3, 1, 1);

        circle_inactive1_59 = new QLabel(scrollAreaWidgetContents_5);
        circle_inactive1_59->setObjectName(QStringLiteral("circle_inactive1_59"));
        circle_inactive1_59->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_59->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(circle_inactive1_59, 13, 3, 1, 1);

        circle_inactive1_60 = new QLabel(scrollAreaWidgetContents_5);
        circle_inactive1_60->setObjectName(QStringLiteral("circle_inactive1_60"));
        circle_inactive1_60->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_60->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(circle_inactive1_60, 14, 3, 1, 1);

        circle_inactive1_61 = new QLabel(scrollAreaWidgetContents_5);
        circle_inactive1_61->setObjectName(QStringLiteral("circle_inactive1_61"));
        circle_inactive1_61->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_61->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(circle_inactive1_61, 15, 3, 1, 1);

        circle_inactive1_62 = new QLabel(scrollAreaWidgetContents_5);
        circle_inactive1_62->setObjectName(QStringLiteral("circle_inactive1_62"));
        circle_inactive1_62->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_62->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(circle_inactive1_62, 16, 3, 1, 1);

        circle_inactive1_63 = new QLabel(scrollAreaWidgetContents_5);
        circle_inactive1_63->setObjectName(QStringLiteral("circle_inactive1_63"));
        circle_inactive1_63->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_63->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(circle_inactive1_63, 17, 3, 1, 1);

        circle_inactive1_64 = new QLabel(scrollAreaWidgetContents_5);
        circle_inactive1_64->setObjectName(QStringLiteral("circle_inactive1_64"));
        circle_inactive1_64->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_64->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(circle_inactive1_64, 18, 3, 1, 1);

        circle_inactive1_65 = new QLabel(scrollAreaWidgetContents_5);
        circle_inactive1_65->setObjectName(QStringLiteral("circle_inactive1_65"));
        circle_inactive1_65->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_65->setAlignment(Qt::AlignCenter);

        gridLayout_16->addWidget(circle_inactive1_65, 20, 3, 1, 1);


        gridLayout_15->addLayout(gridLayout_16, 1, 0, 1, 1);

        scrollArea_2->setWidget(scrollAreaWidgetContents_5);

        gridLayout->addWidget(scrollArea_2, 1, 0, 1, 1);

        horizontalSpacer_79 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_79, 1, 1, 1, 1);


        gridLayout_21->addLayout(gridLayout, 0, 0, 1, 1);

        horizontalSpacer_78 = new QSpacerItem(8, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_21->addItem(horizontalSpacer_78, 0, 1, 1, 1);

        tabWidget->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        gridLayout_30 = new QGridLayout(tab_4);
        gridLayout_30->setSpacing(6);
        gridLayout_30->setContentsMargins(11, 11, 11, 11);
        gridLayout_30->setObjectName(QStringLiteral("gridLayout_30"));
        gridLayout_26 = new QGridLayout();
        gridLayout_26->setSpacing(6);
        gridLayout_26->setObjectName(QStringLiteral("gridLayout_26"));
        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setSpacing(6);
        horizontalLayout_15->setObjectName(QStringLiteral("horizontalLayout_15"));
        horizontalSpacer_111 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_111);

        horizontalSpacer_112 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_112);

        label_ID_9 = new QLabel(tab_4);
        label_ID_9->setObjectName(QStringLiteral("label_ID_9"));
        sizePolicy.setHeightForWidth(label_ID_9->sizePolicy().hasHeightForWidth());
        label_ID_9->setSizePolicy(sizePolicy);
        label_ID_9->setMaximumSize(QSize(16777215, 16777215));
        label_ID_9->setFont(font);
        label_ID_9->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_15->addWidget(label_ID_9);

        horizontalSpacer_113 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_113);

        label_name_10 = new QLabel(tab_4);
        label_name_10->setObjectName(QStringLiteral("label_name_10"));
        label_name_10->setFont(font);
        label_name_10->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_15->addWidget(label_name_10);

        horizontalSpacer_114 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_114);

        horizontalSpacer_115 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_115);

        label_start_9 = new QLabel(tab_4);
        label_start_9->setObjectName(QStringLiteral("label_start_9"));
        sizePolicy1.setHeightForWidth(label_start_9->sizePolicy().hasHeightForWidth());
        label_start_9->setSizePolicy(sizePolicy1);
        label_start_9->setFont(font);
        label_start_9->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_15->addWidget(label_start_9);

        label_msStart_10 = new QLabel(tab_4);
        label_msStart_10->setObjectName(QStringLiteral("label_msStart_10"));
        label_msStart_10->setFont(font1);
        label_msStart_10->setAlignment(Qt::AlignCenter);

        horizontalLayout_15->addWidget(label_msStart_10);

        horizontalSpacer_116 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_116);

        label_duration_9 = new QLabel(tab_4);
        label_duration_9->setObjectName(QStringLiteral("label_duration_9"));
        label_duration_9->setFont(font);
        label_duration_9->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_15->addWidget(label_duration_9);

        label_msDuration_16 = new QLabel(tab_4);
        label_msDuration_16->setObjectName(QStringLiteral("label_msDuration_16"));
        label_msDuration_16->setFont(font1);
        label_msDuration_16->setAlignment(Qt::AlignCenter);

        horizontalLayout_15->addWidget(label_msDuration_16);

        horizontalSpacer_117 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_15->addItem(horizontalSpacer_117);


        gridLayout_26->addLayout(horizontalLayout_15, 0, 0, 1, 1);

        gridLayout_27 = new QGridLayout();
        gridLayout_27->setSpacing(6);
        gridLayout_27->setObjectName(QStringLiteral("gridLayout_27"));
        horizontalSpacer_119 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_27->addItem(horizontalSpacer_119, 8, 2, 1, 1);

        verticalSpacer_32 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_27->addItem(verticalSpacer_32, 9, 0, 1, 1);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setSpacing(6);
        horizontalLayout_16->setObjectName(QStringLiteral("horizontalLayout_16"));
        progressBar_8 = new QProgressBar(tab_4);
        progressBar_8->setObjectName(QStringLiteral("progressBar_8"));
        progressBar_8->setValue(0);

        horizontalLayout_16->addWidget(progressBar_8);

        label_30 = new QLabel(tab_4);
        label_30->setObjectName(QStringLiteral("label_30"));
        label_30->setFont(font3);

        horizontalLayout_16->addWidget(label_30);


        gridLayout_27->addLayout(horizontalLayout_16, 4, 0, 1, 5);

        label_32 = new QLabel(tab_4);
        label_32->setObjectName(QStringLiteral("label_32"));
        label_32->setFont(font8);

        gridLayout_27->addWidget(label_32, 1, 0, 1, 1);

        label_31 = new QLabel(tab_4);
        label_31->setObjectName(QStringLiteral("label_31"));
        label_31->setFont(font8);

        gridLayout_27->addWidget(label_31, 1, 3, 1, 1);

        verticalSpacer_31 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_27->addItem(verticalSpacer_31, 5, 0, 1, 1);

        spinBox_start5_107 = new QSpinBox(tab_4);
        spinBox_start5_107->setObjectName(QStringLiteral("spinBox_start5_107"));
        spinBox_start5_107->setFont(font6);
        spinBox_start5_107->setMaximum(10000000);
        spinBox_start5_107->setValue(0);

        gridLayout_27->addWidget(spinBox_start5_107, 2, 3, 1, 2);

        verticalSpacer_33 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_27->addItem(verticalSpacer_33, 7, 0, 1, 1);

        label_29 = new QLabel(tab_4);
        label_29->setObjectName(QStringLiteral("label_29"));
        label_29->setFont(font7);
        label_29->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_27->addWidget(label_29, 3, 0, 1, 5);

        pushButton_31 = new QPushButton(tab_4);
        pushButton_31->setObjectName(QStringLiteral("pushButton_31"));
        pushButton_31->setMinimumSize(QSize(0, 80));
        pushButton_31->setFont(font2);

        gridLayout_27->addWidget(pushButton_31, 8, 0, 1, 2);

        pushButton_33 = new QPushButton(tab_4);
        pushButton_33->setObjectName(QStringLiteral("pushButton_33"));
        pushButton_33->setMinimumSize(QSize(0, 80));
        pushButton_33->setFont(font2);

        gridLayout_27->addWidget(pushButton_33, 10, 3, 1, 2);

        horizontalSpacer_118 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_27->addItem(horizontalSpacer_118, 0, 1, 1, 1);

        spinBox_start5_106 = new QSpinBox(tab_4);
        spinBox_start5_106->setObjectName(QStringLiteral("spinBox_start5_106"));
        spinBox_start5_106->setFont(font6);
        spinBox_start5_106->setMaximum(10000000);
        spinBox_start5_106->setValue(0);

        gridLayout_27->addWidget(spinBox_start5_106, 2, 0, 1, 2);

        pushButton_32 = new QPushButton(tab_4);
        pushButton_32->setObjectName(QStringLiteral("pushButton_32"));
        pushButton_32->setMinimumSize(QSize(0, 80));
        pushButton_32->setFont(font2);

        gridLayout_27->addWidget(pushButton_32, 10, 0, 1, 2);

        verticalSpacer_30 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_27->addItem(verticalSpacer_30, 0, 0, 1, 1);

        line_8 = new QFrame(tab_4);
        line_8->setObjectName(QStringLiteral("line_8"));
        line_8->setFrameShape(QFrame::HLine);
        line_8->setFrameShadow(QFrame::Sunken);

        gridLayout_27->addWidget(line_8, 6, 0, 1, 5);

        pushButton_34 = new QPushButton(tab_4);
        pushButton_34->setObjectName(QStringLiteral("pushButton_34"));
        pushButton_34->setMinimumSize(QSize(0, 80));
        pushButton_34->setFont(font2);

        gridLayout_27->addWidget(pushButton_34, 8, 3, 1, 2);

        label_msDuration_17 = new QLabel(tab_4);
        label_msDuration_17->setObjectName(QStringLiteral("label_msDuration_17"));
        label_msDuration_17->setFont(font1);
        label_msDuration_17->setAlignment(Qt::AlignCenter);

        gridLayout_27->addWidget(label_msDuration_17, 1, 4, 1, 1);

        verticalSpacer_34 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_27->addItem(verticalSpacer_34, 11, 0, 1, 1);


        gridLayout_26->addLayout(gridLayout_27, 0, 2, 2, 1);

        scrollArea_4 = new QScrollArea(tab_4);
        scrollArea_4->setObjectName(QStringLiteral("scrollArea_4"));
        scrollArea_4->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        scrollArea_4->setWidgetResizable(true);
        scrollAreaWidgetContents_8 = new QWidget();
        scrollAreaWidgetContents_8->setObjectName(QStringLiteral("scrollAreaWidgetContents_8"));
        scrollAreaWidgetContents_8->setGeometry(QRect(0, -224, 654, 740));
        gridLayout_28 = new QGridLayout(scrollAreaWidgetContents_8);
        gridLayout_28->setSpacing(6);
        gridLayout_28->setContentsMargins(11, 11, 11, 11);
        gridLayout_28->setObjectName(QStringLiteral("gridLayout_28"));
        gridLayout_29 = new QGridLayout();
        gridLayout_29->setSpacing(1);
        gridLayout_29->setObjectName(QStringLiteral("gridLayout_29"));
        textFIeld_name5_92 = new QLineEdit(scrollAreaWidgetContents_8);
        textFIeld_name5_92->setObjectName(QStringLiteral("textFIeld_name5_92"));
        textFIeld_name5_92->setFont(font2);

        gridLayout_29->addWidget(textFIeld_name5_92, 12, 4, 1, 1);

        spinBox_start2_15 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_start2_15->setObjectName(QStringLiteral("spinBox_start2_15"));
        spinBox_start2_15->setFont(font6);
        spinBox_start2_15->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_start2_15, 7, 6, 1, 2);

        label_ID4_106 = new QLabel(scrollAreaWidgetContents_8);
        label_ID4_106->setObjectName(QStringLiteral("label_ID4_106"));
        label_ID4_106->setFont(font4);
        label_ID4_106->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_29->addWidget(label_ID4_106, 5, 2, 1, 1);

        circle_inactive3_50 = new QLabel(scrollAreaWidgetContents_8);
        circle_inactive3_50->setObjectName(QStringLiteral("circle_inactive3_50"));
        circle_inactive3_50->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_50->setAlignment(Qt::AlignCenter);

        gridLayout_29->addWidget(circle_inactive3_50, 5, 3, 1, 1);

        horizontalSpacer_120 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_29->addItem(horizontalSpacer_120, 6, 0, 1, 1);

        checkBox_141 = new QCheckBox(scrollAreaWidgetContents_8);
        checkBox_141->setObjectName(QStringLiteral("checkBox_141"));
        checkBox_141->setEnabled(true);
        checkBox_141->setBaseSize(QSize(0, 0));
        checkBox_141->setFont(font5);
        checkBox_141->setTabletTracking(false);
        checkBox_141->setAutoFillBackground(false);
        checkBox_141->setChecked(false);
        checkBox_141->setTristate(false);

        gridLayout_29->addWidget(checkBox_141, 3, 1, 1, 1);

        checkBox_142 = new QCheckBox(scrollAreaWidgetContents_8);
        checkBox_142->setObjectName(QStringLiteral("checkBox_142"));
        checkBox_142->setEnabled(true);
        checkBox_142->setBaseSize(QSize(0, 0));
        checkBox_142->setFont(font5);
        checkBox_142->setTabletTracking(false);
        checkBox_142->setAutoFillBackground(false);
        checkBox_142->setChecked(false);
        checkBox_142->setTristate(false);

        gridLayout_29->addWidget(checkBox_142, 5, 1, 1, 1);

        checkBox_143 = new QCheckBox(scrollAreaWidgetContents_8);
        checkBox_143->setObjectName(QStringLiteral("checkBox_143"));
        checkBox_143->setEnabled(true);
        checkBox_143->setBaseSize(QSize(0, 0));
        checkBox_143->setFont(font5);
        checkBox_143->setTabletTracking(false);
        checkBox_143->setAutoFillBackground(false);
        checkBox_143->setChecked(false);
        checkBox_143->setTristate(false);

        gridLayout_29->addWidget(checkBox_143, 8, 1, 1, 1);

        spinBox_start1_8 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_start1_8->setObjectName(QStringLiteral("spinBox_start1_8"));
        spinBox_start1_8->setFont(font6);
        spinBox_start1_8->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_start1_8, 0, 6, 1, 2);

        checkBox_144 = new QCheckBox(scrollAreaWidgetContents_8);
        checkBox_144->setObjectName(QStringLiteral("checkBox_144"));
        checkBox_144->setEnabled(true);
        checkBox_144->setBaseSize(QSize(0, 0));
        checkBox_144->setFont(font5);
        checkBox_144->setTabletTracking(false);
        checkBox_144->setAutoFillBackground(false);
        checkBox_144->setChecked(false);
        checkBox_144->setTristate(false);

        gridLayout_29->addWidget(checkBox_144, 4, 1, 1, 1);

        circle_inactive3_51 = new QLabel(scrollAreaWidgetContents_8);
        circle_inactive3_51->setObjectName(QStringLiteral("circle_inactive3_51"));
        circle_inactive3_51->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_51->setAlignment(Qt::AlignCenter);

        gridLayout_29->addWidget(circle_inactive3_51, 3, 3, 1, 1);

        textFIeld_name1_8 = new QLineEdit(scrollAreaWidgetContents_8);
        textFIeld_name1_8->setObjectName(QStringLiteral("textFIeld_name1_8"));
        textFIeld_name1_8->setFont(font2);

        gridLayout_29->addWidget(textFIeld_name1_8, 0, 4, 1, 1);

        spinBox_duration2_15 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_duration2_15->setObjectName(QStringLiteral("spinBox_duration2_15"));
        spinBox_duration2_15->setFont(font6);
        spinBox_duration2_15->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_duration2_15, 7, 9, 1, 2);

        spinBox_duration4_15 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_duration4_15->setObjectName(QStringLiteral("spinBox_duration4_15"));
        spinBox_duration4_15->setFont(font6);
        spinBox_duration4_15->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_duration4_15, 3, 9, 1, 2);

        textFIeld_name4_15 = new QLineEdit(scrollAreaWidgetContents_8);
        textFIeld_name4_15->setObjectName(QStringLiteral("textFIeld_name4_15"));
        textFIeld_name4_15->setFont(font2);

        gridLayout_29->addWidget(textFIeld_name4_15, 3, 4, 1, 1);

        spinBox_duration5_92 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_duration5_92->setObjectName(QStringLiteral("spinBox_duration5_92"));
        spinBox_duration5_92->setFont(font6);
        spinBox_duration5_92->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_duration5_92, 6, 9, 1, 2);

        checkBox_145 = new QCheckBox(scrollAreaWidgetContents_8);
        checkBox_145->setObjectName(QStringLiteral("checkBox_145"));
        checkBox_145->setEnabled(true);
        checkBox_145->setBaseSize(QSize(0, 0));
        checkBox_145->setFont(font5);
        checkBox_145->setTabletTracking(false);
        checkBox_145->setAutoFillBackground(false);
        checkBox_145->setChecked(false);
        checkBox_145->setTristate(false);

        gridLayout_29->addWidget(checkBox_145, 14, 1, 1, 1);

        textFIeld_name2_15 = new QLineEdit(scrollAreaWidgetContents_8);
        textFIeld_name2_15->setObjectName(QStringLiteral("textFIeld_name2_15"));
        textFIeld_name2_15->setFont(font2);

        gridLayout_29->addWidget(textFIeld_name2_15, 7, 4, 1, 1);

        spinBox_duration5_93 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_duration5_93->setObjectName(QStringLiteral("spinBox_duration5_93"));
        spinBox_duration5_93->setFont(font6);
        spinBox_duration5_93->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_duration5_93, 10, 9, 1, 2);

        textFIeld_name5_93 = new QLineEdit(scrollAreaWidgetContents_8);
        textFIeld_name5_93->setObjectName(QStringLiteral("textFIeld_name5_93"));
        textFIeld_name5_93->setFont(font2);

        gridLayout_29->addWidget(textFIeld_name5_93, 4, 4, 1, 1);

        spinBox_start5_108 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_start5_108->setObjectName(QStringLiteral("spinBox_start5_108"));
        spinBox_start5_108->setFont(font6);
        spinBox_start5_108->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_start5_108, 4, 6, 1, 2);

        label_ID4_107 = new QLabel(scrollAreaWidgetContents_8);
        label_ID4_107->setObjectName(QStringLiteral("label_ID4_107"));
        label_ID4_107->setFont(font4);
        label_ID4_107->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_29->addWidget(label_ID4_107, 3, 2, 1, 1);

        spinBox_start3_15 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_start3_15->setObjectName(QStringLiteral("spinBox_start3_15"));
        spinBox_start3_15->setFont(font6);
        spinBox_start3_15->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_start3_15, 8, 6, 1, 2);

        label_ID4_108 = new QLabel(scrollAreaWidgetContents_8);
        label_ID4_108->setObjectName(QStringLiteral("label_ID4_108"));
        label_ID4_108->setFont(font4);
        label_ID4_108->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_29->addWidget(label_ID4_108, 9, 2, 1, 1);

        spinBox_duration4_16 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_duration4_16->setObjectName(QStringLiteral("spinBox_duration4_16"));
        spinBox_duration4_16->setFont(font6);
        spinBox_duration4_16->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_duration4_16, 9, 9, 1, 2);

        checkBox_146 = new QCheckBox(scrollAreaWidgetContents_8);
        checkBox_146->setObjectName(QStringLiteral("checkBox_146"));
        checkBox_146->setEnabled(true);
        checkBox_146->setBaseSize(QSize(0, 0));
        checkBox_146->setFont(font5);
        checkBox_146->setTabletTracking(false);
        checkBox_146->setAutoFillBackground(false);
        checkBox_146->setChecked(false);
        checkBox_146->setTristate(false);

        gridLayout_29->addWidget(checkBox_146, 13, 1, 1, 1);

        spinBox_start5_109 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_start5_109->setObjectName(QStringLiteral("spinBox_start5_109"));
        spinBox_start5_109->setFont(font6);
        spinBox_start5_109->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_start5_109, 15, 6, 1, 2);

        spinBox_duration5_94 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_duration5_94->setObjectName(QStringLiteral("spinBox_duration5_94"));
        spinBox_duration5_94->setFont(font6);
        spinBox_duration5_94->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_duration5_94, 15, 9, 1, 2);

        textFIeld_name5_94 = new QLineEdit(scrollAreaWidgetContents_8);
        textFIeld_name5_94->setObjectName(QStringLiteral("textFIeld_name5_94"));
        textFIeld_name5_94->setFont(font2);

        gridLayout_29->addWidget(textFIeld_name5_94, 10, 4, 1, 1);

        label_ID4_109 = new QLabel(scrollAreaWidgetContents_8);
        label_ID4_109->setObjectName(QStringLiteral("label_ID4_109"));
        label_ID4_109->setFont(font4);
        label_ID4_109->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_29->addWidget(label_ID4_109, 14, 2, 1, 1);

        spinBox_start5_110 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_start5_110->setObjectName(QStringLiteral("spinBox_start5_110"));
        spinBox_start5_110->setFont(font6);
        spinBox_start5_110->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_start5_110, 14, 6, 1, 2);

        textFIeld_name4_16 = new QLineEdit(scrollAreaWidgetContents_8);
        textFIeld_name4_16->setObjectName(QStringLiteral("textFIeld_name4_16"));
        textFIeld_name4_16->setFont(font2);

        gridLayout_29->addWidget(textFIeld_name4_16, 9, 4, 1, 1);

        spinBox_start3_16 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_start3_16->setObjectName(QStringLiteral("spinBox_start3_16"));
        spinBox_start3_16->setFont(font6);
        spinBox_start3_16->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_start3_16, 2, 6, 1, 2);

        spinBox_start5_111 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_start5_111->setObjectName(QStringLiteral("spinBox_start5_111"));
        spinBox_start5_111->setFont(font6);
        spinBox_start5_111->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_start5_111, 6, 6, 1, 2);

        horizontalSpacer_121 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_29->addItem(horizontalSpacer_121, 0, 12, 1, 1);

        horizontalSpacer_122 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_29->addItem(horizontalSpacer_122, 10, 5, 1, 1);

        spinBox_duration5_95 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_duration5_95->setObjectName(QStringLiteral("spinBox_duration5_95"));
        spinBox_duration5_95->setFont(font6);
        spinBox_duration5_95->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_duration5_95, 11, 9, 1, 2);

        textFIeld_name5_95 = new QLineEdit(scrollAreaWidgetContents_8);
        textFIeld_name5_95->setObjectName(QStringLiteral("textFIeld_name5_95"));
        textFIeld_name5_95->setFont(font2);

        gridLayout_29->addWidget(textFIeld_name5_95, 5, 4, 1, 1);

        label_ID2_15 = new QLabel(scrollAreaWidgetContents_8);
        label_ID2_15->setObjectName(QStringLiteral("label_ID2_15"));
        label_ID2_15->setFont(font4);
        label_ID2_15->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_29->addWidget(label_ID2_15, 1, 2, 1, 1);

        checkBox_147 = new QCheckBox(scrollAreaWidgetContents_8);
        checkBox_147->setObjectName(QStringLiteral("checkBox_147"));
        checkBox_147->setEnabled(true);
        checkBox_147->setBaseSize(QSize(0, 0));
        checkBox_147->setFont(font5);
        checkBox_147->setTabletTracking(false);
        checkBox_147->setAutoFillBackground(false);
        checkBox_147->setChecked(false);
        checkBox_147->setTristate(false);

        gridLayout_29->addWidget(checkBox_147, 6, 1, 1, 1);

        spinBox_start5_112 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_start5_112->setObjectName(QStringLiteral("spinBox_start5_112"));
        spinBox_start5_112->setFont(font6);
        spinBox_start5_112->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_start5_112, 17, 6, 1, 2);

        textFIeld_name2_16 = new QLineEdit(scrollAreaWidgetContents_8);
        textFIeld_name2_16->setObjectName(QStringLiteral("textFIeld_name2_16"));
        textFIeld_name2_16->setFont(font2);

        gridLayout_29->addWidget(textFIeld_name2_16, 1, 4, 1, 1);

        textFIeld_name5_96 = new QLineEdit(scrollAreaWidgetContents_8);
        textFIeld_name5_96->setObjectName(QStringLiteral("textFIeld_name5_96"));
        textFIeld_name5_96->setFont(font2);

        gridLayout_29->addWidget(textFIeld_name5_96, 6, 4, 1, 1);

        checkBox_148 = new QCheckBox(scrollAreaWidgetContents_8);
        checkBox_148->setObjectName(QStringLiteral("checkBox_148"));
        checkBox_148->setEnabled(true);
        checkBox_148->setBaseSize(QSize(0, 0));
        checkBox_148->setFont(font5);
        checkBox_148->setTabletTracking(false);
        checkBox_148->setAutoFillBackground(false);
        checkBox_148->setChecked(false);
        checkBox_148->setTristate(false);

        gridLayout_29->addWidget(checkBox_148, 7, 1, 1, 1);

        label_ID4_110 = new QLabel(scrollAreaWidgetContents_8);
        label_ID4_110->setObjectName(QStringLiteral("label_ID4_110"));
        label_ID4_110->setFont(font4);
        label_ID4_110->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_29->addWidget(label_ID4_110, 6, 2, 1, 1);

        circle_inactive3_52 = new QLabel(scrollAreaWidgetContents_8);
        circle_inactive3_52->setObjectName(QStringLiteral("circle_inactive3_52"));
        QPalette palette3;
        palette3.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette3.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette3.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        circle_inactive3_52->setPalette(palette3);
        circle_inactive3_52->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_52->setAlignment(Qt::AlignCenter);

        gridLayout_29->addWidget(circle_inactive3_52, 8, 3, 1, 1);

        textFIeld_name3_15 = new QLineEdit(scrollAreaWidgetContents_8);
        textFIeld_name3_15->setObjectName(QStringLiteral("textFIeld_name3_15"));
        textFIeld_name3_15->setFont(font2);

        gridLayout_29->addWidget(textFIeld_name3_15, 2, 4, 1, 1);

        spinBox_duration5_96 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_duration5_96->setObjectName(QStringLiteral("spinBox_duration5_96"));
        spinBox_duration5_96->setFont(font6);
        spinBox_duration5_96->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_duration5_96, 5, 9, 1, 2);

        textFIeld_name3_16 = new QLineEdit(scrollAreaWidgetContents_8);
        textFIeld_name3_16->setObjectName(QStringLiteral("textFIeld_name3_16"));
        textFIeld_name3_16->setFont(font2);

        gridLayout_29->addWidget(textFIeld_name3_16, 8, 4, 1, 1);

        spinBox_duration5_97 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_duration5_97->setObjectName(QStringLiteral("spinBox_duration5_97"));
        spinBox_duration5_97->setFont(font6);
        spinBox_duration5_97->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_duration5_97, 14, 9, 1, 2);

        checkBox_149 = new QCheckBox(scrollAreaWidgetContents_8);
        checkBox_149->setObjectName(QStringLiteral("checkBox_149"));
        checkBox_149->setEnabled(true);
        checkBox_149->setBaseSize(QSize(0, 0));
        checkBox_149->setFont(font5);
        checkBox_149->setTabletTracking(false);
        checkBox_149->setAutoFillBackground(false);
        checkBox_149->setChecked(false);
        checkBox_149->setTristate(false);

        gridLayout_29->addWidget(checkBox_149, 12, 1, 1, 1);

        textFIeld_name5_97 = new QLineEdit(scrollAreaWidgetContents_8);
        textFIeld_name5_97->setObjectName(QStringLiteral("textFIeld_name5_97"));
        textFIeld_name5_97->setFont(font2);

        gridLayout_29->addWidget(textFIeld_name5_97, 18, 4, 1, 1);

        textFIeld_name5_98 = new QLineEdit(scrollAreaWidgetContents_8);
        textFIeld_name5_98->setObjectName(QStringLiteral("textFIeld_name5_98"));
        textFIeld_name5_98->setFont(font2);

        gridLayout_29->addWidget(textFIeld_name5_98, 20, 4, 1, 1);

        spinBox_duration5_98 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_duration5_98->setObjectName(QStringLiteral("spinBox_duration5_98"));
        spinBox_duration5_98->setFont(font6);
        spinBox_duration5_98->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_duration5_98, 4, 9, 1, 2);

        label_ID4_111 = new QLabel(scrollAreaWidgetContents_8);
        label_ID4_111->setObjectName(QStringLiteral("label_ID4_111"));
        label_ID4_111->setFont(font4);
        label_ID4_111->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_29->addWidget(label_ID4_111, 20, 2, 1, 1);

        horizontalSpacer_123 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_29->addItem(horizontalSpacer_123, 10, 8, 1, 1);

        label_ID4_112 = new QLabel(scrollAreaWidgetContents_8);
        label_ID4_112->setObjectName(QStringLiteral("label_ID4_112"));
        label_ID4_112->setFont(font4);
        label_ID4_112->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_29->addWidget(label_ID4_112, 4, 2, 1, 1);

        textFIeld_name5_99 = new QLineEdit(scrollAreaWidgetContents_8);
        textFIeld_name5_99->setObjectName(QStringLiteral("textFIeld_name5_99"));
        textFIeld_name5_99->setFont(font2);

        gridLayout_29->addWidget(textFIeld_name5_99, 13, 4, 1, 1);

        spinBox_start2_16 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_start2_16->setObjectName(QStringLiteral("spinBox_start2_16"));
        spinBox_start2_16->setFont(font6);
        spinBox_start2_16->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_start2_16, 1, 6, 1, 2);

        spinBox_duration5_99 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_duration5_99->setObjectName(QStringLiteral("spinBox_duration5_99"));
        spinBox_duration5_99->setFont(font6);
        spinBox_duration5_99->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_duration5_99, 17, 9, 1, 2);

        label_ID4_113 = new QLabel(scrollAreaWidgetContents_8);
        label_ID4_113->setObjectName(QStringLiteral("label_ID4_113"));
        label_ID4_113->setFont(font4);
        label_ID4_113->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_29->addWidget(label_ID4_113, 10, 2, 1, 1);

        spinBox_duration5_100 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_duration5_100->setObjectName(QStringLiteral("spinBox_duration5_100"));
        spinBox_duration5_100->setFont(font6);
        spinBox_duration5_100->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_duration5_100, 12, 9, 1, 2);

        spinBox_duration5_101 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_duration5_101->setObjectName(QStringLiteral("spinBox_duration5_101"));
        spinBox_duration5_101->setFont(font6);
        spinBox_duration5_101->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_duration5_101, 20, 9, 1, 2);

        spinBox_start5_113 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_start5_113->setObjectName(QStringLiteral("spinBox_start5_113"));
        spinBox_start5_113->setFont(font6);
        spinBox_start5_113->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_start5_113, 20, 6, 1, 2);

        textFIeld_name5_100 = new QLineEdit(scrollAreaWidgetContents_8);
        textFIeld_name5_100->setObjectName(QStringLiteral("textFIeld_name5_100"));
        textFIeld_name5_100->setFont(font2);

        gridLayout_29->addWidget(textFIeld_name5_100, 11, 4, 1, 1);

        spinBox_start5_114 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_start5_114->setObjectName(QStringLiteral("spinBox_start5_114"));
        spinBox_start5_114->setFont(font6);
        spinBox_start5_114->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_start5_114, 13, 6, 1, 2);

        checkBox_150 = new QCheckBox(scrollAreaWidgetContents_8);
        checkBox_150->setObjectName(QStringLiteral("checkBox_150"));
        checkBox_150->setEnabled(true);
        checkBox_150->setBaseSize(QSize(0, 0));
        checkBox_150->setFont(font5);
        checkBox_150->setTabletTracking(false);
        checkBox_150->setAutoFillBackground(false);
        checkBox_150->setChecked(false);
        checkBox_150->setTristate(false);

        gridLayout_29->addWidget(checkBox_150, 15, 1, 1, 1);

        label_ID4_114 = new QLabel(scrollAreaWidgetContents_8);
        label_ID4_114->setObjectName(QStringLiteral("label_ID4_114"));
        label_ID4_114->setFont(font4);
        label_ID4_114->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_29->addWidget(label_ID4_114, 12, 2, 1, 1);

        label_ID4_115 = new QLabel(scrollAreaWidgetContents_8);
        label_ID4_115->setObjectName(QStringLiteral("label_ID4_115"));
        label_ID4_115->setFont(font4);
        label_ID4_115->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_29->addWidget(label_ID4_115, 16, 2, 1, 1);

        spinBox_duration3_15 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_duration3_15->setObjectName(QStringLiteral("spinBox_duration3_15"));
        spinBox_duration3_15->setFont(font6);
        spinBox_duration3_15->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_duration3_15, 8, 9, 1, 2);

        label_ID2_16 = new QLabel(scrollAreaWidgetContents_8);
        label_ID2_16->setObjectName(QStringLiteral("label_ID2_16"));
        label_ID2_16->setFont(font4);
        label_ID2_16->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_29->addWidget(label_ID2_16, 7, 2, 1, 1);

        label_ID4_116 = new QLabel(scrollAreaWidgetContents_8);
        label_ID4_116->setObjectName(QStringLiteral("label_ID4_116"));
        label_ID4_116->setFont(font4);
        label_ID4_116->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_29->addWidget(label_ID4_116, 15, 2, 1, 1);

        checkBox_151 = new QCheckBox(scrollAreaWidgetContents_8);
        checkBox_151->setObjectName(QStringLiteral("checkBox_151"));
        checkBox_151->setEnabled(true);
        checkBox_151->setBaseSize(QSize(0, 0));
        checkBox_151->setFont(font5);
        checkBox_151->setTabletTracking(false);
        checkBox_151->setAutoFillBackground(false);
        checkBox_151->setChecked(false);
        checkBox_151->setTristate(false);

        gridLayout_29->addWidget(checkBox_151, 0, 1, 1, 1);

        circle_inactive3_53 = new QLabel(scrollAreaWidgetContents_8);
        circle_inactive3_53->setObjectName(QStringLiteral("circle_inactive3_53"));
        circle_inactive3_53->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_53->setAlignment(Qt::AlignCenter);

        gridLayout_29->addWidget(circle_inactive3_53, 2, 3, 1, 1);

        label_ID4_117 = new QLabel(scrollAreaWidgetContents_8);
        label_ID4_117->setObjectName(QStringLiteral("label_ID4_117"));
        label_ID4_117->setFont(font4);
        label_ID4_117->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_29->addWidget(label_ID4_117, 13, 2, 1, 1);

        label_ID4_118 = new QLabel(scrollAreaWidgetContents_8);
        label_ID4_118->setObjectName(QStringLiteral("label_ID4_118"));
        label_ID4_118->setFont(font4);
        label_ID4_118->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_29->addWidget(label_ID4_118, 11, 2, 1, 1);

        spinBox_start4_15 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_start4_15->setObjectName(QStringLiteral("spinBox_start4_15"));
        spinBox_start4_15->setFont(font6);
        spinBox_start4_15->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_start4_15, 3, 6, 1, 2);

        textFIeld_name5_101 = new QLineEdit(scrollAreaWidgetContents_8);
        textFIeld_name5_101->setObjectName(QStringLiteral("textFIeld_name5_101"));
        textFIeld_name5_101->setFont(font2);

        gridLayout_29->addWidget(textFIeld_name5_101, 16, 4, 1, 1);

        spinBox_start5_115 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_start5_115->setObjectName(QStringLiteral("spinBox_start5_115"));
        spinBox_start5_115->setFont(font6);
        spinBox_start5_115->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_start5_115, 12, 6, 1, 2);

        spinBox_duration5_102 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_duration5_102->setObjectName(QStringLiteral("spinBox_duration5_102"));
        spinBox_duration5_102->setFont(font6);
        spinBox_duration5_102->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_duration5_102, 18, 9, 1, 2);

        label_ID3_15 = new QLabel(scrollAreaWidgetContents_8);
        label_ID3_15->setObjectName(QStringLiteral("label_ID3_15"));
        label_ID3_15->setFont(font4);
        label_ID3_15->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_29->addWidget(label_ID3_15, 2, 2, 1, 1);

        spinBox_duration1_8 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_duration1_8->setObjectName(QStringLiteral("spinBox_duration1_8"));
        spinBox_duration1_8->setFont(font6);
        spinBox_duration1_8->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_duration1_8, 0, 9, 1, 2);

        spinBox_duration5_103 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_duration5_103->setObjectName(QStringLiteral("spinBox_duration5_103"));
        spinBox_duration5_103->setFont(font6);
        spinBox_duration5_103->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_duration5_103, 16, 9, 1, 2);

        spinBox_start5_116 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_start5_116->setObjectName(QStringLiteral("spinBox_start5_116"));
        spinBox_start5_116->setFont(font6);
        spinBox_start5_116->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_start5_116, 10, 6, 1, 2);

        spinBox_start4_16 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_start4_16->setObjectName(QStringLiteral("spinBox_start4_16"));
        spinBox_start4_16->setFont(font6);
        spinBox_start4_16->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_start4_16, 9, 6, 1, 2);

        spinBox_duration2_16 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_duration2_16->setObjectName(QStringLiteral("spinBox_duration2_16"));
        spinBox_duration2_16->setFont(font6);
        spinBox_duration2_16->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_duration2_16, 1, 9, 1, 2);

        circle_inactive1_92 = new QLabel(scrollAreaWidgetContents_8);
        circle_inactive1_92->setObjectName(QStringLiteral("circle_inactive1_92"));
        circle_inactive1_92->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_92->setAlignment(Qt::AlignCenter);

        gridLayout_29->addWidget(circle_inactive1_92, 0, 3, 1, 1);

        checkBox_152 = new QCheckBox(scrollAreaWidgetContents_8);
        checkBox_152->setObjectName(QStringLiteral("checkBox_152"));
        checkBox_152->setEnabled(true);
        checkBox_152->setBaseSize(QSize(0, 0));
        checkBox_152->setFont(font5);
        checkBox_152->setTabletTracking(false);
        checkBox_152->setAutoFillBackground(false);
        checkBox_152->setChecked(false);
        checkBox_152->setTristate(false);

        gridLayout_29->addWidget(checkBox_152, 1, 1, 1, 1);

        spinBox_duration3_16 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_duration3_16->setObjectName(QStringLiteral("spinBox_duration3_16"));
        spinBox_duration3_16->setFont(font6);
        spinBox_duration3_16->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_duration3_16, 2, 9, 1, 2);

        checkBox_153 = new QCheckBox(scrollAreaWidgetContents_8);
        checkBox_153->setObjectName(QStringLiteral("checkBox_153"));
        checkBox_153->setEnabled(true);
        checkBox_153->setBaseSize(QSize(0, 0));
        checkBox_153->setFont(font5);
        checkBox_153->setTabletTracking(false);
        checkBox_153->setAutoFillBackground(false);
        checkBox_153->setChecked(false);
        checkBox_153->setTristate(false);

        gridLayout_29->addWidget(checkBox_153, 11, 1, 1, 1);

        checkBox_154 = new QCheckBox(scrollAreaWidgetContents_8);
        checkBox_154->setObjectName(QStringLiteral("checkBox_154"));
        checkBox_154->setEnabled(true);
        checkBox_154->setBaseSize(QSize(0, 0));
        checkBox_154->setFont(font5);
        checkBox_154->setTabletTracking(false);
        checkBox_154->setAutoFillBackground(false);
        checkBox_154->setChecked(false);
        checkBox_154->setTristate(false);

        gridLayout_29->addWidget(checkBox_154, 9, 1, 1, 1);

        checkBox_155 = new QCheckBox(scrollAreaWidgetContents_8);
        checkBox_155->setObjectName(QStringLiteral("checkBox_155"));
        checkBox_155->setEnabled(true);
        checkBox_155->setBaseSize(QSize(0, 0));
        checkBox_155->setFont(font5);
        checkBox_155->setTabletTracking(false);
        checkBox_155->setAutoFillBackground(false);
        checkBox_155->setChecked(false);
        checkBox_155->setTristate(false);

        gridLayout_29->addWidget(checkBox_155, 2, 1, 1, 1);

        textFIeld_name5_102 = new QLineEdit(scrollAreaWidgetContents_8);
        textFIeld_name5_102->setObjectName(QStringLiteral("textFIeld_name5_102"));
        textFIeld_name5_102->setFont(font2);

        gridLayout_29->addWidget(textFIeld_name5_102, 15, 4, 1, 1);

        checkBox_156 = new QCheckBox(scrollAreaWidgetContents_8);
        checkBox_156->setObjectName(QStringLiteral("checkBox_156"));
        checkBox_156->setEnabled(true);
        checkBox_156->setBaseSize(QSize(0, 0));
        checkBox_156->setFont(font5);
        checkBox_156->setTabletTracking(false);
        checkBox_156->setAutoFillBackground(false);
        checkBox_156->setChecked(false);
        checkBox_156->setTristate(false);

        gridLayout_29->addWidget(checkBox_156, 10, 1, 1, 1);

        spinBox_start5_117 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_start5_117->setObjectName(QStringLiteral("spinBox_start5_117"));
        spinBox_start5_117->setFont(font6);
        spinBox_start5_117->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_start5_117, 5, 6, 1, 2);

        label_ID3_16 = new QLabel(scrollAreaWidgetContents_8);
        label_ID3_16->setObjectName(QStringLiteral("label_ID3_16"));
        label_ID3_16->setFont(font4);
        label_ID3_16->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_29->addWidget(label_ID3_16, 8, 2, 1, 1);

        label_ID1_8 = new QLabel(scrollAreaWidgetContents_8);
        label_ID1_8->setObjectName(QStringLiteral("label_ID1_8"));
        label_ID1_8->setFont(font4);
        label_ID1_8->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_29->addWidget(label_ID1_8, 0, 2, 1, 1);

        textFIeld_name5_103 = new QLineEdit(scrollAreaWidgetContents_8);
        textFIeld_name5_103->setObjectName(QStringLiteral("textFIeld_name5_103"));
        textFIeld_name5_103->setFont(font2);

        gridLayout_29->addWidget(textFIeld_name5_103, 14, 4, 1, 1);

        spinBox_duration5_104 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_duration5_104->setObjectName(QStringLiteral("spinBox_duration5_104"));
        spinBox_duration5_104->setFont(font6);
        spinBox_duration5_104->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_duration5_104, 13, 9, 1, 2);

        horizontalSpacer_124 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_29->addItem(horizontalSpacer_124, 21, 3, 1, 1);

        label_ID4_119 = new QLabel(scrollAreaWidgetContents_8);
        label_ID4_119->setObjectName(QStringLiteral("label_ID4_119"));
        label_ID4_119->setFont(font4);
        label_ID4_119->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_29->addWidget(label_ID4_119, 18, 2, 1, 1);

        checkBox_157 = new QCheckBox(scrollAreaWidgetContents_8);
        checkBox_157->setObjectName(QStringLiteral("checkBox_157"));
        checkBox_157->setEnabled(true);
        checkBox_157->setBaseSize(QSize(0, 0));
        checkBox_157->setFont(font5);
        checkBox_157->setTabletTracking(false);
        checkBox_157->setAutoFillBackground(false);
        checkBox_157->setChecked(false);
        checkBox_157->setTristate(false);

        gridLayout_29->addWidget(checkBox_157, 18, 1, 1, 1);

        checkBox_158 = new QCheckBox(scrollAreaWidgetContents_8);
        checkBox_158->setObjectName(QStringLiteral("checkBox_158"));
        checkBox_158->setEnabled(true);
        checkBox_158->setBaseSize(QSize(0, 0));
        checkBox_158->setFont(font5);
        checkBox_158->setTabletTracking(false);
        checkBox_158->setAutoFillBackground(false);
        checkBox_158->setChecked(false);
        checkBox_158->setTristate(false);

        gridLayout_29->addWidget(checkBox_158, 20, 1, 1, 1);

        checkBox_159 = new QCheckBox(scrollAreaWidgetContents_8);
        checkBox_159->setObjectName(QStringLiteral("checkBox_159"));
        checkBox_159->setEnabled(true);
        checkBox_159->setBaseSize(QSize(0, 0));
        checkBox_159->setFont(font5);
        checkBox_159->setTabletTracking(false);
        checkBox_159->setAutoFillBackground(false);
        checkBox_159->setChecked(false);
        checkBox_159->setTristate(false);

        gridLayout_29->addWidget(checkBox_159, 17, 1, 1, 1);

        spinBox_start5_118 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_start5_118->setObjectName(QStringLiteral("spinBox_start5_118"));
        spinBox_start5_118->setFont(font6);
        spinBox_start5_118->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_start5_118, 11, 6, 1, 2);

        checkBox_160 = new QCheckBox(scrollAreaWidgetContents_8);
        checkBox_160->setObjectName(QStringLiteral("checkBox_160"));
        checkBox_160->setEnabled(true);
        checkBox_160->setBaseSize(QSize(0, 0));
        checkBox_160->setFont(font5);
        checkBox_160->setTabletTracking(false);
        checkBox_160->setAutoFillBackground(false);
        checkBox_160->setChecked(false);
        checkBox_160->setTristate(false);

        gridLayout_29->addWidget(checkBox_160, 16, 1, 1, 1);

        spinBox_start5_119 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_start5_119->setObjectName(QStringLiteral("spinBox_start5_119"));
        spinBox_start5_119->setFont(font6);
        spinBox_start5_119->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_start5_119, 16, 6, 1, 2);

        label_ID4_120 = new QLabel(scrollAreaWidgetContents_8);
        label_ID4_120->setObjectName(QStringLiteral("label_ID4_120"));
        label_ID4_120->setFont(font4);
        label_ID4_120->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_29->addWidget(label_ID4_120, 17, 2, 1, 1);

        circle_inactive3_54 = new QLabel(scrollAreaWidgetContents_8);
        circle_inactive3_54->setObjectName(QStringLiteral("circle_inactive3_54"));
        circle_inactive3_54->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_54->setAlignment(Qt::AlignCenter);

        gridLayout_29->addWidget(circle_inactive3_54, 4, 3, 1, 1);

        spinBox_start5_120 = new QSpinBox(scrollAreaWidgetContents_8);
        spinBox_start5_120->setObjectName(QStringLiteral("spinBox_start5_120"));
        spinBox_start5_120->setFont(font6);
        spinBox_start5_120->setMaximum(10000000);

        gridLayout_29->addWidget(spinBox_start5_120, 18, 6, 1, 2);

        circle_inactive1_93 = new QLabel(scrollAreaWidgetContents_8);
        circle_inactive1_93->setObjectName(QStringLiteral("circle_inactive1_93"));
        circle_inactive1_93->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_93->setAlignment(Qt::AlignCenter);

        gridLayout_29->addWidget(circle_inactive1_93, 1, 3, 1, 1);

        textFIeld_name5_104 = new QLineEdit(scrollAreaWidgetContents_8);
        textFIeld_name5_104->setObjectName(QStringLiteral("textFIeld_name5_104"));
        textFIeld_name5_104->setFont(font2);

        gridLayout_29->addWidget(textFIeld_name5_104, 17, 4, 1, 1);

        circle_inactive3_55 = new QLabel(scrollAreaWidgetContents_8);
        circle_inactive3_55->setObjectName(QStringLiteral("circle_inactive3_55"));
        circle_inactive3_55->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_55->setAlignment(Qt::AlignCenter);

        gridLayout_29->addWidget(circle_inactive3_55, 6, 3, 1, 1);

        horizontalSpacer_125 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_29->addItem(horizontalSpacer_125, 0, 8, 1, 1);

        circle_inactive3_56 = new QLabel(scrollAreaWidgetContents_8);
        circle_inactive3_56->setObjectName(QStringLiteral("circle_inactive3_56"));
        circle_inactive3_56->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_56->setAlignment(Qt::AlignCenter);

        gridLayout_29->addWidget(circle_inactive3_56, 7, 3, 1, 1);

        circle_inactive1_94 = new QLabel(scrollAreaWidgetContents_8);
        circle_inactive1_94->setObjectName(QStringLiteral("circle_inactive1_94"));
        circle_inactive1_94->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_94->setAlignment(Qt::AlignCenter);

        gridLayout_29->addWidget(circle_inactive1_94, 9, 3, 1, 1);

        circle_inactive1_95 = new QLabel(scrollAreaWidgetContents_8);
        circle_inactive1_95->setObjectName(QStringLiteral("circle_inactive1_95"));
        circle_inactive1_95->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_95->setAlignment(Qt::AlignCenter);

        gridLayout_29->addWidget(circle_inactive1_95, 10, 3, 1, 1);

        circle_inactive1_96 = new QLabel(scrollAreaWidgetContents_8);
        circle_inactive1_96->setObjectName(QStringLiteral("circle_inactive1_96"));
        circle_inactive1_96->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_96->setAlignment(Qt::AlignCenter);

        gridLayout_29->addWidget(circle_inactive1_96, 11, 3, 1, 1);

        circle_inactive1_97 = new QLabel(scrollAreaWidgetContents_8);
        circle_inactive1_97->setObjectName(QStringLiteral("circle_inactive1_97"));
        circle_inactive1_97->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_97->setAlignment(Qt::AlignCenter);

        gridLayout_29->addWidget(circle_inactive1_97, 12, 3, 1, 1);

        circle_inactive1_98 = new QLabel(scrollAreaWidgetContents_8);
        circle_inactive1_98->setObjectName(QStringLiteral("circle_inactive1_98"));
        circle_inactive1_98->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_98->setAlignment(Qt::AlignCenter);

        gridLayout_29->addWidget(circle_inactive1_98, 13, 3, 1, 1);

        circle_inactive1_99 = new QLabel(scrollAreaWidgetContents_8);
        circle_inactive1_99->setObjectName(QStringLiteral("circle_inactive1_99"));
        circle_inactive1_99->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_99->setAlignment(Qt::AlignCenter);

        gridLayout_29->addWidget(circle_inactive1_99, 14, 3, 1, 1);

        circle_inactive1_100 = new QLabel(scrollAreaWidgetContents_8);
        circle_inactive1_100->setObjectName(QStringLiteral("circle_inactive1_100"));
        circle_inactive1_100->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_100->setAlignment(Qt::AlignCenter);

        gridLayout_29->addWidget(circle_inactive1_100, 15, 3, 1, 1);

        circle_inactive1_101 = new QLabel(scrollAreaWidgetContents_8);
        circle_inactive1_101->setObjectName(QStringLiteral("circle_inactive1_101"));
        circle_inactive1_101->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_101->setAlignment(Qt::AlignCenter);

        gridLayout_29->addWidget(circle_inactive1_101, 16, 3, 1, 1);

        circle_inactive1_102 = new QLabel(scrollAreaWidgetContents_8);
        circle_inactive1_102->setObjectName(QStringLiteral("circle_inactive1_102"));
        circle_inactive1_102->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_102->setAlignment(Qt::AlignCenter);

        gridLayout_29->addWidget(circle_inactive1_102, 17, 3, 1, 1);

        circle_inactive1_103 = new QLabel(scrollAreaWidgetContents_8);
        circle_inactive1_103->setObjectName(QStringLiteral("circle_inactive1_103"));
        circle_inactive1_103->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_103->setAlignment(Qt::AlignCenter);

        gridLayout_29->addWidget(circle_inactive1_103, 18, 3, 1, 1);

        circle_inactive1_104 = new QLabel(scrollAreaWidgetContents_8);
        circle_inactive1_104->setObjectName(QStringLiteral("circle_inactive1_104"));
        circle_inactive1_104->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_104->setAlignment(Qt::AlignCenter);

        gridLayout_29->addWidget(circle_inactive1_104, 20, 3, 1, 1);


        gridLayout_28->addLayout(gridLayout_29, 1, 0, 1, 1);

        scrollArea_4->setWidget(scrollAreaWidgetContents_8);

        gridLayout_26->addWidget(scrollArea_4, 1, 0, 1, 1);

        horizontalSpacer_126 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_26->addItem(horizontalSpacer_126, 1, 1, 1, 1);


        gridLayout_30->addLayout(gridLayout_26, 0, 0, 1, 1);

        horizontalSpacer_127 = new QSpacerItem(8, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_30->addItem(horizontalSpacer_127, 0, 1, 1, 1);

        tabWidget->addTab(tab_4, QString());
        tab_5 = new QWidget();
        tab_5->setObjectName(QStringLiteral("tab_5"));
        gridLayout_35 = new QGridLayout(tab_5);
        gridLayout_35->setSpacing(6);
        gridLayout_35->setContentsMargins(11, 11, 11, 11);
        gridLayout_35->setObjectName(QStringLiteral("gridLayout_35"));
        gridLayout_31 = new QGridLayout();
        gridLayout_31->setSpacing(6);
        gridLayout_31->setObjectName(QStringLiteral("gridLayout_31"));
        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setSpacing(6);
        horizontalLayout_17->setObjectName(QStringLiteral("horizontalLayout_17"));
        horizontalSpacer_128 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_17->addItem(horizontalSpacer_128);

        horizontalSpacer_129 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_17->addItem(horizontalSpacer_129);

        label_ID_10 = new QLabel(tab_5);
        label_ID_10->setObjectName(QStringLiteral("label_ID_10"));
        sizePolicy.setHeightForWidth(label_ID_10->sizePolicy().hasHeightForWidth());
        label_ID_10->setSizePolicy(sizePolicy);
        label_ID_10->setMaximumSize(QSize(16777215, 16777215));
        label_ID_10->setFont(font);
        label_ID_10->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_17->addWidget(label_ID_10);

        horizontalSpacer_130 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_17->addItem(horizontalSpacer_130);

        label_name_11 = new QLabel(tab_5);
        label_name_11->setObjectName(QStringLiteral("label_name_11"));
        label_name_11->setFont(font);
        label_name_11->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_17->addWidget(label_name_11);

        horizontalSpacer_131 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_17->addItem(horizontalSpacer_131);

        horizontalSpacer_132 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_17->addItem(horizontalSpacer_132);

        label_start_10 = new QLabel(tab_5);
        label_start_10->setObjectName(QStringLiteral("label_start_10"));
        sizePolicy1.setHeightForWidth(label_start_10->sizePolicy().hasHeightForWidth());
        label_start_10->setSizePolicy(sizePolicy1);
        label_start_10->setFont(font);
        label_start_10->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_17->addWidget(label_start_10);

        label_msStart_11 = new QLabel(tab_5);
        label_msStart_11->setObjectName(QStringLiteral("label_msStart_11"));
        label_msStart_11->setFont(font1);
        label_msStart_11->setAlignment(Qt::AlignCenter);

        horizontalLayout_17->addWidget(label_msStart_11);

        horizontalSpacer_133 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_17->addItem(horizontalSpacer_133);

        label_duration_10 = new QLabel(tab_5);
        label_duration_10->setObjectName(QStringLiteral("label_duration_10"));
        label_duration_10->setFont(font);
        label_duration_10->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_17->addWidget(label_duration_10);

        label_msDuration_18 = new QLabel(tab_5);
        label_msDuration_18->setObjectName(QStringLiteral("label_msDuration_18"));
        label_msDuration_18->setFont(font1);
        label_msDuration_18->setAlignment(Qt::AlignCenter);

        horizontalLayout_17->addWidget(label_msDuration_18);

        horizontalSpacer_134 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_17->addItem(horizontalSpacer_134);


        gridLayout_31->addLayout(horizontalLayout_17, 0, 0, 1, 1);

        gridLayout_32 = new QGridLayout();
        gridLayout_32->setSpacing(6);
        gridLayout_32->setObjectName(QStringLiteral("gridLayout_32"));
        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setSpacing(6);
        horizontalLayout_18->setObjectName(QStringLiteral("horizontalLayout_18"));
        progressBar_9 = new QProgressBar(tab_5);
        progressBar_9->setObjectName(QStringLiteral("progressBar_9"));
        progressBar_9->setValue(0);

        horizontalLayout_18->addWidget(progressBar_9);

        label_33 = new QLabel(tab_5);
        label_33->setObjectName(QStringLiteral("label_33"));
        label_33->setFont(font3);

        horizontalLayout_18->addWidget(label_33);


        gridLayout_32->addLayout(horizontalLayout_18, 4, 0, 1, 5);

        spinBox_start5_121 = new QSpinBox(tab_5);
        spinBox_start5_121->setObjectName(QStringLiteral("spinBox_start5_121"));
        spinBox_start5_121->setFont(font6);
        spinBox_start5_121->setMaximum(10000000);
        spinBox_start5_121->setValue(0);

        gridLayout_32->addWidget(spinBox_start5_121, 2, 3, 1, 2);

        spinBox_start5_122 = new QSpinBox(tab_5);
        spinBox_start5_122->setObjectName(QStringLiteral("spinBox_start5_122"));
        spinBox_start5_122->setFont(font6);
        spinBox_start5_122->setMaximum(10000000);
        spinBox_start5_122->setValue(0);

        gridLayout_32->addWidget(spinBox_start5_122, 2, 0, 1, 2);

        pushButton_35 = new QPushButton(tab_5);
        pushButton_35->setObjectName(QStringLiteral("pushButton_35"));
        pushButton_35->setMinimumSize(QSize(0, 80));
        pushButton_35->setFont(font2);

        gridLayout_32->addWidget(pushButton_35, 10, 3, 1, 2);

        horizontalSpacer_135 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_32->addItem(horizontalSpacer_135, 8, 2, 1, 1);

        verticalSpacer_36 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_32->addItem(verticalSpacer_36, 7, 0, 1, 1);

        pushButton_36 = new QPushButton(tab_5);
        pushButton_36->setObjectName(QStringLiteral("pushButton_36"));
        pushButton_36->setMinimumSize(QSize(0, 80));
        pushButton_36->setFont(font2);

        gridLayout_32->addWidget(pushButton_36, 8, 0, 1, 2);

        label_34 = new QLabel(tab_5);
        label_34->setObjectName(QStringLiteral("label_34"));
        label_34->setFont(font7);
        label_34->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_32->addWidget(label_34, 3, 0, 1, 5);

        label_35 = new QLabel(tab_5);
        label_35->setObjectName(QStringLiteral("label_35"));
        label_35->setFont(font8);

        gridLayout_32->addWidget(label_35, 1, 3, 1, 1);

        line_9 = new QFrame(tab_5);
        line_9->setObjectName(QStringLiteral("line_9"));
        line_9->setFrameShape(QFrame::HLine);
        line_9->setFrameShadow(QFrame::Sunken);

        gridLayout_32->addWidget(line_9, 6, 0, 1, 5);

        horizontalSpacer_136 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_32->addItem(horizontalSpacer_136, 0, 1, 1, 1);

        verticalSpacer_37 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_32->addItem(verticalSpacer_37, 0, 0, 1, 1);

        pushButton_37 = new QPushButton(tab_5);
        pushButton_37->setObjectName(QStringLiteral("pushButton_37"));
        pushButton_37->setMinimumSize(QSize(0, 80));
        pushButton_37->setFont(font2);

        gridLayout_32->addWidget(pushButton_37, 10, 0, 1, 2);

        label_msDuration_19 = new QLabel(tab_5);
        label_msDuration_19->setObjectName(QStringLiteral("label_msDuration_19"));
        label_msDuration_19->setFont(font1);
        label_msDuration_19->setAlignment(Qt::AlignCenter);

        gridLayout_32->addWidget(label_msDuration_19, 1, 4, 1, 1);

        verticalSpacer_38 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_32->addItem(verticalSpacer_38, 5, 0, 1, 1);

        pushButton_38 = new QPushButton(tab_5);
        pushButton_38->setObjectName(QStringLiteral("pushButton_38"));
        pushButton_38->setMinimumSize(QSize(0, 80));
        pushButton_38->setFont(font2);

        gridLayout_32->addWidget(pushButton_38, 8, 3, 1, 2);

        verticalSpacer_39 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_32->addItem(verticalSpacer_39, 9, 0, 1, 1);

        label_36 = new QLabel(tab_5);
        label_36->setObjectName(QStringLiteral("label_36"));
        label_36->setFont(font8);

        gridLayout_32->addWidget(label_36, 1, 0, 1, 1);

        verticalSpacer_40 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_32->addItem(verticalSpacer_40, 11, 0, 1, 1);


        gridLayout_31->addLayout(gridLayout_32, 0, 2, 2, 1);

        scrollArea_5 = new QScrollArea(tab_5);
        scrollArea_5->setObjectName(QStringLiteral("scrollArea_5"));
        scrollArea_5->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        scrollArea_5->setWidgetResizable(true);
        scrollAreaWidgetContents_9 = new QWidget();
        scrollAreaWidgetContents_9->setObjectName(QStringLiteral("scrollAreaWidgetContents_9"));
        scrollAreaWidgetContents_9->setGeometry(QRect(0, -224, 654, 740));
        gridLayout_33 = new QGridLayout(scrollAreaWidgetContents_9);
        gridLayout_33->setSpacing(6);
        gridLayout_33->setContentsMargins(11, 11, 11, 11);
        gridLayout_33->setObjectName(QStringLiteral("gridLayout_33"));
        gridLayout_34 = new QGridLayout();
        gridLayout_34->setSpacing(1);
        gridLayout_34->setObjectName(QStringLiteral("gridLayout_34"));
        textFIeld_name5_105 = new QLineEdit(scrollAreaWidgetContents_9);
        textFIeld_name5_105->setObjectName(QStringLiteral("textFIeld_name5_105"));
        textFIeld_name5_105->setFont(font2);

        gridLayout_34->addWidget(textFIeld_name5_105, 12, 4, 1, 1);

        spinBox_start2_17 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_start2_17->setObjectName(QStringLiteral("spinBox_start2_17"));
        spinBox_start2_17->setFont(font6);
        spinBox_start2_17->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_start2_17, 7, 6, 1, 2);

        label_ID4_121 = new QLabel(scrollAreaWidgetContents_9);
        label_ID4_121->setObjectName(QStringLiteral("label_ID4_121"));
        label_ID4_121->setFont(font4);
        label_ID4_121->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_34->addWidget(label_ID4_121, 5, 2, 1, 1);

        circle_inactive3_57 = new QLabel(scrollAreaWidgetContents_9);
        circle_inactive3_57->setObjectName(QStringLiteral("circle_inactive3_57"));
        circle_inactive3_57->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_57->setAlignment(Qt::AlignCenter);

        gridLayout_34->addWidget(circle_inactive3_57, 5, 3, 1, 1);

        horizontalSpacer_137 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_34->addItem(horizontalSpacer_137, 6, 0, 1, 1);

        checkBox_161 = new QCheckBox(scrollAreaWidgetContents_9);
        checkBox_161->setObjectName(QStringLiteral("checkBox_161"));
        checkBox_161->setEnabled(true);
        checkBox_161->setBaseSize(QSize(0, 0));
        checkBox_161->setFont(font5);
        checkBox_161->setTabletTracking(false);
        checkBox_161->setAutoFillBackground(false);
        checkBox_161->setChecked(false);
        checkBox_161->setTristate(false);

        gridLayout_34->addWidget(checkBox_161, 3, 1, 1, 1);

        checkBox_162 = new QCheckBox(scrollAreaWidgetContents_9);
        checkBox_162->setObjectName(QStringLiteral("checkBox_162"));
        checkBox_162->setEnabled(true);
        checkBox_162->setBaseSize(QSize(0, 0));
        checkBox_162->setFont(font5);
        checkBox_162->setTabletTracking(false);
        checkBox_162->setAutoFillBackground(false);
        checkBox_162->setChecked(false);
        checkBox_162->setTristate(false);

        gridLayout_34->addWidget(checkBox_162, 5, 1, 1, 1);

        checkBox_163 = new QCheckBox(scrollAreaWidgetContents_9);
        checkBox_163->setObjectName(QStringLiteral("checkBox_163"));
        checkBox_163->setEnabled(true);
        checkBox_163->setBaseSize(QSize(0, 0));
        checkBox_163->setFont(font5);
        checkBox_163->setTabletTracking(false);
        checkBox_163->setAutoFillBackground(false);
        checkBox_163->setChecked(false);
        checkBox_163->setTristate(false);

        gridLayout_34->addWidget(checkBox_163, 8, 1, 1, 1);

        spinBox_start1_9 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_start1_9->setObjectName(QStringLiteral("spinBox_start1_9"));
        spinBox_start1_9->setFont(font6);
        spinBox_start1_9->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_start1_9, 0, 6, 1, 2);

        checkBox_164 = new QCheckBox(scrollAreaWidgetContents_9);
        checkBox_164->setObjectName(QStringLiteral("checkBox_164"));
        checkBox_164->setEnabled(true);
        checkBox_164->setBaseSize(QSize(0, 0));
        checkBox_164->setFont(font5);
        checkBox_164->setTabletTracking(false);
        checkBox_164->setAutoFillBackground(false);
        checkBox_164->setChecked(false);
        checkBox_164->setTristate(false);

        gridLayout_34->addWidget(checkBox_164, 4, 1, 1, 1);

        circle_inactive3_58 = new QLabel(scrollAreaWidgetContents_9);
        circle_inactive3_58->setObjectName(QStringLiteral("circle_inactive3_58"));
        circle_inactive3_58->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_58->setAlignment(Qt::AlignCenter);

        gridLayout_34->addWidget(circle_inactive3_58, 3, 3, 1, 1);

        textFIeld_name1_9 = new QLineEdit(scrollAreaWidgetContents_9);
        textFIeld_name1_9->setObjectName(QStringLiteral("textFIeld_name1_9"));
        textFIeld_name1_9->setFont(font2);

        gridLayout_34->addWidget(textFIeld_name1_9, 0, 4, 1, 1);

        spinBox_duration2_17 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_duration2_17->setObjectName(QStringLiteral("spinBox_duration2_17"));
        spinBox_duration2_17->setFont(font6);
        spinBox_duration2_17->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_duration2_17, 7, 9, 1, 2);

        spinBox_duration4_17 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_duration4_17->setObjectName(QStringLiteral("spinBox_duration4_17"));
        spinBox_duration4_17->setFont(font6);
        spinBox_duration4_17->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_duration4_17, 3, 9, 1, 2);

        textFIeld_name4_17 = new QLineEdit(scrollAreaWidgetContents_9);
        textFIeld_name4_17->setObjectName(QStringLiteral("textFIeld_name4_17"));
        textFIeld_name4_17->setFont(font2);

        gridLayout_34->addWidget(textFIeld_name4_17, 3, 4, 1, 1);

        spinBox_duration5_105 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_duration5_105->setObjectName(QStringLiteral("spinBox_duration5_105"));
        spinBox_duration5_105->setFont(font6);
        spinBox_duration5_105->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_duration5_105, 6, 9, 1, 2);

        checkBox_165 = new QCheckBox(scrollAreaWidgetContents_9);
        checkBox_165->setObjectName(QStringLiteral("checkBox_165"));
        checkBox_165->setEnabled(true);
        checkBox_165->setBaseSize(QSize(0, 0));
        checkBox_165->setFont(font5);
        checkBox_165->setTabletTracking(false);
        checkBox_165->setAutoFillBackground(false);
        checkBox_165->setChecked(false);
        checkBox_165->setTristate(false);

        gridLayout_34->addWidget(checkBox_165, 14, 1, 1, 1);

        textFIeld_name2_17 = new QLineEdit(scrollAreaWidgetContents_9);
        textFIeld_name2_17->setObjectName(QStringLiteral("textFIeld_name2_17"));
        textFIeld_name2_17->setFont(font2);

        gridLayout_34->addWidget(textFIeld_name2_17, 7, 4, 1, 1);

        spinBox_duration5_106 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_duration5_106->setObjectName(QStringLiteral("spinBox_duration5_106"));
        spinBox_duration5_106->setFont(font6);
        spinBox_duration5_106->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_duration5_106, 10, 9, 1, 2);

        textFIeld_name5_106 = new QLineEdit(scrollAreaWidgetContents_9);
        textFIeld_name5_106->setObjectName(QStringLiteral("textFIeld_name5_106"));
        textFIeld_name5_106->setFont(font2);

        gridLayout_34->addWidget(textFIeld_name5_106, 4, 4, 1, 1);

        spinBox_start5_123 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_start5_123->setObjectName(QStringLiteral("spinBox_start5_123"));
        spinBox_start5_123->setFont(font6);
        spinBox_start5_123->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_start5_123, 4, 6, 1, 2);

        label_ID4_122 = new QLabel(scrollAreaWidgetContents_9);
        label_ID4_122->setObjectName(QStringLiteral("label_ID4_122"));
        label_ID4_122->setFont(font4);
        label_ID4_122->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_34->addWidget(label_ID4_122, 3, 2, 1, 1);

        spinBox_start3_17 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_start3_17->setObjectName(QStringLiteral("spinBox_start3_17"));
        spinBox_start3_17->setFont(font6);
        spinBox_start3_17->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_start3_17, 8, 6, 1, 2);

        label_ID4_123 = new QLabel(scrollAreaWidgetContents_9);
        label_ID4_123->setObjectName(QStringLiteral("label_ID4_123"));
        label_ID4_123->setFont(font4);
        label_ID4_123->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_34->addWidget(label_ID4_123, 9, 2, 1, 1);

        spinBox_duration4_18 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_duration4_18->setObjectName(QStringLiteral("spinBox_duration4_18"));
        spinBox_duration4_18->setFont(font6);
        spinBox_duration4_18->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_duration4_18, 9, 9, 1, 2);

        checkBox_166 = new QCheckBox(scrollAreaWidgetContents_9);
        checkBox_166->setObjectName(QStringLiteral("checkBox_166"));
        checkBox_166->setEnabled(true);
        checkBox_166->setBaseSize(QSize(0, 0));
        checkBox_166->setFont(font5);
        checkBox_166->setTabletTracking(false);
        checkBox_166->setAutoFillBackground(false);
        checkBox_166->setChecked(false);
        checkBox_166->setTristate(false);

        gridLayout_34->addWidget(checkBox_166, 13, 1, 1, 1);

        spinBox_start5_124 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_start5_124->setObjectName(QStringLiteral("spinBox_start5_124"));
        spinBox_start5_124->setFont(font6);
        spinBox_start5_124->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_start5_124, 15, 6, 1, 2);

        spinBox_duration5_107 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_duration5_107->setObjectName(QStringLiteral("spinBox_duration5_107"));
        spinBox_duration5_107->setFont(font6);
        spinBox_duration5_107->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_duration5_107, 15, 9, 1, 2);

        textFIeld_name5_107 = new QLineEdit(scrollAreaWidgetContents_9);
        textFIeld_name5_107->setObjectName(QStringLiteral("textFIeld_name5_107"));
        textFIeld_name5_107->setFont(font2);

        gridLayout_34->addWidget(textFIeld_name5_107, 10, 4, 1, 1);

        label_ID4_124 = new QLabel(scrollAreaWidgetContents_9);
        label_ID4_124->setObjectName(QStringLiteral("label_ID4_124"));
        label_ID4_124->setFont(font4);
        label_ID4_124->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_34->addWidget(label_ID4_124, 14, 2, 1, 1);

        spinBox_start5_125 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_start5_125->setObjectName(QStringLiteral("spinBox_start5_125"));
        spinBox_start5_125->setFont(font6);
        spinBox_start5_125->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_start5_125, 14, 6, 1, 2);

        textFIeld_name4_18 = new QLineEdit(scrollAreaWidgetContents_9);
        textFIeld_name4_18->setObjectName(QStringLiteral("textFIeld_name4_18"));
        textFIeld_name4_18->setFont(font2);

        gridLayout_34->addWidget(textFIeld_name4_18, 9, 4, 1, 1);

        spinBox_start3_18 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_start3_18->setObjectName(QStringLiteral("spinBox_start3_18"));
        spinBox_start3_18->setFont(font6);
        spinBox_start3_18->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_start3_18, 2, 6, 1, 2);

        spinBox_start5_126 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_start5_126->setObjectName(QStringLiteral("spinBox_start5_126"));
        spinBox_start5_126->setFont(font6);
        spinBox_start5_126->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_start5_126, 6, 6, 1, 2);

        horizontalSpacer_138 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_34->addItem(horizontalSpacer_138, 0, 12, 1, 1);

        horizontalSpacer_139 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_34->addItem(horizontalSpacer_139, 10, 5, 1, 1);

        spinBox_duration5_108 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_duration5_108->setObjectName(QStringLiteral("spinBox_duration5_108"));
        spinBox_duration5_108->setFont(font6);
        spinBox_duration5_108->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_duration5_108, 11, 9, 1, 2);

        textFIeld_name5_108 = new QLineEdit(scrollAreaWidgetContents_9);
        textFIeld_name5_108->setObjectName(QStringLiteral("textFIeld_name5_108"));
        textFIeld_name5_108->setFont(font2);

        gridLayout_34->addWidget(textFIeld_name5_108, 5, 4, 1, 1);

        label_ID2_17 = new QLabel(scrollAreaWidgetContents_9);
        label_ID2_17->setObjectName(QStringLiteral("label_ID2_17"));
        label_ID2_17->setFont(font4);
        label_ID2_17->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_34->addWidget(label_ID2_17, 1, 2, 1, 1);

        checkBox_167 = new QCheckBox(scrollAreaWidgetContents_9);
        checkBox_167->setObjectName(QStringLiteral("checkBox_167"));
        checkBox_167->setEnabled(true);
        checkBox_167->setBaseSize(QSize(0, 0));
        checkBox_167->setFont(font5);
        checkBox_167->setTabletTracking(false);
        checkBox_167->setAutoFillBackground(false);
        checkBox_167->setChecked(false);
        checkBox_167->setTristate(false);

        gridLayout_34->addWidget(checkBox_167, 6, 1, 1, 1);

        spinBox_start5_127 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_start5_127->setObjectName(QStringLiteral("spinBox_start5_127"));
        spinBox_start5_127->setFont(font6);
        spinBox_start5_127->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_start5_127, 17, 6, 1, 2);

        textFIeld_name2_18 = new QLineEdit(scrollAreaWidgetContents_9);
        textFIeld_name2_18->setObjectName(QStringLiteral("textFIeld_name2_18"));
        textFIeld_name2_18->setFont(font2);

        gridLayout_34->addWidget(textFIeld_name2_18, 1, 4, 1, 1);

        textFIeld_name5_109 = new QLineEdit(scrollAreaWidgetContents_9);
        textFIeld_name5_109->setObjectName(QStringLiteral("textFIeld_name5_109"));
        textFIeld_name5_109->setFont(font2);

        gridLayout_34->addWidget(textFIeld_name5_109, 6, 4, 1, 1);

        checkBox_168 = new QCheckBox(scrollAreaWidgetContents_9);
        checkBox_168->setObjectName(QStringLiteral("checkBox_168"));
        checkBox_168->setEnabled(true);
        checkBox_168->setBaseSize(QSize(0, 0));
        checkBox_168->setFont(font5);
        checkBox_168->setTabletTracking(false);
        checkBox_168->setAutoFillBackground(false);
        checkBox_168->setChecked(false);
        checkBox_168->setTristate(false);

        gridLayout_34->addWidget(checkBox_168, 7, 1, 1, 1);

        label_ID4_125 = new QLabel(scrollAreaWidgetContents_9);
        label_ID4_125->setObjectName(QStringLiteral("label_ID4_125"));
        label_ID4_125->setFont(font4);
        label_ID4_125->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_34->addWidget(label_ID4_125, 6, 2, 1, 1);

        circle_inactive3_59 = new QLabel(scrollAreaWidgetContents_9);
        circle_inactive3_59->setObjectName(QStringLiteral("circle_inactive3_59"));
        QPalette palette4;
        palette4.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette4.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette4.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        circle_inactive3_59->setPalette(palette4);
        circle_inactive3_59->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_59->setAlignment(Qt::AlignCenter);

        gridLayout_34->addWidget(circle_inactive3_59, 8, 3, 1, 1);

        textFIeld_name3_17 = new QLineEdit(scrollAreaWidgetContents_9);
        textFIeld_name3_17->setObjectName(QStringLiteral("textFIeld_name3_17"));
        textFIeld_name3_17->setFont(font2);

        gridLayout_34->addWidget(textFIeld_name3_17, 2, 4, 1, 1);

        spinBox_duration5_109 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_duration5_109->setObjectName(QStringLiteral("spinBox_duration5_109"));
        spinBox_duration5_109->setFont(font6);
        spinBox_duration5_109->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_duration5_109, 5, 9, 1, 2);

        textFIeld_name3_18 = new QLineEdit(scrollAreaWidgetContents_9);
        textFIeld_name3_18->setObjectName(QStringLiteral("textFIeld_name3_18"));
        textFIeld_name3_18->setFont(font2);

        gridLayout_34->addWidget(textFIeld_name3_18, 8, 4, 1, 1);

        spinBox_duration5_110 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_duration5_110->setObjectName(QStringLiteral("spinBox_duration5_110"));
        spinBox_duration5_110->setFont(font6);
        spinBox_duration5_110->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_duration5_110, 14, 9, 1, 2);

        checkBox_169 = new QCheckBox(scrollAreaWidgetContents_9);
        checkBox_169->setObjectName(QStringLiteral("checkBox_169"));
        checkBox_169->setEnabled(true);
        checkBox_169->setBaseSize(QSize(0, 0));
        checkBox_169->setFont(font5);
        checkBox_169->setTabletTracking(false);
        checkBox_169->setAutoFillBackground(false);
        checkBox_169->setChecked(false);
        checkBox_169->setTristate(false);

        gridLayout_34->addWidget(checkBox_169, 12, 1, 1, 1);

        textFIeld_name5_110 = new QLineEdit(scrollAreaWidgetContents_9);
        textFIeld_name5_110->setObjectName(QStringLiteral("textFIeld_name5_110"));
        textFIeld_name5_110->setFont(font2);

        gridLayout_34->addWidget(textFIeld_name5_110, 18, 4, 1, 1);

        textFIeld_name5_111 = new QLineEdit(scrollAreaWidgetContents_9);
        textFIeld_name5_111->setObjectName(QStringLiteral("textFIeld_name5_111"));
        textFIeld_name5_111->setFont(font2);

        gridLayout_34->addWidget(textFIeld_name5_111, 20, 4, 1, 1);

        spinBox_duration5_111 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_duration5_111->setObjectName(QStringLiteral("spinBox_duration5_111"));
        spinBox_duration5_111->setFont(font6);
        spinBox_duration5_111->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_duration5_111, 4, 9, 1, 2);

        label_ID4_126 = new QLabel(scrollAreaWidgetContents_9);
        label_ID4_126->setObjectName(QStringLiteral("label_ID4_126"));
        label_ID4_126->setFont(font4);
        label_ID4_126->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_34->addWidget(label_ID4_126, 20, 2, 1, 1);

        horizontalSpacer_140 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_34->addItem(horizontalSpacer_140, 10, 8, 1, 1);

        label_ID4_127 = new QLabel(scrollAreaWidgetContents_9);
        label_ID4_127->setObjectName(QStringLiteral("label_ID4_127"));
        label_ID4_127->setFont(font4);
        label_ID4_127->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_34->addWidget(label_ID4_127, 4, 2, 1, 1);

        textFIeld_name5_112 = new QLineEdit(scrollAreaWidgetContents_9);
        textFIeld_name5_112->setObjectName(QStringLiteral("textFIeld_name5_112"));
        textFIeld_name5_112->setFont(font2);

        gridLayout_34->addWidget(textFIeld_name5_112, 13, 4, 1, 1);

        spinBox_start2_18 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_start2_18->setObjectName(QStringLiteral("spinBox_start2_18"));
        spinBox_start2_18->setFont(font6);
        spinBox_start2_18->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_start2_18, 1, 6, 1, 2);

        spinBox_duration5_112 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_duration5_112->setObjectName(QStringLiteral("spinBox_duration5_112"));
        spinBox_duration5_112->setFont(font6);
        spinBox_duration5_112->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_duration5_112, 17, 9, 1, 2);

        label_ID4_128 = new QLabel(scrollAreaWidgetContents_9);
        label_ID4_128->setObjectName(QStringLiteral("label_ID4_128"));
        label_ID4_128->setFont(font4);
        label_ID4_128->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_34->addWidget(label_ID4_128, 10, 2, 1, 1);

        spinBox_duration5_113 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_duration5_113->setObjectName(QStringLiteral("spinBox_duration5_113"));
        spinBox_duration5_113->setFont(font6);
        spinBox_duration5_113->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_duration5_113, 12, 9, 1, 2);

        spinBox_duration5_114 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_duration5_114->setObjectName(QStringLiteral("spinBox_duration5_114"));
        spinBox_duration5_114->setFont(font6);
        spinBox_duration5_114->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_duration5_114, 20, 9, 1, 2);

        spinBox_start5_128 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_start5_128->setObjectName(QStringLiteral("spinBox_start5_128"));
        spinBox_start5_128->setFont(font6);
        spinBox_start5_128->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_start5_128, 20, 6, 1, 2);

        textFIeld_name5_113 = new QLineEdit(scrollAreaWidgetContents_9);
        textFIeld_name5_113->setObjectName(QStringLiteral("textFIeld_name5_113"));
        textFIeld_name5_113->setFont(font2);

        gridLayout_34->addWidget(textFIeld_name5_113, 11, 4, 1, 1);

        spinBox_start5_129 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_start5_129->setObjectName(QStringLiteral("spinBox_start5_129"));
        spinBox_start5_129->setFont(font6);
        spinBox_start5_129->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_start5_129, 13, 6, 1, 2);

        checkBox_170 = new QCheckBox(scrollAreaWidgetContents_9);
        checkBox_170->setObjectName(QStringLiteral("checkBox_170"));
        checkBox_170->setEnabled(true);
        checkBox_170->setBaseSize(QSize(0, 0));
        checkBox_170->setFont(font5);
        checkBox_170->setTabletTracking(false);
        checkBox_170->setAutoFillBackground(false);
        checkBox_170->setChecked(false);
        checkBox_170->setTristate(false);

        gridLayout_34->addWidget(checkBox_170, 15, 1, 1, 1);

        label_ID4_129 = new QLabel(scrollAreaWidgetContents_9);
        label_ID4_129->setObjectName(QStringLiteral("label_ID4_129"));
        label_ID4_129->setFont(font4);
        label_ID4_129->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_34->addWidget(label_ID4_129, 12, 2, 1, 1);

        label_ID4_130 = new QLabel(scrollAreaWidgetContents_9);
        label_ID4_130->setObjectName(QStringLiteral("label_ID4_130"));
        label_ID4_130->setFont(font4);
        label_ID4_130->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_34->addWidget(label_ID4_130, 16, 2, 1, 1);

        spinBox_duration3_17 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_duration3_17->setObjectName(QStringLiteral("spinBox_duration3_17"));
        spinBox_duration3_17->setFont(font6);
        spinBox_duration3_17->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_duration3_17, 8, 9, 1, 2);

        label_ID2_18 = new QLabel(scrollAreaWidgetContents_9);
        label_ID2_18->setObjectName(QStringLiteral("label_ID2_18"));
        label_ID2_18->setFont(font4);
        label_ID2_18->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_34->addWidget(label_ID2_18, 7, 2, 1, 1);

        label_ID4_131 = new QLabel(scrollAreaWidgetContents_9);
        label_ID4_131->setObjectName(QStringLiteral("label_ID4_131"));
        label_ID4_131->setFont(font4);
        label_ID4_131->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_34->addWidget(label_ID4_131, 15, 2, 1, 1);

        checkBox_171 = new QCheckBox(scrollAreaWidgetContents_9);
        checkBox_171->setObjectName(QStringLiteral("checkBox_171"));
        checkBox_171->setEnabled(true);
        checkBox_171->setBaseSize(QSize(0, 0));
        checkBox_171->setFont(font5);
        checkBox_171->setTabletTracking(false);
        checkBox_171->setAutoFillBackground(false);
        checkBox_171->setChecked(false);
        checkBox_171->setTristate(false);

        gridLayout_34->addWidget(checkBox_171, 0, 1, 1, 1);

        circle_inactive3_60 = new QLabel(scrollAreaWidgetContents_9);
        circle_inactive3_60->setObjectName(QStringLiteral("circle_inactive3_60"));
        circle_inactive3_60->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_60->setAlignment(Qt::AlignCenter);

        gridLayout_34->addWidget(circle_inactive3_60, 2, 3, 1, 1);

        label_ID4_132 = new QLabel(scrollAreaWidgetContents_9);
        label_ID4_132->setObjectName(QStringLiteral("label_ID4_132"));
        label_ID4_132->setFont(font4);
        label_ID4_132->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_34->addWidget(label_ID4_132, 13, 2, 1, 1);

        label_ID4_133 = new QLabel(scrollAreaWidgetContents_9);
        label_ID4_133->setObjectName(QStringLiteral("label_ID4_133"));
        label_ID4_133->setFont(font4);
        label_ID4_133->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_34->addWidget(label_ID4_133, 11, 2, 1, 1);

        spinBox_start4_17 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_start4_17->setObjectName(QStringLiteral("spinBox_start4_17"));
        spinBox_start4_17->setFont(font6);
        spinBox_start4_17->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_start4_17, 3, 6, 1, 2);

        textFIeld_name5_114 = new QLineEdit(scrollAreaWidgetContents_9);
        textFIeld_name5_114->setObjectName(QStringLiteral("textFIeld_name5_114"));
        textFIeld_name5_114->setFont(font2);

        gridLayout_34->addWidget(textFIeld_name5_114, 16, 4, 1, 1);

        spinBox_start5_130 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_start5_130->setObjectName(QStringLiteral("spinBox_start5_130"));
        spinBox_start5_130->setFont(font6);
        spinBox_start5_130->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_start5_130, 12, 6, 1, 2);

        spinBox_duration5_115 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_duration5_115->setObjectName(QStringLiteral("spinBox_duration5_115"));
        spinBox_duration5_115->setFont(font6);
        spinBox_duration5_115->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_duration5_115, 18, 9, 1, 2);

        label_ID3_17 = new QLabel(scrollAreaWidgetContents_9);
        label_ID3_17->setObjectName(QStringLiteral("label_ID3_17"));
        label_ID3_17->setFont(font4);
        label_ID3_17->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_34->addWidget(label_ID3_17, 2, 2, 1, 1);

        spinBox_duration1_9 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_duration1_9->setObjectName(QStringLiteral("spinBox_duration1_9"));
        spinBox_duration1_9->setFont(font6);
        spinBox_duration1_9->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_duration1_9, 0, 9, 1, 2);

        spinBox_duration5_116 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_duration5_116->setObjectName(QStringLiteral("spinBox_duration5_116"));
        spinBox_duration5_116->setFont(font6);
        spinBox_duration5_116->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_duration5_116, 16, 9, 1, 2);

        spinBox_start5_131 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_start5_131->setObjectName(QStringLiteral("spinBox_start5_131"));
        spinBox_start5_131->setFont(font6);
        spinBox_start5_131->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_start5_131, 10, 6, 1, 2);

        spinBox_start4_18 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_start4_18->setObjectName(QStringLiteral("spinBox_start4_18"));
        spinBox_start4_18->setFont(font6);
        spinBox_start4_18->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_start4_18, 9, 6, 1, 2);

        spinBox_duration2_18 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_duration2_18->setObjectName(QStringLiteral("spinBox_duration2_18"));
        spinBox_duration2_18->setFont(font6);
        spinBox_duration2_18->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_duration2_18, 1, 9, 1, 2);

        circle_inactive1_105 = new QLabel(scrollAreaWidgetContents_9);
        circle_inactive1_105->setObjectName(QStringLiteral("circle_inactive1_105"));
        circle_inactive1_105->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_105->setAlignment(Qt::AlignCenter);

        gridLayout_34->addWidget(circle_inactive1_105, 0, 3, 1, 1);

        checkBox_172 = new QCheckBox(scrollAreaWidgetContents_9);
        checkBox_172->setObjectName(QStringLiteral("checkBox_172"));
        checkBox_172->setEnabled(true);
        checkBox_172->setBaseSize(QSize(0, 0));
        checkBox_172->setFont(font5);
        checkBox_172->setTabletTracking(false);
        checkBox_172->setAutoFillBackground(false);
        checkBox_172->setChecked(false);
        checkBox_172->setTristate(false);

        gridLayout_34->addWidget(checkBox_172, 1, 1, 1, 1);

        spinBox_duration3_18 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_duration3_18->setObjectName(QStringLiteral("spinBox_duration3_18"));
        spinBox_duration3_18->setFont(font6);
        spinBox_duration3_18->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_duration3_18, 2, 9, 1, 2);

        checkBox_173 = new QCheckBox(scrollAreaWidgetContents_9);
        checkBox_173->setObjectName(QStringLiteral("checkBox_173"));
        checkBox_173->setEnabled(true);
        checkBox_173->setBaseSize(QSize(0, 0));
        checkBox_173->setFont(font5);
        checkBox_173->setTabletTracking(false);
        checkBox_173->setAutoFillBackground(false);
        checkBox_173->setChecked(false);
        checkBox_173->setTristate(false);

        gridLayout_34->addWidget(checkBox_173, 11, 1, 1, 1);

        checkBox_174 = new QCheckBox(scrollAreaWidgetContents_9);
        checkBox_174->setObjectName(QStringLiteral("checkBox_174"));
        checkBox_174->setEnabled(true);
        checkBox_174->setBaseSize(QSize(0, 0));
        checkBox_174->setFont(font5);
        checkBox_174->setTabletTracking(false);
        checkBox_174->setAutoFillBackground(false);
        checkBox_174->setChecked(false);
        checkBox_174->setTristate(false);

        gridLayout_34->addWidget(checkBox_174, 9, 1, 1, 1);

        checkBox_175 = new QCheckBox(scrollAreaWidgetContents_9);
        checkBox_175->setObjectName(QStringLiteral("checkBox_175"));
        checkBox_175->setEnabled(true);
        checkBox_175->setBaseSize(QSize(0, 0));
        checkBox_175->setFont(font5);
        checkBox_175->setTabletTracking(false);
        checkBox_175->setAutoFillBackground(false);
        checkBox_175->setChecked(false);
        checkBox_175->setTristate(false);

        gridLayout_34->addWidget(checkBox_175, 2, 1, 1, 1);

        textFIeld_name5_115 = new QLineEdit(scrollAreaWidgetContents_9);
        textFIeld_name5_115->setObjectName(QStringLiteral("textFIeld_name5_115"));
        textFIeld_name5_115->setFont(font2);

        gridLayout_34->addWidget(textFIeld_name5_115, 15, 4, 1, 1);

        checkBox_176 = new QCheckBox(scrollAreaWidgetContents_9);
        checkBox_176->setObjectName(QStringLiteral("checkBox_176"));
        checkBox_176->setEnabled(true);
        checkBox_176->setBaseSize(QSize(0, 0));
        checkBox_176->setFont(font5);
        checkBox_176->setTabletTracking(false);
        checkBox_176->setAutoFillBackground(false);
        checkBox_176->setChecked(false);
        checkBox_176->setTristate(false);

        gridLayout_34->addWidget(checkBox_176, 10, 1, 1, 1);

        spinBox_start5_132 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_start5_132->setObjectName(QStringLiteral("spinBox_start5_132"));
        spinBox_start5_132->setFont(font6);
        spinBox_start5_132->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_start5_132, 5, 6, 1, 2);

        label_ID3_18 = new QLabel(scrollAreaWidgetContents_9);
        label_ID3_18->setObjectName(QStringLiteral("label_ID3_18"));
        label_ID3_18->setFont(font4);
        label_ID3_18->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_34->addWidget(label_ID3_18, 8, 2, 1, 1);

        label_ID1_9 = new QLabel(scrollAreaWidgetContents_9);
        label_ID1_9->setObjectName(QStringLiteral("label_ID1_9"));
        label_ID1_9->setFont(font4);
        label_ID1_9->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_34->addWidget(label_ID1_9, 0, 2, 1, 1);

        textFIeld_name5_116 = new QLineEdit(scrollAreaWidgetContents_9);
        textFIeld_name5_116->setObjectName(QStringLiteral("textFIeld_name5_116"));
        textFIeld_name5_116->setFont(font2);

        gridLayout_34->addWidget(textFIeld_name5_116, 14, 4, 1, 1);

        spinBox_duration5_117 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_duration5_117->setObjectName(QStringLiteral("spinBox_duration5_117"));
        spinBox_duration5_117->setFont(font6);
        spinBox_duration5_117->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_duration5_117, 13, 9, 1, 2);

        horizontalSpacer_141 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_34->addItem(horizontalSpacer_141, 21, 3, 1, 1);

        label_ID4_134 = new QLabel(scrollAreaWidgetContents_9);
        label_ID4_134->setObjectName(QStringLiteral("label_ID4_134"));
        label_ID4_134->setFont(font4);
        label_ID4_134->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_34->addWidget(label_ID4_134, 18, 2, 1, 1);

        checkBox_177 = new QCheckBox(scrollAreaWidgetContents_9);
        checkBox_177->setObjectName(QStringLiteral("checkBox_177"));
        checkBox_177->setEnabled(true);
        checkBox_177->setBaseSize(QSize(0, 0));
        checkBox_177->setFont(font5);
        checkBox_177->setTabletTracking(false);
        checkBox_177->setAutoFillBackground(false);
        checkBox_177->setChecked(false);
        checkBox_177->setTristate(false);

        gridLayout_34->addWidget(checkBox_177, 18, 1, 1, 1);

        checkBox_178 = new QCheckBox(scrollAreaWidgetContents_9);
        checkBox_178->setObjectName(QStringLiteral("checkBox_178"));
        checkBox_178->setEnabled(true);
        checkBox_178->setBaseSize(QSize(0, 0));
        checkBox_178->setFont(font5);
        checkBox_178->setTabletTracking(false);
        checkBox_178->setAutoFillBackground(false);
        checkBox_178->setChecked(false);
        checkBox_178->setTristate(false);

        gridLayout_34->addWidget(checkBox_178, 20, 1, 1, 1);

        checkBox_179 = new QCheckBox(scrollAreaWidgetContents_9);
        checkBox_179->setObjectName(QStringLiteral("checkBox_179"));
        checkBox_179->setEnabled(true);
        checkBox_179->setBaseSize(QSize(0, 0));
        checkBox_179->setFont(font5);
        checkBox_179->setTabletTracking(false);
        checkBox_179->setAutoFillBackground(false);
        checkBox_179->setChecked(false);
        checkBox_179->setTristate(false);

        gridLayout_34->addWidget(checkBox_179, 17, 1, 1, 1);

        spinBox_start5_133 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_start5_133->setObjectName(QStringLiteral("spinBox_start5_133"));
        spinBox_start5_133->setFont(font6);
        spinBox_start5_133->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_start5_133, 11, 6, 1, 2);

        checkBox_180 = new QCheckBox(scrollAreaWidgetContents_9);
        checkBox_180->setObjectName(QStringLiteral("checkBox_180"));
        checkBox_180->setEnabled(true);
        checkBox_180->setBaseSize(QSize(0, 0));
        checkBox_180->setFont(font5);
        checkBox_180->setTabletTracking(false);
        checkBox_180->setAutoFillBackground(false);
        checkBox_180->setChecked(false);
        checkBox_180->setTristate(false);

        gridLayout_34->addWidget(checkBox_180, 16, 1, 1, 1);

        spinBox_start5_134 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_start5_134->setObjectName(QStringLiteral("spinBox_start5_134"));
        spinBox_start5_134->setFont(font6);
        spinBox_start5_134->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_start5_134, 16, 6, 1, 2);

        label_ID4_135 = new QLabel(scrollAreaWidgetContents_9);
        label_ID4_135->setObjectName(QStringLiteral("label_ID4_135"));
        label_ID4_135->setFont(font4);
        label_ID4_135->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_34->addWidget(label_ID4_135, 17, 2, 1, 1);

        circle_inactive3_61 = new QLabel(scrollAreaWidgetContents_9);
        circle_inactive3_61->setObjectName(QStringLiteral("circle_inactive3_61"));
        circle_inactive3_61->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_61->setAlignment(Qt::AlignCenter);

        gridLayout_34->addWidget(circle_inactive3_61, 4, 3, 1, 1);

        spinBox_start5_135 = new QSpinBox(scrollAreaWidgetContents_9);
        spinBox_start5_135->setObjectName(QStringLiteral("spinBox_start5_135"));
        spinBox_start5_135->setFont(font6);
        spinBox_start5_135->setMaximum(10000000);

        gridLayout_34->addWidget(spinBox_start5_135, 18, 6, 1, 2);

        circle_inactive1_106 = new QLabel(scrollAreaWidgetContents_9);
        circle_inactive1_106->setObjectName(QStringLiteral("circle_inactive1_106"));
        circle_inactive1_106->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_106->setAlignment(Qt::AlignCenter);

        gridLayout_34->addWidget(circle_inactive1_106, 1, 3, 1, 1);

        textFIeld_name5_117 = new QLineEdit(scrollAreaWidgetContents_9);
        textFIeld_name5_117->setObjectName(QStringLiteral("textFIeld_name5_117"));
        textFIeld_name5_117->setFont(font2);

        gridLayout_34->addWidget(textFIeld_name5_117, 17, 4, 1, 1);

        circle_inactive3_62 = new QLabel(scrollAreaWidgetContents_9);
        circle_inactive3_62->setObjectName(QStringLiteral("circle_inactive3_62"));
        circle_inactive3_62->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_62->setAlignment(Qt::AlignCenter);

        gridLayout_34->addWidget(circle_inactive3_62, 6, 3, 1, 1);

        horizontalSpacer_142 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_34->addItem(horizontalSpacer_142, 0, 8, 1, 1);

        circle_inactive3_63 = new QLabel(scrollAreaWidgetContents_9);
        circle_inactive3_63->setObjectName(QStringLiteral("circle_inactive3_63"));
        circle_inactive3_63->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_63->setAlignment(Qt::AlignCenter);

        gridLayout_34->addWidget(circle_inactive3_63, 7, 3, 1, 1);

        circle_inactive1_107 = new QLabel(scrollAreaWidgetContents_9);
        circle_inactive1_107->setObjectName(QStringLiteral("circle_inactive1_107"));
        circle_inactive1_107->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_107->setAlignment(Qt::AlignCenter);

        gridLayout_34->addWidget(circle_inactive1_107, 9, 3, 1, 1);

        circle_inactive1_108 = new QLabel(scrollAreaWidgetContents_9);
        circle_inactive1_108->setObjectName(QStringLiteral("circle_inactive1_108"));
        circle_inactive1_108->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_108->setAlignment(Qt::AlignCenter);

        gridLayout_34->addWidget(circle_inactive1_108, 10, 3, 1, 1);

        circle_inactive1_109 = new QLabel(scrollAreaWidgetContents_9);
        circle_inactive1_109->setObjectName(QStringLiteral("circle_inactive1_109"));
        circle_inactive1_109->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_109->setAlignment(Qt::AlignCenter);

        gridLayout_34->addWidget(circle_inactive1_109, 11, 3, 1, 1);

        circle_inactive1_110 = new QLabel(scrollAreaWidgetContents_9);
        circle_inactive1_110->setObjectName(QStringLiteral("circle_inactive1_110"));
        circle_inactive1_110->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_110->setAlignment(Qt::AlignCenter);

        gridLayout_34->addWidget(circle_inactive1_110, 12, 3, 1, 1);

        circle_inactive1_111 = new QLabel(scrollAreaWidgetContents_9);
        circle_inactive1_111->setObjectName(QStringLiteral("circle_inactive1_111"));
        circle_inactive1_111->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_111->setAlignment(Qt::AlignCenter);

        gridLayout_34->addWidget(circle_inactive1_111, 13, 3, 1, 1);

        circle_inactive1_112 = new QLabel(scrollAreaWidgetContents_9);
        circle_inactive1_112->setObjectName(QStringLiteral("circle_inactive1_112"));
        circle_inactive1_112->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_112->setAlignment(Qt::AlignCenter);

        gridLayout_34->addWidget(circle_inactive1_112, 14, 3, 1, 1);

        circle_inactive1_113 = new QLabel(scrollAreaWidgetContents_9);
        circle_inactive1_113->setObjectName(QStringLiteral("circle_inactive1_113"));
        circle_inactive1_113->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_113->setAlignment(Qt::AlignCenter);

        gridLayout_34->addWidget(circle_inactive1_113, 15, 3, 1, 1);

        circle_inactive1_114 = new QLabel(scrollAreaWidgetContents_9);
        circle_inactive1_114->setObjectName(QStringLiteral("circle_inactive1_114"));
        circle_inactive1_114->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_114->setAlignment(Qt::AlignCenter);

        gridLayout_34->addWidget(circle_inactive1_114, 16, 3, 1, 1);

        circle_inactive1_115 = new QLabel(scrollAreaWidgetContents_9);
        circle_inactive1_115->setObjectName(QStringLiteral("circle_inactive1_115"));
        circle_inactive1_115->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_115->setAlignment(Qt::AlignCenter);

        gridLayout_34->addWidget(circle_inactive1_115, 17, 3, 1, 1);

        circle_inactive1_116 = new QLabel(scrollAreaWidgetContents_9);
        circle_inactive1_116->setObjectName(QStringLiteral("circle_inactive1_116"));
        circle_inactive1_116->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_116->setAlignment(Qt::AlignCenter);

        gridLayout_34->addWidget(circle_inactive1_116, 18, 3, 1, 1);

        circle_inactive1_117 = new QLabel(scrollAreaWidgetContents_9);
        circle_inactive1_117->setObjectName(QStringLiteral("circle_inactive1_117"));
        circle_inactive1_117->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_117->setAlignment(Qt::AlignCenter);

        gridLayout_34->addWidget(circle_inactive1_117, 20, 3, 1, 1);


        gridLayout_33->addLayout(gridLayout_34, 1, 0, 1, 1);

        scrollArea_5->setWidget(scrollAreaWidgetContents_9);

        gridLayout_31->addWidget(scrollArea_5, 1, 0, 1, 1);

        horizontalSpacer_143 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_31->addItem(horizontalSpacer_143, 1, 1, 1, 1);


        gridLayout_35->addLayout(gridLayout_31, 0, 0, 1, 1);

        horizontalSpacer_144 = new QSpacerItem(8, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_35->addItem(horizontalSpacer_144, 0, 1, 1, 1);

        tabWidget->addTab(tab_5, QString());
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        gridLayout_40 = new QGridLayout(tab);
        gridLayout_40->setSpacing(6);
        gridLayout_40->setContentsMargins(11, 11, 11, 11);
        gridLayout_40->setObjectName(QStringLiteral("gridLayout_40"));
        gridLayout_36 = new QGridLayout();
        gridLayout_36->setSpacing(6);
        gridLayout_36->setObjectName(QStringLiteral("gridLayout_36"));
        horizontalLayout_19 = new QHBoxLayout();
        horizontalLayout_19->setSpacing(6);
        horizontalLayout_19->setObjectName(QStringLiteral("horizontalLayout_19"));
        horizontalSpacer_145 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_19->addItem(horizontalSpacer_145);

        horizontalSpacer_146 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_19->addItem(horizontalSpacer_146);

        label_ID_11 = new QLabel(tab);
        label_ID_11->setObjectName(QStringLiteral("label_ID_11"));
        sizePolicy.setHeightForWidth(label_ID_11->sizePolicy().hasHeightForWidth());
        label_ID_11->setSizePolicy(sizePolicy);
        label_ID_11->setMaximumSize(QSize(16777215, 16777215));
        label_ID_11->setFont(font);
        label_ID_11->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_19->addWidget(label_ID_11);

        horizontalSpacer_147 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_19->addItem(horizontalSpacer_147);

        label_name_12 = new QLabel(tab);
        label_name_12->setObjectName(QStringLiteral("label_name_12"));
        label_name_12->setFont(font);
        label_name_12->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_19->addWidget(label_name_12);

        horizontalSpacer_148 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_19->addItem(horizontalSpacer_148);

        horizontalSpacer_149 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_19->addItem(horizontalSpacer_149);

        label_start_11 = new QLabel(tab);
        label_start_11->setObjectName(QStringLiteral("label_start_11"));
        sizePolicy1.setHeightForWidth(label_start_11->sizePolicy().hasHeightForWidth());
        label_start_11->setSizePolicy(sizePolicy1);
        label_start_11->setFont(font);
        label_start_11->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_19->addWidget(label_start_11);

        label_msStart_12 = new QLabel(tab);
        label_msStart_12->setObjectName(QStringLiteral("label_msStart_12"));
        label_msStart_12->setFont(font1);
        label_msStart_12->setAlignment(Qt::AlignCenter);

        horizontalLayout_19->addWidget(label_msStart_12);

        horizontalSpacer_150 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_19->addItem(horizontalSpacer_150);

        label_duration_11 = new QLabel(tab);
        label_duration_11->setObjectName(QStringLiteral("label_duration_11"));
        label_duration_11->setFont(font);
        label_duration_11->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout_19->addWidget(label_duration_11);

        label_msDuration_20 = new QLabel(tab);
        label_msDuration_20->setObjectName(QStringLiteral("label_msDuration_20"));
        label_msDuration_20->setFont(font1);
        label_msDuration_20->setAlignment(Qt::AlignCenter);

        horizontalLayout_19->addWidget(label_msDuration_20);

        horizontalSpacer_151 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_19->addItem(horizontalSpacer_151);


        gridLayout_36->addLayout(horizontalLayout_19, 0, 0, 1, 1);

        gridLayout_37 = new QGridLayout();
        gridLayout_37->setSpacing(6);
        gridLayout_37->setObjectName(QStringLiteral("gridLayout_37"));
        horizontalLayout_20 = new QHBoxLayout();
        horizontalLayout_20->setSpacing(6);
        horizontalLayout_20->setObjectName(QStringLiteral("horizontalLayout_20"));
        progressBar_10 = new QProgressBar(tab);
        progressBar_10->setObjectName(QStringLiteral("progressBar_10"));
        progressBar_10->setValue(0);

        horizontalLayout_20->addWidget(progressBar_10);

        label_37 = new QLabel(tab);
        label_37->setObjectName(QStringLiteral("label_37"));
        label_37->setFont(font3);

        horizontalLayout_20->addWidget(label_37);


        gridLayout_37->addLayout(horizontalLayout_20, 4, 0, 1, 5);

        spinBox_start5_136 = new QSpinBox(tab);
        spinBox_start5_136->setObjectName(QStringLiteral("spinBox_start5_136"));
        spinBox_start5_136->setFont(font6);
        spinBox_start5_136->setMaximum(10000000);
        spinBox_start5_136->setValue(0);

        gridLayout_37->addWidget(spinBox_start5_136, 2, 3, 1, 2);

        spinBox_start5_137 = new QSpinBox(tab);
        spinBox_start5_137->setObjectName(QStringLiteral("spinBox_start5_137"));
        spinBox_start5_137->setFont(font6);
        spinBox_start5_137->setMaximum(10000000);
        spinBox_start5_137->setValue(0);

        gridLayout_37->addWidget(spinBox_start5_137, 2, 0, 1, 2);

        pushButton_39 = new QPushButton(tab);
        pushButton_39->setObjectName(QStringLiteral("pushButton_39"));
        pushButton_39->setMinimumSize(QSize(0, 80));
        pushButton_39->setFont(font2);

        gridLayout_37->addWidget(pushButton_39, 10, 3, 1, 2);

        horizontalSpacer_152 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_37->addItem(horizontalSpacer_152, 8, 2, 1, 1);

        verticalSpacer_41 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_37->addItem(verticalSpacer_41, 7, 0, 1, 1);

        pushButton_40 = new QPushButton(tab);
        pushButton_40->setObjectName(QStringLiteral("pushButton_40"));
        pushButton_40->setMinimumSize(QSize(0, 80));
        pushButton_40->setFont(font2);

        gridLayout_37->addWidget(pushButton_40, 8, 0, 1, 2);

        label_38 = new QLabel(tab);
        label_38->setObjectName(QStringLiteral("label_38"));
        label_38->setFont(font7);
        label_38->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_37->addWidget(label_38, 3, 0, 1, 5);

        label_39 = new QLabel(tab);
        label_39->setObjectName(QStringLiteral("label_39"));
        label_39->setFont(font8);

        gridLayout_37->addWidget(label_39, 1, 3, 1, 1);

        line_10 = new QFrame(tab);
        line_10->setObjectName(QStringLiteral("line_10"));
        line_10->setFrameShape(QFrame::HLine);
        line_10->setFrameShadow(QFrame::Sunken);

        gridLayout_37->addWidget(line_10, 6, 0, 1, 5);

        horizontalSpacer_153 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_37->addItem(horizontalSpacer_153, 0, 1, 1, 1);

        verticalSpacer_42 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_37->addItem(verticalSpacer_42, 0, 0, 1, 1);

        pushButton_41 = new QPushButton(tab);
        pushButton_41->setObjectName(QStringLiteral("pushButton_41"));
        pushButton_41->setMinimumSize(QSize(0, 80));
        pushButton_41->setFont(font2);

        gridLayout_37->addWidget(pushButton_41, 10, 0, 1, 2);

        label_msDuration_21 = new QLabel(tab);
        label_msDuration_21->setObjectName(QStringLiteral("label_msDuration_21"));
        label_msDuration_21->setFont(font1);
        label_msDuration_21->setAlignment(Qt::AlignCenter);

        gridLayout_37->addWidget(label_msDuration_21, 1, 4, 1, 1);

        verticalSpacer_43 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_37->addItem(verticalSpacer_43, 5, 0, 1, 1);

        pushButton_42 = new QPushButton(tab);
        pushButton_42->setObjectName(QStringLiteral("pushButton_42"));
        pushButton_42->setMinimumSize(QSize(0, 80));
        pushButton_42->setFont(font2);

        gridLayout_37->addWidget(pushButton_42, 8, 3, 1, 2);

        verticalSpacer_44 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_37->addItem(verticalSpacer_44, 9, 0, 1, 1);

        label_40 = new QLabel(tab);
        label_40->setObjectName(QStringLiteral("label_40"));
        label_40->setFont(font8);

        gridLayout_37->addWidget(label_40, 1, 0, 1, 1);

        verticalSpacer_45 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_37->addItem(verticalSpacer_45, 11, 0, 1, 1);


        gridLayout_36->addLayout(gridLayout_37, 0, 2, 2, 1);

        scrollArea_6 = new QScrollArea(tab);
        scrollArea_6->setObjectName(QStringLiteral("scrollArea_6"));
        scrollArea_6->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        scrollArea_6->setWidgetResizable(true);
        scrollAreaWidgetContents_10 = new QWidget();
        scrollAreaWidgetContents_10->setObjectName(QStringLiteral("scrollAreaWidgetContents_10"));
        scrollAreaWidgetContents_10->setGeometry(QRect(0, 0, 654, 740));
        gridLayout_38 = new QGridLayout(scrollAreaWidgetContents_10);
        gridLayout_38->setSpacing(6);
        gridLayout_38->setContentsMargins(11, 11, 11, 11);
        gridLayout_38->setObjectName(QStringLiteral("gridLayout_38"));
        gridLayout_39 = new QGridLayout();
        gridLayout_39->setSpacing(1);
        gridLayout_39->setObjectName(QStringLiteral("gridLayout_39"));
        textFIeld_name5_118 = new QLineEdit(scrollAreaWidgetContents_10);
        textFIeld_name5_118->setObjectName(QStringLiteral("textFIeld_name5_118"));
        textFIeld_name5_118->setFont(font2);

        gridLayout_39->addWidget(textFIeld_name5_118, 12, 4, 1, 1);

        spinBox_start2_19 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_start2_19->setObjectName(QStringLiteral("spinBox_start2_19"));
        spinBox_start2_19->setFont(font6);
        spinBox_start2_19->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_start2_19, 7, 6, 1, 2);

        label_ID4_136 = new QLabel(scrollAreaWidgetContents_10);
        label_ID4_136->setObjectName(QStringLiteral("label_ID4_136"));
        label_ID4_136->setFont(font4);
        label_ID4_136->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_39->addWidget(label_ID4_136, 5, 2, 1, 1);

        circle_inactive3_64 = new QLabel(scrollAreaWidgetContents_10);
        circle_inactive3_64->setObjectName(QStringLiteral("circle_inactive3_64"));
        circle_inactive3_64->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_64->setAlignment(Qt::AlignCenter);

        gridLayout_39->addWidget(circle_inactive3_64, 5, 3, 1, 1);

        horizontalSpacer_154 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_39->addItem(horizontalSpacer_154, 6, 0, 1, 1);

        checkBox_181 = new QCheckBox(scrollAreaWidgetContents_10);
        checkBox_181->setObjectName(QStringLiteral("checkBox_181"));
        checkBox_181->setEnabled(true);
        checkBox_181->setBaseSize(QSize(0, 0));
        checkBox_181->setFont(font5);
        checkBox_181->setTabletTracking(false);
        checkBox_181->setAutoFillBackground(false);
        checkBox_181->setChecked(false);
        checkBox_181->setTristate(false);

        gridLayout_39->addWidget(checkBox_181, 3, 1, 1, 1);

        checkBox_182 = new QCheckBox(scrollAreaWidgetContents_10);
        checkBox_182->setObjectName(QStringLiteral("checkBox_182"));
        checkBox_182->setEnabled(true);
        checkBox_182->setBaseSize(QSize(0, 0));
        checkBox_182->setFont(font5);
        checkBox_182->setTabletTracking(false);
        checkBox_182->setAutoFillBackground(false);
        checkBox_182->setChecked(false);
        checkBox_182->setTristate(false);

        gridLayout_39->addWidget(checkBox_182, 5, 1, 1, 1);

        checkBox_183 = new QCheckBox(scrollAreaWidgetContents_10);
        checkBox_183->setObjectName(QStringLiteral("checkBox_183"));
        checkBox_183->setEnabled(true);
        checkBox_183->setBaseSize(QSize(0, 0));
        checkBox_183->setFont(font5);
        checkBox_183->setTabletTracking(false);
        checkBox_183->setAutoFillBackground(false);
        checkBox_183->setChecked(false);
        checkBox_183->setTristate(false);

        gridLayout_39->addWidget(checkBox_183, 8, 1, 1, 1);

        spinBox_start1_10 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_start1_10->setObjectName(QStringLiteral("spinBox_start1_10"));
        spinBox_start1_10->setFont(font6);
        spinBox_start1_10->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_start1_10, 0, 6, 1, 2);

        checkBox_184 = new QCheckBox(scrollAreaWidgetContents_10);
        checkBox_184->setObjectName(QStringLiteral("checkBox_184"));
        checkBox_184->setEnabled(true);
        checkBox_184->setBaseSize(QSize(0, 0));
        checkBox_184->setFont(font5);
        checkBox_184->setTabletTracking(false);
        checkBox_184->setAutoFillBackground(false);
        checkBox_184->setChecked(false);
        checkBox_184->setTristate(false);

        gridLayout_39->addWidget(checkBox_184, 4, 1, 1, 1);

        circle_inactive3_65 = new QLabel(scrollAreaWidgetContents_10);
        circle_inactive3_65->setObjectName(QStringLiteral("circle_inactive3_65"));
        circle_inactive3_65->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_65->setAlignment(Qt::AlignCenter);

        gridLayout_39->addWidget(circle_inactive3_65, 3, 3, 1, 1);

        textFIeld_name1_10 = new QLineEdit(scrollAreaWidgetContents_10);
        textFIeld_name1_10->setObjectName(QStringLiteral("textFIeld_name1_10"));
        textFIeld_name1_10->setFont(font2);

        gridLayout_39->addWidget(textFIeld_name1_10, 0, 4, 1, 1);

        spinBox_duration2_19 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_duration2_19->setObjectName(QStringLiteral("spinBox_duration2_19"));
        spinBox_duration2_19->setFont(font6);
        spinBox_duration2_19->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_duration2_19, 7, 9, 1, 2);

        spinBox_duration4_19 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_duration4_19->setObjectName(QStringLiteral("spinBox_duration4_19"));
        spinBox_duration4_19->setFont(font6);
        spinBox_duration4_19->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_duration4_19, 3, 9, 1, 2);

        textFIeld_name4_19 = new QLineEdit(scrollAreaWidgetContents_10);
        textFIeld_name4_19->setObjectName(QStringLiteral("textFIeld_name4_19"));
        textFIeld_name4_19->setFont(font2);

        gridLayout_39->addWidget(textFIeld_name4_19, 3, 4, 1, 1);

        spinBox_duration5_118 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_duration5_118->setObjectName(QStringLiteral("spinBox_duration5_118"));
        spinBox_duration5_118->setFont(font6);
        spinBox_duration5_118->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_duration5_118, 6, 9, 1, 2);

        checkBox_185 = new QCheckBox(scrollAreaWidgetContents_10);
        checkBox_185->setObjectName(QStringLiteral("checkBox_185"));
        checkBox_185->setEnabled(true);
        checkBox_185->setBaseSize(QSize(0, 0));
        checkBox_185->setFont(font5);
        checkBox_185->setTabletTracking(false);
        checkBox_185->setAutoFillBackground(false);
        checkBox_185->setChecked(false);
        checkBox_185->setTristate(false);

        gridLayout_39->addWidget(checkBox_185, 14, 1, 1, 1);

        textFIeld_name2_19 = new QLineEdit(scrollAreaWidgetContents_10);
        textFIeld_name2_19->setObjectName(QStringLiteral("textFIeld_name2_19"));
        textFIeld_name2_19->setFont(font2);

        gridLayout_39->addWidget(textFIeld_name2_19, 7, 4, 1, 1);

        spinBox_duration5_119 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_duration5_119->setObjectName(QStringLiteral("spinBox_duration5_119"));
        spinBox_duration5_119->setFont(font6);
        spinBox_duration5_119->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_duration5_119, 10, 9, 1, 2);

        textFIeld_name5_119 = new QLineEdit(scrollAreaWidgetContents_10);
        textFIeld_name5_119->setObjectName(QStringLiteral("textFIeld_name5_119"));
        textFIeld_name5_119->setFont(font2);

        gridLayout_39->addWidget(textFIeld_name5_119, 4, 4, 1, 1);

        spinBox_start5_138 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_start5_138->setObjectName(QStringLiteral("spinBox_start5_138"));
        spinBox_start5_138->setFont(font6);
        spinBox_start5_138->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_start5_138, 4, 6, 1, 2);

        label_ID4_137 = new QLabel(scrollAreaWidgetContents_10);
        label_ID4_137->setObjectName(QStringLiteral("label_ID4_137"));
        label_ID4_137->setFont(font4);
        label_ID4_137->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_39->addWidget(label_ID4_137, 3, 2, 1, 1);

        spinBox_start3_19 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_start3_19->setObjectName(QStringLiteral("spinBox_start3_19"));
        spinBox_start3_19->setFont(font6);
        spinBox_start3_19->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_start3_19, 8, 6, 1, 2);

        label_ID4_138 = new QLabel(scrollAreaWidgetContents_10);
        label_ID4_138->setObjectName(QStringLiteral("label_ID4_138"));
        label_ID4_138->setFont(font4);
        label_ID4_138->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_39->addWidget(label_ID4_138, 9, 2, 1, 1);

        spinBox_duration4_20 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_duration4_20->setObjectName(QStringLiteral("spinBox_duration4_20"));
        spinBox_duration4_20->setFont(font6);
        spinBox_duration4_20->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_duration4_20, 9, 9, 1, 2);

        checkBox_186 = new QCheckBox(scrollAreaWidgetContents_10);
        checkBox_186->setObjectName(QStringLiteral("checkBox_186"));
        checkBox_186->setEnabled(true);
        checkBox_186->setBaseSize(QSize(0, 0));
        checkBox_186->setFont(font5);
        checkBox_186->setTabletTracking(false);
        checkBox_186->setAutoFillBackground(false);
        checkBox_186->setChecked(false);
        checkBox_186->setTristate(false);

        gridLayout_39->addWidget(checkBox_186, 13, 1, 1, 1);

        spinBox_start5_139 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_start5_139->setObjectName(QStringLiteral("spinBox_start5_139"));
        spinBox_start5_139->setFont(font6);
        spinBox_start5_139->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_start5_139, 15, 6, 1, 2);

        spinBox_duration5_120 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_duration5_120->setObjectName(QStringLiteral("spinBox_duration5_120"));
        spinBox_duration5_120->setFont(font6);
        spinBox_duration5_120->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_duration5_120, 15, 9, 1, 2);

        textFIeld_name5_120 = new QLineEdit(scrollAreaWidgetContents_10);
        textFIeld_name5_120->setObjectName(QStringLiteral("textFIeld_name5_120"));
        textFIeld_name5_120->setFont(font2);

        gridLayout_39->addWidget(textFIeld_name5_120, 10, 4, 1, 1);

        label_ID4_139 = new QLabel(scrollAreaWidgetContents_10);
        label_ID4_139->setObjectName(QStringLiteral("label_ID4_139"));
        label_ID4_139->setFont(font4);
        label_ID4_139->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_39->addWidget(label_ID4_139, 14, 2, 1, 1);

        spinBox_start5_140 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_start5_140->setObjectName(QStringLiteral("spinBox_start5_140"));
        spinBox_start5_140->setFont(font6);
        spinBox_start5_140->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_start5_140, 14, 6, 1, 2);

        textFIeld_name4_20 = new QLineEdit(scrollAreaWidgetContents_10);
        textFIeld_name4_20->setObjectName(QStringLiteral("textFIeld_name4_20"));
        textFIeld_name4_20->setFont(font2);

        gridLayout_39->addWidget(textFIeld_name4_20, 9, 4, 1, 1);

        spinBox_start3_20 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_start3_20->setObjectName(QStringLiteral("spinBox_start3_20"));
        spinBox_start3_20->setFont(font6);
        spinBox_start3_20->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_start3_20, 2, 6, 1, 2);

        spinBox_start5_141 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_start5_141->setObjectName(QStringLiteral("spinBox_start5_141"));
        spinBox_start5_141->setFont(font6);
        spinBox_start5_141->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_start5_141, 6, 6, 1, 2);

        horizontalSpacer_155 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_39->addItem(horizontalSpacer_155, 0, 12, 1, 1);

        horizontalSpacer_156 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_39->addItem(horizontalSpacer_156, 10, 5, 1, 1);

        spinBox_duration5_121 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_duration5_121->setObjectName(QStringLiteral("spinBox_duration5_121"));
        spinBox_duration5_121->setFont(font6);
        spinBox_duration5_121->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_duration5_121, 11, 9, 1, 2);

        textFIeld_name5_121 = new QLineEdit(scrollAreaWidgetContents_10);
        textFIeld_name5_121->setObjectName(QStringLiteral("textFIeld_name5_121"));
        textFIeld_name5_121->setFont(font2);

        gridLayout_39->addWidget(textFIeld_name5_121, 5, 4, 1, 1);

        label_ID2_19 = new QLabel(scrollAreaWidgetContents_10);
        label_ID2_19->setObjectName(QStringLiteral("label_ID2_19"));
        label_ID2_19->setFont(font4);
        label_ID2_19->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_39->addWidget(label_ID2_19, 1, 2, 1, 1);

        checkBox_187 = new QCheckBox(scrollAreaWidgetContents_10);
        checkBox_187->setObjectName(QStringLiteral("checkBox_187"));
        checkBox_187->setEnabled(true);
        checkBox_187->setBaseSize(QSize(0, 0));
        checkBox_187->setFont(font5);
        checkBox_187->setTabletTracking(false);
        checkBox_187->setAutoFillBackground(false);
        checkBox_187->setChecked(false);
        checkBox_187->setTristate(false);

        gridLayout_39->addWidget(checkBox_187, 6, 1, 1, 1);

        spinBox_start5_142 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_start5_142->setObjectName(QStringLiteral("spinBox_start5_142"));
        spinBox_start5_142->setFont(font6);
        spinBox_start5_142->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_start5_142, 17, 6, 1, 2);

        textFIeld_name2_20 = new QLineEdit(scrollAreaWidgetContents_10);
        textFIeld_name2_20->setObjectName(QStringLiteral("textFIeld_name2_20"));
        textFIeld_name2_20->setFont(font2);

        gridLayout_39->addWidget(textFIeld_name2_20, 1, 4, 1, 1);

        textFIeld_name5_122 = new QLineEdit(scrollAreaWidgetContents_10);
        textFIeld_name5_122->setObjectName(QStringLiteral("textFIeld_name5_122"));
        textFIeld_name5_122->setFont(font2);

        gridLayout_39->addWidget(textFIeld_name5_122, 6, 4, 1, 1);

        checkBox_188 = new QCheckBox(scrollAreaWidgetContents_10);
        checkBox_188->setObjectName(QStringLiteral("checkBox_188"));
        checkBox_188->setEnabled(true);
        checkBox_188->setBaseSize(QSize(0, 0));
        checkBox_188->setFont(font5);
        checkBox_188->setTabletTracking(false);
        checkBox_188->setAutoFillBackground(false);
        checkBox_188->setChecked(false);
        checkBox_188->setTristate(false);

        gridLayout_39->addWidget(checkBox_188, 7, 1, 1, 1);

        label_ID4_140 = new QLabel(scrollAreaWidgetContents_10);
        label_ID4_140->setObjectName(QStringLiteral("label_ID4_140"));
        label_ID4_140->setFont(font4);
        label_ID4_140->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_39->addWidget(label_ID4_140, 6, 2, 1, 1);

        circle_inactive3_66 = new QLabel(scrollAreaWidgetContents_10);
        circle_inactive3_66->setObjectName(QStringLiteral("circle_inactive3_66"));
        QPalette palette5;
        palette5.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette5.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette5.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        circle_inactive3_66->setPalette(palette5);
        circle_inactive3_66->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_66->setAlignment(Qt::AlignCenter);

        gridLayout_39->addWidget(circle_inactive3_66, 8, 3, 1, 1);

        textFIeld_name3_19 = new QLineEdit(scrollAreaWidgetContents_10);
        textFIeld_name3_19->setObjectName(QStringLiteral("textFIeld_name3_19"));
        textFIeld_name3_19->setFont(font2);

        gridLayout_39->addWidget(textFIeld_name3_19, 2, 4, 1, 1);

        spinBox_duration5_122 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_duration5_122->setObjectName(QStringLiteral("spinBox_duration5_122"));
        spinBox_duration5_122->setFont(font6);
        spinBox_duration5_122->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_duration5_122, 5, 9, 1, 2);

        textFIeld_name3_20 = new QLineEdit(scrollAreaWidgetContents_10);
        textFIeld_name3_20->setObjectName(QStringLiteral("textFIeld_name3_20"));
        textFIeld_name3_20->setFont(font2);

        gridLayout_39->addWidget(textFIeld_name3_20, 8, 4, 1, 1);

        spinBox_duration5_123 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_duration5_123->setObjectName(QStringLiteral("spinBox_duration5_123"));
        spinBox_duration5_123->setFont(font6);
        spinBox_duration5_123->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_duration5_123, 14, 9, 1, 2);

        checkBox_189 = new QCheckBox(scrollAreaWidgetContents_10);
        checkBox_189->setObjectName(QStringLiteral("checkBox_189"));
        checkBox_189->setEnabled(true);
        checkBox_189->setBaseSize(QSize(0, 0));
        checkBox_189->setFont(font5);
        checkBox_189->setTabletTracking(false);
        checkBox_189->setAutoFillBackground(false);
        checkBox_189->setChecked(false);
        checkBox_189->setTristate(false);

        gridLayout_39->addWidget(checkBox_189, 12, 1, 1, 1);

        textFIeld_name5_123 = new QLineEdit(scrollAreaWidgetContents_10);
        textFIeld_name5_123->setObjectName(QStringLiteral("textFIeld_name5_123"));
        textFIeld_name5_123->setFont(font2);

        gridLayout_39->addWidget(textFIeld_name5_123, 18, 4, 1, 1);

        textFIeld_name5_124 = new QLineEdit(scrollAreaWidgetContents_10);
        textFIeld_name5_124->setObjectName(QStringLiteral("textFIeld_name5_124"));
        textFIeld_name5_124->setFont(font2);

        gridLayout_39->addWidget(textFIeld_name5_124, 20, 4, 1, 1);

        spinBox_duration5_124 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_duration5_124->setObjectName(QStringLiteral("spinBox_duration5_124"));
        spinBox_duration5_124->setFont(font6);
        spinBox_duration5_124->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_duration5_124, 4, 9, 1, 2);

        label_ID4_141 = new QLabel(scrollAreaWidgetContents_10);
        label_ID4_141->setObjectName(QStringLiteral("label_ID4_141"));
        label_ID4_141->setFont(font4);
        label_ID4_141->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_39->addWidget(label_ID4_141, 20, 2, 1, 1);

        horizontalSpacer_157 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_39->addItem(horizontalSpacer_157, 10, 8, 1, 1);

        label_ID4_142 = new QLabel(scrollAreaWidgetContents_10);
        label_ID4_142->setObjectName(QStringLiteral("label_ID4_142"));
        label_ID4_142->setFont(font4);
        label_ID4_142->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_39->addWidget(label_ID4_142, 4, 2, 1, 1);

        textFIeld_name5_125 = new QLineEdit(scrollAreaWidgetContents_10);
        textFIeld_name5_125->setObjectName(QStringLiteral("textFIeld_name5_125"));
        textFIeld_name5_125->setFont(font2);

        gridLayout_39->addWidget(textFIeld_name5_125, 13, 4, 1, 1);

        spinBox_start2_20 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_start2_20->setObjectName(QStringLiteral("spinBox_start2_20"));
        spinBox_start2_20->setFont(font6);
        spinBox_start2_20->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_start2_20, 1, 6, 1, 2);

        spinBox_duration5_125 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_duration5_125->setObjectName(QStringLiteral("spinBox_duration5_125"));
        spinBox_duration5_125->setFont(font6);
        spinBox_duration5_125->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_duration5_125, 17, 9, 1, 2);

        label_ID4_143 = new QLabel(scrollAreaWidgetContents_10);
        label_ID4_143->setObjectName(QStringLiteral("label_ID4_143"));
        label_ID4_143->setFont(font4);
        label_ID4_143->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_39->addWidget(label_ID4_143, 10, 2, 1, 1);

        spinBox_duration5_126 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_duration5_126->setObjectName(QStringLiteral("spinBox_duration5_126"));
        spinBox_duration5_126->setFont(font6);
        spinBox_duration5_126->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_duration5_126, 12, 9, 1, 2);

        spinBox_duration5_127 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_duration5_127->setObjectName(QStringLiteral("spinBox_duration5_127"));
        spinBox_duration5_127->setFont(font6);
        spinBox_duration5_127->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_duration5_127, 20, 9, 1, 2);

        spinBox_start5_143 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_start5_143->setObjectName(QStringLiteral("spinBox_start5_143"));
        spinBox_start5_143->setFont(font6);
        spinBox_start5_143->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_start5_143, 20, 6, 1, 2);

        textFIeld_name5_126 = new QLineEdit(scrollAreaWidgetContents_10);
        textFIeld_name5_126->setObjectName(QStringLiteral("textFIeld_name5_126"));
        textFIeld_name5_126->setFont(font2);

        gridLayout_39->addWidget(textFIeld_name5_126, 11, 4, 1, 1);

        spinBox_start5_144 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_start5_144->setObjectName(QStringLiteral("spinBox_start5_144"));
        spinBox_start5_144->setFont(font6);
        spinBox_start5_144->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_start5_144, 13, 6, 1, 2);

        checkBox_190 = new QCheckBox(scrollAreaWidgetContents_10);
        checkBox_190->setObjectName(QStringLiteral("checkBox_190"));
        checkBox_190->setEnabled(true);
        checkBox_190->setBaseSize(QSize(0, 0));
        checkBox_190->setFont(font5);
        checkBox_190->setTabletTracking(false);
        checkBox_190->setAutoFillBackground(false);
        checkBox_190->setChecked(false);
        checkBox_190->setTristate(false);

        gridLayout_39->addWidget(checkBox_190, 15, 1, 1, 1);

        label_ID4_144 = new QLabel(scrollAreaWidgetContents_10);
        label_ID4_144->setObjectName(QStringLiteral("label_ID4_144"));
        label_ID4_144->setFont(font4);
        label_ID4_144->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_39->addWidget(label_ID4_144, 12, 2, 1, 1);

        label_ID4_145 = new QLabel(scrollAreaWidgetContents_10);
        label_ID4_145->setObjectName(QStringLiteral("label_ID4_145"));
        label_ID4_145->setFont(font4);
        label_ID4_145->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_39->addWidget(label_ID4_145, 16, 2, 1, 1);

        spinBox_duration3_19 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_duration3_19->setObjectName(QStringLiteral("spinBox_duration3_19"));
        spinBox_duration3_19->setFont(font6);
        spinBox_duration3_19->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_duration3_19, 8, 9, 1, 2);

        label_ID2_20 = new QLabel(scrollAreaWidgetContents_10);
        label_ID2_20->setObjectName(QStringLiteral("label_ID2_20"));
        label_ID2_20->setFont(font4);
        label_ID2_20->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_39->addWidget(label_ID2_20, 7, 2, 1, 1);

        label_ID4_146 = new QLabel(scrollAreaWidgetContents_10);
        label_ID4_146->setObjectName(QStringLiteral("label_ID4_146"));
        label_ID4_146->setFont(font4);
        label_ID4_146->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_39->addWidget(label_ID4_146, 15, 2, 1, 1);

        checkBox_191 = new QCheckBox(scrollAreaWidgetContents_10);
        checkBox_191->setObjectName(QStringLiteral("checkBox_191"));
        checkBox_191->setEnabled(true);
        checkBox_191->setBaseSize(QSize(0, 0));
        checkBox_191->setFont(font5);
        checkBox_191->setTabletTracking(false);
        checkBox_191->setAutoFillBackground(false);
        checkBox_191->setChecked(false);
        checkBox_191->setTristate(false);

        gridLayout_39->addWidget(checkBox_191, 0, 1, 1, 1);

        circle_inactive3_67 = new QLabel(scrollAreaWidgetContents_10);
        circle_inactive3_67->setObjectName(QStringLiteral("circle_inactive3_67"));
        circle_inactive3_67->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_67->setAlignment(Qt::AlignCenter);

        gridLayout_39->addWidget(circle_inactive3_67, 2, 3, 1, 1);

        label_ID4_147 = new QLabel(scrollAreaWidgetContents_10);
        label_ID4_147->setObjectName(QStringLiteral("label_ID4_147"));
        label_ID4_147->setFont(font4);
        label_ID4_147->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_39->addWidget(label_ID4_147, 13, 2, 1, 1);

        label_ID4_148 = new QLabel(scrollAreaWidgetContents_10);
        label_ID4_148->setObjectName(QStringLiteral("label_ID4_148"));
        label_ID4_148->setFont(font4);
        label_ID4_148->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_39->addWidget(label_ID4_148, 11, 2, 1, 1);

        spinBox_start4_19 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_start4_19->setObjectName(QStringLiteral("spinBox_start4_19"));
        spinBox_start4_19->setFont(font6);
        spinBox_start4_19->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_start4_19, 3, 6, 1, 2);

        textFIeld_name5_127 = new QLineEdit(scrollAreaWidgetContents_10);
        textFIeld_name5_127->setObjectName(QStringLiteral("textFIeld_name5_127"));
        textFIeld_name5_127->setFont(font2);

        gridLayout_39->addWidget(textFIeld_name5_127, 16, 4, 1, 1);

        spinBox_start5_145 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_start5_145->setObjectName(QStringLiteral("spinBox_start5_145"));
        spinBox_start5_145->setFont(font6);
        spinBox_start5_145->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_start5_145, 12, 6, 1, 2);

        spinBox_duration5_128 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_duration5_128->setObjectName(QStringLiteral("spinBox_duration5_128"));
        spinBox_duration5_128->setFont(font6);
        spinBox_duration5_128->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_duration5_128, 18, 9, 1, 2);

        label_ID3_19 = new QLabel(scrollAreaWidgetContents_10);
        label_ID3_19->setObjectName(QStringLiteral("label_ID3_19"));
        label_ID3_19->setFont(font4);
        label_ID3_19->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_39->addWidget(label_ID3_19, 2, 2, 1, 1);

        spinBox_duration1_10 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_duration1_10->setObjectName(QStringLiteral("spinBox_duration1_10"));
        spinBox_duration1_10->setFont(font6);
        spinBox_duration1_10->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_duration1_10, 0, 9, 1, 2);

        spinBox_duration5_129 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_duration5_129->setObjectName(QStringLiteral("spinBox_duration5_129"));
        spinBox_duration5_129->setFont(font6);
        spinBox_duration5_129->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_duration5_129, 16, 9, 1, 2);

        spinBox_start5_146 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_start5_146->setObjectName(QStringLiteral("spinBox_start5_146"));
        spinBox_start5_146->setFont(font6);
        spinBox_start5_146->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_start5_146, 10, 6, 1, 2);

        spinBox_start4_20 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_start4_20->setObjectName(QStringLiteral("spinBox_start4_20"));
        spinBox_start4_20->setFont(font6);
        spinBox_start4_20->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_start4_20, 9, 6, 1, 2);

        spinBox_duration2_20 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_duration2_20->setObjectName(QStringLiteral("spinBox_duration2_20"));
        spinBox_duration2_20->setFont(font6);
        spinBox_duration2_20->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_duration2_20, 1, 9, 1, 2);

        circle_inactive1_118 = new QLabel(scrollAreaWidgetContents_10);
        circle_inactive1_118->setObjectName(QStringLiteral("circle_inactive1_118"));
        circle_inactive1_118->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_118->setAlignment(Qt::AlignCenter);

        gridLayout_39->addWidget(circle_inactive1_118, 0, 3, 1, 1);

        checkBox_192 = new QCheckBox(scrollAreaWidgetContents_10);
        checkBox_192->setObjectName(QStringLiteral("checkBox_192"));
        checkBox_192->setEnabled(true);
        checkBox_192->setBaseSize(QSize(0, 0));
        checkBox_192->setFont(font5);
        checkBox_192->setTabletTracking(false);
        checkBox_192->setAutoFillBackground(false);
        checkBox_192->setChecked(false);
        checkBox_192->setTristate(false);

        gridLayout_39->addWidget(checkBox_192, 1, 1, 1, 1);

        spinBox_duration3_20 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_duration3_20->setObjectName(QStringLiteral("spinBox_duration3_20"));
        spinBox_duration3_20->setFont(font6);
        spinBox_duration3_20->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_duration3_20, 2, 9, 1, 2);

        checkBox_193 = new QCheckBox(scrollAreaWidgetContents_10);
        checkBox_193->setObjectName(QStringLiteral("checkBox_193"));
        checkBox_193->setEnabled(true);
        checkBox_193->setBaseSize(QSize(0, 0));
        checkBox_193->setFont(font5);
        checkBox_193->setTabletTracking(false);
        checkBox_193->setAutoFillBackground(false);
        checkBox_193->setChecked(false);
        checkBox_193->setTristate(false);

        gridLayout_39->addWidget(checkBox_193, 11, 1, 1, 1);

        checkBox_194 = new QCheckBox(scrollAreaWidgetContents_10);
        checkBox_194->setObjectName(QStringLiteral("checkBox_194"));
        checkBox_194->setEnabled(true);
        checkBox_194->setBaseSize(QSize(0, 0));
        checkBox_194->setFont(font5);
        checkBox_194->setTabletTracking(false);
        checkBox_194->setAutoFillBackground(false);
        checkBox_194->setChecked(false);
        checkBox_194->setTristate(false);

        gridLayout_39->addWidget(checkBox_194, 9, 1, 1, 1);

        checkBox_195 = new QCheckBox(scrollAreaWidgetContents_10);
        checkBox_195->setObjectName(QStringLiteral("checkBox_195"));
        checkBox_195->setEnabled(true);
        checkBox_195->setBaseSize(QSize(0, 0));
        checkBox_195->setFont(font5);
        checkBox_195->setTabletTracking(false);
        checkBox_195->setAutoFillBackground(false);
        checkBox_195->setChecked(false);
        checkBox_195->setTristate(false);

        gridLayout_39->addWidget(checkBox_195, 2, 1, 1, 1);

        textFIeld_name5_128 = new QLineEdit(scrollAreaWidgetContents_10);
        textFIeld_name5_128->setObjectName(QStringLiteral("textFIeld_name5_128"));
        textFIeld_name5_128->setFont(font2);

        gridLayout_39->addWidget(textFIeld_name5_128, 15, 4, 1, 1);

        checkBox_196 = new QCheckBox(scrollAreaWidgetContents_10);
        checkBox_196->setObjectName(QStringLiteral("checkBox_196"));
        checkBox_196->setEnabled(true);
        checkBox_196->setBaseSize(QSize(0, 0));
        checkBox_196->setFont(font5);
        checkBox_196->setTabletTracking(false);
        checkBox_196->setAutoFillBackground(false);
        checkBox_196->setChecked(false);
        checkBox_196->setTristate(false);

        gridLayout_39->addWidget(checkBox_196, 10, 1, 1, 1);

        spinBox_start5_147 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_start5_147->setObjectName(QStringLiteral("spinBox_start5_147"));
        spinBox_start5_147->setFont(font6);
        spinBox_start5_147->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_start5_147, 5, 6, 1, 2);

        label_ID3_20 = new QLabel(scrollAreaWidgetContents_10);
        label_ID3_20->setObjectName(QStringLiteral("label_ID3_20"));
        label_ID3_20->setFont(font4);
        label_ID3_20->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_39->addWidget(label_ID3_20, 8, 2, 1, 1);

        label_ID1_10 = new QLabel(scrollAreaWidgetContents_10);
        label_ID1_10->setObjectName(QStringLiteral("label_ID1_10"));
        label_ID1_10->setFont(font4);
        label_ID1_10->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_39->addWidget(label_ID1_10, 0, 2, 1, 1);

        textFIeld_name5_129 = new QLineEdit(scrollAreaWidgetContents_10);
        textFIeld_name5_129->setObjectName(QStringLiteral("textFIeld_name5_129"));
        textFIeld_name5_129->setFont(font2);

        gridLayout_39->addWidget(textFIeld_name5_129, 14, 4, 1, 1);

        spinBox_duration5_130 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_duration5_130->setObjectName(QStringLiteral("spinBox_duration5_130"));
        spinBox_duration5_130->setFont(font6);
        spinBox_duration5_130->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_duration5_130, 13, 9, 1, 2);

        horizontalSpacer_158 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_39->addItem(horizontalSpacer_158, 21, 3, 1, 1);

        label_ID4_149 = new QLabel(scrollAreaWidgetContents_10);
        label_ID4_149->setObjectName(QStringLiteral("label_ID4_149"));
        label_ID4_149->setFont(font4);
        label_ID4_149->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_39->addWidget(label_ID4_149, 18, 2, 1, 1);

        checkBox_197 = new QCheckBox(scrollAreaWidgetContents_10);
        checkBox_197->setObjectName(QStringLiteral("checkBox_197"));
        checkBox_197->setEnabled(true);
        checkBox_197->setBaseSize(QSize(0, 0));
        checkBox_197->setFont(font5);
        checkBox_197->setTabletTracking(false);
        checkBox_197->setAutoFillBackground(false);
        checkBox_197->setChecked(false);
        checkBox_197->setTristate(false);

        gridLayout_39->addWidget(checkBox_197, 18, 1, 1, 1);

        checkBox_198 = new QCheckBox(scrollAreaWidgetContents_10);
        checkBox_198->setObjectName(QStringLiteral("checkBox_198"));
        checkBox_198->setEnabled(true);
        checkBox_198->setBaseSize(QSize(0, 0));
        checkBox_198->setFont(font5);
        checkBox_198->setTabletTracking(false);
        checkBox_198->setAutoFillBackground(false);
        checkBox_198->setChecked(false);
        checkBox_198->setTristate(false);

        gridLayout_39->addWidget(checkBox_198, 20, 1, 1, 1);

        checkBox_199 = new QCheckBox(scrollAreaWidgetContents_10);
        checkBox_199->setObjectName(QStringLiteral("checkBox_199"));
        checkBox_199->setEnabled(true);
        checkBox_199->setBaseSize(QSize(0, 0));
        checkBox_199->setFont(font5);
        checkBox_199->setTabletTracking(false);
        checkBox_199->setAutoFillBackground(false);
        checkBox_199->setChecked(false);
        checkBox_199->setTristate(false);

        gridLayout_39->addWidget(checkBox_199, 17, 1, 1, 1);

        spinBox_start5_148 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_start5_148->setObjectName(QStringLiteral("spinBox_start5_148"));
        spinBox_start5_148->setFont(font6);
        spinBox_start5_148->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_start5_148, 11, 6, 1, 2);

        checkBox_200 = new QCheckBox(scrollAreaWidgetContents_10);
        checkBox_200->setObjectName(QStringLiteral("checkBox_200"));
        checkBox_200->setEnabled(true);
        checkBox_200->setBaseSize(QSize(0, 0));
        checkBox_200->setFont(font5);
        checkBox_200->setTabletTracking(false);
        checkBox_200->setAutoFillBackground(false);
        checkBox_200->setChecked(false);
        checkBox_200->setTristate(false);

        gridLayout_39->addWidget(checkBox_200, 16, 1, 1, 1);

        spinBox_start5_149 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_start5_149->setObjectName(QStringLiteral("spinBox_start5_149"));
        spinBox_start5_149->setFont(font6);
        spinBox_start5_149->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_start5_149, 16, 6, 1, 2);

        label_ID4_150 = new QLabel(scrollAreaWidgetContents_10);
        label_ID4_150->setObjectName(QStringLiteral("label_ID4_150"));
        label_ID4_150->setFont(font4);
        label_ID4_150->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        gridLayout_39->addWidget(label_ID4_150, 17, 2, 1, 1);

        circle_inactive3_68 = new QLabel(scrollAreaWidgetContents_10);
        circle_inactive3_68->setObjectName(QStringLiteral("circle_inactive3_68"));
        circle_inactive3_68->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_68->setAlignment(Qt::AlignCenter);

        gridLayout_39->addWidget(circle_inactive3_68, 4, 3, 1, 1);

        spinBox_start5_150 = new QSpinBox(scrollAreaWidgetContents_10);
        spinBox_start5_150->setObjectName(QStringLiteral("spinBox_start5_150"));
        spinBox_start5_150->setFont(font6);
        spinBox_start5_150->setMaximum(10000000);

        gridLayout_39->addWidget(spinBox_start5_150, 18, 6, 1, 2);

        circle_inactive1_119 = new QLabel(scrollAreaWidgetContents_10);
        circle_inactive1_119->setObjectName(QStringLiteral("circle_inactive1_119"));
        circle_inactive1_119->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_119->setAlignment(Qt::AlignCenter);

        gridLayout_39->addWidget(circle_inactive1_119, 1, 3, 1, 1);

        textFIeld_name5_130 = new QLineEdit(scrollAreaWidgetContents_10);
        textFIeld_name5_130->setObjectName(QStringLiteral("textFIeld_name5_130"));
        textFIeld_name5_130->setFont(font2);

        gridLayout_39->addWidget(textFIeld_name5_130, 17, 4, 1, 1);

        circle_inactive3_69 = new QLabel(scrollAreaWidgetContents_10);
        circle_inactive3_69->setObjectName(QStringLiteral("circle_inactive3_69"));
        circle_inactive3_69->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_69->setAlignment(Qt::AlignCenter);

        gridLayout_39->addWidget(circle_inactive3_69, 6, 3, 1, 1);

        horizontalSpacer_159 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_39->addItem(horizontalSpacer_159, 0, 8, 1, 1);

        circle_inactive3_70 = new QLabel(scrollAreaWidgetContents_10);
        circle_inactive3_70->setObjectName(QStringLiteral("circle_inactive3_70"));
        circle_inactive3_70->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive3_70->setAlignment(Qt::AlignCenter);

        gridLayout_39->addWidget(circle_inactive3_70, 7, 3, 1, 1);

        circle_inactive1_120 = new QLabel(scrollAreaWidgetContents_10);
        circle_inactive1_120->setObjectName(QStringLiteral("circle_inactive1_120"));
        circle_inactive1_120->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_120->setAlignment(Qt::AlignCenter);

        gridLayout_39->addWidget(circle_inactive1_120, 9, 3, 1, 1);

        circle_inactive1_121 = new QLabel(scrollAreaWidgetContents_10);
        circle_inactive1_121->setObjectName(QStringLiteral("circle_inactive1_121"));
        circle_inactive1_121->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_121->setAlignment(Qt::AlignCenter);

        gridLayout_39->addWidget(circle_inactive1_121, 10, 3, 1, 1);

        circle_inactive1_122 = new QLabel(scrollAreaWidgetContents_10);
        circle_inactive1_122->setObjectName(QStringLiteral("circle_inactive1_122"));
        circle_inactive1_122->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_122->setAlignment(Qt::AlignCenter);

        gridLayout_39->addWidget(circle_inactive1_122, 11, 3, 1, 1);

        circle_inactive1_123 = new QLabel(scrollAreaWidgetContents_10);
        circle_inactive1_123->setObjectName(QStringLiteral("circle_inactive1_123"));
        circle_inactive1_123->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_123->setAlignment(Qt::AlignCenter);

        gridLayout_39->addWidget(circle_inactive1_123, 12, 3, 1, 1);

        circle_inactive1_124 = new QLabel(scrollAreaWidgetContents_10);
        circle_inactive1_124->setObjectName(QStringLiteral("circle_inactive1_124"));
        circle_inactive1_124->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_124->setAlignment(Qt::AlignCenter);

        gridLayout_39->addWidget(circle_inactive1_124, 13, 3, 1, 1);

        circle_inactive1_125 = new QLabel(scrollAreaWidgetContents_10);
        circle_inactive1_125->setObjectName(QStringLiteral("circle_inactive1_125"));
        circle_inactive1_125->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_125->setAlignment(Qt::AlignCenter);

        gridLayout_39->addWidget(circle_inactive1_125, 14, 3, 1, 1);

        circle_inactive1_126 = new QLabel(scrollAreaWidgetContents_10);
        circle_inactive1_126->setObjectName(QStringLiteral("circle_inactive1_126"));
        circle_inactive1_126->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_126->setAlignment(Qt::AlignCenter);

        gridLayout_39->addWidget(circle_inactive1_126, 15, 3, 1, 1);

        circle_inactive1_127 = new QLabel(scrollAreaWidgetContents_10);
        circle_inactive1_127->setObjectName(QStringLiteral("circle_inactive1_127"));
        circle_inactive1_127->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_127->setAlignment(Qt::AlignCenter);

        gridLayout_39->addWidget(circle_inactive1_127, 16, 3, 1, 1);

        circle_inactive1_128 = new QLabel(scrollAreaWidgetContents_10);
        circle_inactive1_128->setObjectName(QStringLiteral("circle_inactive1_128"));
        circle_inactive1_128->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_128->setAlignment(Qt::AlignCenter);

        gridLayout_39->addWidget(circle_inactive1_128, 17, 3, 1, 1);

        circle_inactive1_129 = new QLabel(scrollAreaWidgetContents_10);
        circle_inactive1_129->setObjectName(QStringLiteral("circle_inactive1_129"));
        circle_inactive1_129->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_129->setAlignment(Qt::AlignCenter);

        gridLayout_39->addWidget(circle_inactive1_129, 18, 3, 1, 1);

        circle_inactive1_130 = new QLabel(scrollAreaWidgetContents_10);
        circle_inactive1_130->setObjectName(QStringLiteral("circle_inactive1_130"));
        circle_inactive1_130->setPixmap(QPixmap(QString::fromUtf8("UI Elements/FeedbackCircles-01.png")));
        circle_inactive1_130->setAlignment(Qt::AlignCenter);

        gridLayout_39->addWidget(circle_inactive1_130, 20, 3, 1, 1);


        gridLayout_38->addLayout(gridLayout_39, 1, 0, 1, 1);

        scrollArea_6->setWidget(scrollAreaWidgetContents_10);

        gridLayout_36->addWidget(scrollArea_6, 1, 0, 1, 1);

        horizontalSpacer_160 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_36->addItem(horizontalSpacer_160, 1, 1, 1, 1);


        gridLayout_40->addLayout(gridLayout_36, 0, 0, 1, 1);

        horizontalSpacer_161 = new QSpacerItem(8, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_40->addItem(horizontalSpacer_161, 0, 1, 1, 1);

        tabWidget->addTab(tab, QString());

        gridLayout_6->addWidget(tabWidget, 0, 0, 1, 1);

        MainWindowApp->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindowApp);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1061, 22));
        menuOpen = new QMenu(menuBar);
        menuOpen->setObjectName(QStringLiteral("menuOpen"));
        MainWindowApp->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindowApp);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindowApp->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindowApp);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindowApp->setStatusBar(statusBar);
        QWidget::setTabOrder(tabWidget, scrollArea_8);
        QWidget::setTabOrder(scrollArea_8, checkBox_231);
        QWidget::setTabOrder(checkBox_231, textFIeld_name1_12);
        QWidget::setTabOrder(textFIeld_name1_12, textFIeld_name1_13);
        QWidget::setTabOrder(textFIeld_name1_13, checkBox_232);
        QWidget::setTabOrder(checkBox_232, textFIeld_name1_14);
        QWidget::setTabOrder(textFIeld_name1_14, textFIeld_name1_18);
        QWidget::setTabOrder(textFIeld_name1_18, checkBox_235);
        QWidget::setTabOrder(checkBox_235, textFIeld_name1_15);
        QWidget::setTabOrder(textFIeld_name1_15, textFIeld_name1_19);
        QWidget::setTabOrder(textFIeld_name1_19, checkBox_221);
        QWidget::setTabOrder(checkBox_221, textFIeld_name1_16);
        QWidget::setTabOrder(textFIeld_name1_16, textFIeld_name1_20);
        QWidget::setTabOrder(textFIeld_name1_20, checkBox_224);
        QWidget::setTabOrder(checkBox_224, textFIeld_name1_17);
        QWidget::setTabOrder(textFIeld_name1_17, textFIeld_name1_21);
        QWidget::setTabOrder(textFIeld_name1_21, textFIeld_name1_22);
        QWidget::setTabOrder(textFIeld_name1_22, scrollArea_7);
        QWidget::setTabOrder(scrollArea_7, checkBox_211);
        QWidget::setTabOrder(checkBox_211, textFIeld_name1_11);
        QWidget::setTabOrder(textFIeld_name1_11, spinBox_start1_11);
        QWidget::setTabOrder(spinBox_start1_11, spinBox_duration1_11);
        QWidget::setTabOrder(spinBox_duration1_11, checkBox_212);
        QWidget::setTabOrder(checkBox_212, textFIeld_name2_22);
        QWidget::setTabOrder(textFIeld_name2_22, spinBox_start2_22);
        QWidget::setTabOrder(spinBox_start2_22, spinBox_duration2_22);
        QWidget::setTabOrder(spinBox_duration2_22, checkBox_215);
        QWidget::setTabOrder(checkBox_215, textFIeld_name3_21);
        QWidget::setTabOrder(textFIeld_name3_21, spinBox_start3_22);
        QWidget::setTabOrder(spinBox_start3_22, spinBox_duration3_22);
        QWidget::setTabOrder(spinBox_duration3_22, checkBox_201);
        QWidget::setTabOrder(checkBox_201, textFIeld_name4_21);
        QWidget::setTabOrder(textFIeld_name4_21, spinBox_start4_21);
        QWidget::setTabOrder(spinBox_start4_21, spinBox_duration4_21);
        QWidget::setTabOrder(spinBox_duration4_21, checkBox_204);
        QWidget::setTabOrder(checkBox_204, textFIeld_name5_132);
        QWidget::setTabOrder(textFIeld_name5_132, spinBox_start5_153);
        QWidget::setTabOrder(spinBox_start5_153, spinBox_duration5_137);
        QWidget::setTabOrder(spinBox_duration5_137, checkBox_202);
        QWidget::setTabOrder(checkBox_202, textFIeld_name5_134);
        QWidget::setTabOrder(textFIeld_name5_134, spinBox_start5_162);
        QWidget::setTabOrder(spinBox_start5_162, spinBox_duration5_135);
        QWidget::setTabOrder(spinBox_duration5_135, checkBox_207);
        QWidget::setTabOrder(checkBox_207, textFIeld_name5_135);
        QWidget::setTabOrder(textFIeld_name5_135, spinBox_start5_156);
        QWidget::setTabOrder(spinBox_start5_156, spinBox_duration5_131);
        QWidget::setTabOrder(spinBox_duration5_131, checkBox_208);
        QWidget::setTabOrder(checkBox_208, textFIeld_name2_21);
        QWidget::setTabOrder(textFIeld_name2_21, spinBox_start2_21);
        QWidget::setTabOrder(spinBox_start2_21, spinBox_duration2_21);
        QWidget::setTabOrder(spinBox_duration2_21, checkBox_203);
        QWidget::setTabOrder(checkBox_203, textFIeld_name3_22);
        QWidget::setTabOrder(textFIeld_name3_22, spinBox_start3_21);
        QWidget::setTabOrder(spinBox_start3_21, spinBox_duration3_21);
        QWidget::setTabOrder(spinBox_duration3_21, checkBox_214);
        QWidget::setTabOrder(checkBox_214, textFIeld_name4_22);
        QWidget::setTabOrder(textFIeld_name4_22, spinBox_start4_22);
        QWidget::setTabOrder(spinBox_start4_22, spinBox_duration4_22);
        QWidget::setTabOrder(spinBox_duration4_22, checkBox_216);
        QWidget::setTabOrder(checkBox_216, textFIeld_name5_133);
        QWidget::setTabOrder(textFIeld_name5_133, spinBox_start5_161);
        QWidget::setTabOrder(spinBox_start5_161, spinBox_duration5_132);
        QWidget::setTabOrder(spinBox_duration5_132, scrollArea_2);
        QWidget::setTabOrder(scrollArea_2, textFIeld_name5_53);
        QWidget::setTabOrder(textFIeld_name5_53, spinBox_start2_9);
        QWidget::setTabOrder(spinBox_start2_9, checkBox_81);
        QWidget::setTabOrder(checkBox_81, checkBox_82);
        QWidget::setTabOrder(checkBox_82, checkBox_83);
        QWidget::setTabOrder(checkBox_83, spinBox_start1_5);
        QWidget::setTabOrder(spinBox_start1_5, checkBox_84);
        QWidget::setTabOrder(checkBox_84, textFIeld_name1_5);
        QWidget::setTabOrder(textFIeld_name1_5, spinBox_duration2_9);
        QWidget::setTabOrder(spinBox_duration2_9, spinBox_duration4_9);
        QWidget::setTabOrder(spinBox_duration4_9, textFIeld_name4_9);
        QWidget::setTabOrder(textFIeld_name4_9, spinBox_duration5_53);
        QWidget::setTabOrder(spinBox_duration5_53, checkBox_85);
        QWidget::setTabOrder(checkBox_85, textFIeld_name2_9);
        QWidget::setTabOrder(textFIeld_name2_9, spinBox_duration5_54);
        QWidget::setTabOrder(spinBox_duration5_54, textFIeld_name5_54);
        QWidget::setTabOrder(textFIeld_name5_54, spinBox_start5_61);
        QWidget::setTabOrder(spinBox_start5_61, spinBox_start3_9);
        QWidget::setTabOrder(spinBox_start3_9, spinBox_duration4_10);
        QWidget::setTabOrder(spinBox_duration4_10, checkBox_86);
        QWidget::setTabOrder(checkBox_86, spinBox_start5_62);
        QWidget::setTabOrder(spinBox_start5_62, spinBox_duration5_55);
        QWidget::setTabOrder(spinBox_duration5_55, textFIeld_name5_55);
        QWidget::setTabOrder(textFIeld_name5_55, spinBox_start5_63);
        QWidget::setTabOrder(spinBox_start5_63, textFIeld_name4_10);
        QWidget::setTabOrder(textFIeld_name4_10, spinBox_start3_10);
        QWidget::setTabOrder(spinBox_start3_10, spinBox_start5_64);
        QWidget::setTabOrder(spinBox_start5_64, spinBox_duration5_56);
        QWidget::setTabOrder(spinBox_duration5_56, textFIeld_name5_56);
        QWidget::setTabOrder(textFIeld_name5_56, checkBox_87);
        QWidget::setTabOrder(checkBox_87, spinBox_start5_65);
        QWidget::setTabOrder(spinBox_start5_65, textFIeld_name2_10);
        QWidget::setTabOrder(textFIeld_name2_10, textFIeld_name5_57);
        QWidget::setTabOrder(textFIeld_name5_57, checkBox_88);
        QWidget::setTabOrder(checkBox_88, textFIeld_name3_9);
        QWidget::setTabOrder(textFIeld_name3_9, spinBox_duration5_57);
        QWidget::setTabOrder(spinBox_duration5_57, textFIeld_name3_10);
        QWidget::setTabOrder(textFIeld_name3_10, spinBox_duration5_58);
        QWidget::setTabOrder(spinBox_duration5_58, checkBox_89);
        QWidget::setTabOrder(checkBox_89, textFIeld_name5_58);
        QWidget::setTabOrder(textFIeld_name5_58, textFIeld_name5_59);
        QWidget::setTabOrder(textFIeld_name5_59, spinBox_duration5_59);
        QWidget::setTabOrder(spinBox_duration5_59, textFIeld_name5_60);
        QWidget::setTabOrder(textFIeld_name5_60, spinBox_start2_10);
        QWidget::setTabOrder(spinBox_start2_10, spinBox_duration5_60);
        QWidget::setTabOrder(spinBox_duration5_60, spinBox_duration5_61);
        QWidget::setTabOrder(spinBox_duration5_61, spinBox_duration5_62);
        QWidget::setTabOrder(spinBox_duration5_62, spinBox_start5_66);
        QWidget::setTabOrder(spinBox_start5_66, textFIeld_name5_61);
        QWidget::setTabOrder(textFIeld_name5_61, spinBox_start5_67);
        QWidget::setTabOrder(spinBox_start5_67, checkBox_90);
        QWidget::setTabOrder(checkBox_90, spinBox_duration3_9);
        QWidget::setTabOrder(spinBox_duration3_9, checkBox_91);
        QWidget::setTabOrder(checkBox_91, spinBox_start4_9);
        QWidget::setTabOrder(spinBox_start4_9, textFIeld_name5_62);
        QWidget::setTabOrder(textFIeld_name5_62, spinBox_start5_68);
        QWidget::setTabOrder(spinBox_start5_68, spinBox_duration5_63);
        QWidget::setTabOrder(spinBox_duration5_63, spinBox_duration1_5);
        QWidget::setTabOrder(spinBox_duration1_5, spinBox_duration5_64);
        QWidget::setTabOrder(spinBox_duration5_64, spinBox_start5_69);
        QWidget::setTabOrder(spinBox_start5_69, spinBox_start4_10);
        QWidget::setTabOrder(spinBox_start4_10, spinBox_duration2_10);
        QWidget::setTabOrder(spinBox_duration2_10, checkBox_92);
        QWidget::setTabOrder(checkBox_92, spinBox_duration3_10);
        QWidget::setTabOrder(spinBox_duration3_10, checkBox_93);
        QWidget::setTabOrder(checkBox_93, checkBox_94);
        QWidget::setTabOrder(checkBox_94, checkBox_95);
        QWidget::setTabOrder(checkBox_95, textFIeld_name5_63);
        QWidget::setTabOrder(textFIeld_name5_63, checkBox_96);
        QWidget::setTabOrder(checkBox_96, spinBox_start5_70);
        QWidget::setTabOrder(spinBox_start5_70, textFIeld_name5_64);
        QWidget::setTabOrder(textFIeld_name5_64, spinBox_duration5_65);
        QWidget::setTabOrder(spinBox_duration5_65, checkBox_97);
        QWidget::setTabOrder(checkBox_97, checkBox_98);
        QWidget::setTabOrder(checkBox_98, checkBox_99);
        QWidget::setTabOrder(checkBox_99, spinBox_start5_71);
        QWidget::setTabOrder(spinBox_start5_71, checkBox_100);
        QWidget::setTabOrder(checkBox_100, spinBox_start5_72);
        QWidget::setTabOrder(spinBox_start5_72, spinBox_start5_73);
        QWidget::setTabOrder(spinBox_start5_73, textFIeld_name5_65);
        QWidget::setTabOrder(textFIeld_name5_65, pushButton_19);
        QWidget::setTabOrder(pushButton_19, spinBox_start5_74);
        QWidget::setTabOrder(spinBox_start5_74, pushButton_20);
        QWidget::setTabOrder(pushButton_20, pushButton_21);
        QWidget::setTabOrder(pushButton_21, pushButton_22);
        QWidget::setTabOrder(pushButton_22, spinBox_start5_75);
        QWidget::setTabOrder(spinBox_start5_75, pushButton_31);
        QWidget::setTabOrder(pushButton_31, spinBox_start5_106);
        QWidget::setTabOrder(spinBox_start5_106, pushButton_32);
        QWidget::setTabOrder(pushButton_32, pushButton_33);
        QWidget::setTabOrder(pushButton_33, pushButton_34);
        QWidget::setTabOrder(pushButton_34, spinBox_start5_107);
        QWidget::setTabOrder(spinBox_start5_107, scrollArea_4);
        QWidget::setTabOrder(scrollArea_4, textFIeld_name5_92);
        QWidget::setTabOrder(textFIeld_name5_92, spinBox_start2_15);
        QWidget::setTabOrder(spinBox_start2_15, checkBox_141);
        QWidget::setTabOrder(checkBox_141, checkBox_142);
        QWidget::setTabOrder(checkBox_142, checkBox_143);
        QWidget::setTabOrder(checkBox_143, spinBox_start1_8);
        QWidget::setTabOrder(spinBox_start1_8, checkBox_144);
        QWidget::setTabOrder(checkBox_144, textFIeld_name1_8);
        QWidget::setTabOrder(textFIeld_name1_8, spinBox_duration2_15);
        QWidget::setTabOrder(spinBox_duration2_15, spinBox_duration4_15);
        QWidget::setTabOrder(spinBox_duration4_15, textFIeld_name4_15);
        QWidget::setTabOrder(textFIeld_name4_15, spinBox_duration5_92);
        QWidget::setTabOrder(spinBox_duration5_92, checkBox_145);
        QWidget::setTabOrder(checkBox_145, textFIeld_name2_15);
        QWidget::setTabOrder(textFIeld_name2_15, spinBox_duration5_93);
        QWidget::setTabOrder(spinBox_duration5_93, textFIeld_name5_93);
        QWidget::setTabOrder(textFIeld_name5_93, spinBox_start5_108);
        QWidget::setTabOrder(spinBox_start5_108, spinBox_start3_15);
        QWidget::setTabOrder(spinBox_start3_15, spinBox_duration4_16);
        QWidget::setTabOrder(spinBox_duration4_16, checkBox_146);
        QWidget::setTabOrder(checkBox_146, spinBox_start5_109);
        QWidget::setTabOrder(spinBox_start5_109, spinBox_duration5_94);
        QWidget::setTabOrder(spinBox_duration5_94, textFIeld_name5_94);
        QWidget::setTabOrder(textFIeld_name5_94, spinBox_start5_110);
        QWidget::setTabOrder(spinBox_start5_110, textFIeld_name4_16);
        QWidget::setTabOrder(textFIeld_name4_16, spinBox_start3_16);
        QWidget::setTabOrder(spinBox_start3_16, spinBox_start5_111);
        QWidget::setTabOrder(spinBox_start5_111, spinBox_duration5_95);
        QWidget::setTabOrder(spinBox_duration5_95, textFIeld_name5_95);
        QWidget::setTabOrder(textFIeld_name5_95, checkBox_147);
        QWidget::setTabOrder(checkBox_147, spinBox_start5_112);
        QWidget::setTabOrder(spinBox_start5_112, textFIeld_name2_16);
        QWidget::setTabOrder(textFIeld_name2_16, textFIeld_name5_96);
        QWidget::setTabOrder(textFIeld_name5_96, checkBox_148);
        QWidget::setTabOrder(checkBox_148, textFIeld_name3_15);
        QWidget::setTabOrder(textFIeld_name3_15, spinBox_duration5_96);
        QWidget::setTabOrder(spinBox_duration5_96, textFIeld_name3_16);
        QWidget::setTabOrder(textFIeld_name3_16, spinBox_duration5_97);
        QWidget::setTabOrder(spinBox_duration5_97, checkBox_149);
        QWidget::setTabOrder(checkBox_149, textFIeld_name5_97);
        QWidget::setTabOrder(textFIeld_name5_97, textFIeld_name5_98);
        QWidget::setTabOrder(textFIeld_name5_98, spinBox_duration5_98);
        QWidget::setTabOrder(spinBox_duration5_98, textFIeld_name5_99);
        QWidget::setTabOrder(textFIeld_name5_99, spinBox_start2_16);
        QWidget::setTabOrder(spinBox_start2_16, spinBox_duration5_99);
        QWidget::setTabOrder(spinBox_duration5_99, spinBox_duration5_100);
        QWidget::setTabOrder(spinBox_duration5_100, spinBox_duration5_101);
        QWidget::setTabOrder(spinBox_duration5_101, spinBox_start5_113);
        QWidget::setTabOrder(spinBox_start5_113, textFIeld_name5_100);
        QWidget::setTabOrder(textFIeld_name5_100, spinBox_start5_114);
        QWidget::setTabOrder(spinBox_start5_114, checkBox_150);
        QWidget::setTabOrder(checkBox_150, spinBox_duration3_15);
        QWidget::setTabOrder(spinBox_duration3_15, checkBox_151);
        QWidget::setTabOrder(checkBox_151, spinBox_start4_15);
        QWidget::setTabOrder(spinBox_start4_15, textFIeld_name5_101);
        QWidget::setTabOrder(textFIeld_name5_101, spinBox_start5_115);
        QWidget::setTabOrder(spinBox_start5_115, spinBox_duration5_102);
        QWidget::setTabOrder(spinBox_duration5_102, spinBox_duration1_8);
        QWidget::setTabOrder(spinBox_duration1_8, spinBox_duration5_103);
        QWidget::setTabOrder(spinBox_duration5_103, spinBox_start5_116);
        QWidget::setTabOrder(spinBox_start5_116, spinBox_start4_16);
        QWidget::setTabOrder(spinBox_start4_16, spinBox_duration2_16);
        QWidget::setTabOrder(spinBox_duration2_16, checkBox_152);
        QWidget::setTabOrder(checkBox_152, spinBox_duration3_16);
        QWidget::setTabOrder(spinBox_duration3_16, checkBox_153);
        QWidget::setTabOrder(checkBox_153, checkBox_154);
        QWidget::setTabOrder(checkBox_154, checkBox_155);
        QWidget::setTabOrder(checkBox_155, textFIeld_name5_102);
        QWidget::setTabOrder(textFIeld_name5_102, checkBox_156);
        QWidget::setTabOrder(checkBox_156, spinBox_start5_117);
        QWidget::setTabOrder(spinBox_start5_117, textFIeld_name5_103);
        QWidget::setTabOrder(textFIeld_name5_103, spinBox_duration5_104);
        QWidget::setTabOrder(spinBox_duration5_104, checkBox_157);
        QWidget::setTabOrder(checkBox_157, checkBox_158);
        QWidget::setTabOrder(checkBox_158, checkBox_159);
        QWidget::setTabOrder(checkBox_159, spinBox_start5_118);
        QWidget::setTabOrder(spinBox_start5_118, checkBox_160);
        QWidget::setTabOrder(checkBox_160, spinBox_start5_119);
        QWidget::setTabOrder(spinBox_start5_119, spinBox_start5_120);
        QWidget::setTabOrder(spinBox_start5_120, textFIeld_name5_104);
        QWidget::setTabOrder(textFIeld_name5_104, spinBox_start5_121);
        QWidget::setTabOrder(spinBox_start5_121, spinBox_start5_122);
        QWidget::setTabOrder(spinBox_start5_122, pushButton_35);
        QWidget::setTabOrder(pushButton_35, pushButton_36);
        QWidget::setTabOrder(pushButton_36, pushButton_37);
        QWidget::setTabOrder(pushButton_37, pushButton_38);
        QWidget::setTabOrder(pushButton_38, scrollArea_5);
        QWidget::setTabOrder(scrollArea_5, textFIeld_name5_105);
        QWidget::setTabOrder(textFIeld_name5_105, spinBox_start2_17);
        QWidget::setTabOrder(spinBox_start2_17, checkBox_161);
        QWidget::setTabOrder(checkBox_161, checkBox_162);
        QWidget::setTabOrder(checkBox_162, checkBox_163);
        QWidget::setTabOrder(checkBox_163, spinBox_start1_9);
        QWidget::setTabOrder(spinBox_start1_9, checkBox_164);
        QWidget::setTabOrder(checkBox_164, textFIeld_name1_9);
        QWidget::setTabOrder(textFIeld_name1_9, spinBox_duration2_17);
        QWidget::setTabOrder(spinBox_duration2_17, spinBox_duration4_17);
        QWidget::setTabOrder(spinBox_duration4_17, textFIeld_name4_17);
        QWidget::setTabOrder(textFIeld_name4_17, spinBox_duration5_105);
        QWidget::setTabOrder(spinBox_duration5_105, checkBox_165);
        QWidget::setTabOrder(checkBox_165, textFIeld_name2_17);
        QWidget::setTabOrder(textFIeld_name2_17, spinBox_duration5_106);
        QWidget::setTabOrder(spinBox_duration5_106, textFIeld_name5_106);
        QWidget::setTabOrder(textFIeld_name5_106, spinBox_start5_123);
        QWidget::setTabOrder(spinBox_start5_123, spinBox_start3_17);
        QWidget::setTabOrder(spinBox_start3_17, spinBox_duration4_18);
        QWidget::setTabOrder(spinBox_duration4_18, checkBox_166);
        QWidget::setTabOrder(checkBox_166, spinBox_start5_124);
        QWidget::setTabOrder(spinBox_start5_124, spinBox_duration5_107);
        QWidget::setTabOrder(spinBox_duration5_107, textFIeld_name5_107);
        QWidget::setTabOrder(textFIeld_name5_107, spinBox_start5_125);
        QWidget::setTabOrder(spinBox_start5_125, textFIeld_name4_18);
        QWidget::setTabOrder(textFIeld_name4_18, spinBox_start3_18);
        QWidget::setTabOrder(spinBox_start3_18, spinBox_start5_126);
        QWidget::setTabOrder(spinBox_start5_126, spinBox_duration5_108);
        QWidget::setTabOrder(spinBox_duration5_108, textFIeld_name5_108);
        QWidget::setTabOrder(textFIeld_name5_108, checkBox_167);
        QWidget::setTabOrder(checkBox_167, spinBox_start5_127);
        QWidget::setTabOrder(spinBox_start5_127, textFIeld_name2_18);
        QWidget::setTabOrder(textFIeld_name2_18, textFIeld_name5_109);
        QWidget::setTabOrder(textFIeld_name5_109, checkBox_168);
        QWidget::setTabOrder(checkBox_168, textFIeld_name3_17);
        QWidget::setTabOrder(textFIeld_name3_17, spinBox_duration5_109);
        QWidget::setTabOrder(spinBox_duration5_109, textFIeld_name3_18);
        QWidget::setTabOrder(textFIeld_name3_18, spinBox_duration5_110);
        QWidget::setTabOrder(spinBox_duration5_110, checkBox_169);
        QWidget::setTabOrder(checkBox_169, textFIeld_name5_110);
        QWidget::setTabOrder(textFIeld_name5_110, textFIeld_name5_111);
        QWidget::setTabOrder(textFIeld_name5_111, spinBox_duration5_111);
        QWidget::setTabOrder(spinBox_duration5_111, textFIeld_name5_112);
        QWidget::setTabOrder(textFIeld_name5_112, spinBox_start2_18);
        QWidget::setTabOrder(spinBox_start2_18, spinBox_duration5_112);
        QWidget::setTabOrder(spinBox_duration5_112, spinBox_duration5_113);
        QWidget::setTabOrder(spinBox_duration5_113, spinBox_duration5_114);
        QWidget::setTabOrder(spinBox_duration5_114, spinBox_start5_128);
        QWidget::setTabOrder(spinBox_start5_128, textFIeld_name5_113);
        QWidget::setTabOrder(textFIeld_name5_113, spinBox_start5_129);
        QWidget::setTabOrder(spinBox_start5_129, checkBox_170);
        QWidget::setTabOrder(checkBox_170, spinBox_duration3_17);
        QWidget::setTabOrder(spinBox_duration3_17, checkBox_171);
        QWidget::setTabOrder(checkBox_171, spinBox_start4_17);
        QWidget::setTabOrder(spinBox_start4_17, textFIeld_name5_114);
        QWidget::setTabOrder(textFIeld_name5_114, spinBox_start5_130);
        QWidget::setTabOrder(spinBox_start5_130, spinBox_duration5_115);
        QWidget::setTabOrder(spinBox_duration5_115, spinBox_duration1_9);
        QWidget::setTabOrder(spinBox_duration1_9, spinBox_duration5_116);
        QWidget::setTabOrder(spinBox_duration5_116, spinBox_start5_131);
        QWidget::setTabOrder(spinBox_start5_131, spinBox_start4_18);
        QWidget::setTabOrder(spinBox_start4_18, spinBox_duration2_18);
        QWidget::setTabOrder(spinBox_duration2_18, checkBox_172);
        QWidget::setTabOrder(checkBox_172, spinBox_duration3_18);
        QWidget::setTabOrder(spinBox_duration3_18, checkBox_173);
        QWidget::setTabOrder(checkBox_173, checkBox_174);
        QWidget::setTabOrder(checkBox_174, checkBox_175);
        QWidget::setTabOrder(checkBox_175, textFIeld_name5_115);
        QWidget::setTabOrder(textFIeld_name5_115, checkBox_176);
        QWidget::setTabOrder(checkBox_176, spinBox_start5_132);
        QWidget::setTabOrder(spinBox_start5_132, textFIeld_name5_116);
        QWidget::setTabOrder(textFIeld_name5_116, spinBox_duration5_117);
        QWidget::setTabOrder(spinBox_duration5_117, checkBox_177);
        QWidget::setTabOrder(checkBox_177, checkBox_178);
        QWidget::setTabOrder(checkBox_178, checkBox_179);
        QWidget::setTabOrder(checkBox_179, spinBox_start5_133);
        QWidget::setTabOrder(spinBox_start5_133, checkBox_180);
        QWidget::setTabOrder(checkBox_180, spinBox_start5_134);
        QWidget::setTabOrder(spinBox_start5_134, spinBox_start5_135);
        QWidget::setTabOrder(spinBox_start5_135, textFIeld_name5_117);
        QWidget::setTabOrder(textFIeld_name5_117, spinBox_start5_136);
        QWidget::setTabOrder(spinBox_start5_136, spinBox_start5_137);
        QWidget::setTabOrder(spinBox_start5_137, pushButton_39);
        QWidget::setTabOrder(pushButton_39, pushButton_40);
        QWidget::setTabOrder(pushButton_40, pushButton_41);
        QWidget::setTabOrder(pushButton_41, pushButton_42);
        QWidget::setTabOrder(pushButton_42, scrollArea_6);
        QWidget::setTabOrder(scrollArea_6, textFIeld_name5_118);
        QWidget::setTabOrder(textFIeld_name5_118, spinBox_start2_19);
        QWidget::setTabOrder(spinBox_start2_19, checkBox_181);
        QWidget::setTabOrder(checkBox_181, checkBox_182);
        QWidget::setTabOrder(checkBox_182, checkBox_183);
        QWidget::setTabOrder(checkBox_183, spinBox_start1_10);
        QWidget::setTabOrder(spinBox_start1_10, checkBox_184);
        QWidget::setTabOrder(checkBox_184, textFIeld_name1_10);
        QWidget::setTabOrder(textFIeld_name1_10, spinBox_duration2_19);
        QWidget::setTabOrder(spinBox_duration2_19, spinBox_duration4_19);
        QWidget::setTabOrder(spinBox_duration4_19, textFIeld_name4_19);
        QWidget::setTabOrder(textFIeld_name4_19, spinBox_duration5_118);
        QWidget::setTabOrder(spinBox_duration5_118, checkBox_185);
        QWidget::setTabOrder(checkBox_185, textFIeld_name2_19);
        QWidget::setTabOrder(textFIeld_name2_19, spinBox_duration5_119);
        QWidget::setTabOrder(spinBox_duration5_119, textFIeld_name5_119);
        QWidget::setTabOrder(textFIeld_name5_119, spinBox_start5_138);
        QWidget::setTabOrder(spinBox_start5_138, spinBox_start3_19);
        QWidget::setTabOrder(spinBox_start3_19, spinBox_duration4_20);
        QWidget::setTabOrder(spinBox_duration4_20, checkBox_186);
        QWidget::setTabOrder(checkBox_186, spinBox_start5_139);
        QWidget::setTabOrder(spinBox_start5_139, spinBox_duration5_120);
        QWidget::setTabOrder(spinBox_duration5_120, textFIeld_name5_120);
        QWidget::setTabOrder(textFIeld_name5_120, spinBox_start5_140);
        QWidget::setTabOrder(spinBox_start5_140, textFIeld_name4_20);
        QWidget::setTabOrder(textFIeld_name4_20, spinBox_start3_20);
        QWidget::setTabOrder(spinBox_start3_20, spinBox_start5_141);
        QWidget::setTabOrder(spinBox_start5_141, spinBox_duration5_121);
        QWidget::setTabOrder(spinBox_duration5_121, textFIeld_name5_121);
        QWidget::setTabOrder(textFIeld_name5_121, checkBox_187);
        QWidget::setTabOrder(checkBox_187, spinBox_start5_142);
        QWidget::setTabOrder(spinBox_start5_142, textFIeld_name2_20);
        QWidget::setTabOrder(textFIeld_name2_20, textFIeld_name5_122);
        QWidget::setTabOrder(textFIeld_name5_122, checkBox_188);
        QWidget::setTabOrder(checkBox_188, textFIeld_name3_19);
        QWidget::setTabOrder(textFIeld_name3_19, spinBox_duration5_122);
        QWidget::setTabOrder(spinBox_duration5_122, textFIeld_name3_20);
        QWidget::setTabOrder(textFIeld_name3_20, spinBox_duration5_123);
        QWidget::setTabOrder(spinBox_duration5_123, checkBox_189);
        QWidget::setTabOrder(checkBox_189, textFIeld_name5_123);
        QWidget::setTabOrder(textFIeld_name5_123, textFIeld_name5_124);
        QWidget::setTabOrder(textFIeld_name5_124, spinBox_duration5_124);
        QWidget::setTabOrder(spinBox_duration5_124, textFIeld_name5_125);
        QWidget::setTabOrder(textFIeld_name5_125, spinBox_start2_20);
        QWidget::setTabOrder(spinBox_start2_20, spinBox_duration5_125);
        QWidget::setTabOrder(spinBox_duration5_125, spinBox_duration5_126);
        QWidget::setTabOrder(spinBox_duration5_126, spinBox_duration5_127);
        QWidget::setTabOrder(spinBox_duration5_127, spinBox_start5_143);
        QWidget::setTabOrder(spinBox_start5_143, textFIeld_name5_126);
        QWidget::setTabOrder(textFIeld_name5_126, spinBox_start5_144);
        QWidget::setTabOrder(spinBox_start5_144, checkBox_190);
        QWidget::setTabOrder(checkBox_190, spinBox_duration3_19);
        QWidget::setTabOrder(spinBox_duration3_19, checkBox_191);
        QWidget::setTabOrder(checkBox_191, spinBox_start4_19);
        QWidget::setTabOrder(spinBox_start4_19, textFIeld_name5_127);
        QWidget::setTabOrder(textFIeld_name5_127, spinBox_start5_145);
        QWidget::setTabOrder(spinBox_start5_145, spinBox_duration5_128);
        QWidget::setTabOrder(spinBox_duration5_128, spinBox_duration1_10);
        QWidget::setTabOrder(spinBox_duration1_10, spinBox_duration5_129);
        QWidget::setTabOrder(spinBox_duration5_129, spinBox_start5_146);
        QWidget::setTabOrder(spinBox_start5_146, spinBox_start4_20);
        QWidget::setTabOrder(spinBox_start4_20, spinBox_duration2_20);
        QWidget::setTabOrder(spinBox_duration2_20, checkBox_192);
        QWidget::setTabOrder(checkBox_192, spinBox_duration3_20);
        QWidget::setTabOrder(spinBox_duration3_20, checkBox_193);
        QWidget::setTabOrder(checkBox_193, checkBox_194);
        QWidget::setTabOrder(checkBox_194, checkBox_195);
        QWidget::setTabOrder(checkBox_195, textFIeld_name5_128);
        QWidget::setTabOrder(textFIeld_name5_128, checkBox_196);
        QWidget::setTabOrder(checkBox_196, spinBox_start5_147);
        QWidget::setTabOrder(spinBox_start5_147, textFIeld_name5_129);
        QWidget::setTabOrder(textFIeld_name5_129, spinBox_duration5_130);
        QWidget::setTabOrder(spinBox_duration5_130, checkBox_197);
        QWidget::setTabOrder(checkBox_197, checkBox_198);
        QWidget::setTabOrder(checkBox_198, checkBox_199);
        QWidget::setTabOrder(checkBox_199, spinBox_start5_148);
        QWidget::setTabOrder(spinBox_start5_148, checkBox_200);
        QWidget::setTabOrder(checkBox_200, spinBox_start5_149);
        QWidget::setTabOrder(spinBox_start5_149, spinBox_start5_150);
        QWidget::setTabOrder(spinBox_start5_150, textFIeld_name5_130);
        QWidget::setTabOrder(textFIeld_name5_130, spinBox_start5_151);
        QWidget::setTabOrder(spinBox_start5_151, spinBox_start5_152);
        QWidget::setTabOrder(spinBox_start5_152, pushButton_43);
        QWidget::setTabOrder(pushButton_43, pushButton_44);
        QWidget::setTabOrder(pushButton_44, pushButton_45);
        QWidget::setTabOrder(pushButton_45, pushButton_46);
        QWidget::setTabOrder(pushButton_46, textFIeld_name5_131);
        QWidget::setTabOrder(textFIeld_name5_131, checkBox_205);
        QWidget::setTabOrder(checkBox_205, checkBox_206);
        QWidget::setTabOrder(checkBox_206, spinBox_start5_154);
        QWidget::setTabOrder(spinBox_start5_154, spinBox_duration5_133);
        QWidget::setTabOrder(spinBox_duration5_133, spinBox_start5_155);
        QWidget::setTabOrder(spinBox_start5_155, spinBox_duration5_134);
        QWidget::setTabOrder(spinBox_duration5_134, spinBox_start5_157);
        QWidget::setTabOrder(spinBox_start5_157, spinBox_duration5_136);
        QWidget::setTabOrder(spinBox_duration5_136, checkBox_209);
        QWidget::setTabOrder(checkBox_209, textFIeld_name5_136);
        QWidget::setTabOrder(textFIeld_name5_136, textFIeld_name5_137);
        QWidget::setTabOrder(textFIeld_name5_137, textFIeld_name5_138);
        QWidget::setTabOrder(textFIeld_name5_138, spinBox_duration5_138);
        QWidget::setTabOrder(spinBox_duration5_138, spinBox_duration5_139);
        QWidget::setTabOrder(spinBox_duration5_139, spinBox_duration5_140);
        QWidget::setTabOrder(spinBox_duration5_140, spinBox_start5_158);
        QWidget::setTabOrder(spinBox_start5_158, textFIeld_name5_139);
        QWidget::setTabOrder(textFIeld_name5_139, spinBox_start5_159);
        QWidget::setTabOrder(spinBox_start5_159, checkBox_210);
        QWidget::setTabOrder(checkBox_210, textFIeld_name5_140);
        QWidget::setTabOrder(textFIeld_name5_140, spinBox_start5_160);
        QWidget::setTabOrder(spinBox_start5_160, spinBox_duration5_141);
        QWidget::setTabOrder(spinBox_duration5_141, spinBox_duration5_142);
        QWidget::setTabOrder(spinBox_duration5_142, checkBox_213);
        QWidget::setTabOrder(checkBox_213, textFIeld_name5_141);
        QWidget::setTabOrder(textFIeld_name5_141, textFIeld_name5_142);
        QWidget::setTabOrder(textFIeld_name5_142, spinBox_duration5_143);
        QWidget::setTabOrder(spinBox_duration5_143, checkBox_217);
        QWidget::setTabOrder(checkBox_217, checkBox_218);
        QWidget::setTabOrder(checkBox_218, checkBox_219);
        QWidget::setTabOrder(checkBox_219, spinBox_start5_163);
        QWidget::setTabOrder(spinBox_start5_163, checkBox_220);
        QWidget::setTabOrder(checkBox_220, spinBox_start5_164);
        QWidget::setTabOrder(spinBox_start5_164, spinBox_start5_165);
        QWidget::setTabOrder(spinBox_start5_165, textFIeld_name5_143);

        menuBar->addAction(menuOpen->menuAction());
        menuOpen->addAction(actionSave);
        menuOpen->addAction(actionOpen);

        retranslateUi(MainWindowApp);

        tabWidget->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(MainWindowApp);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindowApp)
    {
        MainWindowApp->setWindowTitle(QApplication::translate("MainWindowApp", "MainWindowApp", nullptr));
        actionSave->setText(QApplication::translate("MainWindowApp", "Save", nullptr));
        actionOpen->setText(QApplication::translate("MainWindowApp", "Open", nullptr));
        label_ID_13->setText(QApplication::translate("MainWindowApp", "Node", nullptr));
        label_start_13->setText(QApplication::translate("MainWindowApp", "Start", nullptr));
        label_msStart_14->setText(QApplication::translate("MainWindowApp", "(ms)", nullptr));
        label_duration_13->setText(QApplication::translate("MainWindowApp", "Duration", nullptr));
        label_msDuration_24->setText(QApplication::translate("MainWindowApp", "(ms)", nullptr));
        label_duration_14->setText(QApplication::translate("MainWindowApp", "Total Duration", nullptr));
        textFIeld_name1_22->setText(QApplication::translate("MainWindowApp", "3200", nullptr));
        label_msDuration_26->setText(QApplication::translate("MainWindowApp", "(ms)", nullptr));
        label_duration_15->setText(QApplication::translate("MainWindowApp", "Total Progress", nullptr));
        label_45->setText(QApplication::translate("MainWindowApp", "(1/1)", nullptr));
        label_ID4_167->setText(QApplication::translate("MainWindowApp", "4", nullptr));
        checkBox_235->setText(QString());
        textFIeld_name1_12->setText(QApplication::translate("MainWindowApp", "200", nullptr));
        circle_inactive3_79->setText(QString());
        label_ID3_23->setText(QApplication::translate("MainWindowApp", "3", nullptr));
        circle_inactive3_83->setText(QString());
        label_ID2_23->setText(QApplication::translate("MainWindowApp", "2", nullptr));
        checkBox_224->setText(QString());
        textFIeld_name1_13->setText(QApplication::translate("MainWindowApp", "3200", nullptr));
        label_ID1_12->setText(QApplication::translate("MainWindowApp", "1", nullptr));
        checkBox_231->setText(QString());
        checkBox_221->setText(QString());
        circle_inactive3_82->setText(QString());
        checkBox_232->setText(QString());
        textFIeld_name1_17->setText(QApplication::translate("MainWindowApp", "0", nullptr));
        textFIeld_name1_21->setText(QApplication::translate("MainWindowApp", "0", nullptr));
        circle_inactive3_81->setText(QString());
        label_ID4_172->setText(QApplication::translate("MainWindowApp", "5", nullptr));
        textFIeld_name1_18->setText(QApplication::translate("MainWindowApp", "0", nullptr));
        circle_inactive3_84->setText(QString());
        textFIeld_name1_19->setText(QApplication::translate("MainWindowApp", "0", nullptr));
        textFIeld_name1_16->setText(QApplication::translate("MainWindowApp", "0", nullptr));
        textFIeld_name1_14->setText(QApplication::translate("MainWindowApp", "0", nullptr));
        textFIeld_name1_20->setText(QApplication::translate("MainWindowApp", "0", nullptr));
        textFIeld_name1_15->setText(QApplication::translate("MainWindowApp", "0", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MainWindowApp", "Overview", nullptr));
        label_ID_12->setText(QApplication::translate("MainWindowApp", "ID", nullptr));
        label_name_13->setText(QApplication::translate("MainWindowApp", "Name    ", nullptr));
        label_start_12->setText(QApplication::translate("MainWindowApp", "  Start", nullptr));
        label_msStart_13->setText(QApplication::translate("MainWindowApp", "(ms)", nullptr));
        label_duration_12->setText(QApplication::translate("MainWindowApp", "Duration", nullptr));
        label_msDuration_22->setText(QApplication::translate("MainWindowApp", "(ms)", nullptr));
        label_41->setText(QApplication::translate("MainWindowApp", "(3/12)", nullptr));
        pushButton_43->setText(QApplication::translate("MainWindowApp", "Stop/Reset", nullptr));
        pushButton_44->setText(QApplication::translate("MainWindowApp", "WriteConfig", nullptr));
        label_42->setText(QApplication::translate("MainWindowApp", "Minimum: 30000 ms", nullptr));
        label_43->setText(QApplication::translate("MainWindowApp", "Cycle Duration", nullptr));
        pushButton_45->setText(QApplication::translate("MainWindowApp", "Start", nullptr));
        label_msDuration_23->setText(QApplication::translate("MainWindowApp", "(ms)", nullptr));
        pushButton_46->setText(QApplication::translate("MainWindowApp", "ReadConfig", nullptr));
        label_44->setText(QApplication::translate("MainWindowApp", "Cycle Counter", nullptr));
        textFIeld_name5_131->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_151->setText(QApplication::translate("MainWindowApp", "6", nullptr));
        circle_inactive3_71->setText(QString());
        checkBox_201->setText(QString());
        checkBox_202->setText(QString());
        checkBox_203->setText(QString());
        checkBox_204->setText(QString());
        circle_inactive3_72->setText(QString());
        textFIeld_name1_11->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name4_21->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_205->setText(QString());
        textFIeld_name2_21->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name5_132->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_152->setText(QApplication::translate("MainWindowApp", "4", nullptr));
        label_ID4_153->setText(QApplication::translate("MainWindowApp", "10", nullptr));
        checkBox_206->setText(QString());
        textFIeld_name5_133->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_154->setText(QApplication::translate("MainWindowApp", "15", nullptr));
        textFIeld_name4_22->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name5_134->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID2_21->setText(QApplication::translate("MainWindowApp", "2", nullptr));
        checkBox_207->setText(QString());
        textFIeld_name2_22->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name5_135->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_208->setText(QString());
        label_ID4_155->setText(QApplication::translate("MainWindowApp", "7", nullptr));
        circle_inactive3_73->setText(QString());
        textFIeld_name3_21->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name3_22->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_209->setText(QString());
        textFIeld_name5_136->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name5_137->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_156->setText(QApplication::translate("MainWindowApp", "20", nullptr));
        label_ID4_157->setText(QApplication::translate("MainWindowApp", "5", nullptr));
        textFIeld_name5_138->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_158->setText(QApplication::translate("MainWindowApp", "11", nullptr));
        textFIeld_name5_139->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_210->setText(QString());
        label_ID4_159->setText(QApplication::translate("MainWindowApp", "13", nullptr));
        label_ID4_160->setText(QApplication::translate("MainWindowApp", "17", nullptr));
        label_ID2_22->setText(QApplication::translate("MainWindowApp", "8", nullptr));
        label_ID4_161->setText(QApplication::translate("MainWindowApp", "16", nullptr));
        checkBox_211->setText(QString());
        circle_inactive3_74->setText(QString());
        label_ID4_162->setText(QApplication::translate("MainWindowApp", "14", nullptr));
        label_ID4_163->setText(QApplication::translate("MainWindowApp", "12", nullptr));
        textFIeld_name5_140->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID3_21->setText(QApplication::translate("MainWindowApp", "3", nullptr));
        circle_inactive1_131->setText(QString());
        checkBox_212->setText(QString());
        checkBox_213->setText(QString());
        checkBox_214->setText(QString());
        checkBox_215->setText(QString());
        textFIeld_name5_141->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_216->setText(QString());
        label_ID3_22->setText(QApplication::translate("MainWindowApp", "9", nullptr));
        label_ID1_11->setText(QApplication::translate("MainWindowApp", "1", nullptr));
        textFIeld_name5_142->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_164->setText(QApplication::translate("MainWindowApp", "19", nullptr));
        checkBox_217->setText(QString());
        checkBox_218->setText(QString());
        checkBox_219->setText(QString());
        checkBox_220->setText(QString());
        label_ID4_165->setText(QApplication::translate("MainWindowApp", "18", nullptr));
        circle_inactive3_75->setText(QString());
        circle_inactive1_132->setText(QString());
        textFIeld_name5_143->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        circle_inactive3_76->setText(QString());
        circle_inactive3_77->setText(QString());
        circle_inactive1_133->setText(QString());
        circle_inactive1_134->setText(QString());
        circle_inactive1_135->setText(QString());
        circle_inactive1_136->setText(QString());
        circle_inactive1_137->setText(QString());
        circle_inactive1_138->setText(QString());
        circle_inactive1_139->setText(QString());
        circle_inactive1_140->setText(QString());
        circle_inactive1_141->setText(QString());
        circle_inactive1_142->setText(QString());
        circle_inactive1_143->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(Node_1), QApplication::translate("MainWindowApp", "1_Node", nullptr));
        label_ID_6->setText(QApplication::translate("MainWindowApp", "ID", nullptr));
        label_name_7->setText(QApplication::translate("MainWindowApp", "Name    ", nullptr));
        label_start_6->setText(QApplication::translate("MainWindowApp", "  Start", nullptr));
        label_msStart_7->setText(QApplication::translate("MainWindowApp", "(ms)", nullptr));
        label_duration_6->setText(QApplication::translate("MainWindowApp", "Duration", nullptr));
        label_msDuration_10->setText(QApplication::translate("MainWindowApp", "(ms)", nullptr));
        label_18->setText(QApplication::translate("MainWindowApp", "(0/0)", nullptr));
        pushButton_21->setText(QApplication::translate("MainWindowApp", "Stop/Reset", nullptr));
        pushButton_19->setText(QApplication::translate("MainWindowApp", "WriteConfig", nullptr));
        label_17->setText(QApplication::translate("MainWindowApp", "Minimum: 0 ms", nullptr));
        label_19->setText(QApplication::translate("MainWindowApp", "Cycle Duration", nullptr));
        pushButton_20->setText(QApplication::translate("MainWindowApp", "Start", nullptr));
        label_msDuration_11->setText(QApplication::translate("MainWindowApp", "(ms)", nullptr));
        pushButton_22->setText(QApplication::translate("MainWindowApp", "ReadConfig", nullptr));
        label_20->setText(QApplication::translate("MainWindowApp", "Cycle Counter", nullptr));
        textFIeld_name5_53->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_61->setText(QApplication::translate("MainWindowApp", "6", nullptr));
        circle_inactive3_29->setText(QString());
        checkBox_81->setText(QString());
        checkBox_82->setText(QString());
        checkBox_83->setText(QString());
        checkBox_84->setText(QString());
        circle_inactive3_30->setText(QString());
        textFIeld_name1_5->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name4_9->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_85->setText(QString());
        textFIeld_name2_9->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name5_54->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_62->setText(QApplication::translate("MainWindowApp", "4", nullptr));
        label_ID4_63->setText(QApplication::translate("MainWindowApp", "10", nullptr));
        checkBox_86->setText(QString());
        textFIeld_name5_55->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_64->setText(QApplication::translate("MainWindowApp", "15", nullptr));
        textFIeld_name4_10->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name5_56->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID2_9->setText(QApplication::translate("MainWindowApp", "2", nullptr));
        checkBox_87->setText(QString());
        textFIeld_name2_10->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name5_57->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_88->setText(QString());
        label_ID4_65->setText(QApplication::translate("MainWindowApp", "7", nullptr));
        circle_inactive3_31->setText(QString());
        textFIeld_name3_9->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name3_10->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_89->setText(QString());
        textFIeld_name5_58->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name5_59->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_66->setText(QApplication::translate("MainWindowApp", "20", nullptr));
        label_ID4_67->setText(QApplication::translate("MainWindowApp", "5", nullptr));
        textFIeld_name5_60->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_68->setText(QApplication::translate("MainWindowApp", "11", nullptr));
        textFIeld_name5_61->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_90->setText(QString());
        label_ID4_69->setText(QApplication::translate("MainWindowApp", "13", nullptr));
        label_ID4_70->setText(QApplication::translate("MainWindowApp", "17", nullptr));
        label_ID2_10->setText(QApplication::translate("MainWindowApp", "8", nullptr));
        label_ID4_71->setText(QApplication::translate("MainWindowApp", "16", nullptr));
        checkBox_91->setText(QString());
        circle_inactive3_32->setText(QString());
        label_ID4_72->setText(QApplication::translate("MainWindowApp", "14", nullptr));
        label_ID4_73->setText(QApplication::translate("MainWindowApp", "12", nullptr));
        textFIeld_name5_62->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID3_9->setText(QApplication::translate("MainWindowApp", "3", nullptr));
        circle_inactive1_53->setText(QString());
        checkBox_92->setText(QString());
        checkBox_93->setText(QString());
        checkBox_94->setText(QString());
        checkBox_95->setText(QString());
        textFIeld_name5_63->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_96->setText(QString());
        label_ID3_10->setText(QApplication::translate("MainWindowApp", "9", nullptr));
        label_ID1_5->setText(QApplication::translate("MainWindowApp", "1", nullptr));
        textFIeld_name5_64->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_74->setText(QApplication::translate("MainWindowApp", "19", nullptr));
        checkBox_97->setText(QString());
        checkBox_98->setText(QString());
        checkBox_99->setText(QString());
        checkBox_100->setText(QString());
        label_ID4_75->setText(QApplication::translate("MainWindowApp", "18", nullptr));
        circle_inactive3_33->setText(QString());
        circle_inactive1_54->setText(QString());
        textFIeld_name5_65->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        circle_inactive3_34->setText(QString());
        circle_inactive3_35->setText(QString());
        circle_inactive1_55->setText(QString());
        circle_inactive1_56->setText(QString());
        circle_inactive1_57->setText(QString());
        circle_inactive1_58->setText(QString());
        circle_inactive1_59->setText(QString());
        circle_inactive1_60->setText(QString());
        circle_inactive1_61->setText(QString());
        circle_inactive1_62->setText(QString());
        circle_inactive1_63->setText(QString());
        circle_inactive1_64->setText(QString());
        circle_inactive1_65->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("MainWindowApp", "2_Node", nullptr));
        label_ID_9->setText(QApplication::translate("MainWindowApp", "ID", nullptr));
        label_name_10->setText(QApplication::translate("MainWindowApp", "Name    ", nullptr));
        label_start_9->setText(QApplication::translate("MainWindowApp", "  Start", nullptr));
        label_msStart_10->setText(QApplication::translate("MainWindowApp", "(ms)", nullptr));
        label_duration_9->setText(QApplication::translate("MainWindowApp", "Duration", nullptr));
        label_msDuration_16->setText(QApplication::translate("MainWindowApp", "(ms)", nullptr));
        label_30->setText(QApplication::translate("MainWindowApp", "(0/0)", nullptr));
        label_32->setText(QApplication::translate("MainWindowApp", "Cycle Counter", nullptr));
        label_31->setText(QApplication::translate("MainWindowApp", "Cycle Duration", nullptr));
        label_29->setText(QApplication::translate("MainWindowApp", "Minimum: 0 ms", nullptr));
        pushButton_31->setText(QApplication::translate("MainWindowApp", "WriteConfig", nullptr));
        pushButton_33->setText(QApplication::translate("MainWindowApp", "Stop/Reset", nullptr));
        pushButton_32->setText(QApplication::translate("MainWindowApp", "Start", nullptr));
        pushButton_34->setText(QApplication::translate("MainWindowApp", "ReadConfig", nullptr));
        label_msDuration_17->setText(QApplication::translate("MainWindowApp", "(ms)", nullptr));
        textFIeld_name5_92->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_106->setText(QApplication::translate("MainWindowApp", "6", nullptr));
        circle_inactive3_50->setText(QString());
        checkBox_141->setText(QString());
        checkBox_142->setText(QString());
        checkBox_143->setText(QString());
        checkBox_144->setText(QString());
        circle_inactive3_51->setText(QString());
        textFIeld_name1_8->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name4_15->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_145->setText(QString());
        textFIeld_name2_15->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name5_93->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_107->setText(QApplication::translate("MainWindowApp", "4", nullptr));
        label_ID4_108->setText(QApplication::translate("MainWindowApp", "10", nullptr));
        checkBox_146->setText(QString());
        textFIeld_name5_94->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_109->setText(QApplication::translate("MainWindowApp", "15", nullptr));
        textFIeld_name4_16->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name5_95->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID2_15->setText(QApplication::translate("MainWindowApp", "2", nullptr));
        checkBox_147->setText(QString());
        textFIeld_name2_16->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name5_96->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_148->setText(QString());
        label_ID4_110->setText(QApplication::translate("MainWindowApp", "7", nullptr));
        circle_inactive3_52->setText(QString());
        textFIeld_name3_15->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name3_16->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_149->setText(QString());
        textFIeld_name5_97->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name5_98->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_111->setText(QApplication::translate("MainWindowApp", "20", nullptr));
        label_ID4_112->setText(QApplication::translate("MainWindowApp", "5", nullptr));
        textFIeld_name5_99->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_113->setText(QApplication::translate("MainWindowApp", "11", nullptr));
        textFIeld_name5_100->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_150->setText(QString());
        label_ID4_114->setText(QApplication::translate("MainWindowApp", "13", nullptr));
        label_ID4_115->setText(QApplication::translate("MainWindowApp", "17", nullptr));
        label_ID2_16->setText(QApplication::translate("MainWindowApp", "8", nullptr));
        label_ID4_116->setText(QApplication::translate("MainWindowApp", "16", nullptr));
        checkBox_151->setText(QString());
        circle_inactive3_53->setText(QString());
        label_ID4_117->setText(QApplication::translate("MainWindowApp", "14", nullptr));
        label_ID4_118->setText(QApplication::translate("MainWindowApp", "12", nullptr));
        textFIeld_name5_101->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID3_15->setText(QApplication::translate("MainWindowApp", "3", nullptr));
        circle_inactive1_92->setText(QString());
        checkBox_152->setText(QString());
        checkBox_153->setText(QString());
        checkBox_154->setText(QString());
        checkBox_155->setText(QString());
        textFIeld_name5_102->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_156->setText(QString());
        label_ID3_16->setText(QApplication::translate("MainWindowApp", "9", nullptr));
        label_ID1_8->setText(QApplication::translate("MainWindowApp", "1", nullptr));
        textFIeld_name5_103->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_119->setText(QApplication::translate("MainWindowApp", "19", nullptr));
        checkBox_157->setText(QString());
        checkBox_158->setText(QString());
        checkBox_159->setText(QString());
        checkBox_160->setText(QString());
        label_ID4_120->setText(QApplication::translate("MainWindowApp", "18", nullptr));
        circle_inactive3_54->setText(QString());
        circle_inactive1_93->setText(QString());
        textFIeld_name5_104->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        circle_inactive3_55->setText(QString());
        circle_inactive3_56->setText(QString());
        circle_inactive1_94->setText(QString());
        circle_inactive1_95->setText(QString());
        circle_inactive1_96->setText(QString());
        circle_inactive1_97->setText(QString());
        circle_inactive1_98->setText(QString());
        circle_inactive1_99->setText(QString());
        circle_inactive1_100->setText(QString());
        circle_inactive1_101->setText(QString());
        circle_inactive1_102->setText(QString());
        circle_inactive1_103->setText(QString());
        circle_inactive1_104->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QApplication::translate("MainWindowApp", "3_Node", nullptr));
        label_ID_10->setText(QApplication::translate("MainWindowApp", "ID", nullptr));
        label_name_11->setText(QApplication::translate("MainWindowApp", "Name    ", nullptr));
        label_start_10->setText(QApplication::translate("MainWindowApp", "  Start", nullptr));
        label_msStart_11->setText(QApplication::translate("MainWindowApp", "(ms)", nullptr));
        label_duration_10->setText(QApplication::translate("MainWindowApp", "Duration", nullptr));
        label_msDuration_18->setText(QApplication::translate("MainWindowApp", "(ms)", nullptr));
        label_33->setText(QApplication::translate("MainWindowApp", "(0/0)", nullptr));
        pushButton_35->setText(QApplication::translate("MainWindowApp", "Stop/Reset", nullptr));
        pushButton_36->setText(QApplication::translate("MainWindowApp", "WriteConfig", nullptr));
        label_34->setText(QApplication::translate("MainWindowApp", "Minimum: 0 ms", nullptr));
        label_35->setText(QApplication::translate("MainWindowApp", "Cycle Duration", nullptr));
        pushButton_37->setText(QApplication::translate("MainWindowApp", "Start", nullptr));
        label_msDuration_19->setText(QApplication::translate("MainWindowApp", "(ms)", nullptr));
        pushButton_38->setText(QApplication::translate("MainWindowApp", "ReadConfig", nullptr));
        label_36->setText(QApplication::translate("MainWindowApp", "Cycle Counter", nullptr));
        textFIeld_name5_105->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_121->setText(QApplication::translate("MainWindowApp", "6", nullptr));
        circle_inactive3_57->setText(QString());
        checkBox_161->setText(QString());
        checkBox_162->setText(QString());
        checkBox_163->setText(QString());
        checkBox_164->setText(QString());
        circle_inactive3_58->setText(QString());
        textFIeld_name1_9->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name4_17->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_165->setText(QString());
        textFIeld_name2_17->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name5_106->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_122->setText(QApplication::translate("MainWindowApp", "4", nullptr));
        label_ID4_123->setText(QApplication::translate("MainWindowApp", "10", nullptr));
        checkBox_166->setText(QString());
        textFIeld_name5_107->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_124->setText(QApplication::translate("MainWindowApp", "15", nullptr));
        textFIeld_name4_18->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name5_108->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID2_17->setText(QApplication::translate("MainWindowApp", "2", nullptr));
        checkBox_167->setText(QString());
        textFIeld_name2_18->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name5_109->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_168->setText(QString());
        label_ID4_125->setText(QApplication::translate("MainWindowApp", "7", nullptr));
        circle_inactive3_59->setText(QString());
        textFIeld_name3_17->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name3_18->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_169->setText(QString());
        textFIeld_name5_110->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name5_111->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_126->setText(QApplication::translate("MainWindowApp", "20", nullptr));
        label_ID4_127->setText(QApplication::translate("MainWindowApp", "5", nullptr));
        textFIeld_name5_112->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_128->setText(QApplication::translate("MainWindowApp", "11", nullptr));
        textFIeld_name5_113->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_170->setText(QString());
        label_ID4_129->setText(QApplication::translate("MainWindowApp", "13", nullptr));
        label_ID4_130->setText(QApplication::translate("MainWindowApp", "17", nullptr));
        label_ID2_18->setText(QApplication::translate("MainWindowApp", "8", nullptr));
        label_ID4_131->setText(QApplication::translate("MainWindowApp", "16", nullptr));
        checkBox_171->setText(QString());
        circle_inactive3_60->setText(QString());
        label_ID4_132->setText(QApplication::translate("MainWindowApp", "14", nullptr));
        label_ID4_133->setText(QApplication::translate("MainWindowApp", "12", nullptr));
        textFIeld_name5_114->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID3_17->setText(QApplication::translate("MainWindowApp", "3", nullptr));
        circle_inactive1_105->setText(QString());
        checkBox_172->setText(QString());
        checkBox_173->setText(QString());
        checkBox_174->setText(QString());
        checkBox_175->setText(QString());
        textFIeld_name5_115->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_176->setText(QString());
        label_ID3_18->setText(QApplication::translate("MainWindowApp", "9", nullptr));
        label_ID1_9->setText(QApplication::translate("MainWindowApp", "1", nullptr));
        textFIeld_name5_116->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_134->setText(QApplication::translate("MainWindowApp", "19", nullptr));
        checkBox_177->setText(QString());
        checkBox_178->setText(QString());
        checkBox_179->setText(QString());
        checkBox_180->setText(QString());
        label_ID4_135->setText(QApplication::translate("MainWindowApp", "18", nullptr));
        circle_inactive3_61->setText(QString());
        circle_inactive1_106->setText(QString());
        textFIeld_name5_117->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        circle_inactive3_62->setText(QString());
        circle_inactive3_63->setText(QString());
        circle_inactive1_107->setText(QString());
        circle_inactive1_108->setText(QString());
        circle_inactive1_109->setText(QString());
        circle_inactive1_110->setText(QString());
        circle_inactive1_111->setText(QString());
        circle_inactive1_112->setText(QString());
        circle_inactive1_113->setText(QString());
        circle_inactive1_114->setText(QString());
        circle_inactive1_115->setText(QString());
        circle_inactive1_116->setText(QString());
        circle_inactive1_117->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab_5), QApplication::translate("MainWindowApp", "4_Node", nullptr));
        label_ID_11->setText(QApplication::translate("MainWindowApp", "ID", nullptr));
        label_name_12->setText(QApplication::translate("MainWindowApp", "Name    ", nullptr));
        label_start_11->setText(QApplication::translate("MainWindowApp", "  Start", nullptr));
        label_msStart_12->setText(QApplication::translate("MainWindowApp", "(ms)", nullptr));
        label_duration_11->setText(QApplication::translate("MainWindowApp", "Duration", nullptr));
        label_msDuration_20->setText(QApplication::translate("MainWindowApp", "(ms)", nullptr));
        label_37->setText(QApplication::translate("MainWindowApp", "(0/0)", nullptr));
        pushButton_39->setText(QApplication::translate("MainWindowApp", "Stop/Reset", nullptr));
        pushButton_40->setText(QApplication::translate("MainWindowApp", "WriteConfig", nullptr));
        label_38->setText(QApplication::translate("MainWindowApp", "Minimum: 0 ms", nullptr));
        label_39->setText(QApplication::translate("MainWindowApp", "Cycle Duration", nullptr));
        pushButton_41->setText(QApplication::translate("MainWindowApp", "Start", nullptr));
        label_msDuration_21->setText(QApplication::translate("MainWindowApp", "(ms)", nullptr));
        pushButton_42->setText(QApplication::translate("MainWindowApp", "ReadConfig", nullptr));
        label_40->setText(QApplication::translate("MainWindowApp", "Cycle Counter", nullptr));
        textFIeld_name5_118->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_136->setText(QApplication::translate("MainWindowApp", "6", nullptr));
        circle_inactive3_64->setText(QString());
        checkBox_181->setText(QString());
        checkBox_182->setText(QString());
        checkBox_183->setText(QString());
        checkBox_184->setText(QString());
        circle_inactive3_65->setText(QString());
        textFIeld_name1_10->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name4_19->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_185->setText(QString());
        textFIeld_name2_19->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name5_119->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_137->setText(QApplication::translate("MainWindowApp", "4", nullptr));
        label_ID4_138->setText(QApplication::translate("MainWindowApp", "10", nullptr));
        checkBox_186->setText(QString());
        textFIeld_name5_120->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_139->setText(QApplication::translate("MainWindowApp", "15", nullptr));
        textFIeld_name4_20->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name5_121->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID2_19->setText(QApplication::translate("MainWindowApp", "2", nullptr));
        checkBox_187->setText(QString());
        textFIeld_name2_20->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name5_122->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_188->setText(QString());
        label_ID4_140->setText(QApplication::translate("MainWindowApp", "7", nullptr));
        circle_inactive3_66->setText(QString());
        textFIeld_name3_19->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name3_20->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_189->setText(QString());
        textFIeld_name5_123->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        textFIeld_name5_124->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_141->setText(QApplication::translate("MainWindowApp", "20", nullptr));
        label_ID4_142->setText(QApplication::translate("MainWindowApp", "5", nullptr));
        textFIeld_name5_125->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_143->setText(QApplication::translate("MainWindowApp", "11", nullptr));
        textFIeld_name5_126->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_190->setText(QString());
        label_ID4_144->setText(QApplication::translate("MainWindowApp", "13", nullptr));
        label_ID4_145->setText(QApplication::translate("MainWindowApp", "17", nullptr));
        label_ID2_20->setText(QApplication::translate("MainWindowApp", "8", nullptr));
        label_ID4_146->setText(QApplication::translate("MainWindowApp", "16", nullptr));
        checkBox_191->setText(QString());
        circle_inactive3_67->setText(QString());
        label_ID4_147->setText(QApplication::translate("MainWindowApp", "14", nullptr));
        label_ID4_148->setText(QApplication::translate("MainWindowApp", "12", nullptr));
        textFIeld_name5_127->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID3_19->setText(QApplication::translate("MainWindowApp", "3", nullptr));
        circle_inactive1_118->setText(QString());
        checkBox_192->setText(QString());
        checkBox_193->setText(QString());
        checkBox_194->setText(QString());
        checkBox_195->setText(QString());
        textFIeld_name5_128->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        checkBox_196->setText(QString());
        label_ID3_20->setText(QApplication::translate("MainWindowApp", "9", nullptr));
        label_ID1_10->setText(QApplication::translate("MainWindowApp", "1", nullptr));
        textFIeld_name5_129->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        label_ID4_149->setText(QApplication::translate("MainWindowApp", "19", nullptr));
        checkBox_197->setText(QString());
        checkBox_198->setText(QString());
        checkBox_199->setText(QString());
        checkBox_200->setText(QString());
        label_ID4_150->setText(QApplication::translate("MainWindowApp", "18", nullptr));
        circle_inactive3_68->setText(QString());
        circle_inactive1_119->setText(QString());
        textFIeld_name5_130->setText(QApplication::translate("MainWindowApp", "Relay", nullptr));
        circle_inactive3_69->setText(QString());
        circle_inactive3_70->setText(QString());
        circle_inactive1_120->setText(QString());
        circle_inactive1_121->setText(QString());
        circle_inactive1_122->setText(QString());
        circle_inactive1_123->setText(QString());
        circle_inactive1_124->setText(QString());
        circle_inactive1_125->setText(QString());
        circle_inactive1_126->setText(QString());
        circle_inactive1_127->setText(QString());
        circle_inactive1_128->setText(QString());
        circle_inactive1_129->setText(QString());
        circle_inactive1_130->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindowApp", "5_Node", nullptr));
        menuOpen->setTitle(QApplication::translate("MainWindowApp", "File", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindowApp: public Ui_MainWindowApp {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOWAPP_H
