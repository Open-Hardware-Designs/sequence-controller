#ifndef MAINWINDOWAPP_H
#define MAINWINDOWAPP_H

#include <QMainWindow>

namespace Ui {
class MainWindowApp;
}

class MainWindowApp : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindowApp(QWidget *parent = 0);
    ~MainWindowApp();

private:
    Ui::MainWindowApp *ui;
};

#endif // MAINWINDOWAPP_H
