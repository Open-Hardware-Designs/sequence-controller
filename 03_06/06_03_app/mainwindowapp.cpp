#include "mainwindowapp.h"
#include "ui_mainwindowapp.h"

MainWindowApp::MainWindowApp(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindowApp)
{
    ui->setupUi(this);
}

MainWindowApp::~MainWindowApp()
{
    delete ui;
}
